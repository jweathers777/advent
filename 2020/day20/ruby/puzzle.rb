#!/usr/bin/env ruby

require 'set'

class Image
  attr_accessor :pixels

  SEA_MONSTER_LOCS = [[0,0], [1,1], [1,4], [0,5], [0,6], [1,7], [1,10],
                      [0,11], [0,12], [1,13], [1,16], [0,17], [0,18], [-1,18], [0,19]]

  class << self
    def from_s(s)
      Image.new(s.split("\n").map {|r| r.split('')})
    end

    def opposite_side(side)
      {top: :bottom, bottom: :top, left: :right, right: :left}[side]
    end

    def get_transform(orig_side, new_side, orientation)
      case [orig_side, new_side, orientation]
      when [:left, :top, -1], [:top, :right, 1], [:bottom, :left, 1], [:right, :bottom, -1]
        :rot90
      when [:left, :right, -1], [:top, :bottom, -1], [:bottom, :top, -1], [:right, :left, -1]
        :rot180
      when [:left, :bottom, 1], [:top, :left, -1], [:bottom, :right, -1], [:right, :top, 1]
        :rot270
      when [:left, :left, -1], [:top, :bottom, 1], [:bottom, :top, 1], [:right, :right, -1]
        :ref0
      when [:left, :bottom, -1], [:top, :right, -1], [:bottom, :left, -1], [:right, :top, -1]
        :ref45
      when [:left, :right, 1], [:top, :top, -1], [:bottom, :bottom, -1], [:right, :left, 1]
        :ref90
      when [:left, :top, 1], [:top, :left, 1], [:bottom, :right, 1], [:right, :bottom, 1]
        :ref135
      else
        raise "Unknown transformation for #{orig_side}, #{new_side}, #{orientation}"
      end
    end

    def top(row, col)
      [row-1,col]
    end

    def bottom(row, col)
      [row+1,col]
    end

    def left(row, col)
      [row, col-1]
    end

    def right(row, col)
      [row, col+1]
    end
  end

  def initialize(pixels)
    @pixels = pixels
  end

  def [](index)
    @pixels[index]
  end

  def []=(index, value)
    @pixels[index] = value
  end

  def to_s
    "\n".tap do |s|
      @pixels.each do |row|
        s << row.join("")
        s << "\n"
      end
    end
  end

  def inspect
    to_s
  end

  def size
    @pixels.length
  end

  def remove_sides
    Image.new(
      @pixels[1..-2].map {|r| r[1..-2]}
    )
  end

  def rot0
    self
  end

  def rot90
    Image.new(
      (@pixels.first.length-1).downto(0).map do |c|
        @pixels.map {|r| r[c]}
      end
    )
  end

  def rot180
    Image.new(
      @pixels.reverse.map do |row|
        row.reverse
      end
    )
  end

  def rot270
    Image.new(
      0.upto(@pixels.length - 1).map do |c|
        @pixels.reverse.map {|r| r[c]}
      end
    )
  end

  def ref0
    Image.new(@pixels.reverse)
  end

  def ref90
    Image.new(@pixels.map {|r| r.reverse})
  end

  def ref45
    rot90.ref90
  end

  def ref135
    ref90.rot90
  end

  def transformations
    [:rot0, :rot90, :rot180, :rot270, :ref0, :ref45, :ref90, :ref135]
  end

  def top
    @pixels.first
  end

  def bottom
    @pixels.last
  end

  def left
    @pixels.map {|row| row.first}
  end

  def right
    @pixels.map {|row| row.last}
  end

  def sides
    {top: top, bottom: bottom, left: left, right: right}
  end

  def count_pixel_value(ch)
    @pixels.reduce(0) {|a,r| a+r.count(ch)}
  end

  def count_sea_monsters
    max_row = size-2
    max_col = size-20

    sea_monsters = 0
    row = 1
    while row <= max_row
      col = 0
      while col <= max_col
        if SEA_MONSTER_LOCS.map {|dr,dc| @pixels[row+dr][col+dc]}.all?('#')
          sea_monsters += 1
        end
        col += 1
      end
      row += 1
    end
    sea_monsters
  end

  def water_roughness
    transformations.each do |t|
      im = self.send(t)
      sea_monsters = im.count_sea_monsters
      if sea_monsters > 0
        set_pixels = @pixels.reduce(0) {|a,r| a+r.count('#')}
        return set_pixels - sea_monsters*SEA_MONSTER_LOCS.length
      end
    end
    raise "No sea monsters found!"
  end
end

class Tile
  attr_accessor :id, :image

  def self.from_s(s)
    tokens = s.split("\n",2)
    tokens[0] =~ /^Tile (\d+):/
    Tile.new($1.to_i, Image.from_s(tokens[1]))
  end

  def initialize(id, image)
    @id = id
    @image = image
  end

  def to_s
    "\n".tap do |s|
      s << "Tile #{id}:\n"
      s << image.to_s
    end
  end

  def inspect
    to_s
  end

  def size
    @image.size
  end

  [:top, :bottom, :left, :right, :sides].each do |m|
    define_method m do
      @image.send(m)
    end
  end

  [:rot90, :rot180, :rot270, :ref0, :ref45, :ref90, :ref135].each do |m|
    define_method m do
      Tile.new(@id, @image.send(m))
    end
  end

  def sides
    @image.sides
  end
end

class TileSet
  attr_accessor :tiles

  def self.from_file(fname)
    TileSet.new(IO.read(fname).split("\n\n").map {|s| Tile.from_s(s)})
  end

  def initialize(tiles)
    @tiles = tiles.map {|tile| [tile.id, tile]}.to_h
  end

  def size
    @size ||= Math.sqrt(@tiles.length).to_i
  end

  def clear_cache
    @size = nil
    @sides = nil
    @neighbors = nil
    @corner_ids = nil
    @layout = nil
  end

  def [](id)
    @tiles[id]
  end

  def []=(id, value)
    @tiles[id] = value
    clear_cache
  end

  def sides
    @sides ||= {}.tap do |sides_hash|
      @tiles.values.each do |tile|
        tile.sides.each do |pos,value|
          key = value
          orientation = 1
          if sides_hash[value].nil?
            rev_value = value.reverse
            if sides_hash[rev_value].nil?
              sides_hash[value] = []
            else
              key = rev_value
              orientation = -1
            end
          end
          sides_hash[key].push({tile_id: tile.id, pos: pos, orientation: orientation})
        end
      end
    end
  end

  def neighbors
    @neighbors ||= {}.tap do |neighbors_hash|
      sides.each do |side, tiles|
        if tiles.length == 2
					tiles.each_with_index do |tile, index|
						neighbors_hash[tile[:tile_id]] ||= {}
            other_tile = tiles[1-index]
            orientation = tile[:orientation] * other_tile[:orientation]
						neighbors_hash[tile[:tile_id]][tile[:pos]] =
              {tile_id: other_tile[:tile_id], pos: other_tile[:pos], orientation: orientation}
					end
				end
      end
    end
  end

  def corner_ids
    @corner_ids ||= neighbors.keys.select {|n| neighbors[n].length == 2}
  end

  def layout
    @layout ||= Array.new(size) { Array.new(size) { nil } }.tap do |grid|
      corners = neighbors.select {|id| corner_ids.include?(id)}

      start = corners.find {|c,n| n.keys.sort == [:bottom, :right]}[0]
      to_visit = [[start, 0, 0]]
      seen = Set.new
      seen.add(start)
      while !to_visit.empty?
        tile_id, row, col = to_visit.shift
        grid[row][col] = tile_id
        neighbors[tile_id].each do |pos, neighbor|
          r, c = Image.send(pos, row, col)
          if !seen.member?(neighbor[:tile_id])
            to_visit.push([neighbor[:tile_id], r, c])
            seen.add(neighbor[:tile_id])
            expected_pos = Image.opposite_side(pos)
            actual_pos = neighbor[:pos]
            if expected_pos != actual_pos or neighbor[:orientation] == -1
              transform = Image.get_transform(expected_pos, actual_pos, neighbor[:orientation])
              self[neighbor[:tile_id]] = self[neighbor[:tile_id]].send(transform)
            end
          end
        end
      end
    end
  end

  def arranged_image
    tile_size = @tiles.values.first.size
    pixels = []

    (0...size).each do |layout_row|
      (0...tile_size).each do |tile_row|
        pixels.push([])
        (0...size).each do |layout_col|
          tile = self[layout[layout_row][layout_col]]
          (0...tile_size).each do |tile_col|
            pixels[-1].push(tile.image.pixels[tile_row][tile_col])
          end
        end
      end
    end
    Image.new(pixels)
  end

  def remove_tile_sides!
    layout
    @size = nil
    @tiles.values.each do |tile|
      tile.image = tile.image.remove_sides
    end
  end

  def print_arranged_image
    lcols = layout.first.length
    trows = tiles.values.first.image.pixels.length
    layout.each do |lrow|
      0.upto(trows-1) do |trow|
        0.upto(lcols-1) do |lcol|
          print self[lrow[lcol]].image[trow].join("")
          print " "
        end
        puts ""
      end
      puts "\n"
    end
    nil
  end
end

def print_grid(grid)
  grid.each do |row|
    puts row.join (" ")
  end
  puts "---"
  nil
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  ts = TileSet.from_file(fname)

  if part == 1
    puts ts.corner_ids.reduce(&:*)
  else
    ts.remove_tile_sides!
    im = ts.arranged_image
    puts im.water_roughness
  end
end

if __FILE__ == $0
  main(ARGV)
end
