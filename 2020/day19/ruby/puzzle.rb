#!/usr/bin/env ruby

class Grammar
  attr :start, :non_terminals, :terminals, :production_rules

  def initialize(s, part)
    base_rules = s.split("\n").map(&:strip).
      map {|r| r.split(": ")}.
      map {|r| [r[0].to_i, r[1].split(" | ")]}.
      sort_by {|r| r[0]}.
      map {|sym, prods| prods.map {|prod| [sym, prod]}}.
      flatten(1).
      map {|sym, prod| [sym, prod.split.map {|v| v =~ /^\d+$/ ? v.to_i : v.gsub('"','')}]}

    if part == 2
      if base_rules.include?([8, [42]])
        base_rules.push([8, [42, 8]])
      end

      if base_rules.include?([11, [42, 31]])
        base_rules.push([11, [42, 11, 31]])
      end
    end

    @start = base_rules[0][0]
    base_non_terminals = base_rules.flatten.uniq.select {|e| e.is_a?(Numeric)}.sort
    @terminals = base_rules.flatten.uniq.select {|e| e.is_a?(String)}

    next_non_terminal = base_non_terminals.max + 1

    @production_rules = []
    base_rules.each do |sym, prod|
      if prod.length <= 2
        @production_rules.push([sym, prod])
      else
        next_sym = sym
        prod[..-3].each do |p|
          @production_rules.push([next_sym, [p, next_non_terminal]])
          next_sym = next_non_terminal
          next_non_terminal += 1
        end
        @production_rules.push([next_sym, prod[-2..]])
      end
    end

    unit_rules = @production_rules.select {|r| r[1].length == 1 and r[1][0].is_a?(Numeric)}
    @production_rules.reject! {|r| r[1].length == 1 and r[1][0].is_a?(Numeric)}
    unit_rules.each do |sym, prod|
      src_rules = @production_rules.select {|r| r[0] == prod[0]}
      src_rules.each {|r| @production_rules.push([sym, r[1]])}
    end

    @terminal_rules = @production_rules.select {|r| r[1].length == 1 and !r[1][0].is_a?(Numeric)}

    @non_terminals = @production_rules.flatten.uniq.select {|e| e.is_a?(Numeric)}.sort
    @non_terminal_rules = @production_rules.select {|r| r[1].length > 1}
    @non_terminal_indices = @non_terminals.each_with_index.to_h
  end

  def is_member?(s)
    n = s.length
    m = @non_terminals.length
    p = Array.new(n) { Array.new(n) { Array.new(m) { false } } }

    s.chars.each_with_index do |a,z|
      @terminal_rules.each do |r|
        if r[1][0] == a
          v = @non_terminal_indices[r[0]]
          p[0][z][v] = true
        end
      end
    end

    1.upto(n-1) do |l|
      0.upto(n-l-1) do |z|
        0.upto(l-1) do |r|
          @non_terminal_rules.each do |rule|
            a = @non_terminal_indices[rule[0]]
            b = @non_terminal_indices[rule[1][0]]
            c = @non_terminal_indices[rule[1][1]]
            if p[r][z][b] and p[l-r-1][z+r+1][c]
              p[l][z][a] = true
            end
          end
        end
      end
    end

    p[n-1][0][0]
  end
end

def read_grammar_and_messages(fname, part=1)
  tokens = IO.read(fname).split("\n\n")
  [Grammar.new(tokens[0], part), messages = tokens[1].split("\n").map(&:strip)]
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  grammar, messages = read_grammar_and_messages(fname, part)

  puts messages.each_with_index.count {|m, index| puts "Processing #{index}"; grammar.is_member?(m)}
end

if __FILE__ == $0
  main(ARGV)
end
