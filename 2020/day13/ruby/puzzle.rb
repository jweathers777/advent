#!/usr/bin/env ruby

def extended_euclidean(x, y)
  r0, r1 = [x, y].max, [x, y].min
  s0, s1 = [1, 0]
  t0, t1 = [0, 1]

  while r1 != 0
    q = r0 / r1
    r0, r1 = r1, r0 % r1
    s0, s1 = s1, s0 - q*s1
    t0, t1 = t1, t0 - q*t1
  end

  [r0, s0, t0]
end

def solve_pair(a1, n1, a2, n2)
  _, s, t = extended_euclidean(n1, n2)
  if n1 > n2
    m1,m2 = s,t
  else
    m2,m1 = s,t
  end

  a1*m2*n2 + a2*m1*n1
end

def solve_set(pairs)
  while pairs.length > 1
    a1,n1 = pairs.pop
    a2,n2 = pairs.pop
    a12 = solve_pair(a1,n1,a2,n2)
    pairs.push([a12, n1*n2])
  end
  a1,n1 = pairs.pop
  q = a1/n1
  a1 - q*n1
end

class Schedule
  attr_reader :earliest_timestamp, :bus_ids, :bus_minutes

  def initialize(s)
    lines = s.split("\n")
    @earliest_timestamp = lines[0].to_i
    @bus_minutes = lines[1].split(',').
      each_with_index.reject {|e,i| e == 'x'}.
      map {|e,i| [e.to_i,i]}
    @bus_ids = @bus_minutes.map {|e,i| e}
    @bus_minutes = @bus_minutes.to_h
  end

  def earliest_bus
    best_id = nil
    earliest = @earliest_timestamp**2
    @bus_ids.each do |bus_id|
      ts = bus_id
      while ts < @earliest_timestamp
        ts += bus_id
      end
      if ts < earliest
        earliest = ts
        best_id = bus_id
      end
    end
    [best_id, earliest]
  end

  def find_best_timestamp
    pairs = @bus_minutes.map {|k,v| [-v,k]}
    solve_set(pairs)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  schedule = Schedule.new(IO.read(fname))

  if part == 1
    earliest_bus_id, earliest_bus_time = schedule.earliest_bus
    puts (earliest_bus_time - schedule.earliest_timestamp)*earliest_bus_id
  else
    puts schedule.find_best_timestamp
  end
end

if __FILE__ == $0
  main(ARGV)
end
