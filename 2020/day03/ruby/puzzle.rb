#!/usr/bin/env ruby

class Grid
  def initialize(file_name)
    @cells = IO.readlines(file_name).map {|l| l.strip.split('')}
    @row_count = @cells.length
    @col_count = @cells[0].length
  end

  def tree?(row, col)
    @cells[row % @row_count][col % @col_count] == "#"
  end

  def count_trees(dc, dr)
    r, c, trees = 0, 0, 0
    while r < @row_count do
      trees += 1 if tree?(r, c)
      r += dr
      c += dc
    end

    trees
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = Grid.new(fname)

  if part == 1
    puts grid.count_trees(3, 1)
  else
    puts [[1,1], [3,1], [5,1], [7,1], [1,2]].map {|dc,dr| grid.count_trees(dc, dr)}.inject(1) {|p,e| p*e}
  end
end

if __FILE__ == $0
  main(ARGV)
end
