use std::env;
use std::fs;

struct Grid {
    cells: Vec<Vec<char>>,
    row_period: usize,
    col_period: usize,
}

impl Grid {
    fn new(g: &String) -> Grid {
        let cells: Vec<Vec<char>> = g.lines().
            map(|l| l.trim().chars().collect()).
            collect();
        let row_period = cells.len();
        let col_period = if row_period > 0 { cells[0].len() } else { 1 };
        Grid{cells, row_period, col_period}
    }

    fn is_tree(&self, row: usize, col: usize) -> bool {
        self.cells[row % self.row_period][col % self.col_period] == '#'
    }

    fn count_trees(&self, dc: usize, dr: usize) -> u32 {
        let (mut r, mut c, mut trees) = (0, 0, 0);
        while r < self.row_period {
            if self.is_tree(r, c) {
                trees += 1;
            }
            r += dr;
            c += dc;
        }
        trees
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments") }

    let part: u32 = args[1].parse().expect("Invalid part!");
    let s = fs::read_to_string(&args[2]).expect("File not found!");
    let grid = Grid::new(&s);

    if part == 1 {
        println!("{}", grid.count_trees(3, 1));
    } else {
        let slopes = [[1,1], [3,1], [5,1], [7,1], [1,2]];
        let result: u32 = slopes.iter().
            map(|[dc,dr]| grid.count_trees(*dc, *dr)).
            product();
        println!("{}", result);
    }
}
