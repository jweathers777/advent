module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import qualified Data.Vector as V

data Grid = Grid { cells :: V.Vector ( V.Vector Char )
                 , rowPeriod :: Int
                 , colPeriod :: Int
                 } deriving (Show)

makeGrid :: [String] -> Grid
makeGrid rows = Grid {cells=cells, rowPeriod=rowPeriod, colPeriod=colPeriod}
  where
    cells = V.fromList $ map V.fromList rows
    rowPeriod = length rows
    colPeriod =
      if rowPeriod > 0
         then length $ head rows
         else 1

isTree :: Grid -> Int -> Int -> Bool
isTree grid row col = '#' == ((cells grid) V.! r) V.! c
  where
    r = row `mod` (rowPeriod grid)
    c = col `mod` (colPeriod grid)

countTrees :: Grid -> Int -> Int -> Int
countTrees grid dc dr = countTrees' grid dc dr 0 0 0
  where
    countTrees' grid dc dr row col count =
      if row >= (rowPeriod grid)
         then count
         else
           (if isTree grid row col
               then countTrees' grid dc dr row' col' (count + 1)
               else countTrees' grid dc dr row' col' count
           )
      where
        row' = row + dr
        col' = col + dc

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1
       contents <- readFile fileName
       let grid = makeGrid $ lines contents
       if part == 1
          then putStrLn $ show $ countTrees grid 3 1
          else
            let slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]
                f (dc, dr) = countTrees grid dc dr
            in putStrLn $ show $ product $ map f slopes
