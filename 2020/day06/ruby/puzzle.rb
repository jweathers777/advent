#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]

  groups = IO.read(fname).split("\n\n").map {|g| g.split("\n")}

  if part == 1
    puts groups.map {|g| g.inject(&:+).chars.uniq.count}.sum
  else
    puts groups.map {|g| g.map(&:chars).inject {|p,e| p.intersection(e)}.count}.sum
  end
end

if __FILE__ == $0
  main(ARGV)
end
