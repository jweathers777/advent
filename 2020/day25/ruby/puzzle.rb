#!/usr/bin/env ruby

def transform(subject, loop_size)
  value = 1
  loop_size.times do
    value *= subject
    value = value % 20201227
  end

  value
end

def find_loop_size(key)
  loop_size = (Math.log(key)/Math.log(7)).ceil
  value = transform(7, loop_size)
  while value != key
    value *= 7
    value = value % 20201227
    loop_size += 1
  end
  loop_size
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  if part == 1
    card_key, door_key = IO.readlines(fname, chomp: true).map(&:to_i)
    card_loop_size = find_loop_size(card_key)
    door_loop_size = find_loop_size(door_key)

    encryption_key = transform(door_key, card_loop_size)
    puts encryption_key
    encryption_key = transform(card_key, door_loop_size)
    puts encryption_key
  end
end

if __FILE__ == $0
  main(ARGV)
end
