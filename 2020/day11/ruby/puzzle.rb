#!/usr/bin/env ruby

class Grid
  FLOOR = '.'
  EMPTY = 'L'
  OCCUPIED = '#'
  DIRECTIONS = [:up, :down, :left, :right, :up_left, :up_right, :down_left, :down_right]

  attr_reader :data

  def initialize(fname)
    @data = IO.readlines(fname).map {|l| l.strip.chars()}
    @height = @data.length
    @width = @data.first.length
  end

  def up(r,c)
    r == 0 ? nil : [r-1,c]
  end

  def down(r,c)
    r == @height-1 ? nil : [r+1,c]
  end

  def left(r,c)
    c == 0 ? nil : [r,c-1]
  end

  def right(r,c)
    c == @width-1 ? nil : [r,c+1]
  end

  def up_left(r,c)
    (r == 0 || c == 0) ? nil : [r-1,c-1]
  end

  def up_right(r,c)
    (r == 0 || c == @width-1) ? nil : [r-1,c+1]
  end

  def down_left(r,c)
    (r == @height-1 || c == 0) ? nil : [r+1,c-1]
  end

  def down_right(r,c)
    (r == @height-1 || c == @width-1) ? nil : [r+1,c+1]
  end

  def slide(direction, r, c)
    occupied = false
    while not occupied do
      p = send(direction, r, c)
      if p.nil?
        return nil
      else
        r, c = p[0], p[1]
      end
      if @data[r][c] != FLOOR
        return [r, c]
      end
    end
    nil
  end

  def count_adjacent(r, c, kind)
    DIRECTIONS.map {|f| send(f, r, c)}.
      reject(&:nil?).
      map {|p| @data[p[0]][p[1]]}.
      count {|v| v == kind}
  end

  def count_visible(r, c, kind)
    DIRECTIONS.map {|f| slide(f, r, c)}.
      reject(&:nil?).
      map {|p| @data[p[0]][p[1]]}.
      count {|v| v == kind}
  end

  def count_state(kind)
    count = 0
    @data.each do |row|
      row.each do |col|
        if col == kind
          count += 1
        end
      end
    end
    count
  end

  def apply_first_rules
    changes = 0
    new_data = []
    @data.each_with_index do |row, r|
      new_data[r] = []
      row.each_with_index do |col, c|
        if col == EMPTY && count_adjacent(r, c, OCCUPIED) == 0
          new_data[r][c] = OCCUPIED
          changes += 1
        elsif col == OCCUPIED && count_adjacent(r, c, OCCUPIED) >= 4
          new_data[r][c] = EMPTY
          changes += 1
        else
          new_data[r][c] = col
        end
      end
    end
    @data = new_data
    changes
  end

  def apply_second_rules
    changes = 0
    new_data = []
    @data.each_with_index do |row, r|
      new_data[r] = []
      row.each_with_index do |col, c|
        if col == EMPTY && count_visible(r, c, OCCUPIED) == 0
          new_data[r][c] = OCCUPIED
          changes += 1
        elsif col == OCCUPIED && count_visible(r, c, OCCUPIED) >= 5
          new_data[r][c] = EMPTY
          changes += 1
        else
          new_data[r][c] = col
        end
      end
    end
    @data = new_data
    changes
  end

  def apply_rules_until_fixed(rules)
    changes = 1
    while changes > 0
      changes = send(rules)
    end
  end

  def to_s
    @data.each do |row|
      row.each {|c| print(c)}
      puts ""
    end
    puts ""
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  grid = Grid.new(fname)
  if part == 1
    grid.apply_rules_until_fixed(:apply_first_rules)
  else
    grid.apply_rules_until_fixed(:apply_second_rules)
  end
  puts grid.count_state(Grid::OCCUPIED)
end

if __FILE__ == $0
  main(ARGV)
end
