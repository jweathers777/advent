#!/usr/bin/env ruby

require 'set'

class TicketDocument
  attr_accessor :field_ranges, :field_positions, :tickets

  def initialize(fname)
    @field_ranges = {}
    @field_positions = {}
    @tickets = []

    index = 0
    lines = IO.readlines(fname, chomp: true)
    while lines[index].strip != ''
      section, values = lines[index].split(": ")
      @field_ranges[section] = parse_ranges(values)
      @field_positions[section] = nil
      index += 1
    end
    @tickets.push(parse_ticket(lines[index+2]))
    lines[index+5..].each {|l| @tickets.push(parse_ticket(l))}
  end

  def parse_ranges(s)
    s.split(" or ").
      map {|s| s.split('-')}.
      map {|p| (p[0].to_i..p[1].to_i)}
  end

  def parse_ticket(t)
    t.split(",").map(&:to_i)
  end

  def ticket_scanning_error_rate
    all_ranges = @field_ranges.values.inject(&:+)

    error_rate = 0
    @tickets[1..].each do |ticket|
      error_rate += ticket.select {|v| all_ranges.none? {|r| r.include?(v)}}.sum
    end
    error_rate
  end

  def reject_invalid_tickets!
    all_ranges = @field_ranges.values.inject(&:+)
    @tickets.reject!  do |ticket|
      ticket.any? {|v| all_ranges.none? {|r| r.include?(v)}}
    end
  end

  def find_all_valid_positions(field)
    ranges = @field_ranges[field]
    1.upto(@tickets[0].length).select do |column|
      index = column - 1
      @tickets.all? {|ticket| ranges.any? {|r| r.include?(ticket[index])}}
    end
  end

  def compute_field_positions
    valid_field_positions = @field_positions.keys.
      map {|f| [f, Set.new(find_all_valid_positions(f))]}.
      sort_by {|p| p[1].length}

    n = @field_positions.length-1
    index = 0
    while index <= n && valid_field_positions[index][1].length == 1
      field, positions = valid_field_positions[index]
      position = positions.first
      @field_positions[field] = position
      index += 1
      index.upto(n).each do |j|
        valid_field_positions[j][1].delete(position)
      end
    end

    @field_positions
  end

  def get_departure_values
    @field_positions.keys.select {|k| k =~ /^departure/}.
      map {|k| @field_positions[k] - 1}.
      map {|c| @tickets[0][c]}
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  ticket_document = TicketDocument.new(fname)
  if part == 1
    puts ticket_document.ticket_scanning_error_rate
  else
    ticket_document.reject_invalid_tickets!
    ticket_document.compute_field_positions
    puts ticket_document.get_departure_values.inject(&:*)
  end
end

if __FILE__ == $0
  main(ARGV)
end
