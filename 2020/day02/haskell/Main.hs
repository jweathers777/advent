module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.List.Split

data Policy = Policy { low :: Int
                     , high :: Int
                     , char :: Char
                     , password :: String
                     } deriving (Show)

readPolicy :: String -> Policy
readPolicy s = Policy {low=low, high=high, char=char, password=password}
  where
    [lowHigh, char : ":", password] = words s
    [low, high] = map (\v -> read v :: Int) $ splitOn "-" lowHigh

minMaxTest :: Policy -> Bool
minMaxTest p = minVal <= count && count <= maxVal
  where
    minVal = low p
    maxVal = high p
    count = length $ filter (==(char p)) (password p)

xor :: Bool -> Bool -> Bool
xor True a = not a
xor False a = a

xorPosTest :: Policy -> Bool
xorPosTest p = ((pw !! i) == ch) `xor` ((pw !! j) == ch)
  where
    pw = password p
    ch = char p
    i = (low p) - 1
    j = (high p) - 1

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       let policyRecords = lines contents
       let policies = map readPolicy policyRecords
       let test = if part == 1
                     then minMaxTest
                     else xorPosTest

       putStrLn $ show $ length $ filter test policies

