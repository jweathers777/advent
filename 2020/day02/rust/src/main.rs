use std::env;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;

fn valid_password_one(s: &str) -> bool {
    let tokens: Vec<&str> = s.split_whitespace().collect();
    let values: Vec<usize> = tokens[0].split("-").
        map(|t| t.parse::<usize>().expect("Invalid number!")).collect();
    let ch = tokens[1].strip_suffix(":").unwrap();
    let password = tokens[2];

    let ch_count = password.matches(ch).count();

    ch_count >= values[0] && ch_count <= values[1]
}

fn valid_password_two(s: &str) -> bool {
    let tokens: Vec<&str> = s.split_whitespace().collect();
    let values: Vec<usize> = tokens[0].split("-").
        map(|t| t.parse::<usize>().expect("Invalid number!")).
        map(|v| v - 1).collect();

    let ch_str_vec: Vec<char> = tokens[1].chars().collect();
    let ch = ch_str_vec[0];
    let password: Vec<char> = tokens[2].chars().collect();

    (password[values[0]] == ch) ^ (password[values[1]] == ch)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments") }

    let part: u32 = args[1].parse().expect("Invalid part!");
    let f = File::open(&args[2]).expect("File not found!");
    let reader = BufReader::new(&f);

    let password_validator = if part == 1 {
        valid_password_one
    } else {
        valid_password_two
    };

    let valid_passwords = reader.lines().
        map(|l| l.unwrap()).
        filter(|s| password_validator(s.trim())).
        count();

    println!("{}", valid_passwords);
}
