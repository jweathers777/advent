#!/usr/bin/env ruby

part = ARGV[0].to_i
fname = ARGV[1]

valid_passwords = 0

if part == 1
  IO.readlines(fname).each do |line|
    tokens = line.strip.split
    minv, maxv = tokens[0].split("-").map(&:to_i)
    ch = tokens[1].chop
    password = tokens[2]
    ch_count = password.count(ch)
    if ch_count >= minv && ch_count <= maxv
      valid_passwords += 1
    end
  end
else
  IO.readlines(fname).each do |line|
    tokens = line.strip.split
    i, j = tokens[0].split("-").map {|s| s.to_i - 1}
    ch = tokens[1].chop
    password = tokens[2]

    if (password[i] == ch) ^ (password[j] == ch)
      valid_passwords += 1
    end
  end
end

puts valid_passwords
