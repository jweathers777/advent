module Main where

import System.Environment (getArgs)
import System.IO (readFile)

import Data.Char
import Data.List
import Data.List.Split

import Data.Map (Map)
import qualified Data.Map as Map

data Passport = Passport { byr :: String
                         , iyr :: String
                         , eyr :: String
                         , hgt :: String
                         , hcl :: String
                         , ecl :: String
                         , pid :: String
                         , cid :: String
                         } deriving (Eq, Show)

fromString :: String -> Passport
fromString s = Passport byr iyr eyr hgt hcl ecl pid cid
  where
    mkPair [a,b] = (a,b)
    m = Map.fromList $ map mkPair $ sort $ map (splitOn ":") $ words s
    members = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"]
    mapper x = Map.findWithDefault "" x m
    [byr,iyr,eyr,hgt,hcl,ecl,pid,cid] = map mapper members

hasAllFields :: Passport -> Bool
hasAllFields p = and $ map (\f -> (f p) /= "") members
  where
    members = [byr,iyr,eyr,hgt,hcl,ecl,pid]

validYear :: String -> Int -> Int -> Bool
validYear s l h =
  if all isDigit s
     then let n = read s :: Int
          in and [(length s) == 4, l <= n, n <= h]
     else False

validHeight :: Passport -> Bool
validHeight p =
  if valStr == ""
     then False
     else let value = read valStr :: Int
          in case unit of
            "cm" -> 150 <= value && value <= 193
            "in" -> 59 <= value && value <= 76
            _  -> False
  where
    (valStr, unit) = span isDigit (hgt p)

validHairColor :: Passport -> Bool
validHairColor p =
  if isPrefixOf "#" hairColor
     then let hex = tail hairColor
           in length hex == 6 && all (\x -> elem x hexChars) hex
     else False
  where
    hairColor = hcl p
    hexChars = "0123456789abcdef"

validEyeColor :: Passport -> Bool
validEyeColor p = elem (ecl p) ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

validPassportId :: Passport -> Bool
validPassportId p = length (pid p) == 9 && all isDigit (pid p)

hasAllValidFields :: Passport -> Bool
hasAllValidFields p = and
  [ hasAllFields p
  , validYear (byr p) 1920 2002
  , validYear (iyr p) 2010 2020
  , validYear (eyr p) 2020 2030
  , validHeight p
  , validHairColor p
  , validEyeColor p
  , validPassportId p ]

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1
       contents <- readFile fileName
       let policies = map fromString $ splitOn "\n\n" contents
       let validator = if part == 1 then hasAllFields else hasAllValidFields
       putStrLn $ show $ length $ filter validator policies
