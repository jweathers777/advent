#!/usr/bin/env ruby

require 'set'

required_fields = %w{byr iyr eyr hgt hcl ecl pid cid}.map(&:to_sym)

class Passport < Struct.new(*required_fields)
  EYE_COLORS = %w{amb blu brn gry grn hzl oth}

  def self.from_string(s)
    fields = s.split.map {|f| f.split(':')}.to_h
    Passport.new.tap do |p|
      members.each {|m| p[m] = fields[m.to_s] if fields[m.to_s]}
    end
  end

  def has_all_fields?
    members.reject {|m| m == :cid}.count {|m| self[m] == nil} == 0
  end

  def valid_year?(s, l, h)
    n = s.to_i
    s.length == 4 && l <= n && n <= h
  end

  def valid_height?
    return false unless self.hgt =~ /^(\d+)(cm|in)$/
    value = $1.to_i
    unit = $2
    case unit
    when "cm"
      150 <= value && value <= 193
    when "in"
      59 <= value && value <= 76
    else
      false
    end
  end

  def valid_hair_color?
    self.hcl =~ /^#[0-9a-f]{6,6}$/
  end

  def valid_eye_color?
    EYE_COLORS.include?(self.ecl)
  end

  def valid_passport_id?
    self.pid =~ /^\d{9,9}$/
  end

  def has_all_valid_fields?
    has_all_fields? &&
      valid_year?(self.byr, 1920, 2002) &&
      valid_year?(self.iyr, 2010, 2020) &&
      valid_year?(self.eyr, 2020, 2030) &&
      valid_height? &&
      valid_hair_color? &&
      valid_eye_color? &&
      valid_passport_id?
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  passports = IO.read(fname).split("\n\n").
    map {|p| Passport.from_string(p)}

  case part
  when 1
    puts passports.count(&:has_all_fields?)
  when 2
    puts passports.count(&:has_all_valid_fields?)
  end
end

if __FILE__ == $0
  main(ARGV)
end
