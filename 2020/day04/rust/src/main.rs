use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments") }

    let part: u32 = args[1].parse().expect("Invalid part!");
    let s = fs::read_to_string(&args[2]).expect("File not found!");

    if part == 1 {
        println!("Unimplemented!");
    } else {
        println!("Unimplemented!");
    }
}
