#!/usr/bin/env ruby

class ParseTree
  attr_accessor :root

  @@precedence = {'+' => 1, '*' => 1}

  def self.set_precedence(x, y)
    @@precedence[x] = y
  end

  class Node < Struct.new(:token, :parent, :left, :right)
    def copy
      l = left ? left.copy : nil
      r = right ? right.copy : nil
      n = Node.new(token, parent, l, r)
      l.parent = n if l
      r.parent = n if r
      n
    end

    def get_root
      current = self
      while current.parent != nil
        current = current.parent
      end
      current
    end

    def get_branch_top
      current = self
      while current.parent != nil and current.parent.token != '('
        current = current.parent
      end
      current
    end

    def get_left_most_leaf
      current = self
      while current.left != nil
        current = current.left
      end
      current
    end

    def insert_left(token)
      self.left = Node.new(token, self)
    end

    def insert_right(token)
      self.right = Node.new(token, self)
    end

    def insert_above(token)
      top = self.parent
      if top == nil
        top = Node.new(token, nil, self)
      else
        if top.left == self
          top.left = Node.new(token, top, self)
          top = top.left
        else
          top.right = Node.new(token, top, self)
          top = top.right
        end
      end
      self.parent = top
      top
    end

    def take_parent_position
      node = self.parent
      self.parent = node.parent
      self.parent.right = self unless self.parent.nil?
      node.parent, node.left, node.right = nil, nil, nil
      self
    end

    def to_s
      symbol = token == '(' ? 'C' : token
      if left.nil? and right.nil?
        "#{symbol}"
      else
        "(#{symbol} #{left} #{right})"
      end
    end
  end

  def self.is_digit?(ch)
    ('0'..'9').include?(ch)
  end

  def initialize(s)
    current = nil
    token = nil

    index = 0
    while index < s.length
      ch = s[index]

      # Parse one or more characters into a token
      if ParseTree.is_digit?(ch)
        token = ""
        while ParseTree.is_digit?(ch)
          token << ch
          index += 1
          ch = s[index]
        end
        index -= 1
      else
        token = ch
      end

      # Handle each token appropriately
      case token
      when '*', '+'
        if !current.parent.nil? and %w{* +}.include?(current.parent.token)
          current = current.get_branch_top
          if @@precedence[token] < @@precedence[current.token]
            current = current.right
          end
        end
        current = current.insert_above(token)
      when ')'
        # Find the top of the current branch
        current = current.get_branch_top

        # Make sure that we have reached a '('
        raise "Missing '('" unless current.parent
        current.take_parent_position
      when ' '
        false # Skip this character
      else
        if current.nil?
          current = Node.new(token)
        elsif %w{* +}.include?(current.token)
          current = current.insert_right(token)
        else
          current = current.insert_left(token)
        end
      end
      index += 1
    end

    @root = current.get_root
  end

  def evaluate
    current = @root.copy
    node = current
    loop do
      current = current.get_left_most_leaf
      if current.parent.nil?
        return current.token.to_i
      else
        current = current.parent
        if %w{* +}.include?(current.right.token)
          current = current.right
        else
          l = current.left.token.to_i
          r = current.right.token.to_i
          current.token = l.send(current.token, r)
          current.left = current.right = nil
        end
      end
    end
  end

  def to_s
    @root.to_s
  end
end

def compute(s)
  ParseTree.new(s).evaluate
end


def main(args)
  part = args[0].to_i
  fname = args[1]

  if part == 1
    ParseTree.set_precedence('+', 1)
    ParseTree.set_precedence('*', 1)
  else
    ParseTree.set_precedence('+', 0)
    ParseTree.set_precedence('*', 1)
  end

  puts IO.readlines(fname, chomp: true).map {|s| compute(s)}.sum
end

if __FILE__ == $0
  main(ARGV)
end
