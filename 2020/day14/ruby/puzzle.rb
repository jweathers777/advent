#!/usr/bin/env ruby

class Machine
  attr_accessor :mask, :memory, :version

  def initialize(version)
    @memory = {}
    @mask = 'X'*36
    @version = version
  end

  def get_digits(value, bits=36)
    digits = value.to_s(2)
    padding = bits - digits.length
    '0'*padding + digits
  end

  def apply_mask_to_value(value)
    digits = get_digits(value)
    new_digits = @mask.chars.zip(digits.chars).
      map {|m,d| m == 'X' ? d : m}.join('')
    new_digits.to_i(2)
  end

  def apply_mask_to_address(address)
    digits = get_digits(address)
    template = @mask.chars.zip(digits.chars).
      map {|m,d| m == '0' ? d : m}.join('')
    bits = template.count('X')
    variations = 2**bits
    (0...variations).map do |variation|
      get_digits(variation, bits).chars.
        inject(template) {|t,v| t.sub('X', v)}.to_i(2)
    end
  end

  def perform_update(address, value)
    if @version == 1
      @memory[address] = apply_mask_to_value(value)
    else
      apply_mask_to_address(address).each do |addr|
        @memory[addr] = value
      end
    end
  end

  def run_program(s)
    s.split("\n").each do |line|
      command, value = line.split(" = ")
      if command == "mask"
        @mask = value
      else
        command =~ /^mem\[(\d+)\]$/
        address = $1.to_i
        value = value.to_i
        perform_update(address, value)
      end
    end
  end
end

def main(args)
  version = args[0].to_i
  fname = args[1]
  machine = Machine.new(version)
  machine.run_program(IO.read(fname))
  puts machine.memory.values.sum
end

if __FILE__ == $0
  main(ARGV)
end
