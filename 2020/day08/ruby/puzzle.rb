#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('~/advent/common/ruby')

require 'machine'

def main(args)
  part = args[0].to_i
  fname = args[1]

  program = IO.read(fname)
  machine = Machine.new(program)
  if part == 1
    machine.execute_instructions
    puts "Accumulator before infinite loop: #{machine.accumulator}"
  else
    machine.find_and_fix_bad_jmp
    puts "Accumulator after running fixed program: #{machine.accumulator}"
  end
end

if __FILE__ == $0
  main(ARGV)
end
