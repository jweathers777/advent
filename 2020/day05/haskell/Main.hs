module Main where

import System.Environment (getArgs)
import System.IO (readFile)

import Data.List

decodeSection :: String -> Int -> Int -> Char -> Char -> Int
decodeSection s low high highChar lowChar = lastLow
    where
      (lastLow, _) = foldl' updateRange (low,high) s
      updateRange (l, h) char
        | char == highChar = (l, (l + h) `div` 2)
        | char == lowChar = ((l + h) `div` 2 + 1, h)
        | otherwise = error $ "Invalid character " ++ [char]

decodeSeat :: String -> Int
decodeSeat s = row * 8 + col
  where
    (front, back) = splitAt 7 s
    row = decodeSection front 0 127 'F' 'B'
    col = decodeSection back 0 7 'L' 'R'

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1
       contents <- readFile fileName
       let seatIds = map decodeSeat $ words contents
       let maxSeatId = maximum seatIds
       if part == 1
          then putStrLn $ show maxSeatId
          else let minSeatId = minimum seatIds
                   missingSeatId = head $ [minSeatId..maxSeatId] \\ seatIds
               in putStrLn $ show missingSeatId
