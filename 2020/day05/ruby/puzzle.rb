#!/usr/bin/env ruby

def decode_section(s, low, high, high_ch, low_ch)
  s.each_char do |ch|
    mid = (low + high) / 2
    case ch
    when high_ch
      high = mid
    when low_ch
      low = mid + 1
    else
      raise "Invalid character #{ch}"
    end
  end
  low
end


def decode_seat(s)
  row = decode_section(s[...7], 0, 127, 'F', 'B')
  col = decode_section(s[7..], 0, 7, 'L', 'R')
  row*8 + col
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  seats = IO.readlines(fname).map(&:strip).map {|l| decode_seat(l)}
  if part == 1
    puts seats.max
  else
    seats.sort!
    all_seats = (seats[0]..seats[-1]).to_a
    puts (all_seats - seats)[0]
  end
end

if __FILE__ == $0
  main(ARGV)
end
