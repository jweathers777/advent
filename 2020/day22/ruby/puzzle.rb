#!/usr/bin/env ruby

require 'set'

class CombatGame
  attr_accessor :decks

  def initialize(fname)
    @decks = IO.read(fname).split("\n\n").map {|d| d.split("\n")[1..].map(&:to_i) }
  end

  def play_round
    tops = @decks.map(&:shift)

    if tops[0] > tops[1]
      @decks[0].push(tops[0], tops[1])
    else
      @decks[1].push(tops[1], tops[0])
    end
  end

  def play_game
    while @decks.none?(&:empty?)
      play_round
    end
    winner = @decks.find {|deck| !deck.empty?}
    (winner.length).downto(1).zip(winner).reduce(0) {|a,e| a + (e[0]*e[1])}
  end
end

class RecursiveCombatGame
  attr_accessor :decks

  def self.from_file(fname)
    decks = IO.read(fname).split("\n\n").map {|d| d.split("\n")[1..].map(&:to_i) }
    RecursiveCombatGame.new(decks)
  end

  def initialize(decks)
    @decks = decks
    @seen_decks = Set.new
  end

  def decks_key
    "#{@decks[0]}:#{@decks[1]}"
  end

  def decks_seen?
    @seen_decks.member?(decks_key)
  end

  def observe_decks
    @seen_decks.add(decks_key)
  end

  def play_round
    observe_decks
    tops = @decks.map(&:shift)

    if tops.zip(@decks).all? {|top,deck| top <= deck.length}
      subdecks = tops.zip(@decks).map {|top,deck| deck[0,top]}
      game = RecursiveCombatGame.new(subdecks)
      winner = game.play_game
    else
      winner = (tops[0] > tops[1]) ? 0 : 1
		end

    @decks[winner].push(tops[winner], tops[1 - winner])
  end

  def play_game
    loser = nil
    while loser.nil?
      if decks_seen?
        loser = 1
      else
        play_round
        loser = @decks.find_index(&:empty?)
      end
    end

    1 - loser
  end

  def deck_score(player)
    deck = @decks[player]
    (deck.length).downto(1).zip(deck).reduce(0) {|a,e| a + (e[0]*e[1])}
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  if part == 1
    game = CombatGame.new(fname)
    puts game.play_game
  else
    game = RecursiveCombatGame.from_file(fname)
    winner = game.play_game
    puts game.deck_score(winner)
  end
end

if __FILE__ == $0
  main(ARGV)
end
