module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.List.Split

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn  "Unimplemented!"

