#!/usr/bin/env ruby

class RuleSet
  def initialize(fname)
    rule_pairs = IO.readlines(fname).map {|r| RuleSet.parse_rule(r)}
    @contains = rule_pairs.to_h
    @contained_by = rule_pairs.map {|a,b| b.map {|c,d| [c,a]}}.
      reject {|e| e.empty?}.
      flatten(1).
      group_by {|p| p[0]}.
      map {|a,b| [a, b.map {|p| p[1]}]}.to_h
  end

  def self.parse_rule(s)
    container, contained = s.strip.chop.split(" contain ")
    container.sub!(' bags', '')
    contained = contained.split(", ").
      map {|c| c =~ /^(\d+) (.+) bags?/ ? [$2,$1.to_i] : nil}.
      reject(&:nil?)
    [container, contained]
  end

  def find_all_containers(target)
    containers = {}
    targets = [target]
    while not targets.empty?
      (@contained_by[targets.pop] || []).each do |container|
        unless containers[container]
          containers[container] = container
          targets.push(container)
        end
      end
    end
    contains.keys
  end

  def count_nested_contents(target)
    items = {}
    targets = [target]
    while not targets.empty?
      (@contains[targets.pop] || []).each do |item, number|
        items[item] = (items[item] || 0) + number
        number.times { targets.push(item) }
      end
    end
    items.values.sum
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  rs = RuleSet.new(fname)

  if part == 1
    puts rs.find_all_containers("shiny gold").length
  else
    puts rs.count_nested_contents("shiny gold")
  end
end

if __FILE__ == $0
  main(ARGV)
end
