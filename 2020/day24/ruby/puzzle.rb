#!/usr/bin/env ruby

require 'pry'
require 'set'

class Grid
  attr_accessor :black_tiles, :ref, :days

  DIRECTIONS = {
    "nw" => :north_west,
    "ne" => :north_east,
    "e" => :east,
    "w" => :west,
    "se" => :south_east,
    "sw" => :south_west
  }

  def initialize
    @black_tiles = Set.new
    @ref = [0,0,0]
    @days = 0
  end

  def north_east(loc)
    x,y,z = loc
    [x+1,y,z+1]
  end

  def north_west(loc)
    x,y,z = loc
    [x,y+1,z+1]
  end

  def east(loc)
    x,y,z = loc
    [x+1,y-1,z]
  end

  def west(loc)
    x,y,z = loc
    [x-1,y+1,z]
  end

  def south_east(loc)
    x,y,z = loc
    [x,y-1,z-1]
  end

  def south_west(loc)
    x,y,z = loc
    [x-1,y,z-1]
  end

  def neighbors(loc)
    [:north_west, :north_east, :west, :east, :south_west, :south_east].map do |dir|
      self.send(dir, loc)
    end
  end

  def black_tile?(loc)
    @black_tiles.member?(loc)
  end

  def white_tile?(loc)
    !@black_tiles.member?(loc)
  end

  def new_location(s,steps=-1)
    loc = @ref
    insts = s.gsub("e","e;").gsub("w","w;").split(";")[0..steps]

    insts.map {|dir| DIRECTIONS[dir]}.each do |dir|
      loc = self.send(dir, loc)
    end
    loc
  end

  def flip(loc)
    if black_tile?(loc)
      @black_tiles.delete(loc)
    else
      @black_tiles.add(loc)
    end
  end

  def execute_directions(s, flip=true)
    loc = new_location(s)
    if flip
      flip(loc)
    else
      @black_tiles.add(loc)
    end
  end

  def advance_day
    @days += 1
    new_black_tiles = @black_tiles.clone
    #binding.pry
    @black_tiles.map {|loc| neighbors(loc) + [loc]}.flatten(1).uniq.each do |loc|
      if black_tile?(loc)
        c = neighbors(loc).count {|n| black_tile?(n)}
        if c == 0 or c > 2
          new_black_tiles.delete(loc)
        end
      else
        c = neighbors(loc).count {|n| black_tile?(n)}
        if c == 2
          new_black_tiles.add(loc)
        end
      end
    end
    puts "Day #{days}: #{new_black_tiles.size}"
    @black_tiles = new_black_tiles
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = Grid.new
  lines = IO.readlines(fname, chomp: true)
  lines.each {|s| grid.execute_directions(s)}

  if part == 1
    puts grid.black_tiles.length
  else
    100.times { grid.advance_day }
  end
end

if __FILE__ == $0
  main(ARGV)
end
