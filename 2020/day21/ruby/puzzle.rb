#!/usr/bin/env ruby

require 'set'

class Food
  attr_accessor :ingredients, :allergens

  def initialize(s)
    s1,s2 = s.split(" (contains ")
    s2.chop!

    @ingredients = s1.split(" ").map(&:strip)
    @allergens = s2.split(", ").map(&:strip)
  end
end

class FoodList
  attr_accessor :items

  def initialize(fname)
    @items = IO.readlines(fname, chomp: true).map {|s| Food.new(s)}
  end

  def total_occurences(ingredients)
    @items.map {|item| item.ingredients}.flatten.
      count {|ingredient| ingredients.include?(ingredient)}
  end

  def ingredient_with_no_allergens
    @items.map {|item| item.ingredients}.flatten.uniq - allergens_to_ingredient.values
  end

  def dangerous_list
    allergens_to_ingredient.sort_by {|k,v| k}.map {|k,v| v}.join(",")
  end

  def allergens_to_ingredient
    allergens = @items.map(&:allergens).
      flatten.tally.sort_by {|k,v| v}.map {|k,v| k}

    food_with_allergens_list = allergens.map do |allergen|
      items.select {|item| item.allergens.include?(allergen)}
    end

    common_ingredients_list = food_with_allergens_list.map do |containers|
      Set.new(containers.map(&:ingredients).reduce(&:intersection))
    end

    allergens_to_ingredients = allergens.zip(common_ingredients_list).
      sort_by {|a,i| i.size}

    allergens_to_ingredient = {}

    while not allergens_to_ingredients.empty?
      allergen, ingredients = allergens_to_ingredients.shift
      ingredient = ingredients.first
      allergens_to_ingredient[allergen] = ingredient
      allergens_to_ingredients.each do |a,i|
        i.delete(ingredient)
      end
      allergens_to_ingredients.sort_by! {|a,i| i.size}
    end

    allergens_to_ingredient
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  food_list = FoodList.new(fname)
  if part == 1
    puts food_list.total_occurences(food_list.ingredient_with_no_allergens)
  else
    puts food_list.dangerous_list
  end
end

if __FILE__ == $0
  main(ARGV)
end
