#!/usr/bin/env ruby

ACTIONS = [:north, :south, :east, :west, :left, :right, :forward]
ACTION_NOTATION = %w{N S E W L R F}.zip(ACTIONS).to_h

def parse_move(s)
  s =~ /^(.)(\d+)$/
  [ACTION_NOTATION[$1], $2.to_i]
end

def apply_move(pos, move)
  x,y,o = pos
  action,step = move

  case action
  when :north
    y += step
  when :south
    y -= step
  when :east
    x += step
  when :west
    x -= step
  when :left
    o += step
  when :right
    o -= step
  when :forward
    x += step*Math.cos(o*Math::PI/180.0)
    y += step*Math.sin(o*Math::PI/180.0)
  else
    raise "Unknown action #{action}"
  end
  [x,y,o]
end

def rotate_waypoint(wx, wy, deg)
  wr = Math.sqrt(wx**2 + wy**2)
  wth = Math.atan2(wy,wx)
  rad = deg * Math::PI/180.0
  [wr * Math.cos(wth+rad), wr * Math.sin(wth+rad)]
end

def apply_move_with_waypoint(pos, move)
  x,y,wx,wy = pos
  action,step = move

  case action
  when :north
    wy += step
  when :south
    wy -= step
  when :east
    wx += step
  when :west
    wx -= step
  when :left
    wx, wy = rotate_waypoint(wx, wy, step)
  when :right
    wx, wy = rotate_waypoint(wx, wy, -step)
  when :forward
    x += step*wx
    y += step*wy
  else
    raise "Unknown action #{action}"
  end
  [x,y,wx,wy]
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  moves = IO.readlines(fname, chomp: true).map {|m| parse_move(m)}

  if part == 1
    start_pos = [0.0,0.0,0.0]
    end_pos = moves.inject(start_pos) {|pos,move| apply_move(pos,move)}

  else
    start_pos = [0.0,0.0,10.0,1.0]
    end_pos = moves.inject(start_pos) do |pos,move|
      apply_move_with_waypoint(pos,move)
    end
  end

  x,y = end_pos[0], end_pos[1]
  manhatten = (x.abs+y.abs).round
  puts manhatten
end

if __FILE__ == $0
  main(ARGV)
end
