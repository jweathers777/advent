#!/usr/bin/env ruby

def first_invalid_number(preamble, values)
  index = 0
  previous_values = []
  preamble.times do
    previous_values.push(values[index])
    index +=1
  end

  values[index..].each do |value|
    if previous_values.combination(2).map {|a,b| a+b}.include?(value)
      previous_values.shift
      previous_values.push(value)
    else
      return value
    end
  end
end

def find_contiguous_sum(values, target_value)
  0.upto(values.length - 1).to_a.combination(2).map do |a,b|
    if values[a..b].sum == target_value
      return [a,b]
    end
  end
  return [-1,-1]
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  preamble = args[2].to_i
  values = IO.readlines(fname).map(&:to_i)

  invalid_number = first_invalid_number(preamble, values)
  if part == 1
    puts invalid_number
  else
    a,b = find_contiguous_sum(values, invalid_number)
    weakness = values[a..b].min + values[a..b].max
    puts weakness
  end
end

if __FILE__ == $0
  main(ARGV)
end
