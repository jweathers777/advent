#!/usr/bin/env ruby

class CupCircle
  attr_accessor :current_cup, :max_value

  class Node
    attr_accessor :value, :pred, :succ

    def initialize(value, pred=nil, succ=nil)
      @value = value
      @pred = pred
      @succ = succ
    end
  end

  def self.from_file(fname, augment=false)
    values = IO.read(fname).strip.split('').map(&:to_i)
    if augment
      max_value = values.max
      values = values + (max_value+1..1000000).to_a
    end
    CupCircle.new(values)
  end

  def initialize(cups)
    @max_value = cups.max
    @cups_by_value = Array.new(cups.max)

    first_cup = Node.new(cups[0])
    @current_cup = first_cup
    @cups_by_value[cups[0]] = first_cup


    cups[1..].each_with_index do |cup,index|
      @current_cup.succ = Node.new(cup, @current_cup)
      @current_cup = @current_cup.succ
      @cups_by_value[cup] = @current_cup
    end
    @current_cup.succ = first_cup
    first_cup.pred = @current_cup
    @current_cup = first_cup
  end

  def to_s
    s = ""
    first_cup = @current_cup
    cup_to_visit = first_cup
    visiting = true
    while visiting
      if cup_to_visit == first_cup
        s << "(#{cup_to_visit.value}) "
      else
        s << "#{cup_to_visit.value} "
      end
      cup_to_visit = cup_to_visit.succ
      visiting = cup_to_visit != first_cup
    end
    s
  end

  def inspect
    to_s
  end

  def take_after_current(n)
    first_cup = @current_cup.succ
    last_cup = @current_cup
    n.times do
      last_cup = last_cup.succ
    end
    @current_cup.succ = last_cup.succ
    @current_cup.succ.pred = @current_cup
    [first_cup, last_cup]
  end

  def next_destination(cups)
    filtered = []
    current_cup, last_cup = cups
    while current_cup != last_cup
      filtered.push(current_cup.value)
      current_cup = current_cup.succ
    end
    filtered.push(last_cup.value)

    value = @current_cup.value
    seeking_destination = true
    while seeking_destination
      value = value == 1 ? @max_value : (value - 1)
      seeking_destination = filtered.include?(value)
    end

    @cups_by_value[value]
  end

  def insert_after_current(pred_cup, cups)
    first_cup, last_cup = cups

    last_cup.succ = pred_cup.succ
    pred_cup.succ = first_cup
    first_cup.pred = pred_cup
  end

  def values_after(value)
    cup_to_visit = @current_cup
    while cup_to_visit.value != value
      cup_to_visit = cup_to_visit.succ
    end

    first_cup = cup_to_visit
    cup_to_visit = first_cup.succ
    visiting = true
    values = []
    while visiting
      values.push(cup_to_visit.value)
      cup_to_visit = cup_to_visit.succ
      visiting = cup_to_visit != first_cup
    end
    values
  end

  def find_node(value)
    cup_to_visit = @current_cup
    while cup_to_visit.value != value
      cup_to_visit = cup_to_visit.succ
    end
    cup_to_visit
  end

  def make_move
    #print "cups: "
    #puts self

    pickup = take_after_current(3)
    #print "pick up: #{pickup[0].value}, "
    #print "#{pickup[0].succ.value}, "
    #puts "#{pickup[1].value}"

    destination = next_destination(pickup)
    #puts "destination: #{destination.value}"

    insert_after_current(destination, pickup)
    @current_cup = @current_cup.succ
    self
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  if part == 1
    circle = CupCircle.from_file(fname)
    100.times { circle.make_move }
    puts circle.values_after(1).map(&:to_s).join("")
  else
    circle = CupCircle.from_file(fname, true)

    n = 10_000_000
    n.times { circle.make_move }

    one_cup = circle.find_node(1)

    value1 = one_cup.succ.value
    value2 = one_cup.succ.succ.value
    puts value1 * value2
  end
end

if __FILE__ == $0
  main(ARGV)
end
