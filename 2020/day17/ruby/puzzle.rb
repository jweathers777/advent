#!/usr/bin/env ruby

require 'set'

class Grid
  INACTIVE = '.'
  ACTIVE = '#'

  attr_reader :data

  def initialize(fname, dim)
    @dim = dim
    @data = {}
    lines = IO.readlines(fname, chomp: true)

    lines.map(&:chars).each_with_index do |row, y|
      row.each_with_index do |col, x|
        key = [x,y] + [0]*(@dim-2)
        @data[key] = col
      end
    end

    delta_base = (-1..1).to_a
    @neighbor_deltas = (1..@dim).inject([[]]) do |a,_|
      [].tap do |a_next|
        a.each do |e|
          delta_base.each do |de|
            a_next.push(e + [de])
          end
        end
      end
    end

    @neighbor_deltas.delete([0]*@dim)
  end

  def neighbors(v)
    @neighbor_deltas.map {|dv| v.zip(dv).map {|e,de| e+de}}
  end

  def execute_cycle
    all_cubes = Set.new
    @data.keys.each do |key|
      all_cubes.add(key)
      neighbors(key).each do |nk|
        all_cubes.add(nk)
      end
    end

    updates = {}
    all_cubes.each do |key|
      if @data[key] == ACTIVE
        active_neighbors = neighbors(key).map {|nk| @data[nk]}.count(ACTIVE)
        unless active_neighbors == 2 or active_neighbors == 3
          updates[key] = INACTIVE
        end
      else
        active_neighbors = neighbors(key).map {|nk| @data[nk]}.count(ACTIVE)
        if active_neighbors == 3
          updates[key] = ACTIVE
        end
      end
    end

    updates.keys.each do |key|
      @data[key] = updates[key]
    end
  end

  def active_cycles_count
    @data.values.count(ACTIVE)
  end
end

def main(args)
  dims = args[0].to_i
  fname = args[1]

  grid = Grid.new(fname, dims)

  6.times do
    grid.execute_cycle
  end
  puts grid.active_cycles_count
end

if __FILE__ == $0
  main(ARGV)
end
