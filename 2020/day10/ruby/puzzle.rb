#!/usr/bin/env ruby

def combinations(n, k)
  return 1 if k == 0 or k == n
  (k + 1..n).inject(:*) / (1..n-k).inject(:*)
end


def read_adapters(fname)
  IO.readlines(fname).map(&:to_i).sort
end

def removal_options(adapters)
  diffs = chain_diffs(adapters)
  adapters.each_index.inject([[],3]) do |a,e|
    a0_p = (a[1] + diffs[e] <= 3) ? a[0].push(e-1) : a[0]
    [a0_p, diffs[e]]
  end
end

def count_chains(adapters)
  option_indexes, _ = removal_options(adapters)
  options = option_indexes.map {|i| adapters[i]}

  groups = [[options[0]]]
  options.each_cons(2) do |a,b|
    if b-a == 1
      groups[-1].push(b)
    else
      groups.push([b])
    end
  end

  groups.map {|g| n = g.length; n < 3 ? 2**n : (1+combinations(n,1)+combinations(n,2))}.inject(&:*)
end

def chain_diffs(adapters)
  ([0] + adapters).zip(adapters + [adapters[-1] + 3]).map {|a,b| b-a}
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  adapters = read_adapters(fname)

  if part == 1
    diffs = chain_diffs(adapters)
    puts diffs.count(1) * diffs.count(3)
  else
    puts count_chains(adapters)
  end
end

if __FILE__ == $0
  main(ARGV)
end
