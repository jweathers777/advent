#!/usr/bin/env ruby

def say_numbers(s, n)
  numbers = s.split(",").map(&:to_i)
  last_turn_spoken = {}
  start = numbers.length + 1
  last_said = nil

  numbers.each_with_index do |number,index|
    last_turn = index
    prev_turn, last_turn_spoken[last_said] = last_turn_spoken[last_said], last_turn
    last_said = number
  end

  start.upto(n) do |turn|
    last_turn = turn - 1
    prev_turn, last_turn_spoken[last_said] = last_turn_spoken[last_said], last_turn
    last_said = prev_turn ? last_turn - prev_turn : 0
  end

  last_said
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  turns = part == 1 ? 2020 : 30000000
  puts say_numbers(IO.read(fname), turns)
end

if __FILE__ == $0
  main(ARGV)
end
