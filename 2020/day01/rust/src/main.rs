use std::env;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;

fn find_sublist_for_sum_helper(values: &[u32], sublist_size: u32, target_sum: u32) -> Vec<u32> {
    let mut list = Vec::new();

    if sublist_size == 1 {
        for &value in values {
            if value > target_sum {
                break;
            } else if value == target_sum {
                list.push(value);
                break;
            }
        }
    } else {
        for (pos, &value) in values.iter().enumerate() {
            if value > target_sum {
                break;
            }
            list = find_sublist_for_sum_helper(&values[(pos+1)..], sublist_size-1, target_sum - value);
            if list.len() > 0 {
                list.push(value);
                break;
            }
        }
    }
    return list;
}

fn find_sublist_for_sum(values: &Vec<u32>, sublist_size: u32, target_sum: u32) -> Vec<u32> {
    let mut vals: Vec<u32> = values.to_vec();
    vals.sort();
    return find_sublist_for_sum_helper(&vals[0..], sublist_size, target_sum);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments") }

    let size: u32 = args[1].parse().expect("Invalid size!");
    let f = File::open(&args[2]).expect("File not found!");
    let reader = BufReader::new(&f);

    let values: Vec<u32> = reader.lines().
        map(|l| l.unwrap().trim().parse::<u32>().expect("Invalid value!")).
        collect();

    let sublist = find_sublist_for_sum(&values, size, 2020);
    let product: u64 = sublist.iter().fold(1, |p, &v| p * (v as u64));

    println!("{}", product);
}
