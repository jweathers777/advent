module Main where

import System.Environment
import System.IO
import Data.List

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let termCount = read (args !! 0) :: Integer
           fileName = args !! 1

       contents <- readFile fileName
       let values = map (\l -> read l :: Integer) (lines contents)
       let terms = findTermsForSum values termCount 2020
       if null terms
          then putStrLn "Unable to find a solution!"
          else putStrLn (show (product terms))

findTermsForSum :: Real a => [a] -> a -> a -> [a]
findTermsForSum values termCount targetSum =
  findTermsForSum' (sort values) termCount targetSum

findTermsForSum' :: Real a => [a] -> a -> a -> [a]
findTermsForSum' values 1 targetSum =
  takeWhile (==targetSum) $ take 1 $ dropWhile (<targetSum) values

findTermsForSum' values termCount targetSum  =
  findTermsForSumLoop (takeWhile (<targetSum) values) termCount targetSum

findTermsForSumLoop :: Real a => [a] -> a -> a -> [a]
findTermsForSumLoop [] _ _  = []
findTermsForSumLoop candidates termCount targetSum  =
  if null terms
     then findTermsForSumLoop remaining termCount targetSum
     else candidate : terms
  where
    candidate = head candidates
    remaining = tail candidates
    terms = findTermsForSum' remaining (termCount - 1) (targetSum - candidate)
