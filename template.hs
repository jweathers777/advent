module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List

solve :: Int -> String -> Int
solve _ _ = 0

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
