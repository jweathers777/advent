module GraphUtils where

import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

type Edge = (String, String, Int)
data Graph = Graph { nodes :: Set String
                   , edges :: [Edge]
                   } deriving (Show)

mkGraph :: String -> Graph
mkGraph s = Graph nodes edges
  where
    edge tokens = (tokens !! 0, tokens !! 2, (read (tokens !! 4) :: Int))
    edges = map (edge . words) $ lines s
    starts = map (\(s,_,_) -> s) edges
    ends = map (\(_,e,_) -> e) edges
    nodes = Set.fromList (starts ++ ends)

adjEdges :: Graph -> String -> [Edge]
adjEdges graph node =
  filter (\(n, _, _) -> n == node) $ edges graph

shortestDistance :: Graph -> String -> String -> Int
shortestDistance graph start finish = helper start unvisited distances
  where
    unvisited = nodes graph
    maxDistance = maximum $ map (\(_,_,d) -> d) $ edges graph
    infinity = maxDistance * (Set.size unvisited)
    initialDistancePair n = if n /= start then (n,infinity) else (n,0)
    distances = Map.fromList $ map initialDistancePair $ Set.toList unvisited
    helper current unv dists
      | current == finish = case Map.lookup current dists of
                              Nothing -> error ("Distance not found for "++current)
                              Just v -> v
      | otherwise = helper next unv' dists'
        where
          neighbors = adjEdges graph current
          unvNeighbors = filter (\(_, y, _) -> Set.member y unv) neighbors
          currentDist = case Map.lookup current dists of
                          Nothing -> error ("Distance not found for "++current)
                          Just v -> v
          neighborDists = map (\(_, y, d) -> (y, currentDist + d)) unvNeighbors
          combine ds (y, d) = Map.insertWith min y d ds
          dists' = foldl' combine dists neighborDists
          unv' = Set.delete current unv
          chooseUnv (minY, minD) (y, d) =
            if d < minD || (d == minD && y == finish)
               then (y, d)
               else (minY, minD)
          unvDists = filter (\(y, _) -> Set.member y unv) $ Map.toList dists'
          (next, _) = foldl1' chooseUnv unvDists
