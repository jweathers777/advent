require 'matrix'

class Direction < Vector
  NORTH = Direction[0,-1]
  NORTH_EAST = Direction[1,-1]
  EAST = Direction[1,0]
  SOUTH_EAST = Direction[1,1]
  SOUTH = Direction[0,1]
  SOUTH_WEST = Direction[-1,1]
  WEST = Direction[-1,0]
  NORTH_WEST = Direction[-1,-1]

  DIRECTIONS = [
    NORTH, NORTH_EAST, EAST, SOUTH_EAST,
    SOUTH, SOUTH_WEST, WEST, NORTH_WEST
  ]

  CARDINAL_DIRECTIONS = [NORTH, EAST, SOUTH, WEST]

  def rot90
    @m90 ||= Matrix[[0,-1],[1,0]]
    @m90 * self
  end

  def rot270
    @m270 ||= Matrix[[0,1],[-1,0]]
    @m270 * self
  end
end

