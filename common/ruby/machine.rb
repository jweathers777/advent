class Machine
  attr_reader :accumulator, :pointer, :visited_addresses, :instructions

  def initialize(s)
    reset
    @instructions = s.split("\n").map {|l| l.split}.
      map {|o,v| [o.to_sym, v.sub('+','').to_i]}
  end

  def reset
    @accumulator =  0
    @pointer = 0
    @visited_addresses = {}
  end

  def execute_instructions
    is_success = true
    while is_success && @instructions[@pointer]
      op, val = @instructions[@pointer]
      is_success = execute_instruction(op, val)
    end
    is_success
  end

  def execute_instruction(op, val)
    current_accumulator = @accumulator
    send(op, val)
    @pointer += 1

    if @visited_addresses[@pointer]
      @accumulator = current_accumulator
      false
    else
      @visited_addresses[@pointer] = true
    end
  end

  def find_instructions(op)
    @instructions.each_with_index.
      select {|inst,indx| inst[0] == op}.map {|op,val| val}
  end

  def find_and_fix_bad_jmp
    find_instructions(:jmp).each do |address|
      reset
      @instructions[address][0] = :nop
      if not execute_instructions
        @instructions[address][0] = :jmp
      else
        return address
      end
    end
    return -1
  end

  ##############
  # Instructions
  ##############

  def acc(value)
    @accumulator += value
  end

  def jmp(value)
    @pointer += value - 1 # Executing always advances 1 so subtract 1 here to offset
  end

  def nop(value)
    # Do nothing
  end
end
