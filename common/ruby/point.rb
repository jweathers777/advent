class Point < Array
  def binary_op(op, other)
    Point.new(self.zip(other).map {|a,b| a.send(op, b)})
  end

  def scalar_op(op, scalar)
    Point.new(self.map {|a| a.send(op,scalar)})
  end

  def +(other)
    binary_op("+", other)
  end

  def -(other)
    binary_op("-", other)
  end

  def *(scalar)
    scalar_op("*", scalar)
  end

  def /(scalar)
    scalar_op("/", scalar)
  end

  def to_s
    "(#{self.map(&:to_s).join(',')})"
  end

  def Point.in_bounds?(bounds, p)
    bounds.zip(p).all? {|b,d| d >= 0 && d < b}
  end
end

