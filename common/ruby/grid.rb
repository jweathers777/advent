require 'matrix'
require '~/advent/common/ruby/direction.rb'

class Grid
  include Enumerable

  private
  def initialize(cells={}, width=0, height=0)
    @cells = cells
    @width = width
    @height = height
  end

  public
  def each(&block)
    @cells.each(&block)
    self
  end

  def each_all(&block)
    @width.times do |r|
      @height.times do |c|
        block.call(@cells[Vector[c,r]])
      end
    end
  end

  def each_all_with_index(&block)
    @width.times do |r|
      @height.times do |c|
        index = Vector[c,r]
        block.call(@cells[index], index)
      end
    end
  end

  def [](c,r)
    @cells[Vector[c,r]]
  end

  def []=(c,r,value)
    @cells[Vector[c,r]] = value
  end

  def empty_cell_sym
    '.'
  end

  def cell_join_sym
    ''
  end

  def to_s
    each_all {|cell| cell ? cell : empty_cell_sym}.join(cell_join_sym)
  end

  def Grid.from_s(s)
    height = 0
    width = nil
    cells = {}
    s.split("\n").each_with_index do |l,r|
      height += 1
      row = l.strip.split('')
      width ||= row.length
      row.each_with_index do |cell,c|
        cells[Vector[c,r]] = cell
      end
    end

    Grid.new(cells, width, height)
  end
end
