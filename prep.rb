#!/usr/bin/env ruby

require 'fileutils'

def copy_template(lang, dir)
  case lang
  when "ruby"
    src = "template.rb"
    dest = "#{dir}/ruby/puzzle.rb"
  when "rust"
    src = "template.rs"
    dest = "#{dir}/rust/src/main.rs"
  when "haskell"
    src = "template.hs"
    dest = "#{dir}/haskell/Main.hs"
  end

  FileUtils.cp(src, dest) unless File.exist?(dest)

  if lang == "ruby"
    FileUtils.chmod "ug+x", "#{dir}/ruby/puzzle.rb"
  end
end

def init_subdir(lang, subdir)
  return if Dir.exist? subdir
  case lang
  when "ruby"
    FileUtils.mkdir_p subdir
  when "rust"
    `cargo new --name puzzle #{subdir}`
  when "haskell"
    pwd = FileUtils.pwd
    FileUtils.mkdir_p subdir
    FileUtils.cd subdir
    `cabal init -p puzzle`
    FileUtils.rm 'Main.hs'
    FileUtils.cd pwd
  else
    raise "Unsupported language #{lang}"
  end
end

def main(args)
  year = args[0]
  dir = "#{year}/#{args[1]}"
  langs = args[2..] || %w{ruby rust haskell}

  data_files = %w{test.dat input.dat}.map {|f| "#{dir}/#{f}"}
  FileUtils.mkdir_p dir
  data_files.each {|fn| FileUtils.touch fn}

  langs.each do |lang|
    subdir = "#{dir}/#{lang}"
    init_subdir(lang, subdir)
    data_files.each do |f|
      src = "../#{File.basename(f)}"
      dest = "#{subdir}/#{File.basename(f)}"
      FileUtils.ln_s(src, subdir) unless File.exist?(dest)
    end

    copy_template(lang, dir)
  end
end

if __FILE__ == $0
  main(ARGV)
end
