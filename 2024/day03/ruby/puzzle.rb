#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]

  program = IO.read(fname)

  if part == 1
    re = /mul\((\d+),(\d+)\)/

    puts program.scan(re).map {|a,b| a.to_i*b.to_i}.sum
  else
    re = /(?:(do)\(\))|(?:(don't)\(\))|(?:(mul)\((\d+),(\d+)\))/
    ops = program.scan(re).map(&:compact)

    _, result = ops.inject([true,0]) do |state, op|
      active, sum = state
      opname, *args = op

      case opname
      when "mul"
        if active
          sum += args.map(&:to_i).inject(1, &:*)
        end
      when "do"
        active = true
      else
        active = false
      end

      [active, sum]
    end

    puts result
  end
end

if __FILE__ == $0
  main(ARGV)
end
