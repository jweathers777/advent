#!/usr/bin/env ruby

require 'matrix'
require '~/advent/common/ruby/direction.rb'

def in_bounds?(bounds, p)
  bounds.zip(p).all? {|b,d| d >= 0 && d < b}
end

$gmap = nil

class Border
  attr_reader :exterior, :interior

  def initialize(exterior,interior)
    @exterior = exterior
    @interior = interior
  end

  def ==(other)
    self.class == other.class &&
      @exterior == other.exterior &&
      @interior == other.interior
  end

  alias eql? ==

  def orthogonal
    Direction[*(@exterior - @interior).to_a]
  end

  def direction
    orthogonal.rot90
  end

  def hash
    (@exterior + @interior).hash
  end

  def to_s
    "#{@exterior}:#{@interior}"
  end
end

class Region
  ORDINARY_PRICE = 1
  DISCOUNT_PRICE = 2

  @@price_type = ORDINARY_PRICE

  attr_reader :plant, :points

  def initialize(plant, points=[])
    @plant = plant
    @points = Set.new(points)
  end

  def add(p)
    @points.add(p)
  end

  def intersect?(r)
    @points.intersect?(r.points)
  end

  def merge!(r)
    @points += r.points
  end

  def area
    @points.size
  end

  def neighbors(p)
    Direction::CARDINAL_DIRECTIONS.map {|d| p+d}.
      select {|q| @points.member?(q)}
  end

  def perimeter
    cardinal_directions = Direction::CARDINAL_DIRECTIONS.length
    @points.map {|p| cardinal_directions - neighbors(p).length }.sum
  end

  def side_count
    #debug = self.plant == "F"
    #debug = true

    # Find all borders
    to_expand = []
    expanded = Set.new
    borders = Set.new

    to_expand.push(@points.to_a[0])

    while !to_expand.empty?
      p = to_expand.pop
      Direction::CARDINAL_DIRECTIONS.each do |d|
        n = p+d
        if @points.member?(n)
          to_expand.push(n) unless expanded.member?(n)
        else
          b = Border.new(n,p)
          borders.add(b)
        end
      end
      expanded.add(p)
    end

    # Label all borders
    border_labels = {}
    next_label = 0
    borders.each do |b|
      current_label = nil
      e = b.exterior
      i = b.interior

      # Direction of border
      d = b.direction

      # Find neighboring borders
      nbs = []
      [e+d,e-d].zip([i+d,i-d]).map do |ne,ni|
        nb = Border.new(ne,ni)
        nbs.push(nb) if borders.member?(nb)
      end

      # Look for already labeled neighbors
      labels = nbs.map {|nb| border_labels[nb]}.compact.uniq.sort
      if labels.length == 1
        # We only have one labeled neighbor
        # Use its label
        current_label = labels.first
      elsif labels.length == 2
        # We have a conflict of labels
        # Use first one and relabel others
        current_label = labels.first
        borders_by_label = border_labels.keys.group_by {|k| border_labels[k]}
        labels.each do |l|
          borders_by_label[l].each do |b|
            border_labels[b] = current_label
          end
        end
      else
        # We found nolabels so take the next one
        current_label = next_label
        next_label += 1
      end

      ([b] + nbs).each do |tb|
        border_labels[tb] = current_label
      end
    end

    c = border_labels.values.uniq.length
    c
  end

  def lbl(l)
    (l.ord+97).chr
  end

  def print_map(border_labels)
    mul = 3
    hs = 2
    ws = 5

    h =  hs + $gmap.height*mul
    w =  ws + $gmap.width*mul

    a = Array.new(h)
    h.times.each do |r|
      a[r] = Array.new(w, " ")
    end

    $gmap.width.times.map do |c|
      i = mul*c + ws
      a[0][i] = c.to_s
    end

    $gmap.height.times.map do |r|
      j = mul*r + hs
      a[j][0] = r.to_s
      $gmap.width.times.map do |c|
        i = mul*c + ws
        p = Vector[c,r]
        if @points.member?(p)
          a[j][i] = @plant
        else
          a[j][i] = '.'
        end
      end
    end

    border_labels.each do |b,l|
      o = b.orthogonal
      p = b.interior
      i = mul*p[0] + ws
      j = mul*p[1] + hs
      q = Vector[i,j]
      q += o
      a[q[1]][q[0]] = lbl(l)
    end

    s = ""
    a.each do |row|
      row.each do |val|
        s << val
      end
      s << "\n"
    end
    s << "\n"
    puts s
  end

  def price
    case @@price_type
    when ORDINARY_PRICE
      area * perimeter
    else
      area * side_count
    end
  end

  def to_s
    "#{@plant} (#{@points.size}): #{@points.map(&:to_s).join(' ')}"
  end

  def Region.price_type=(price_type)
    @@price_type = price_type
  end
end

class GardenMap
  attr_reader :regions, :height, :width, :cells

  def initialize(s, price_type)
    Region.price_type = price_type
    @cells = {}
    @height = 0
    @width = 0
    s.strip.split("\n").each_with_index.map do |row,r|
      @height += 1
      @width = 0
      row.split('').each_with_index do |value,c|
        @cells[Vector[c,r]] = value
        @width += 1
      end
    end

    @bounds = Vector[@width, @height]

    find_regions
  end

  def to_s
    "  "+@width.times.map(&:to_s).join(' ') + "\n" +
    @height.times.map do |r|
      r.to_s + ' ' +
      @width.times.map {|c| @cells[Point.new([c,r])]}.join(' ')
    end.join("\n")
  end

  def neighbors(p)
    Direction::CARDINAL_DIRECTIONS.map {|d| p+d}.
      select {|q| in_bounds?(@bounds, q)}
  end

  def find_regions
    @regions = []
    to_explore = []
    to_explore_later = []
    seen = Set.new
    start = Vector[0,0]
    region = nil

    to_explore.push(start)
    seen.add(start)
    while true
      if to_explore.empty?
        @regions << region
        region = nil

        point = nil
        while !to_explore_later.empty?
          point = to_explore_later.pop
          if point
            if seen.member?(point)
              point = nil
              next
            end
            seen.add(point)
            break
          end
        end
        break unless point
      else
        point = to_explore.pop
      end
      region ||= Region.new(@cells[point], [point])

      neighbors(point).each do |n|
        next if seen.member?(n)
        if @cells[point] == @cells[n]
          region.add(n)
          to_explore.push(n)
          seen.add(n)
        else
          to_explore_later.push(n)
        end
      end
    end
    @regions
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  garden_map = GardenMap.new(IO.read(fname), part)
  $gmap = garden_map
  puts garden_map.regions.map(&:price).sum
end

if __FILE__ == $0
  main(ARGV)
end
