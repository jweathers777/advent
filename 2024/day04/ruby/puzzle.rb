#!/usr/bin/env ruby


def cell_to_s(cell)
  "(#{cell[0]}, #{cell[1]})"
end

class PathNode
  attr_reader :start_cell, :cell, :index, :direction

  def initialize(start_cell, cell, index, direction)
    @start_cell = start_cell
    @cell = cell
    @index = index
    @direction = direction
  end

  def mid_cell
    r0,c0 = @start_cell
    r1,c1 = @cell

    [(r0+r1)/2,(c0+c1)/2]
  end

  def row_index
    @cell[0]
  end

  def col_index
    @cell[1]
  end

  def to_s
    "#{cell_to_s(@start_cell)}, #{cell_to_s(@cell)}, #{@index}, #{cell_to_s(direction)}"
  end
end

class Grid
  NORTH = [0,-1]
  NORTH_EAST = [1,-1]
  EAST = [1,0]
  SOUTH_EAST = [1,1]
  SOUTH = [0,1]
  SOUTH_WEST = [-1,1]
  WEST = [-1,0]
  NORTH_WEST = [-1,-1]

  DIRECTIONS = [
    NORTH, NORTH_EAST, EAST, SOUTH_EAST,
    SOUTH, SOUTH_WEST, WEST, NORTH_WEST
  ]

  attr_reader :height, :width

  def initialize(c)
    @cells = c
    @height = @cells.length
    @width = @cells[0].length
  end

  def to_s
    @cells.map {|r| r.join('')}.join("\n")
  end

  def Grid.from_s(s)
    Grid.new(s.strip.split("\n").map {|r| r.split('')})

  end

  def cell_inbounds?(row_index, col_index)
    row_index >= 0 && row_index < @width &&
      col_index >= 0 && col_index < @height
  end

  def replace_all_cells(ch)
    @cells.each do |row|
      row.each_index do |col_index|
        row[col_index] = ch
      end
    end
  end

  def replace_cells(ch, locations)
    locations.each do |row_index, col_index|
      @cells[row_index][col_index] = ch
    end
  end

  def find_cells(ch)
    [].tap do |cell_coords|
      @cells.each_with_index do |row,r|
        row.each_with_index do |cell,c|
          cell_coords << [r,c] if cell == ch
        end
      end
    end
  end

  def find_string(s)
    start_cells = find_cells(s[0])

    nodes_to_visit = []
    start_cells.each do |start_cell|
      DIRECTIONS.each do |direction|
        node = PathNode.new(start_cell, start_cell, 0, direction)
        nodes_to_visit.push(node)
      end
    end

    [].tap do |matches|
      while !nodes_to_visit.empty?
        node_to_visit = nodes_to_visit.pop

        row_index = node_to_visit.row_index
        col_index = node_to_visit.col_index

        cell_value = @cells[row_index][col_index]
        char_value = s[node_to_visit.index]

        if cell_value == char_value
          next_index = node_to_visit.index + 1

          if next_index == s.length
            matches << node_to_visit
          elsif next_index < s.length
            row_delta, col_delta = node_to_visit.direction

            row_index += row_delta
            col_index += col_delta

            if cell_inbounds?(row_index, col_index)
              start_cell = node_to_visit.start_cell
              next_cell = [row_index, col_index]
              direction = node_to_visit.direction

              node = PathNode.new(
                start_cell, next_cell, next_index, direction
              )
              nodes_to_visit.push(node)
            end
          end
        end
      end
    end
  end
end

def find_paired_matches(matches)
  pairs = []
  centers = matches.map(&:mid_cell)

  [].tap do |pairs|
    center_indices = centers.each_index.to_a

    center_indices.each do |i|
      m1 = matches[i]
      c1 = centers[i]

      center_indices[i+1..].each do |j|
        m2 = matches[j]
        c2 = centers[j]
        has_shared_centers = c1[0] == c2[0] && c1[1] == c2[1]

        if (c1[0] == c2[0] && c1[1] == c2[1]) &&
            m1.direction.all? {|v| v.abs == 1} &&
            m2.direction.all? {|v| v.abs == 1}
          pairs << [m1,m2]
        end
      end
    end
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = Grid.from_s(IO.read(fname))

  if part == 1
    puts grid.find_string("XMAS").length
  else
    matches = grid.find_string("MAS")
    puts find_paired_matches(matches).length
  end
end

if __FILE__ == $0
  main(ARGV)
end
