#!/usr/bin/env ruby

class ComputeStep
  attr_accessor :op, :inputs, :output, :state

  def initialize(op, inputs, output, state=:EXPAND)
    @op = op
    @inputs = inputs
    @output = output
    @state = state
  end

  def compute(computed_values)
    if @op == :ASSIGN
      @inputs.first
    else
      input_values = @inputs.map {|i| computed_values[i]}
      if input_values.any?(&:nil?)
        puts self
        abort
      end
      case @op
      when :AND
        input_values.inject(&:&)
      when :OR
        input_values.inject(&:|)
      when :XOR
        input_values.inject(&:^)
      end
    end
  end

  def to_s
    "#{@inputs.join(" #{@op} ")} -> #{@output}"
  end
end

class GateSystem
  def initialize(s)
    @steps_by_output = {}
    @computed_wires = {}

    top,bottom = s.split("\n\n")

    top.split("\n").each do |l|
      name, value = l.strip.split(": ")
      @steps_by_output[name] = ComputeStep.new(:ASSIGN, [value.to_i], name, :COMPUTE)
    end

    bottom.split("\n").each do |l|
      a, op, b, _, output  = l.split
      inputs = [a,b]
      @steps_by_output[output] = ComputeStep.new(op.to_sym, inputs, output)
    end
  end

  def compute
    @computed_wires = {}

    steps = []
    @steps_by_output.each do |output, step|
      steps.push(step) if output =~ /^z/
    end
    steps.reverse!

    while !steps.empty?
      step = steps.pop
      next if @computed_wires[step.output]

      if step.state == :COMPUTE
        @computed_wires[step.output] = step.compute(@computed_wires)
      else
        step.state = :COMPUTE
        steps.push(step)
        step.inputs.reverse.each do |input|
          steps.push(@steps_by_output[input])
        end
      end
    end
    @computed_wires
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  gate_system = GateSystem.new(IO.read(fname))

  computed_values = gate_system.compute
  #computed_values.keys.sort.each do |name|
    #next if name =~ /^(x|y)/
    #puts "#{name}: #{computed_values[name]}"
  #end
  zwires = computed_values.keys.select {|name| name =~ /^z/}.
    sort_by {|name| -1 * name.sub('z','').to_i}
  puts zwires.map {|n| computed_values[n]}.join('').to_i(2)
end

if __FILE__ == $0
  main(ARGV)
end
