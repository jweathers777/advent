#!/usr/bin/env ruby

require 'matrix'

class ClawMachine
  A_TOKEN_COST = 3
  B_TOKEN_COST = 1

  ERROR_CORRECTION = 10000000000000
  BUTTON_LIMIT = 100

  @@correct_errors = false
  @@has_button_limit = true

  def ClawMachine.correct_errors=(v)
    @@correct_errors = v
    @@has_button_limit = !v
  end

  def initialize(a_delta, b_delta, prize_pos)
    @a_delta = a_delta
    @b_delta = b_delta
    @prize_pos = prize_pos

    if @@correct_errors
      @prize_pos[0] += ERROR_CORRECTION
      @prize_pos[1] += ERROR_CORRECTION
    end
  end

  def to_s
    "Button A: X+#{@a_delta[0]}, Y+#{@a_delta[1]}\n" +
      "Button B: X+#{@b_delta[0]}, Y+#{@b_delta[1]}\n" +
      "Prize: X=#{@prize_pos[0]}, Y=#{@prize_pos[1]}\n\n"
  end

  def solve
    m = Matrix.columns([@a_delta, @b_delta])
    return nil if m.det == 0

    s = m.inverse * @prize_pos
    return nil if (@@has_button_limit &&
      (s[0] > BUTTON_LIMIT || s[1] > BUTTON_LIMIT)) ||
      s[0].denominator != 1 ||
      s[1].denominator != 1

    s
  end

  def cost
    s = solve
    return 0 unless s

    c = A_TOKEN_COST*s[0] + B_TOKEN_COST*s[1]
    return 0 unless c.denominator == 1

    c = c.to_i

    c > 0 ? c : 0
  end

  def ClawMachine.from_s(s)
    lines = s.strip.split("\n")

    lines[0] =~ /Button A: X\+(\d+), Y\+(\d+)/
    a_delta = Vector[$1.to_i, $2.to_i]

    lines[1] =~ /Button B: X\+(\d+), Y\+(\d+)/
    b_delta = Vector[$1.to_i, $2.to_i]

    lines[2] =~ /Prize: X=(\d+), Y=(\d+)/
    prize_pos = Vector[$1.to_i, $2.to_i]

    ClawMachine.new(a_delta, b_delta, prize_pos)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  ClawMachine.correct_errors = true if part != 1

  claw_machines = IO.read(fname).split("\n\n").
    map {|l| ClawMachine.from_s(l)}

  puts claw_machines.map {|m| m.cost}.sum.to_i
end

if __FILE__ == $0
  main(ARGV)
end
