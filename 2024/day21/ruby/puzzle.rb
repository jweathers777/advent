#!/usr/bin/env ruby

require 'matrix'

STDOUT.sync = true

KEY_OPERATIONS = {
  '^' => -> (pos) {pos + Vector[0,-1]},
  'v' => -> (pos) {pos + Vector[0,1]},
  '<' => -> (pos) {pos + Vector[-1,0]},
  '>' => -> (pos) {pos + Vector[1,0]},
}

class Keypad
  GAP = ' '

  attr_reader :initial_position, :keys, :key_operations, :width, :height, :area

  def initialize(s, key_operations=nil)
    @keys = s.split(',').map {|r| r.split('')}
    @height = @keys.length
    @width = @keys.first.length
    @area = @width*@height
    @positions_by_key = {}
    @keys.each_with_index do |row,r|
      row.each_with_index do |key,c|
        pos = Vector[c,r]
        if key == 'A'
          @initial_position = pos
        end
        @positions_by_key[key] = pos
      end
    end
    @key_operations = key_operations
  end

  def key_at(pos)
    @keys[pos[1]][pos[0]]
  end

  def pos_for(key)
    @positions_by_key[key]
  end

  def to_s
    s = ""
    @keys.each_with_index do |row,r|
      if r == 0
        s << (row.first == GAP ? GAP : '+')
        s << (row.map {|c| (c == GAP ? c : '-')*3}.join('+'))
        s << (row.last == GAP ? "\n" : "+\n")
      end

      s << (row.first == GAP ? '  ' : '| ')
      s << row.join(' | ')
      s << (row.last == GAP ? "\n" : " |\n")

      if r == @height - 1
        s << (row.first == GAP ? ' ' : '+')
        s << row.map {|c| (c == GAP ? c : '-')*3}.join('+')
        s << (row.last == GAP ? "\n" : "+\n")
      else
        if row.first == GAP && @keys[r+1].first == GAP
          s << GAP
        else
          s << '+'
        end
        s << row.each_index.map do |i|
          c1 = row[i]
          c2 = @keys[r+1][i]
          if c1 == GAP && c2 == GAP
            GAP*3
          else
            '-'*3
          end
        end.join('+')
        if row.last == GAP && @keys[r+1].last == GAP
          s << "\n"
        else
          s << "+\n"
        end
      end
    end
    s << "\n\n"
  end
end

class Panel
  attr_reader :key_pad, :target_keypad

  def initialize(keypad, target_keypad)
    @keypad = keypad
    @target_keypad = target_keypad
  end

  def translate(input_keys)
    arm_pos = @target_keypad.initial_position
    output_keys = []
    input_keys.split('').each do |key|
      if key == 'A'
        output_keys << @target_keypad.key_at(arm_pos)
      elsif key == Keypad::GAP
        abort "Invalid key press"
      else
        arm_pos = @keypad.key_operations[key].call(arm_pos)
      end
    end
    output_keys.join('')
  end

  def find_shortest_distances(start=@target_keypad.initial_position)
    dist_upper_bound = @target_keypad.area**2
    dist = {}
    prev = {}

    nodes_to_visit = {}
    @target_keypad.keys.each_with_index do |row, r|
      row.each_with_index do |key,c|
        next if key == Keypad::GAP
        pos = Vector[c,r]
        dist[pos] = dist_upper_bound
        nodes_to_visit[pos] = true
      end
    end
    dist[start] = 0

    while !nodes_to_visit.empty?
      node = nodes_to_visit.keys.min_by {|pos| dist[pos]}
      nodes_to_visit.delete(node)

      @keypad.key_operations.each do |key,op|
        n = op.call(node)
        next unless nodes_to_visit.member?(n)
        alt_dist = dist[node] + 1
        if alt_dist <= dist[n]
          dist[n] = alt_dist
          target_key_at_n = @target_keypad.key_at(n)
          target_key_at_node = @target_keypad.key_at(node)
          prev[target_key_at_n] ||= Set.new
          prev[target_key_at_n].add([target_key_at_node, key])
        end
      end
    end

    [dist, prev]
  end

  def find_shortest_paths(target, start=@target_keypad.initial_position)
    source = @target_keypad.key_at(start)

    @paths ||= {}
    @paths[source] ||= {}
    return @paths[source][target] if @paths[source][target]

    @prev ||= {}
    @prev[start] ||= nil
    unless @prev[start]
      _,p = find_shortest_distances(start)
      @prev[start] = p
    end

    prev = @prev[start]

    paths = []
    nodes = [[target, []]]
    while !nodes.empty?
      target_key, path = nodes.pop
      if target_key == source
        path = path.reverse + ['A']
        paths.push(path.join(''))
      else
        if prev[target_key]
          prev[target_key].each do |prev_target_key, prev_key|
            nodes.push([prev_target_key, path + [prev_key]])
          end
        end
      end
    end

    @paths[source][target] = paths
  end

  def find_shortest_groups(target)
    @shortest_groups ||= {}
    if @shortest_groups[target]
      #puts "Using cached group set for #{target}"
      @shortest_groups[target]
    else
      #puts "Computing shortest group set for #{target}"
      initial_pos = @target_keypad.initial_position
      @shortest_groups[target] = target.each_char.map do |ch|
        paths = find_shortest_paths(ch, initial_pos)
        initial_pos = @target_keypad.pos_for(ch)
        paths
      end
    end
  end
end

class Node
  attr_reader :value, :depth, :kind

  OPEN_GROUP_NODE = :OPEN_GROUP_NODE
  CLOSE_GROUP_NODE= :CLOSE_GROUP_NODE
  OPEN_DEPTH_NODE = :OPEN_DEPTH_NODE
  CLOSE_DEPTH_NODE = :CLOSE_DEPTH_NODE
  DEFAULT_NODE = :DEFAULT_NODE

  def initialize(value, depth, kind=:DEFAULT_NODE)
    @value = value
    @depth = depth
    @kind = kind
  end

  def to_s
    "#{@value},#{@depth},#{@kind}"
  end
end

def choose_best(current_value, new_value)
  if current_value.nil? || current_value > new_value
    new_value
  else
    current_value
  end
end

def print_buffers(aggregate_str, best_str, prefix=nil)
  return
  puts "   #{prefix}" if prefix
  aggregate_str.keys.each do |depth|
    puts "   Best Str (#{depth}): #{best_str[depth]}"
    puts "   Aggregate Str (#{depth}): #{aggregate_str[depth]}"
  end
end

def print_node(push_or_pop, node)
  return
  #return unless node.kind == Node::DEFAULT_NODE
  puts "#{push_or_pop},#{node}"
end

def compute_complexity(target, panels)
  puts "\nComputing complexity for #{target}"
  max_depth = panels.length

  nodes = [ Node.new(target, 0) ]

  aggregate_str_length = Hash[(1..(panels.length)).map {|i| [i,0]}]
  best_str_length = Hash[(1..(panels.length)).map {|i| [i,nil]}]

  best_str_cache = {}

  while !nodes.empty?
    node = nodes.pop
    #puts "Cache Size: #{best_str_cache.each.to_a.flatten.join('').length}"
    print_node("<-", node)
    #print_buffers(aggregate_str, best_str, "Before:")
    case node.kind
    when Node::OPEN_DEPTH_NODE
      aggregate_str_length[node.depth] = 0
      best_str_length[node.depth] = nil
    when Node::OPEN_GROUP_NODE
      best_str_length[node.depth] = nil
    when Node::CLOSE_GROUP_NODE
      aggregate_str_length[node.depth] += best_str_length[node.depth]
    when Node::CLOSE_DEPTH_NODE
      parent_depth = node.depth - 1
      best_str_length[parent_depth] =
        choose_best(best_str_length[parent_depth], aggregate_str_length[node.depth])
      best_str_cache[node.value] ||= {}
      #if best_str_cache[node.value][parent_depth]
        #if best_str_cache[node.value][parent_depth] != best_str[parent_depth]
          #puts "#{node.value},#{parent_depth}: #{best_str_cache[node.value][parent_depth]} != #{best_str[parent_depth]}"
          #abort
        #end
      #end
      best_str_cache[node.value][parent_depth] = best_str_length[parent_depth]
      #puts "  #{node.value},#{parent_depth} = #{best_str[parent_depth]}"
    else
      if node.depth < max_depth
        panel = panels[node.depth]
        next_depth = node.depth + 1
        if best_str_cache[node.value] && best_str_cache[node.value][node.depth]
          best_str_length[node.depth] = best_str_cache[node.value][node.depth]
          next
          #puts "  CacheHit: #{node.value},#{node.depth} = #{best_str_cache[node.value][node.depth]}"
        end
        child_node = Node.new(node.value, next_depth, Node::CLOSE_DEPTH_NODE)
        nodes.push(child_node)
        print_node("->", child_node)
        groups = panel.find_shortest_groups(node.value)
        groups.reverse.each do |group|
          child_node = Node.new(nil, next_depth, Node::CLOSE_GROUP_NODE)
          nodes.push(child_node)
          print_node("->", child_node)
          group.reverse.each do |child_target|
            child_node = Node.new(child_target, next_depth)
            nodes.push(child_node)
            print_node("->", child_node)
          end
          child_node = Node.new(nil, next_depth, Node::OPEN_GROUP_NODE)
          nodes.push(child_node)
          print_node("->", child_node)
        end
        child_node = Node.new(node.value, next_depth, Node::OPEN_DEPTH_NODE)
        nodes.push(child_node)
        print_node("->", child_node)
      else
        best_str_length[node.depth] = choose_best(best_str_length[node.depth], node.value.length)
      end
    end
    #print_buffers(aggregate_str, best_str, "After:")
  end

  #print_buffers(aggregate_str, best_str)
  #shortest_seq = aggregate_str[1]

  #shortest_seq_length = shortest_seq.length
  shortest_seq_length = aggregate_str_length[1]
  target =~ /^0*(\d+)\D?/
  numeric_part = $1.to_i
  result = shortest_seq_length * numeric_part

  puts "Shortest Seq Length: #{shortest_seq_length}"
  puts "Numeric Part of Code: #{numeric_part}"
  puts "Complexity: #{result}"
  result
end

def create_panels
  numeric_keypad = Keypad.new("789,456,123, 0A")
  directional_keypad = Keypad.new(" ^A,<v>", KEY_OPERATIONS)

  first_panel = Panel.new(directional_keypad, numeric_keypad)
  second_panel = Panel.new(directional_keypad, directional_keypad)

  [first_panel, second_panel]
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  codes = IO.readlines(fname).map(&:strip)
  first_panel, second_panel = create_panels

  panels = [first_panel]
  n = (part == 1 ? 2 : 25)
  n.times { panels << second_panel }
  #puts compute_complexity(codes[0], panels)
  puts codes.map {|code| compute_complexity(code, panels)}.sum
end

if __FILE__ == $0
  main(ARGV)
end
