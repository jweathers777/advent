#!/usr/bin/env ruby


def increasing(r)
  r.each_cons(2).all? {|a,b| a < b}
end

def decreasing(r)
  r.each_cons(2).all? {|a,b| a > b}
end

def step_between(r, least, most)
  r.each_cons(2).map {|a,b| (a-b).abs}.
    all? {|d| d >= least && d <= most}
end

def safe(r)
  (increasing(r) || decreasing(r)) && step_between(r, 1, 3)
end

def safe_with_dampener(r)
  safe(r) || r.each_index.any? do |i|
    dampened = r.dup
    dampened.delete_at(i)
    safe(dampened)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  reports = IO.readlines(fname, "\n").map {|r| r.split.map(&:to_i)}

  if part == 1
    puts reports.count {|r| safe(r)}
  else
    puts reports.count {|r| safe_with_dampener(r)}
  end
end

if __FILE__ == $0
  main(ARGV)
end
