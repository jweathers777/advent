#!/usr/bin/env ruby

require 'matrix'
require '~/advent/common/ruby/direction.rb'

class Warehouse
  EMPTY = '.'
  WALL = '#'
  BOX = 'O'
  LEFT_BOX = '['
  RIGHT_BOX = ']'
  ROBOT = '@'

  UP = '^'
  DOWN = 'v'
  LEFT = '<'
  RIGHT = '>'

  MOVES = {
    UP => Vector[0,-1],
    DOWN => Vector[0,1],
    LEFT => Vector[-1,0],
    RIGHT => Vector[1,0]
  }
  MOVE_NAMES = {}.tap do |h|
    MOVES.each do |key,value|
      h[value] = key
    end
  end

  @@double_width = false
  @@show_moves = false

  def Warehouse.show_moves=(value)
    @@show_moves = value
  end

  def Warehouse.double_width=(value)
    @@double_width = value
  end

  def initialize(s)
    grid_map, moves = s.split("\n\n")

    @grid = {}
    @height = 0
    @width = nil
    @robot_pos = nil
    grid_map.split("\n").each_with_index do |l,r|
      @height += 1
      row = l.strip.split('')
      @width ||= row.length * (@@double_width ? 2 : 1)
      row.each_with_index do |cell,c|
        if @@double_width
         indices = [2*c,2*c+1]
         boxes = [LEFT_BOX, RIGHT_BOX]
        else
         indices = [c]
         boxes = [BOX]
        end

        indices.each_with_index do |ci, i|
          pos = Vector[ci,r]
          if cell == ROBOT && i == 0
            @robot_pos = pos if i == 0
            @grid[pos] = ROBOT
          elsif cell == BOX
            @grid[pos] = boxes[i]
          elsif cell == WALL
            @grid[pos] = WALL
          end
        end
      end
    end

    @move_seq = moves.split("\n").join('').
      split('').map {|m| MOVES[m.strip]}

    @next_move_index = 0
  end

  def has_moves_left?
    @next_move_index < @move_seq.length
  end

  def next_move
    MOVE_NAMES[@move_seq[@next_move_index]]
  end

  def gps(pos)
    pos[0] + 100*pos[1]
  end

  def total_box_gps
    box = @@double_width ? LEFT_BOX : BOX
    @grid.inject(0) {|a,p| p[1] == box ? (a + gps(p[0])) : a}
  end

  def next
    @@double_width ? next_double_width : next_single_width
  end

  def next_single_width
    step = @move_seq[@next_move_index]
    @next_move_index += 1

    looking_for_space = true
    current_pos = @robot_pos
    while looking_for_space
      current_pos += step
      case @grid[current_pos]
      when nil
        looking_for_space = false
        @grid.delete(@robot_pos)
        @robot_pos = @robot_pos + step
        @grid[@robot_pos] = ROBOT
        @grid[current_pos] = BOX if @robot_pos != current_pos
      when BOX
        next
      when WALL
        looking_for_space = false
      end
    end
    self
  end

  def next_double_width
    step = @move_seq[@next_move_index]
    return next_single_width if step == LEFT || step == RIGHT

    @next_move_index += 1

    positions_to_shift = Set[@robot_pos]
    positions_to_expand = [@robot_pos]

    while !positions_to_expand.empty?
      current_pos = positions_to_expand.pop
      current_value = @grid[current_pos]

      shifts = []
      case current_value
      when WALL
        return self
      when ROBOT
        shifts = [step]
      when LEFT_BOX
        shifts = [step, MOVES[RIGHT]]
      when RIGHT_BOX
        shifts = [step, MOVES[LEFT]]
      else
        next
      end

      neighbors = shifts.map {|s| current_pos + s}
      neighbors.each do |n|
        if !positions_to_shift.member?(n)
          n_value = @grid[n]
          if n_value
            positions_to_shift.add(n)
            positions_to_expand.push(n)
          end
        end
      end
    end

    @robot_pos += step
    old_positions = positions_to_shift.to_a
    old_values = old_positions.map {|p| @grid[p]}
    new_positions = old_positions.map {|p| p+step}

    old_positions.each {|p| @grid.delete(p)}
    old_values.zip(new_positions).each do |v,p|
      @grid[p] = v unless v == WALL
    end

    self
  end

  def to_s
    s = ""
    @height.times do |r|
      @width.times do |c|
        pos = Vector[c,r]
        cell = @grid[pos]
        s << (cell ? cell : EMPTY)
      end
      s << "\n"
    end
    s << "\n"
    if @@show_moves
      @move_seq.each do |m|
        s << MOVE_NAMES[m]
      end
      s << "\n"
    end
    s
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  Warehouse.double_width = part == 2

  warehouse = Warehouse.new(IO.read(fname))

  while warehouse.has_moves_left?
    warehouse.next
  end
  puts warehouse.total_box_gps
end

if __FILE__ == $0
  main(ARGV)
end
