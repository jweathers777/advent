#!/usr/bin/env ruby

class Buyer
  attr_reader :secret, :price, :last_price

  def initialize(secret)
    @initial_secret = secret
    reset
  end

  def reset
    @secret = @initial_secret
    @last_price = nil
    @price = compute_price
  end

  def compute_price
    @secret % 10
  end

  def last_price_change
    @last_price ? (@price - @last_price) : nil
  end

  def mix(a,b)
    a ^ b
  end

  def prune(a)
    a % 16777216
  end

  def update_secret
    value = prune(mix(@secret, @secret << 6))
    value = prune(mix(value, value >> 5))
    @secret = prune(mix(value, value << 11))
    @last_price = @price
    @price = compute_price
    self
  end

  def advance
    update_secret.secret
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  buyers = IO.readlines(fname).map {|l| Buyer.new(l.to_i)}

  n = 2000

  if part == 1
    sum = 0
    buyers.each do |b|
      s0 = b.secret
      n.times { b.advance }
      sn = b.secret
      sum += sn
      #puts "#{s0}: #{sn}"
    end
    puts sum
  else
    total_price_by_seq = Hash.new
    buyer_prices_by_seq = Array.new(buyers.length, nil)
    buyers.each_with_index do |b,i|
      last_four = Array.new(4,nil)
      n.times do
        b.update_secret
        last_four.shift
        last_four.push(b.last_price_change)
        buyer_prices_by_seq[i] ||= Hash.new
        index = last_four.map(&:to_s).join(',')
        if buyer_prices_by_seq[i][index].nil?
          buyer_prices_by_seq[i][index] = b.price
          total_price_by_seq[index] ||= 0
          total_price_by_seq[index] += b.price
        end
      end
    end
    #buyer_prices_by_seq[0].each do |seq, price|
      #puts "#{seq} -> #{price}"
    #end
    best_seq, best_price = total_price_by_seq.max_by {|seq,price| price}
    #puts best_seq
    puts best_price
  end
end

if __FILE__ == $0
  main(ARGV)
end
