#!/usr/bin/env ruby

class Equation
  @@operators = ['+', '*']

  attr_reader :value, :args

  def initialize(a)
    @value, *@args = a.map(&:to_i)
  end

  def to_s
    "#{@value}: #{@args.join(' ')}"
  end

  def perform_op(op, arg1, arg2)
    case op
    when '+'
      arg1 + arg2
    when '*'
      arg1 * arg2
    when '||'
      "#{arg1}#{arg2}".to_i
    end
  end

  def has_solution?
    (@args.length - 2).times.map.
      inject(@@operators) {|p,_| p.product(@@operators).map(&:flatten)}.
      any? do |ops|
        ops = [ops] if ops.is_a?(String)
        v = ops.each_with_index.inject(nil) do |a,e|
          op,i = e
          perform_op(op, a ? a : @args[i], @args[i+1])
        end
        v == @value
      end
  end

  def Equation.operators
    @@operators
  end

  def Equation.operators=(ops)
    @@operators = ops
  end

  def Equation.from_s(s)
    Equation.new(s.split(/:?\s+/))
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  equations = IO.readlines(fname).map {|l| Equation.from_s(l)}
  Equation.operators << '||' if part == 2
  puts equations.select(&:has_solution?).map(&:value).sum
end

if __FILE__ == $0
  main(ARGV)
end
