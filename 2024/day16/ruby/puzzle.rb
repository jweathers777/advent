#!/usr/bin/env ruby

require 'rainbow/refinement'
using Rainbow

require 'matrix'
require '~/advent/common/ruby/direction.rb'

$debug = true

def puts_d(s)
  puts s if $debug
end

class State
  attr_accessor :pos, :dir

  def initialize(pos, dir)
    @pos = pos
    @dir = dir
  end

  def ==(other)
    other.class == State &&
      @pos == other.pos && @dir == other.dir
  end

  alias eql? ==

  def hash
    #[@pos, @dir].hash
    [@pos[0],@pos[1],@dir[0],@dir[1]].hash
  end

  def to_s
    "P:#{pos},D:#{dir}"
  end
end

class Grid
  EMPTY = '.'
  WALL = '#'
  START = 'S'
  FINISH = 'E'

  MOVE_COSTS = {
    :STEP => 1,
    :ROT_90 => 1001,
    :ROT_270 => 1001
  }

  DIRECTION_TO_STR = {
    Direction::NORTH => '^',
    Direction::WEST => '<',
    Direction::EAST => '>',
    Direction::SOUTH => 'v'
  }

  ROTATION_TYPE  = {
    Direction::NORTH => {
      Direction::WEST => :ROT_90,
      Direction::SOUTH => :ROT_180,
      Direction::EAST => :ROT_270
    },
    Direction::SOUTH => {
      Direction::EAST => :ROT_90,
      Direction::NORTH => :ROT_180,
      Direction::WEST => :ROT_270
    },
    Direction::EAST => {
      Direction::NORTH => :ROT_90,
      Direction::WEST => :ROT_180,
      Direction::SOUTH => :ROT_270
    },
    Direction::WEST => {
      Direction::SOUTH => :ROT_90,
      Direction::EAST => :ROT_180,
      Direction::NORTH => :ROT_270
    }
  }

  def initialize(s)
    @height = 0
    @width = nil
    @cells = {}

    s.split("\n").each_with_index do |l,r|
      @height += 1
      row = l.strip.split('')
      @width ||= row.length
      row.each_with_index do |cell,c|
        pos = Vector[c,r]
        case cell
        when START
          @state = State.new(pos, Direction::EAST)
          @start = pos
        when FINISH
          @finish = pos
        end
        @cells[Vector[c,r]] = cell
      end
    end
  end

  def [](c,r)
    @cells[Vector[c,r]]
  end

  def []=(c,r,value)
    @cells[Vector[c,r]] = value
  end

  def search_best_path(start=@state, finish=@finish)
    # start is starting state
    # finish is finishing position
    worst_score = @width*@height*1000

    dist = Hash.new(worst_score)
    prev = {}
    all_prev = {}
    states_to_expand = Set.new
    @cells.each do |pos,cell|
      unless cell == WALL
        Direction::CARDINAL_DIRECTIONS.each do |d|
          states_to_expand.add(State.new(pos, d))
        end
      end
    end
    puts_d "States Space Size: #{states_to_expand.length}"

    dist[start] = 0

    last_state = nil
    iterations = 0
    while !states_to_expand.empty?
      state, state_dist = dist.select {|k,v| states_to_expand.member?(k)}.
        min_by {|k,v| v}

      puts_d "Processing #{state.pos} facing #{DIRECTION_TO_STR[state.dir]}"

      if state.pos == finish
        last_state = state
        break
      end

      states_to_expand.delete(state)

      Direction::CARDINAL_DIRECTIONS.each do |d|
        npos = state.pos + d
        nstate = State.new(npos, d)

        next if @cells[npos] == WALL || !states_to_expand.member?(nstate)
        puts_d "Considering moving #{DIRECTION_TO_STR[d]} to position #{npos}"

        move_type = state.dir == d ? :STEP : ROTATION_TYPE[state.dir][d]
        next if move_type == :ROT_180
        cost = MOVE_COSTS[move_type]

        new_dist = state_dist + cost
        puts_d "Move will cost #{cost} resulting in new distance of #{new_dist}"
        puts_d "Current best distance: #{dist[nstate]}"
        puts_d "Prev States for #{nstate}: #{all_prev[nstate]}"
        iterations += 1
        if new_dist < dist[nstate]
          puts_d "New distance is better than #{dist[nstate]}"
          dist[nstate] = new_dist
          prev[nstate] = state
          all_prev[nstate] ||= Set.new
          all_prev[nstate].add(state)
          puts_d "Updated Prev States for #{nstate}: #{all_prev[nstate]}"
        elsif new_dist == dist[nstate]
          all_prev[nstate] ||= Set.new
          all_prev[nstate].add(state)
          puts_d "Updated Prev States for #{nstate}: #{all_prev[nstate]}"
        end
      end
    end
    puts "Iterations: #{iterations}"

    [last_state, dist, prev, all_prev]
  end

  def find_a_best_path(start=@state, finish=@finish)
    # start is starting state
    # finish is finishing position
    last_state, dist, prev, _ = search_best_path(start, finish)

    path = []
    state = last_state
    if prev[state] || state == start
      while state
        path.push(state)
        state = prev[state]
      end
    end
    [dist[last_state], path.reverse]
  end

  def find_all_best_path_positions(start=@state, finish=@finish)
    # start is starting state
    # finish is finishing position
    last_state, dist, _, all_prev = search_best_path(start, finish)

    nodes = [[last_state]]
    paths = []
    while !nodes.empty?
      path = nodes.pop
      state = path.last
      if state == start
        paths.push(path.reverse)
      elsif all_prev[state]
        all_prev[state].each do |prev_state|
          nodes.push(path + [prev_state])
        end
      end
    end
    path_costs = paths.map {|p| cost_for_path(p)}
    min_path_cost = path_costs.min
    paths.zip(path_costs).select {|p,c| c == min_path_cost}.
      map {|p| p[0]}
  end

  def cost_for_path(path)
    steps = 0
    turns = 0
    path.each_cons(2) do |state, next_state|
      steps += 1
      if state.dir != next_state.dir
        turns += 1
      end
    end
    steps + 1000*turns
  end

  def count_best_path_positions(start=@state, finish=@finish)
    # start is starting state
    # finish is finishing position
    paths = find_all_best_path_positions(start, finish)
    puts "Number of Best Paths: #{paths.length}"
    tiles = Set.new
    paths.each do |path|
      puts_d path.length
      path.each do |state|
        tiles.add(state.pos)
      end
    end
    puts marked_with_tiles(tiles)
    tiles.size
  end

  def to_s
    "".tap do |s|
      @height.times do |r|
        @width.times do |c|
          pos = Vector[c,r]
          cell = @cells[pos]
          s << (cell ? cell : EMPTY)
        end
        s << "\n"
      end
    end
  end

  def marked_with_tiles(positions)
    digits = @width.to_s.length
    "".tap do |s|
      digits.times do |d|
        s << " "*digits
        @width.times do |c|
          s << Rainbow(sprintf("%#{digits}d", c)[d]).yellow
        end
        s << "\n"
      end
      @height.times do |r|
        s << Rainbow(sprintf("%#{digits}d", r)).yellow
        @width.times do |c|
          pos = Vector[c,r]
          if positions.member?(pos)
            s << Rainbow("0").green
          else
            cell = @cells[pos]
            s << (cell ? cell : EMPTY)
          end
        end
        s << "\n"
      end
    end
  end

  def path_to_s(path)
    steps = 0
    turns = 0

    path_cells = {}
    path.each_cons(2) do |state, next_state|
      steps += 1
      if state.dir != next_state.dir
        turns += 1
      end
      path_cells[state.pos] = DIRECTION_TO_STR[next_state.dir]
    end

    "Steps: #{steps}\nTurns: #{turns}\n".tap do |s|
      (@height+1).times do |r|
        (@width+1).times do |c|
          if r == 0 && c == 0
            s << "  "
            next
          elsif r == 0
            s << sprintf("%2d", c-1) << " "
            next
          elsif c == 0
            s << sprintf("%2d", r-1) << " "
            next
          end
          pos = Vector[c-1,r-1]
          cell = @cells[pos]
          case cell
          when START, FINISH, WALL
            s << cell
          else
            path_cell = path_cells[pos]
            s << (path_cell ? path_cell : (cell ? cell : EMPTY))
          end
          s << "  "
        end
        s << "\n"
      end
      s << "\n"
      s << "Current Path Length: #{path.length-1}\n"
    end
  end
end


def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = Grid.new(IO.read(fname))

  if part == 1
    best_score, best_path = grid.find_a_best_path
    puts "Best Score: #{best_score}"
    puts best_path.length
    puts_d grid.path_to_s(best_path)
  else
    puts "Best Seat Count: #{grid.count_best_path_positions}"
  end
end

if __FILE__ == $0
  main(ARGV)
end
