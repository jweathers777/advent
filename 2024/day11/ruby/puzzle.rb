#!/usr/bin/env ruby

class StoneLine
  def initialize(a)
    @cache = {0 => [1]}
    @values = {}
    a.each do |value|
      @values[value] ||= 0
      @values[value] += 1
    end
  end

  def transform(s)
    if @cache[s]
      @cache[s]
    else
      str = s.to_s
      if str.length % 2 == 0
        n = str.length / 2
        @cache[s] = [str[0...n].to_i, str[n..].to_i]
      else
        @cache[s] = [s*2024]
      end
    end
  end

  def blink
    h = {}
    @values.each do |value,count|
      next if count == 0
      transform(value).each do |next_value|
        h[next_value] ||= 0
        h[next_value] += count
      end
    end
    @values = h
    self
  end

  def length
    @values.values.sum
  end

  def to_s
    @values.select {|value,count| count > 0}.
      map {|value,count| "(#{value},#{count})"}.join(' ')
  end

  def StoneLine.from_s(s)
    StoneLine.new(s.strip.split.map(&:to_i))
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  if args[2]
    n = args[2].to_i
  elsif part == 1
    n = 25
  elsif part == 2
    n = 75
  end

  line = StoneLine.from_s(IO.read(fname))
  n.times do |i|
    line = line.blink
  end
  puts line.length
end

if __FILE__ == $0
  main(ARGV)
end
