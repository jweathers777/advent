#!/usr/bin/env ruby

class BlockGroup
  attr_accessor :value, :start, :length

  def initialize(value, start, length)
    @value = value
    @start = start
    @length = length
  end

  def to_s
    "BlockGroup: #{@value}, #{@start}, #{@length}"
  end
end

class DiskMap
  EMPTY = '.'

  def initialize(s)
    file_id = 0
    block_index = 0
    @block_groups_by_index = []
    s.strip.split('').map(&:to_i)
      .each_with_index do |v,i|
      if i % 2 == 0
        label = file_id
        file_id += 1
      else
        label = EMPTY
      end
      block_group = BlockGroup.new(label, block_index, v)
      v.times do
        @block_groups_by_index << block_group
        block_index += 1
      end
    end

    update_from_block_groups_by_index
  end

  def update_from_block_groups_by_index
    @blocks = []
    @empty_block_indices = []
    @files = []
    @empty_chunks = []

    last_block_group = nil
    @block_groups_by_index.each_with_index do |block_group, block_index|
      if block_group.value == EMPTY
        groups = @empty_chunks
        @empty_block_indices << block_index
      else
        groups = @files
      end
      if last_block_group != block_group
        groups << block_group
        last_block_group = block_group
      end
      @blocks << block_group.value
    end

    self
  end

  def compact_blocks
    empty_block_indices = @empty_block_indices.reverse
    non_empty_block_count = @blocks.length - empty_block_indices.length

    @blocks.each_with_index.reverse_each do |block,i|
      break if i < non_empty_block_count
      if block != EMPTY
        first_empty_block = empty_block_indices.pop
        @blocks[first_empty_block] = block
        @blocks[i] = EMPTY
      end
    end
    @empty_block_indices = (non_empty_block_count..@blocks.length).to_a
    self
  end

  def compact_files
    # Construct a hash for looking up empty chunks by start index
    empty_chunks_by_start = {}
    @empty_chunks.each do |chunk|
      empty_chunks_by_start[chunk.start] = chunk
    end

    empty_chunk_starts = empty_chunks_by_start.keys.sort

    # Iterate over file block groups in reverse
    @files.each_with_index.reverse_each do |file,i|
      # Search for first empty chunk large
      # enough to contain this file and that
      # is to the right of the file
      chunk_found = false
      chunk = nil
      file_start = nil
      file_size = nil
      chunk_size = nil

      empty_chunk_starts.each do |i|
        chunk = empty_chunks_by_start[i]
        break if chunk.start >= file.start
        file_start = file.start
        file_size = file.length
        chunk_size = chunk.length

        if file_size <= chunk_size
          chunk_found = true
          break
        end
      end

      # We have found a suitable chunk
      if chunk_found
        # Remove this chunk for our hash
        empty_chunks_by_start.delete(chunk.start)

        # Fill in old chunk with file data
        file_size.times do |i|
          @block_groups_by_index[chunk.start+i] = file
        end

        # Update file start to used chunk's start
        file.start = chunk.start

        # Update the chunk size to remove file size
        updated_chunk_size = chunk_size - file_size
        # If we have any remaining space in the chunk
        if updated_chunk_size > 0
          # Update the chunk's start and size
          chunk.length = updated_chunk_size
          chunk.start += file_size
          # Add the updated chunk back to our hash
          # using its new start index
          empty_chunks_by_start[chunk.start] = chunk
        end

        # Determine whether we have neighboring empty chunks
        left_neighbor = @block_groups_by_index[file_start - 1]
        if left_neighbor && left_neighbor != EMPTY
          left_neighbor = nil
        end
        right_neighbor = @block_groups_by_index[file_start + file_size]
        if right_neighbor && right_neighbor != EMPTY
          right_neighbor = nil
        end

        fill_size = file_size
        fill_chunk = if left_neighbor.nil? && right_neighbor.nil?
          # If we have neither a left or right empty neighbor
          # Then we will replace the old file block group
          # with a new empty chunk
          BlockGroup.new(EMPTY, file_start, file_size)
        elsif left_neighbor && right_neighbor.nil?
          # If we have just a left empty neighbor
          # then we shall expand the left neighbor's size
          # and then fill up the old file block group
          # with the expanded left neighbor
          left_neighbor.tap {|n| n.length = file_size}
        elsif left_neighbor.nil? && right_neighbor
          # If we have just a right empty neighbor
          # then we shall expand the right neighbor's size
          # and start index and then fill up the old
          # file block group with the expanded right neighbor
          right_neighbor.tap do |n|
            empty_chunks_by_start.delete(n.start)
            n.start = file_start
            n.length += file_size
            empty_chunks_by_start[n.start] = n
          end
        else # left_neighbor && right_neighbor
          # If we have both an empty left and right neighbor
          # then we will expand the left neighbor across the
          # old file and the right neighbor
          # Then, we will remove the right neighbor from
          # our hash and use the left neighbor to fill
          # in the file block slots and the right neighbor slots
          file_size = file_size + right_neighbor.length
          left_neighbor.tap do |n|
            n.length += file_size + right_neighbor.length
          end
          empty_chunks_by_start.delete(right_neighbor.start)
        end
        fill_size.times do |i|
          @block_groups_by_index[file_start+i] = fill_chunk
        end

        # Update the sorted list of empty chunk starts
        empty_chunk_starts = empty_chunks_by_start.keys.sort
      end
    end

    self.update_from_block_groups_by_index
  end

  def checksum
    @blocks.each_with_index.select {|b,i| b != EMPTY}.
      map {|b,i| b*i}.sum
  end

  def to_s
    @block_groups_by_index.map(&:value).join('')
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  disk_map = DiskMap.new(IO.read(fname))
  if part == 1
    disk_map.compact_blocks
  else
    disk_map.compact_files
  end
  puts disk_map.checksum
end

if __FILE__ == $0
  main(ARGV)
end
