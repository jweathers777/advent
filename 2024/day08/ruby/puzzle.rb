#!/usr/bin/env ruby

require "~/advent/common/ruby/point.rb"

def _gcd(a,b)
  q = a/b
  r = a - q*b
  r == 0 ? b : _gcd(b, r)
end

def gcd(a,b)
  a = a.abs
  b = b.abs
  a,b = b,a if a < b
  _gcd(a,b)
end

class AntennaGrid
  attr_reader :antinodes

  EMPTY = '.'

  def initialize(s, meth)
    @antennas_locations = {}
    @antennas_by_frequency = {}

    @cells = s.split("\n").each_with_index.map do |row,r|
      row.split('').each_with_index.map do |cell,c|
        if cell != EMPTY
          p = Point.new([c,r])
          @antennas_locations[p] = cell
          @antennas_by_frequency[cell] ||= []
          @antennas_by_frequency[cell] << p
        end
        cell
      end
    end

    @bounds = Point.new([@cells.length, @cells.first.length])

    @antinodes = find_antinodes(meth)
  end

  def find_antinodes(meth)
    {}.tap do |result|
      if meth == 1
        @antennas_by_frequency.each do |frequency, antennas|
          antennas.combination(2).each do |p1,p2|
            delta = p1 - p2
            [p1 + delta, p2 - delta].each do |a|
              if Point.in_bounds?(@bounds, a)
                result[a] ||= Set.new
                result[a].add(frequency)
              end
            end
          end
        end
      else
        @antennas_by_frequency.each do |frequency, antennas|
          antennas.combination(2).each do |p1,p2|
            delta = p1 - p2
            d = gcd(delta[0], delta[1])
            unit_delta = delta / d

            a = p1
            while Point.in_bounds?(@bounds, a)
              result[a] ||= Set.new
              result[a].add(frequency)
              a += unit_delta
            end
            a = p1-unit_delta
            while Point.in_bounds?(@bounds, a)
              result[a] ||= Set.new
              result[a].add(frequency)
              a -= unit_delta
            end
          end
        end
      end
    end
  end

  def to_s
    @cells.map {|r| r.join('')}.join("\n")
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  g = AntennaGrid.new(IO.read(fname), part)
  puts g.antinodes.length
end

if __FILE__ == $0
  main(ARGV)
end
