#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]
  rows = IO.readlines(fname).map(&:split)

  if part == 1
    left = rows.map {|r| r[0].to_i }.sort
    right = rows.map {|r| r[1].to_i }.sort

    distances = left.zip(right).map {|f,s| (f-s).abs }

    total_distance = distances.sum

    puts total_distance
  else
    left = rows.map {|r| r[0].to_i }
    right = rows.map {|r| r[1].to_i }

    score = left.reduce(0) do |s,l|
      c = right.count {|r| l == r}
      s + l*c
    end

    puts score
  end
end

if __FILE__ == $0
  main(ARGV)
end
