#!/usr/bin/env ruby

class Coords
  attr_reader :x, :y

  def initialize(x,y)
    @x = x
    @y = y
  end

  def +(other)
    Coords.new(self.x+other.x,self.y+other.y)
  end

  def ==(other)
    self.x == other.x && self.y == other.y
  end

  alias eql? ==

  def hash
    [@x, @y].hash
  end

  def to_s
    "(#{@x},#{@y})"
  end
end

class Direction < Coords
  NORTH = Direction.new(0,-1)
  SOUTH = Direction.new(0,1)
  EAST = Direction.new(1,0)
  WEST = Direction.new(-1,0)

  def turn_right
    case self
    when NORTH
      EAST
    when EAST
      SOUTH
    when SOUTH
      WEST
    else
      NORTH
    end
  end

  def to_s
    case self
      when NORTH
        "NORTH"
      when SOUTH
        "SOUTH"
      when EAST
        "EAST"
      when WEST
        "WEST"
      else
        super
    end
  end
end

class Guard
  attr_accessor :position, :direction

  def initialize(ch, pos)
    @position = pos
    @direction = case ch
    when "^"
      Direction::NORTH
    when ">"
      Direction::EAST
    when "v"
      Direction::SOUTH
    when "<"
      Direction::WEST
    else
      raise "Invalid character: #{ch}"
    end
  end

  def ==(other)
    other.class == Guard &&
      self.position == other.position && self.direction == other.direction
  end

  alias eql? ==

  def hash
    [@position, @direction].hash
  end

  def state
    [@position.x, @position.y, @direction.x, @direction.y]
  end

  def move(grid)
    new_position = @position + @direction
    if grid.is_obstacle?(new_position)
      @direction = @direction.turn_right
      false
    else
      @position = new_position
      true
    end
  end

  def to_s
    case @direction
    when Direction::NORTH
      "^"
    when Direction::EAST
      ">"
    when Direction::SOUTH
      "v"
    when Direction::WEST
      "<"
    end
  end
end

class Grid
  OBSTACLE = "#"
  EMPTY = "."

  attr_reader :guard, :cells

  def initialize(s)
    @guard_in_grid = false
    @cells = s.split("\n").each_with_index.map do |row, r|
      row.split('').each_with_index.map do |cell, c|
        case cell
        when EMPTY, OBSTACLE
          cell
        else
          @guard_in_grid = true
          @guard = Guard.new(cell, Coords.new(c,r))
        end
      end
    end

    @height = @cells.length
    @width = @cells.first.length
  end

  def add_obstacle(r,c)
    @cells[r][c] = OBSTACLE
  end

  def clear_cell(r,c)
    @cells[r][c] = EMPTY
  end

  def replace_guard(new_guard)
    if @guard_in_grid
      clear_cell(@guard.position.y, @guard.position.x)
      @guard_in_grid = false
    end

    if in_bounds?(new_guard.position)
      @guard_in_grid = true
      @cells[new_guard.position.y][new_guard.position.x] = new_guard
    end

    @guard = new_guard
  end

  def is_obstacle?(pos)
    in_bounds?(pos) && @cells[pos.y][pos.x] == OBSTACLE
  end

  def in_bounds?(pos)
    pos.x >= 0 && pos.x < @width &&
      pos.y >= 0 && pos.y < @height
  end

  def guard_in_grid?
    @guard_in_grid
  end

  def next_state
    start_position = @guard.position
    if @guard.move(self)
      @cells[start_position.y][start_position.x] = EMPTY

      if in_bounds?(@guard.position)
        @cells[@guard.position.y][@guard.position.x] = @guard
      else
        @guard_in_grid = false
      end
    end
    self
  end

  def walk_until_loop_or_exit
    is_loop = false
    visited_positions = Set.new
    guard_states = Set.new
    while self.guard_in_grid?
      if guard_states.member?(@guard.state)
        is_loop = true
        break
      end
      visited_positions.add(@guard.position)
      guard_states.add(@guard.state)
      self.next_state
    end
    [visited_positions, is_loop]
  end

  def to_s
    @cells.map {|row| row.map {|cell| cell.to_s}.join('')}.join("\n")
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = Grid.new(IO.read(fname))

  guard_copy = grid.guard.clone
  visited_positions, _ = grid.walk_until_loop_or_exit
  grid.replace_guard(guard_copy)

  if part == 1
    puts visited_positions.size
  else
    locations = []
    visited_positions.each do |pos|
      next if pos == guard_copy.position
      r = pos.y
      c = pos.x
      value = grid.cells[r][c]
      if value == Grid::EMPTY
        location = Coords.new(c,r)
        guard_copy = grid.guard.clone
        grid.add_obstacle(r,c)

        visited_positions, is_loop = grid.walk_until_loop_or_exit
        if is_loop
          locations << location
        end

        grid.clear_cell(r,c)
        grid.replace_guard(guard_copy)
      end
    end
    puts locations.length
  end
end

if __FILE__ == $0
  main(ARGV)
end
