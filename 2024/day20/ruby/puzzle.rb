#!/usr/bin/env ruby

require 'matrix'
require '~/advent/common/ruby/direction.rb'

STDOUT.sync = true

class RaceTrack
  TRACK_START = 'S'
  TRACK_END = 'E'
  TRACK_POS = '.'
  TRACK_WALL = '#'

  def initialize(s, radius)
    @cells = s.split("\n").
      map {|l| l.strip.split('')}
    @height = @cells.length
    @width = @cells.first.length

    @radius = radius

    @cells.each_with_index do |row, r|
      row.each_with_index do |cell, c|
        if cell == TRACK_START
          @start_pos = Vector[c,r]
        elsif cell == TRACK_END
          @finish_pos = Vector[c,r]
        end
      end
    end

    build_reachable_map
    find_base_path
    find_cheats
  end

  def cell_at(pos)
    @cells[pos[1]][pos[0]]
  end

  def within_bounds?(pos)
    pos[0] >= 0 && pos[0] < @width &&
      pos[1] >= 0 && pos[1] < @height
  end

  def is_a_track_point?(pos)
    within_bounds?(pos) &&
      cell_at(pos) != TRACK_WALL
  end

  def is_a_wall_point?(pos)
    within_bounds?(pos) &&
      cell_at(pos) == TRACK_WALL
  end

  def build_reachable_map
    @reachable_map = []
    (-@radius..@radius).each do |i|
      j_max = @radius - (i.abs)
      (-j_max..j_max).each do |j|
        @reachable_map.push(Vector[i,j]) unless i == 0 && j == 0
      end
    end
    @reachable_map
  end

  def find_base_path
    @base_path = []
    current_pos = @start_pos
    visited = Set.new
    while current_pos != @finish_pos
      visited.add(current_pos)
      @base_path.push(current_pos)
      ns = track_neighbors(current_pos)
      unvisited = ns.find {|p| !visited.member?(p)}
      current_pos = unvisited
    end
    @base_path.push(@finish_pos)
    @steps_from_start = Hash[@base_path.each_with_index.
      map {|pos,i| [pos,i]}]
    @steps_from_finish = Hash[@base_path.reverse.each_with_index.
      map {|pos,i| [pos,i]}]
    @base_path
  end

  def base_path
    @base_path ||= find_base_path
  end

  def distance(p,q)
    (p[0]-q[0]).abs + (p[1]-q[1]).abs
  end

  def in_order?(p,q)
    @steps_from_finish[q] < @steps_from_finish[p]
  end

  def build_circle
  end

  def find_cheats
    @cheats = {}
    base_path_steps = @base_path.length - 1
    @base_path.each do |entry_point|
      reachable = @reachable_map.
        map {|p| p+entry_point}.
        select {|p| is_a_track_point?(p) && in_order?(entry_point, p)}
      #puts marked_to_s(reachable)
      reachable.each do |exit_point|
        point_pair = [entry_point,exit_point]
        cheat_steps = @steps_from_start[entry_point] +
          @steps_from_finish[exit_point] +
          distance(entry_point, exit_point)
        saved_time = base_path_steps - cheat_steps
        @cheats[point_pair] = saved_time
      end
    end
    @cheats
  end

  def group_cheats_by_saved_time
    base_path_steps = @base_path.length - 1

    puts "Considering #{@cheats.length} cheats"
    cheat_counts_by_saved_time = Hash.new(0)
    @cheats.each do |point_pair, saved_time|
      entry_point, exit_point = point_pair
      if saved_time > 0
        cheat_counts_by_saved_time[saved_time] += 1
      end
    end

    cheat_counts_by_saved_time
  end

  def neighbors(pos)
    Direction::CARDINAL_DIRECTIONS.map {|d| pos+d}.
      select {|p| within_bounds?(p)}
  end

  def track_neighbors(pos)
    Direction::CARDINAL_DIRECTIONS.
      map {|d| pos + d}.select {|p| is_a_track_point?(p)}
  end

  def wall_neighbors(pos)
    Direction::CARDINAL_DIRECTIONS.
      map {|d| pos + d}.select {|p| is_a_wall_point?(p)}
  end

  def to_s
    @cells.map {|r| r.join('')}.join("\n") + "\n\n"
  end

  def marked_to_s(marked)
    cells = @cells.map {|r| r.dup}
    marked.each_with_index do |pos, index|
      cells[pos[1]][pos[0]] = "O"
    end

    cells.map {|r| r.join('')}.join("\n") + "\n\n"
  end

  def path_to_s(path)
    cells = @cells.map {|r| r.dup}
    path.each_with_index do |pos, index|
      cells[pos[1]][pos[0]] = (index % 10).to_s
    end

    cells.map {|r| r.join('')}.join("\n") + "\n\n"
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  radius = part == 1 ? 2 : 20
  print_path = args[2].to_i

  track = RaceTrack.new(IO.read(fname), radius)
  puts "Base Path Time: #{track.base_path.length - 1}"
  cheat_counts_by_saved_time = track.group_cheats_by_saved_time
  puts "Total Cheats: #{cheat_counts_by_saved_time.length}"
  if print_path == 1
    cheat_counts_by_saved_time.keys.sort.each do |saved_time|
      count = cheat_counts_by_saved_time[saved_time]
      next unless saved_time >= 50 && part == 2
      puts "There #{count == 1 ? "is" : "are"} #{count} cheat#{count == 1 ? '' : 's'} that save #{saved_time} picoseconds."
    end
  else
    cheats_saving_enough_time = cheat_counts_by_saved_time.keys.
      select {|saved_time| saved_time >= 100}.
      map {|saved_time| cheat_counts_by_saved_time[saved_time]}.sum
    puts cheats_saving_enough_time
  end
end

if __FILE__ == $0
  main(ARGV)
end
