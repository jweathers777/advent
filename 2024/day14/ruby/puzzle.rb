#!/usr/bin/env ruby

require 'matrix'
require '~/advent/common/ruby/direction.rb'

class Robot
  @@next_robot_id = 0

  attr_reader :id
  attr_accessor :pos, :vel

  def initialize(pos, vel)
    @id = @@next_robot_id
    @@next_robot_id += 1

    @pos = pos
    @vel = vel
  end

  def to_s
    "p=#{@pos[0]},#{@pos[1]} v=#{@vel[0]},#{@vel[1]}"
  end

  def Robot.from_s(s)
    s =~ /p=(-?\d+),(-?\d+) v=(-?\d+),(-?\d+)/
    Robot.new(Vector[$1.to_i,$2.to_i], Vector[$3.to_i,$4.to_i])
  end
end

class Room
  HIDDEN = ' '
  EMPTY = '.'

  attr_reader :robots, :width, :height
  attr_reader :iterations
  attr_accessor :show_quadrants

  def initialize(robots)
    @robots = robots
    @width = robots.map {|r| r.pos[0]}.max + 1
    @height = robots.map {|r| r.pos[1]}.max + 1

    @show_quadrants = false
    @grid = {}

    @robots.each do |robot|
      @grid[robot.pos] ||= Set.new
      @grid[robot.pos].add(robot.id)
    end
  end

  def area
    @width * @height
  end

  def occupied_area
    @grid.keys.length
  end

  def quadrant(i)
    w = @width/2
    h = @height/2

    case i
    when 1
      Vector[0,0,w-1,h-1]
    when 2
      Vector[@width-w,0,@width-1,h-1]
    when 3
      Vector[0,@height-h,w-1,@height-1]
    when 4
      Vector[@width-w,@height-h,@width-1,@height-1]
    end
  end

  def in_bounds?(pos)
    pos[0] >= 0 && pos[0] < @width &&
      pos[1] >= 0 && pos[1] < @height
  end

  def in_quadrant?(pos,i)
    q = quadrant(i)
    pos[0] >= q[0] && pos[0] <= q[2] &&
      pos[1] >= q[1] && pos[1] <= q[3]
  end

  def has_vertical_middle_filled?
    w = @width/2
    cols = w % 2 == 1 ? [w+1] : [w,w+1]

    !cols.any? do |c|
      @height.times.any? do |r|
        @grid[Vector[c,r]].nil?
      end
    end
  end

  def robots_in_quadrant(i)
    @robots.select {|r| in_quadrant?(r.pos,i)}
  end

  def neighbors(p)
    Direction::DIRECTIONS.map {|d| p+d}.
      select {|q| in_bounds?(q)}
  end

  def score
    total = 0
    rc = @robots.length
    center = Vector[@width/2, @height/2]
    @robots.each do |r|
      total += (r.pos - center).magnitude
    end
    total
  end

  def next
    @iterations ||= 0
    @iterations += 1

    @robots.each do |robot|
      new_pos = robot.pos + robot.vel
      new_pos[0] = new_pos[0] % @width
      new_pos[1] = new_pos[1] % @height

      old_cell = @grid[robot.pos]
      old_cell.delete(robot.id)
      if old_cell.empty?
        @grid.delete(robot.pos)
      end

      if !@grid[new_pos]
        @grid[new_pos] = Set.new
      end
      @grid[new_pos].add(robot.id)

      robot.pos = new_pos
    end

    self
  end

  def to_quadrant_s(i)
    q = quadrant(i)
    "Q#{i}\n"+
    q[0].upto(q[2]).map do |r|
      q[1].upto(q[3]).map do |c|
        pos = Vector[c,r]
        cell = @grid[pos]
        if cell
          cell.size.to_s
        else
          EMPTY
        end
      end.join('')
    end.join("\n")
  end

  def to_key
    @height.times.map do |r|
      @width.times.map do |c|
        pos = Vector[c,r]
        @grid[pos] ? '*' : EMPTY
      end.join('')
    end.join("")
  end

  def to_s
    @height.times.map do |r|
      @width.times.map do |c|
        pos = Vector[c,r]
        if show_quadrants && !(1..4).any? {|i| in_quadrant?(pos,i)}
          HIDDEN
        else
          cell = @grid[pos]
          if cell
            cell.size.to_s
          else
            EMPTY
          end
        end
      end.join('')
    end.join("\n")
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  robots = IO.readlines(fname).map {|l| Robot.from_s(l)}
  room = Room.new(robots)

  if part == 1
    100.times { room.next }

    safety_factor = (1..4).
      map {|i| room.robots_in_quadrant(i).length}.
      inject(1, &:*)

    puts safety_factor
  elsif part == 2
    puts "Start"
    puts room
    puts "\n"

    min_score = (room.robots.length**2) *
      Math.sqrt(room.width**2 + room.height**2)
    tree_room = nil
    tree_iterations = nil

    while room.iterations != room.area
      room.next
      s = room.score
      if s < min_score
        min_score = s
        tree_room = room.to_s
        tree_iterations = room.iterations
      end
    end
    puts "Final"
    puts room
    puts "Iteration: #{room.iterations}"
    puts "Optimal"
    puts tree_room
    puts "Optimal Iteration: #{tree_iterations}"
  end
end

if __FILE__ == $0
  main(ARGV)
end
