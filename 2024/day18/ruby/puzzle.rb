pos =pos =  #!/usr/bin/env ruby

require 'matrix'
require 'rainbow/refinement'
using Rainbow

$debug = false

def puts_d(s)
  puts(s) if $debug
end

DIRECTIONS = {
  UP: Vector[0,-1],
  DOWN: Vector[0,1],
  LEFT: Vector[1,0],
  RIGHT: Vector[-1,0]
}

class Grid
  EMPTY = '.'
  CORRUPT = '#'
  MARKED_POSITION= 'O'

  attr_reader :start, :finish, :width, :height

  def initialize(s, size=nil)
    if size
      @height = size
      @width = size
    else
      @height = 0
      @width = nil
    end

    @cells = {}

    s.split("\n").each_with_index do |l,r|
      @height += 1 unless size
      row = l.strip.split('')
      @width ||= row.length
      row.each_with_index do |cell,c|
        @cells[Vector[c,r]] = cell unless cell == EMPTY
      end
    end

    @start = Vector[0,0]
    @finish = Vector[@width-1, @height-1]
  end

  def [](c,r=nil)
    if c.class == Vector
      @cells[c]
    else
      @cells[Vector[c,r]]
    end
  end

  def []=(*args)
    if args.length == 2
      c,value = args
      @cells[c] = value
    else
      c,r,value = args
      @cells[Vector[c,r]] = value
    end
    self
  end

  def mark_corrupt(pos)
    @cells[pos] = CORRUPT
    self
  end

  def mark_empty(pos)
    @cells.delete(pos)
    self
  end

  def search_best_path(start=@start, finish=@finish)
    worst_score = @width*@height
    dist = Hash.new(worst_score)
    prev = {}

    states_to_expand = Set.new
    @width.times do |r|
      @height.times do |c|
        pos = Vector[c,r]
        cell = @cells[pos]
        states_to_expand.add(pos) unless cell == CORRUPT
      end
    end
    puts_d "States Space Size: #{states_to_expand.length}"

    dist[start] = 0
    last_state = nil
    while !states_to_expand.empty?
      puts_d "States to Expand: #{states_to_expand}"
      puts_d "Current Distances: #{dist}"
      state, state_dist = dist.select {|k,v| states_to_expand.member?(k)}.
        min_by {|k,v| v}
      if state.nil?
        states_to_expand.each do |s|
          state = s
          break
        end
        state_dist = dist[state]
      end
      puts_d "Processing #{state} with dist #{state_dist}"

      last_state = state
      break if state == finish

      states_to_expand.delete(state)

      DIRECTIONS.each do |name, d|
        nstate = state + d
        puts_d "Considering moving #{name} from #{state} to #{nstate}"
        next if @cells[nstate] == CORRUPT || !states_to_expand.member?(nstate)
        puts_d "Making move"
        cost = 1

        new_dist = state_dist + cost
        puts_d "Current best distance: #{dist[nstate]}"
        puts_d "Prev State for #{nstate}: #{prev[nstate]}"
        if new_dist < dist[nstate]
          puts_d "New distance is better than #{dist[nstate]}"
          dist[nstate] = new_dist
          prev[nstate] = state
          puts_d "Updated Prev State for #{nstate}: #{prev[nstate]}"
        end
      end
    end

    [last_state, dist, prev]
  end

  def find_a_best_path(start=@start, finish=@finish)
    last_state, dist, prev = search_best_path(start, finish)
    reconstruct_path(start, finish, dist, prev)
  end

  def reconstruct_path(start, finish, dist, prev)
    path = []
    state = finish
    if prev[state] || state == start
      while state
        path.push(state)
        state = prev[state]
      end
    end

    [dist[finish], path.reverse]
  end

  def to_s
    digits = @width.to_s.length
    "".tap do |s|
      digits.times do |d|
        s << " "*digits
        @width.times do |c|
          s << sprintf("%#{digits}d", c)[d]
        end
        s << "\n"
      end
      @height.times do |r|
        s << sprintf("%#{digits}d", r)
        @width.times do |c|
          pos = Vector[c,r]
          cell = @cells[pos]
          s << (cell ? cell : EMPTY)
        end
        s << "\n"
      end
    end
  end

  def mark_positions(positions)
    digits = @width.to_s.length
    "".tap do |s|
      digits.times do |d|
        s << " "*digits
        @width.times do |c|
          #s << Rainbow(sprintf("%#{digits}d", c)[d]).yellow
          s << sprintf("%#{digits}d", c)[d]
        end
        s << "\n"
      end
      @height.times do |r|
        #s << Rainbow(sprintf("%#{digits}d", r)).yellow
        s << sprintf("%#{digits}d", r)
        @width.times do |c|
          pos = Vector[c,r]
          if positions.member?(pos)
            #s << Rainbow(MARKED_POSITION).green
            s << MARKED_POSITION
          else
            cell = @cells[pos]
            s << (cell ? cell : EMPTY)
          end
        end
        s << "\n"
      end
    end
  end
end

DEFAULT_PARAMS = {
  'test.dat' => {size: 7, bytes: 12},
  'input.dat' => {size: 71, bytes: 1024}
}
DEFAULT_PARAMS.default = {}

def main(args)
  part = args[0].to_i
  fname = args[1]
  size = (args[2] || DEFAULT_PARAMS[fname][:size]).to_i
  bytes = (args[3] || DEFAULT_PARAMS[fname][:bytes]).to_i

  grid = Grid.new('', size)
  positions = IO.readlines(fname).
    map {|s| Vector[*s.strip.split(',').map(&:to_i)]}
  if part == 1
    bytes.times {|i| grid.mark_corrupt(positions[i])}

    _,path = grid.find_a_best_path
    puts "Shortest Path Length: #{path.length-1}"
    puts grid.mark_positions(path)
  else
    all_pos = Set.new
    grid.width.times do |r|
      grid.height.times do |c|
        pos = Vector[c,r]
        cell = grid[pos]
        all_pos.add(pos) unless cell == Grid::CORRUPT
      end
    end

    positions.each {|p| grid.mark_corrupt(p)}
    last_state, dist, prev = grid.search_best_path
    #dist.keys.each do |pos|
      #_, path = grid.reconstruct_path(grid.start, pos, dist, prev)
      #puts "Path from #{grid.start} to #{pos}"
      #puts "Shortest Path Length: #{path.length-1}"
      #puts grid.mark_positions(path)
    #end
    seeking = prev[grid.finish].nil?
    connected = Set.new(dist.keys)
    #not_connected = all_pos - connected
    while seeking && !positions.empty?
      pos = positions.pop
      puts "Adding #{pos} back"
      grid.mark_empty(pos)
      last_state, dist, prev = grid.search_best_path(pos)
      new_connected = Set.new(dist.keys)
      if connected.intersect?(new_connected)
        connected = connected.union(new_connected)
      end

      seeking = !(connected.member?(grid.start) && connected.member?(grid.finish))
    end
    if !seeking
      puts "First Breaking Pos: #{pos}"
      #_, path = grid.reconstruct_path(grid.start, pos, dist, prev)
      #puts "Path from #{grid.start} to #{pos}"
      #puts "Shortest Path Length: #{path.length-1}"
      #puts grid.mark_positions(path)
    end
  end
end

if __FILE__ == $0
  main(ARGV)
end
