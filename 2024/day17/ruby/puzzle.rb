#!/usr/bin/env ruby

$debug = false
def puts_d(s)
  puts s if $debug
end

class Machine
  attr_accessor :registers, :pointer, :program, :outputs

  OPS = [
    :adv,
    :bxl,
    :bst,
    :jnz,
    :bxc,
    :out,
    :bdv,
    :cdv
  ]
  OPCODES = Hash[OPS.each_with_index.to_a]

  def initialize(s)
    top, bottom = s.split("\n\n")

    @program = bottom.split(': ')[1].split(',').map(&:to_i)

    @original_registers = Hash[
      top.split("\n").map {|l| l.split(' ')[1..]}.
      map {|k,v| [k.chop, v.to_i]}
    ]
    reset
  end

  def reset
    @registers = @original_registers.clone
    @pointer = 0
    @outputs = []
  end

  def to_s
    "".tap do |s|
      @registers.keys.each do |name|
        s << "Register #{name}: #{@registers[name]}\n"
      end
      s << "\n"
      s << "Program: #{@program.join(',')}" << "\n"
      s << "         "
      s << "  "*@pointer << "^"
      if @outputs.length > 0
        s << "\nOutputs: "
        s << @outputs.map(&:to_s).join(',')
      end
      s << "\n\n"
    end
  end

  def execute_program
    while @pointer >= 0 && @pointer < @program.length
      execute_instruction(@program[@pointer], @program[@pointer+1])
    end
  end

  def execute_instruction(opcode, operand)
    puts_d self
    puts_d "Current pointer position #{@pointer}"
    current_pointer = @pointer
    op = OPS[opcode]
    puts_d "Executing #{op} (#{opcode}) on #{operand}"
    send(OPS[opcode], operand)
    @pointer += 2 if current_pointer == @pointer
    puts_d "New pointer position #{@pointer}"
  end

  #########################
  # Supported Instructions
  #########################

  def combo_operand(operand)
    case operand
    when 0..3
      operand
    when 4..6
      @registers["ABC"[operand-4]]
    when 7
      abort "Invalid program"
    end
  end

  def adv(operand)
    c_operand = combo_operand(operand)
    puts_d "Dividing A (#{@registers['A']}) by 2 raised to combo #{c_operand} to store in A"
    @registers['A'] /= 2**c_operand
  end

  def bxl(operand)
    puts_d "XORing B (#{@registers['B']}) with #{operand} to store in B"
    @registers['B'] ^= operand
  end

  def bst(operand)
    c_operand = combo_operand(operand)
    puts_d "Moding by 8 combo #{c_operand} and storing in B"
    @registers['B'] = c_operand % 8
  end

  def jnz(operand)
    puts_d "Jumping pointer to #{operand} if A contains 0"
    @pointer = operand unless@registers['A'] == 0
  end

  def bxc(_)
    puts_d "XORing B (#{@registers['B']}) with C (#{@registers['C']}) to store in B"
    @registers['B'] ^= @registers['C']
  end

  def out(operand)
    c_operand = combo_operand(operand)
    puts_d "Outputing combo #{c_operand} mod 8"
    @outputs.push(c_operand % 8)
  end

  def bdv(operand)
    c_operand = combo_operand(operand)
    puts_d "Dividing A (#{@registers['A']}) by 2 raised to combo #{c_operand} to store in B"
    @registers['B'] = @registers['A'] /  2**c_operand
  end

  def cdv(operand)
    c_operand = combo_operand(operand)
    puts_d "Dividing A (#{@registers['A']}) by 2 raised to combo #{c_operand} to store in C"
    @registers['C'] = @registers['A'] /  2**c_operand
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  machine = Machine.new(IO.read(fname))

  if part == 1
    machine.execute_program
    puts_d "\n"
    puts machine
  elsif part == 2
    # Only works for my particular program input sequence
    index_count = machine.program.length
    program = machine.program

    indices = []
    index = 0
    last_i = 1
    start_value = 1
    n = 7
    match_found = false
    while index < index_count
      puts "index = #{index}"
      puts "start_value = #{start_value}"
      if index > 0
        n = 7 * 8**(index)
      end

      value = start_value
      n.times do |i|
        machine.reset
        machine.registers['A'] = value
        machine.execute_program
        if machine.outputs == program[-(index+1)..]
          match_found = true
          indices[index] = i
          puts "i = #{i}"
          puts "value = #{value}"
          puts "indices = [#{indices.map(&:to_s).join(',')}]"
          puts "outputs = [#{machine.outputs.map(&:to_s).join(',')}]\n\n"
          break
        elsif machine.outputs.length - 1 > index
          abort "Our output size got too big without finding an answer."
        end
        value += 1
      end
      index += 1
      start_value = 8*(start_value + indices.last)
    end
    puts value
  else
    value = 1
    while true #&& value < 117441
      #puts "Trying value #{value} for A"
      print "#{value} -> "
      machine.registers['A'] = value
      #puts_d machine
      machine.execute_program
      puts machine.outputs.map(&:to_i).join(',')
      #puts machine
      break if machine.program == machine.outputs
      machine.reset
      value += 1
    end
    #puts machine.registers['A']
    puts value
  end
end

if __FILE__ == $0
  main(ARGV)
end
