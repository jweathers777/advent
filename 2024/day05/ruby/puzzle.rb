#!/usr/bin/env ruby

class OrderRule
  attr_reader :left, :right

  def initialize(left, right)
    @left = left
    @right = right
  end

  def to_s
    "#{@left}|#{@right}"
  end

  def OrderRule.from_s(s)
    OrderRule.new(*s.split('|').map(&:to_i))
  end
end

class OrderMap
  def initialize(order_rules)
    @map = {}
    order_rules.each do |rule|
      @map[[rule.left,rule.right]] = true
    end
  end

  def in_valid_order?(left, right)
    @map[[left,right]]
  end

  def compare(left, right)
    if left == right
      0
    elsif @map[[left,right]]
      1
    else
      -1
    end
  end
end

class PageUpdate < Array
  def middle_page
    middle_index = (self.length - 1) / 2
    self[middle_index]
  end

  def to_s
    self.map(&:to_s).join(',')
  end

  def PageUpdate.from_s(s)
    PageUpdate.new(s.split(',').map(&:to_i))
  end
end

class PrintProgram
  def initialize(s)
    rule_section, update_section = s.strip.split("\n\n")
    @order_rules = rule_section.split("\n").
      map {|s| OrderRule.from_s(s)}
    @order_map = OrderMap.new(@order_rules)
    @page_updates = update_section.split("\n").
      map {|s| PageUpdate.from_s(s)}
  end

  def valid_page_update?(page_update)
    is_valid = true
    i = 0
    while is_valid && i < page_update.length
      j = i + 1
      while is_valid && j < page_update.length
        left = page_update[i]
        right = page_update[j]
        is_valid &&= @order_map.in_valid_order?(left,right)
        j += 1
      end
      i += 1
    end
    is_valid
  end

  def fix_page_update_order!(page_update)
    page_update.sort! {|a,b| @order_map.compare(a,b)}
  end

  def valid_page_updates
    @page_updates.select {|u| self.valid_page_update?(u)}
  end

  def invalid_page_updates
    @page_updates.reject {|u| self.valid_page_update?(u)}
  end

  def fix_invalid_page_updates
    self.invalid_page_updates.each {|p| fix_page_update_order!(p)}
  end

  def to_s
    @order_rules.map {|r| r.to_s}.join("\n") + "\n\n" +
      @page_updates.map {|u| u.to_s}.join("\n")
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  program = PrintProgram.new(IO.read(fname))
  if part == 1
    puts program.valid_page_updates.map(&:middle_page).sum
  else
    puts program.fix_invalid_page_updates.map(&:middle_page).sum
  end
end

if __FILE__ == $0
  main(ARGV)
end
