#!/usr/bin/env ruby

class NetworkMap
  def initialize(s)
    @links = Hash.new

    s.split("\n").each do |link|
      c1,c2 = link.split('-')

      @links[c1] ||= Set.new
      @links[c1].add(c2)

      @links[c2] ||= Set.new
      @links[c2].add(c1)
    end
  end

  def n_sized_sets(n, criteria=nil)
    sets = []
    visited = Set.new

    cs = @links.keys
    cs.select! {|c| criteria.call(c)} if criteria

    nodes = cs.map {|c| [c,Set[c]]}

    while !nodes.empty?
      c, s = nodes.pop

      visited.add(s)

      if s.size == n
        sets.push(s)
        next
      end

      neighbors = @links[c] - s
      neighbors.each do |nb|
        next unless s.all? {|e| @links[e].member?(nb)}

        ns = s + [nb]
        next if visited.member?(ns)

        nodes.push([nb, ns])
      end
    end
    sets
  end

  def largest_connected_set
    best = Set.new
    visited = Set.new

    nodes = @links.keys.map {|c| [c,Set[c]]}

    while !nodes.empty?
      c, s = nodes.pop

      visited.add(s)

      if s.size > best.size
        best = s
        next
      end

      neighbors = @links[c] - s
      neighbors.each do |nb|
        next unless s.all? {|e| @links[e].member?(nb)}

        ns = s + [nb]
        next if visited.member?(ns)

        nodes.push([nb, ns])
      end
    end
    best
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  network = NetworkMap.new(IO.read(fname))

  if part == 1
    criteria = -> (c) { c.start_with?('t') }
    puts network.n_sized_sets(3, criteria).length
    #puts network.n_sized_sets(3, criteria).
      #map {|s| s.map(&:to_s).sort.join(',')}.
      #sort.join("\n")
  else
    largest = network.largest_connected_set
    password = largest.sort.join(',')
    puts password
  end
end

if __FILE__ == $0
  main(ARGV)
end
