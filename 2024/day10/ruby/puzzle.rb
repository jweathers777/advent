#!/usr/bin/env ruby

require "~/advent/common/ruby/point.rb"

class TrailHead
  attr_accessor :start, :paths

  def initialize(start, paths)
    @start = start
    @paths = paths
  end

  def value
    @paths.length
  end
end

class TopographicMap
  attr_reader :trail_heads

  VALUE_BY_SCORE = 1
  VALUE_BY_RATING = 2

  UP = Point.new([0,-1])
  DOWN = Point.new([0,1])
  LEFT = Point.new([-1,0])
  RIGHT = Point.new([1,0])

  DIRECTIONS = [UP, DOWN, LEFT, RIGHT]

  MAX_VALUE = 9

  def initialize(s, value_metric)
    @value_metric = value_metric
    @cells = s.strip.split("\n").map {|r| r.split('').map(&:to_i)}
    @bounds = Point.new([@cells.first.length, @cells.length])
    find_trail_heads
  end

  def value_by_score?
    @value_metric == VALUE_BY_SCORE
  end

  def neighbors(p)
    DIRECTIONS.map {|d| p+d}.
      select {|q| Point.in_bounds?(@bounds, q)}
  end

  def explore_cell(c,r)
    start = Point.new([c,r])
    paths = []
    finish_points = Set.new

    nodes = [[start]]

    while !nodes.empty?
      node = nodes.pop
      p = node.last
      target_value = @cells[p[1]][p[0]] + 1
      neighbors(p).each do |n|
        next if value_by_score? && finish_points.member?(n)
        value = @cells[n[1]][n[0]]
        if value == target_value
          next_node = node.clone
          next_node << n
          if target_value == MAX_VALUE
            finish_points.add(n)
            paths << next_node
          else
            nodes.push(next_node)
          end
        end
      end
    end

    paths.length > 0 ? TrailHead.new(start, paths) : nil
  end

  def find_trail_heads
    @trail_heads = []

    @cells.each_with_index do |row, r|
      row.each_with_index do |value,c|
        if value == 0
          trail_head = explore_cell(c,r)
          @trail_heads << trail_head if trail_head
        end
      end
    end

    @trail_heads
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  tmap = TopographicMap.new(IO.read(fname), part)
  puts tmap.trail_heads.map(&:value).sum
end

if __FILE__ == $0
  main(ARGV)
end
