#!/usr/bin/env ruby

class WordMapNode
  attr_reader :char, :parent, :children

  def initialize(char, parent=nil, children=[])
    @char = char
    @parent = parent
    @children = children
  end

  def word
    @word ||= (parent && parent.char != :root) ? parent.word + @char : @char
  end
end

class WordMap
  def initialize
    @root = WordMapNode.new(:root)
    @words = Set.new
  end

  def add_word(word)
    return word if @words.member?(word)
    @words.add(word)

    current_node = @root
    appending = false
    word.each_char do |ch|
      if appending
        node = WordMapNode.new(ch, current_node)
        current_node.children.push(node)
      else
        node = current_node.children.find {|n| n.char == ch}
        if node.nil?
          node = WordMapNode.new(ch, current_node)
          appending = true

          if current_node.children[0] && current_node.children[0].char == :leaf
            i = (current_node.children[1..].bsearch_index {|c| c.char >= ch} || 0)+1
          else
            i = current_node.children.bsearch_index {|c| c.char >= ch} || 0
          end
          current_node.children.insert(i, node)
        end
      end
      current_node = node
    end
    current_node.children.unshift(WordMapNode.new(:leaf, current_node))
    word
  end

  def find_prefixes(word)
    current_node = @root
    prefixes = []
    word.each_char do |ch|
      current_node = current_node.children.find {|n| n.char == ch}
      break if current_node.nil?
      if current_node.children[0] && current_node.children[0].char == :leaf
        prefixes.push(current_node.word)
      end
    end
    prefixes
  end
end

class WordTreeNode
  attr_accessor :prefix, :remainder, :parent, :children

  @@pattern_map = WordMap.new
  @@cached_expansions = {}
  @@dead_ends = {}

  def WordTreeNode.patterns=(patterns)
    @@pattern_map = WordMap.new
    patterns.each do |p|
      @@pattern_map.add_word(p)
    end
  end

  def WordTreeNode.from_word(word)
    WordTreeNode.new("", word)
  end

  def ==(other)
    other.class == WordTreeNode &&
      other.prefix == self.prefix &&
      other.remainder == self.remainder
  end

  alias eql? ==

  def hash
    word.hash
  end

  def initialize(prefix, remainder, parent=nil, children=Set.new)
    @prefix = prefix
    @remainder = remainder
    @parent = parent
    @children = children
    @parent.children.add(self) if @parent
  end

  def word
    "#{@prefix}#{@remainder}"
  end

  def broken_word
    "#{@prefix}|#{@remainder}"
  end

  def is_dead_end?
    @@dead_ends[self.remainder]
  end

  def full_path
    path = []
    current_node = self
    while current_node.parent
      path.push(current_node.prefix)
      current_node = current_node.parent
    end
    path.reverse
  end

  def back_propogate_failure
    #puts "Backpropating failure for #{broken_word}"
    current_node = self
    while true
      #puts "Marking #{current_node.remainder} as dead"
      @@dead_ends[current_node.remainder] = true
      current_node = current_node.parent
      unless current_node &&
          current_node.children.all? {|n| @@dead_ends[n.remainder]}
        #puts "Finished back propogations with #{current_node}"
        break
      end
    end
  end

  def expand
    @@cached_expansions[@remainder] ||=
      begin
        prefixes = @@pattern_map.find_prefixes(@remainder)
        if prefixes.length > 0
          remainders = prefixes.map {|p| @remainder[p.length..]}
          if remainders.all? {|r| @@dead_ends[r]}
            #puts "Marking #{@remainder} as dead"
            @@dead_ends[@remainder] = true
            back_propogate_failure
            []
          else
            prefixes.zip(remainders).to_a
          end
        else
          back_propogate_failure
          []
        end
      end
    if @@cached_expansions[@remainder].all? {|p,r| @@dead_ends[r]}
      #puts "Marking #{@remainder} as dead"
      @@dead_ends[@remainder] = true
      back_propogate_failure
      []
    else
      not_dead = @@cached_expansions[@remainder].find {|p,r| !@@dead_ends[r]}
      #puts "Not dead yet: #{not_dead}"
    end
    @@cached_expansions[@remainder].
      map {|p,r| WordTreeNode.new(p, r, self)}
  end

  def to_s
    "(#{prefix}|#{remainder}): parent -> (#{parent ? parent.broken_word : ''}), children -> [#{@children.map(&:broken_word).join(',')}]"
  end
end

def parse_patterns_and_designs(s)
  top, bottom = s.split("\n\n")

  patterns = top.split(", ")
  designs = bottom.split("\n")

  [patterns, designs]
end

def first_leaf(word)
  root = WordTreeNode.from_word(word)
  n = word.length
  stack = [root]
  while !stack.empty?
    node = stack.pop
    if node.is_dead_end?
      #puts sprintf("X %#{n-2}s", node.broken_word)
    else
      children = node.expand
      if children.length > 0
        #puts sprintf("%#{n}s", node.broken_word)
        children.each do |child|
          if child.is_dead_end?
            #puts sprintf("X %#{n-2}s", child.broken_word)
            next
          end

          if child.remainder == ""
            return child
          else
            stack.push(child)
          end
        end
      else
        node.back_propogate_failure
        #puts sprintf("[] %#{n-3}s", node.broken_word)
      end
    end
  end
  nil
end

def count_leaves(word)
  root = WordTreeNode.from_word(word)
  remainder_leaf_counts = {"" => 1}
  current_depth = 0
  current_node = root

  stack = [[current_node, current_depth]]
  while !stack.empty?
    #puts "\nstack:\n #{stack.map {|n,d| "#{d}: #{n.to_s}"}.join("\n ")}"
    node, depth = stack.pop
    #puts "node <- #{depth} - #{node}"

    if remainder_leaf_counts[node.remainder].nil?
      children = node.expand
      #puts "Expanded #{node}"
      if children.length > 0
        children.each {|child| stack.push([child,depth+1])}
      else
        #puts "Zeroing counts for #{node.broken_word}"
        remainder_leaf_counts[node.remainder] = 0
        #puts remainder_leaf_counts
      end
    end

    steps = current_depth - depth
    if stack.empty?
      steps = current_depth+1
    elsif steps < 0
      steps = 0
    else
      current_node = current_node.parent
    end

    steps.times do
      #puts "Computing counts for #{current_node.broken_word}"
      remainder_leaf_counts[current_node.remainder] ||=
        current_node.children.map {|c| remainder_leaf_counts[c.remainder]}.sum
      #puts remainder_leaf_counts
      current_node = current_node.parent
    end

    current_node = node
    current_depth = depth

  end
  remainder_leaf_counts[word]
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  patterns, designs = parse_patterns_and_designs(IO.read(fname))
  WordTreeNode.patterns = patterns

  if part == 1
    possible_designs = 0
    designs.each do |design|
      leaf = first_leaf(design)
      if leaf
        possible_designs += 1
        puts "Possible to construct: #{design}"
        puts "  #{leaf.full_path.join('|')}"
      else
        puts "Impossible to construct: #{design}"
        puts ""
      end
    end
    puts "Possible designs: #{possible_designs}"
  else
    total = 0
    designs.each do |design|
      subtotal = count_leaves(design)
      total += subtotal
      puts "#{subtotal} constructions for #{design}"
    end
    puts "All constructions for designs: #{total}"
  end
end

if __FILE__ == $0
  main(ARGV)
end
