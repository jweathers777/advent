use std::env;
use std::fs;

fn cyclic_dot_product(elements: &Vec<i32>, pattern: &Vec<i32>) -> i32 {
    pattern.iter().cycle().skip(1).zip(elements.iter()).
        map(|(p,e)| p*e).sum()
}

fn get_pattern(index: usize) -> Vec<i32> {
    let mut pattern = Vec::new();
    let times = index + 1;

    for p in &[0, 1, 0, -1] {
        for _ in 0..times {
           pattern.push(*p);
        }
    }
    pattern
}

fn next_signal_element(signal: &Vec<i32>, index: usize) -> i32 {
    cyclic_dot_product(signal, &get_pattern(index)).abs() % 10 as i32
}

fn next_signal(signal: &Vec<i32>) -> Vec<i32> {
    let mut result = Vec::new();
    for (index, _value) in signal.iter().enumerate().rev() {
        result.push(next_signal_element(signal, index));
    }
    result.reverse();
    result
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let mut signal: Vec<i32> = fs::read_to_string(&args[1]).
        expect("File not found!").
        chars().
        map(|c| c.to_digit(10).expect(&format!("Invalid digit {}!", c)) as i32).
        collect();
    let part: i32 = args[2].parse().expect(&format!("Invalid part {}!", args[2]));

    if part == 1 {
        for _ in 0..100 {
            signal = next_signal(&signal);
        }
    } else if part == 2 {
        let size = signal.len() * 10000;
        let offset = signal.iter().take(7).
            fold(0, |acc,e| 10*acc + e) as usize;
        signal = signal.iter().cycle().take(size).
            skip(offset).cloned().collect();

        let end = signal.len() - 1;

        for _ in 0..100 {
            for i in (0..end).rev() {
                signal[i] = (signal[i] + signal[i+1]) % 10;
            }
        }
    }

    for i in 0..8 {
        print!("{}", signal[i]);
    }
    println!("");
}
