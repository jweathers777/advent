use std::env;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;

fn fuel_for_mass(mass: u32) -> u32 {
    let divided = mass / 3;

    if divided > 2 { divided - 2 } else { 0 }
}

fn total_fuel_for_module(mass: u32) -> u32 {
    let mut m = mass;
    let mut total = 0;

    loop {
        m = fuel_for_mass(m);
        if m > 0 {
            total += m;
        } else {
            break;
        }
    }
    total
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let f = File::open(&args[1]).expect("File not found!");
    let reader = BufReader::new(&f);

    let part: u32 = args[2].parse().expect("Invalid part!");
    let modules = reader.
        lines().
        map(|l| l.unwrap().trim().parse::<u32>().unwrap());

    if part == 1 {
        let fuel: u32 = modules.map(fuel_for_mass).sum();
        println!("Total Fuel: {}", fuel);
    } else {
        let fuel: u32 = modules.map(total_fuel_for_module).sum();
        println!("Total Fuel: {}", fuel);
    }
}
