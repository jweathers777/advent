extern crate intcode;

use std::env;
use std::fs;

use std::collections::HashMap;
use std::collections::HashSet;

enum Direction {
    UP, LEFT, RIGHT, DOWN
}

use Direction::*;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord {
    x: i32,
    y: i32,
}

impl Coord {
    fn new(x: i32, y: i32) -> Coord {
        Coord{x: x, y: y}
    }
}

struct Robot {
    position: Coord,
    direction: Direction,
    grid: HashMap<Coord, i64>,
    min_x: i32,
    max_x: i32,
    min_y: i32,
    max_y: i32,
}

impl Robot {
    fn new() -> Robot {
        Robot{
            position: Coord::new(0,0),
            direction: UP,
            grid: HashMap::new(),
            min_x: 0,
            max_x: 0,
            min_y: 0,
            max_y: 0,
        }
    }

    fn detect_color(&self) -> i64 {
        if self.grid.contains_key(&self.position) {
            *self.grid.get(&self.position).unwrap()
        } else {
            0
        }
    }

    fn paint(&mut self, color_to_paint: i64) {
        self.grid.insert(self.position, color_to_paint);
    }

    fn turn(&mut self, turn_direction: i64) {
        if turn_direction == 0 {
            match self.direction {
                UP => self.direction = LEFT,
                LEFT => self.direction = DOWN,
                RIGHT => self.direction = UP,
                DOWN => self.direction = RIGHT,
            }
        } else if turn_direction == 1 {
            match self.direction {
                UP => self.direction = RIGHT,
                LEFT => self.direction = UP,
                RIGHT => self.direction = DOWN,
                DOWN => self.direction = LEFT,
            }
        }
    }

    fn step(&mut self) {
        match self.direction {
            UP => {
                self.position.y += 1;
                if self.position.y > self.max_y {
                    self.max_y = self.position.y;
                }
            },
            LEFT => {
                self.position.x -= 1;
                if self.position.x < self.min_x {
                    self.min_x = self.position.x;
                }
            },
            RIGHT => {
                self.position.x += 1;
                if self.position.x > self.max_x {
                    self.max_x = self.position.x;
                }
            },
            DOWN => {
                self.position.y -= 1;
                if self.position.y < self.min_y {
                    self.min_y = self.position.y;
                }
            },
        }
    }

    fn print_grid(&self) {
        for y in (self.min_y..=self.max_y).rev() {
            for x in self.min_x..=self.max_x {
                let pos = Coord::new(x, y);
                let color = if self.grid.contains_key(&pos) {
                    *self.grid.get(&pos).unwrap()
                } else {
                    0
                };
                if color == 0 {
                    print!(" ");
                } else {
                    print!("#");
                }
            }
            println!("");
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut painted: HashSet<Coord> = HashSet::new();
    let mut robot = Robot::new();
    let mut machine = intcode::Machine::new();
    machine.load_program(&program);

    if part == 2 {
        robot.paint(1);
    }

    loop {
        let current_color = robot.detect_color();
        machine.send_input(current_color);

        let halted = machine.execute_memory();
        if halted { break }

        let color_to_paint = machine.fetch_output().expect("Failed to receive color!");
        let turn_direction = machine.fetch_output().expect("Failed to receive turn instruction!");

        painted.insert(robot.position);

        robot.paint(color_to_paint);
        robot.turn(turn_direction);
        robot.step();
    }
    robot.print_grid();
    println!("{}", painted.len());
}
