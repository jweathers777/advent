use std::fmt;
use std::collections::HashMap;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl Coord {
    pub fn new(x: i32, y: i32) -> Coord {
        Coord{x: x, y: y}
    }
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

pub struct Grid<T> {
    pub cells: HashMap<Coord,T>,
    min_bounds: Coord,
    max_bounds: Coord,
    empty_cell: char,
}

impl<T> Grid<T> where T: Copy {
    pub fn new(empty_cell: char) -> Grid<T> {
        let cells = HashMap::new();
        let min_bounds = Coord::new(0, 0);
        let max_bounds = Coord::new(0, 0);

        Grid{cells, max_bounds, min_bounds, empty_cell}
    }

    pub fn has_cell_value(&self, coord: Coord) -> bool {
        self.cells.contains_key(&coord)
    }

    pub fn get_cell_value(&self, coord: Coord) -> T {
        if self.cells.contains_key(&coord) {
            *self.cells.get(&coord).unwrap()
        } else {
            panic!("No value for this cell!")
        }
    }

    pub fn set_cell_value(&mut self, coord: Coord, value: T) {
        self.cells.insert(coord, value);
        self.min_bounds.x = std::cmp::min(self.min_bounds.x, coord.x);
        self.max_bounds.x = std::cmp::max(self.max_bounds.x, coord.x);
        self.min_bounds.y = std::cmp::min(self.min_bounds.y, coord.y);
        self.max_bounds.y = std::cmp::max(self.max_bounds.y, coord.y);
    }

    pub fn cell_in_bounds(&self, coord: Coord) -> bool {
        self.min_bounds.x <= coord.x && coord.x <= self.max_bounds.x &&
        self.min_bounds.y <= coord.y && coord.y <= self.max_bounds.y
    }

    pub fn get_neighbors(&self, coord: Coord) -> Vec<Coord> {
        let mut neighbors = Vec::new();
        for (dx, dy) in &[(-1,0), (1,0), (0,-1),(0,1)] {
            let x = (coord.x as i32 + dx) as i32;
            let y = (coord.y as i32 + dy) as i32;
            let coord = Coord::new(x, y);
            if self.cell_in_bounds(coord) {
                neighbors.push(coord);
            }
        }
        neighbors
    }
}

impl<T> fmt::Display for Grid<T> where T: std::fmt::Display {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in (self.min_bounds.y)..=(self.max_bounds.y) {
            for x in (self.min_bounds.x)..=(self.max_bounds.x) {
                let coord = Coord::new(x, y);
                if self.cells.contains_key(&coord) {
                    write!(f, "{}", self.cells.get(&coord).unwrap());
                } else {
                    write!(f, "{}", self.empty_cell);
                }
            }
            writeln!(f, "");
        }
        write!(f, "")
    }
}

