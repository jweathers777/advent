use std::env;
use std::fmt;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::num::ParseIntError;
use std::str::FromStr;

use std::collections::HashMap;

struct Reaction {
    inputs: Vec<(String,u32)>,
    outputs: Vec<(String,u32)>,
}

impl fmt::Display for Reaction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut first = true;
        for (chemical, amount) in self.inputs.iter() {
            if first {
                write!(f, "{} {}", amount, chemical);
                first = false;
            } else {
                write!(f, ", {} {}", amount, chemical);
            }
        }
        write!(f, " => ");
        first = true;
        for (chemical, amount) in self.outputs.iter() {
            if first {
                write!(f, "{} {}", amount, chemical);
                first = false;
            } else {
                write!(f, ", {} {}", amount, chemical);
            }
        }
        write!(f, "")
    }
}

impl FromStr for Reaction {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let halves: Vec<Vec<&str>> = s.split("=>").
            map(|r| r.split(',').map(|t| t.trim()).collect()).
            collect();

        let mut inputs: Vec<(String,u32)> = Vec::new();
        for pair_str in halves[0].iter() {
            let tokens: Vec<&str> = pair_str.split_whitespace().collect();
            let amount: u32 = tokens[0].parse().expect("Invalid amount");
            let chemical: String = tokens[1].to_string();
            inputs.push((chemical,amount));
        }

        let mut outputs: Vec<(String,u32)> = Vec::new();
        for pair_str in halves[1].iter() {
            let tokens: Vec<&str> = pair_str.split_whitespace().collect();
            let amount: u32 = tokens[0].parse().expect("Invalid amount");
            let chemical: String = tokens[1].to_string();
            outputs.push((chemical, amount));
        }

        Ok(Reaction{inputs: inputs, outputs: outputs})
    }
}

fn print_chemicals(chemicals: &HashMap<String, u32>) {
    let mut first = true;
    for (chemical, amount) in chemicals.iter() {
        if first {
            first = false;
        } else {
            print!(", ");
        }
        print!("{} {}", amount, chemical);
    }
    println!("");
}

fn produce_fuel(extra_chemicals: &mut HashMap<String,u32>, chemical_sources: &HashMap<String,&Reaction>) -> u32 {
    let mut required_ore = 0;
    let mut chemicals_to_reduce: HashMap<String,u32> = HashMap::new();
    chemicals_to_reduce.insert(String::from("FUEL"), 1);

    while !chemicals_to_reduce.is_empty() {
        let mut reduced_chemicals: HashMap<String,u32> = HashMap::new();
        for (chemical, &required_amount) in chemicals_to_reduce.iter() {
            let reaction = chemical_sources.get(chemical).unwrap();
            let (_, output_amount) = reaction.outputs[0];

            let available_amount = extra_chemicals.
                entry(chemical.to_string()).
                or_insert(0);

            let remaining_required_amount;
            if required_amount >= *available_amount {
                remaining_required_amount = required_amount - *available_amount;
                *available_amount = 0;
            } else {
                remaining_required_amount = 0;
                *available_amount -= required_amount;
            }

            if remaining_required_amount > 0 {
                let multiplier = (
                    (remaining_required_amount as f64) / (output_amount as f64)
                ).ceil() as u32;
                let produced_amount = multiplier * output_amount;
                let extra_amount = produced_amount - remaining_required_amount;

                if extra_amount > 0 {
                    *available_amount += extra_amount;
                }

                for (src_chemical, src_amount) in reaction.inputs.iter() {
                    let required_src_amount = multiplier * *src_amount;

                    if src_chemical == "ORE" {
                        required_ore += required_src_amount;
                    } else {
                        let produced_amount = reduced_chemicals.
                            entry(src_chemical.to_string()).
                            or_insert(0);
                        *produced_amount += required_src_amount;
                    }
                }
            }
        }
        chemicals_to_reduce = reduced_chemicals;
    }

    let empties: Vec<String> = extra_chemicals.iter().
        filter(|(_,&v)| v == 0).
        map(|(k,_)| k.to_string()).
        collect();

    for empty in empties {
        extra_chemicals.remove(&empty);
    }

    required_ore
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let f = File::open(&args[1]).expect("File not found!");
    let reader = BufReader::new(&f);

    let part: u32 = args[2].parse().expect("Invalid part!");
    let reactions: Vec<Reaction> = reader.
        lines().
        map(|l| l.unwrap().parse::<Reaction>().expect("Invalid reaction!")).
        collect();

    let mut chemical_sources: HashMap<String,&Reaction> = HashMap::new();
    for reaction in reactions.iter() {
        for (chemical, _) in reaction.outputs.iter() {
            chemical_sources.insert(chemical.to_string(), &reaction);
        }
    }

    let mut required_ore;
    let mut extra_chemicals: HashMap<String,u32> = HashMap::new();

    if part == 1 {
        required_ore = produce_fuel(&mut extra_chemicals, &chemical_sources);

        println!("Minimum ORE required: {}", required_ore);
        println!("Extra Chemicals:");
        print_chemicals(&extra_chemicals);
    } else {
        let total_ore_units = 1e12;
        let mut consumed_ore_per_cycle = 0u64;
        let mut fuel_unit = 1;
        let mut consumed_all = false;
        loop {
            required_ore = produce_fuel(&mut extra_chemicals, &chemical_sources);
            consumed_ore_per_cycle += required_ore as u64;

            //println!("Minimum ORE required to produce FUEL unit {}: {}", fuel_unit, required_ore);
            //println!("Extra Chemicals:");
            //print_chemicals(&extra_chemicals);

            if consumed_ore_per_cycle >= 1000000000000 {
                consumed_all = true;
                fuel_unit -=1 ;
                break;
            } else if extra_chemicals.len() > 0 {
                fuel_unit += 1;
                //println!("Fuel Units = {}", fuel_unit);
                //println!("ORE Consumed = {}", consumed_ore_per_cycle);
            } else {
                break;
            }
        }

        if consumed_all == true {
            println!("Maximum amount of fuel produced: {}", fuel_unit);
        } else {
            let fuel_produced_by_cycle = fuel_unit;
            let max_cycles = (total_ore_units / (consumed_ore_per_cycle as f64)).floor() as u32;
            let fuel_produced_by_max_cycles = max_cycles * fuel_produced_by_cycle;

            let mut remaining_ore_to_consume = 1;
            for _ in 0..12 {
                remaining_ore_to_consume = (remaining_ore_to_consume * 10) % consumed_ore_per_cycle;
            }

            let mut consumed_ore = 0;
            let mut extra_fuel_units = 0;
            loop {
                consumed_ore += produce_fuel(&mut extra_chemicals, &chemical_sources);
                if consumed_ore as u64 <= remaining_ore_to_consume {
                    extra_fuel_units += 1;
                } else {
                    break;
                }
            }

            let max_produced_fuel = fuel_produced_by_max_cycles + extra_fuel_units;

            println!("ORE consumed per cycle: {}", consumed_ore_per_cycle);
            println!("Cycle Length: {}", fuel_unit);
            println!("Max Cycles: {}", max_cycles);
            println!("Remaining Ore to Consume: {}", remaining_ore_to_consume);
            println!("Maximum amount of fuel produced: {}", max_produced_fuel);
        }
    }
}
