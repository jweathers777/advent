use std::env;

fn digits(val: u32) -> Vec<u32> {
    let mut ds = Vec::new();
    let mut v = val;

    while v > 0 {
        ds.push(v % 10);
        v = v / 10;
    }

    ds.reverse();
    ds
}

fn like_password(val: u32, at_least_one_double: bool) -> bool {
    let ds = digits(val);
    let mut adjacent_pair = false;
    let mut seeking_adjacent_pair = true;
    let mut last_adjacent_pair_match = false;

    for i in 1..(ds.len()) {
        if ds[i] < ds[i-1] {
            return false;
        } else if ds[i] == ds[i-1] {
            if seeking_adjacent_pair {
                if at_least_one_double && last_adjacent_pair_match {
                    adjacent_pair = false;
                } else {
                    adjacent_pair = true;
                }
            }
            last_adjacent_pair_match = true;
        } else {
            last_adjacent_pair_match = false;
            if adjacent_pair {
                seeking_adjacent_pair = false;
            }
        }
    }
    adjacent_pair
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let bounds: Vec<u32> = args[1].
        split('-').
        map(|s| s.parse::<u32>().expect("Invalid range!")).
        collect();

    let part: u32 = args[2].parse().expect("Invalid part!");

    if part == 1 {
        let matching = (bounds[0]..(bounds[1]+1)).
            filter(|&x| like_password(x, false)).
            count();
        println!("{}", matching);
    } else {
        let matching = (bounds[0]..(bounds[1]+1)).
            filter(|&x| like_password(x, true)).
            count();
        println!("{}", matching);
    }
}
