use std::env;
use std::io::{BufReader, BufRead};
use std::fs::File;

use std::collections::HashMap;

struct TreeNode {
    label: String,
    parent: Option<usize>,
    children: Vec<usize>,
}

impl TreeNode {
    fn new(label: &str) -> TreeNode {
        TreeNode{label: String::from(label), parent: None, children: Vec::new()}
    }
}

struct Tree {
    nodes: Vec<TreeNode>,
    nodes_by_label: HashMap<String, usize>,
}

impl Tree {
    fn new() -> Tree {
        Tree{nodes: Vec::new(), nodes_by_label: HashMap::new()}
    }

    fn count_ancestor_relationships(&self) -> u32 {
        let root_index = self.get_root_index();
        let mut total = 0;
        let mut stack: Vec<(usize, u32)> = vec![(root_index,0)];
        while !stack.is_empty() {
            let (index, mut ancestor_count) = stack.pop().unwrap();
            total += ancestor_count;
            ancestor_count += 1;

            let node = &self.nodes[index];
            for child_index in &node.children {
                stack.push((*child_index, ancestor_count));
            }
        }

        total
    }

    fn get_root_index(&self) -> usize {
        match self.nodes_by_label.get("COM") {
            Some(&index) => index,
            None => panic!("No root index found!"),
        }
    }

    fn get_node_index(&self, label: &str) -> Option<&usize> {
        self.nodes_by_label.get(label)
    }

    fn get_node(&self, label: &str) -> Option<&TreeNode> {
        match self.nodes_by_label.get(label) {
            Some(&index) => Some(&self.nodes[index]),
            None => None,
        }
    }

    fn get_node_mut(&mut self, label: &str) -> Option<&mut TreeNode> {
        match self.nodes_by_label.get(label) {
            Some(&index) => Some(&mut self.nodes[index]),
            None => None,
        }
    }

    fn get_path(&self, label: &str) -> Vec<usize> {
        let mut path: Vec<usize> = Vec::new();
        match self.get_node_index(label) {
            None => path,
            Some(&start_index) => {
                let mut current_index = start_index;
                loop {
                    path.push(current_index);
                    let current_node = &self.nodes[current_index];
                    match current_node.parent {
                        None => break,
                        Some(parent_index) => {
                            current_index = parent_index;
                        }
                    }
                }
                path.reverse();
                path
            }
        }
    }

    fn insert_node(&mut self, label: &str) -> usize {
        let index;
        if self.nodes_by_label.contains_key(label) {
            index = *self.nodes_by_label.get(label).unwrap()
        } else {
            index = self.nodes.len();
            self.nodes_by_label.insert(String::from(label), index);
            self.nodes.push(TreeNode::new(label));
        }
        index
    }

    fn insert_edge(&mut self, parent: &str, child: &str) {
        let parent_index = self.insert_node(parent);
        let child_index = self.insert_node(child);

        self.nodes[parent_index].children.push(child_index);
        self.nodes[child_index].parent = Some(parent_index);
    }
}


fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let f = File::open(&args[1]).expect("File not found!");
    let reader = BufReader::new(&f);

    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut tree = Tree::new();

    for result in reader.lines() {
        match result {
            Ok(line) => {
                let labels: Vec<&str> = line.trim().split(')').collect();
                if labels.len() != 2 {
                    panic!("Invalid edge string: {}", line);
                }
                tree.insert_edge(labels[0], labels[1]);
            },
            Err(e) => panic!("Error reading file: {}", e),
        }
    }

    if part == 1 {
        println!("{}", tree.count_ancestor_relationships());
    }
    else {
        let path_to_you = tree.get_path("YOU");
        let path_to_santa = tree.get_path("SAN");

        let steps_to_you = path_to_you.len();
        let steps_to_santa = path_to_santa.len();
        let common_steps = path_to_you.iter().
            zip(path_to_santa.iter()).
            take_while(|(x,y)| x == y).
            count();

        println!("steps to you: {}", steps_to_you);
        println!("steps to santa: {}", steps_to_santa);
        println!("common steps: {}", common_steps);

        let steps_to_santa = steps_to_you + steps_to_santa - (2 * common_steps);
        let transfers = steps_to_santa - 2;

        println!("{}", transfers);
    }
}
