extern crate itertools;
extern crate arith;

use std::env;
use std::fmt;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::num::ParseIntError;
use std::str::FromStr;

use itertools::Itertools;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord {
    x: i32,
    y: i32,
    z: i32,
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<x={}, y={}, z={}>", self.x, self.y, self.z)
    }
}

impl FromStr for Coord {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let coords: Vec<&str> = s.trim_matches(|c| c == '<' || c == '>').
            split(',').
            map(|t| t.trim().split('=').last().unwrap()).
            collect();

        let x = coords[0].parse::<i32>()?;
        let y = coords[1].parse::<i32>()?;
        let z = coords[2].parse::<i32>()?;

        Ok(Coord{x: x, y: y, z: z})
    }
}

impl Coord {
    fn new(x: i32, y: i32, z: i32) -> Coord {
        Coord{x: x, y: y, z: z}
    }
}

#[derive(PartialEq, Eq, Hash, Clone)]
struct Matrix {
    rows: Vec<Vec<i32>>,
    col_count: usize,
    row_count: usize,
}

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in &self.rows {
            for col in row {
                write!(f, "{:5} ", col);
            }
            writeln!(f, "");
        }
        writeln!(f, "")
    }
}

impl Matrix {
    fn new(rows: &Vec<Vec<i32>>) -> Matrix {
        Matrix{rows: rows.to_vec(), row_count: rows.len(), col_count: rows[0].len()}
    }
}

#[derive(PartialEq, Eq, Hash, Clone)]
struct LunarSystem {
    matrix: Matrix,
}

impl LunarSystem {
    fn new(positions: &Vec<Coord>) -> LunarSystem {
        let mut rows: Vec<Vec<i32>> = Vec::new();
        for &position in positions {
            let row = vec![position.x, position.y, position.z, 0, 0, 0];
            rows.push(row);
        }

        LunarSystem{matrix: Matrix::new(&rows)}
    }

    fn dim_slice(&self, dim: usize) -> Matrix {
        let mut rows: Vec<Vec<i32>> = Vec::new();
        for r_i in 0..(self.matrix.row_count) {
            let mut row: Vec<i32> = Vec::new();
            row.push(self.matrix.rows[r_i][dim]);
            row.push(self.matrix.rows[r_i][dim+3]);
            rows.push(row);
        }

        Matrix::new(&rows)
    }


    fn forward(&mut self) {
        for indices in (0..(self.matrix.row_count)).combinations(2) {
            let r_i = indices[0];
            let r_j = indices[1];

            for c_i in 0..3 {
                let shift = (self.matrix.rows[r_j][c_i] - self.matrix.rows[r_i][c_i]).signum();
                self.matrix.rows[r_i][c_i+3] += shift;
                self.matrix.rows[r_j][c_i+3] -= shift;
            }
        }

        for r_i in 0..(self.matrix.row_count) {
            for c_i in 0..3 {
                self.matrix.rows[r_i][c_i] += self.matrix.rows[r_i][c_i+3];
            }
        }
    }

    fn total_energy(&self) -> u64 {
        let mut total = 0;
        for r_i in 0..(self.matrix.row_count) {
            let mut potential = 0;
            let mut kinetic = 0;
            for c_i in 0..3 {
                potential += self.matrix.rows[r_i][c_i].abs() as u64;
                kinetic += self.matrix.rows[r_i][c_i+3].abs() as u64;
            }
            total += potential * kinetic;
        }
        total
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let f = File::open(&args[1]).expect("File not found!");
    let reader = BufReader::new(&f);

    let part: u64 = args[2].parse().expect("Invalid part!");
    let moon_positions: Vec<Coord> = reader.
        lines().
        map(|l| l.unwrap().parse::<Coord>().expect("Invalid position!")).
        collect();

    let mut lunar_system = LunarSystem::new(&moon_positions);

    if part == 1 {
        for step in 1..=1000 {
            lunar_system.forward();
        }

        println!("total sys: {}", lunar_system.total_energy());
    } else {
        let mut periods: Vec<u64> = Vec::new();
        for dim in 0..3 {
            lunar_system = LunarSystem::new(&moon_positions);
            let initial_state = lunar_system.dim_slice(dim);
            let mut steps = 0;
            loop {
                lunar_system.forward();
                steps += 1;
                let current_state = lunar_system.dim_slice(dim);
                if current_state == initial_state {
                    periods.push(steps);
                    println!("Took {} steps to repeat dimension {}", steps, dim);
                    break;
                }
            }
        }
        println!("Total system period: {}", arith::lcm(&periods));
    }
}
