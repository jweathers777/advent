extern crate intcode;
extern crate itertools;

use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: i64 = args[2].parse().expect("Invalid part!");

    let mut machine = intcode::Machine::new();
    machine.send_input(part);
    machine.load_program(&program);
    machine.execute_memory();
    machine.print_outputs();
}
