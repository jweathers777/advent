use std::env;
use std::fs;
use std::fmt;

use std::collections::HashMap;

fn gcd(a: isize, b: isize) -> isize {
    let mut a_i = std::cmp::max(a.abs(), b.abs());
    let mut b_i = std::cmp::min(a.abs(), b.abs());
    let mut r = a_i;

    if a == 0 || b == 0 {
        return 0;
    }

    loop {
        r = a_i % b_i;
        if r > 0 {
            a_i = b_i;
            b_i = r;
        } else {
            break;
        }
    }
    b_i
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord {
    x: isize,
    y: isize,
}

impl Coord {
    fn new(x: isize, y: isize) -> Coord {
        Coord{x: x, y: y}
    }

    fn diff(&self, c: &Coord) -> Coord {
        Coord::new(self.x - c.x, self.y - c.y)
    }

    fn angle(&self) -> f64 {
        let mut a: f64 = std::f64::consts::FRAC_PI_2 + (self.y as f64).atan2(self.x as f64);
        if a >= 0.0 {
            a
        } else {
            a + std::f64::consts::PI * 2.0
        }
    }
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

struct AsteroidMap {
    positions: HashMap<Coord, u32>,
    width: usize,
    height: usize,
}

impl AsteroidMap {
    fn new(s: &str) -> AsteroidMap {
        let mut positions = HashMap::new();
        let mut height = 0;
        let mut width = 0;

        for (y, line) in s.lines().enumerate() {
            height += 1;
            width = 0;
            for (x, c) in line.chars().enumerate() {
                let coord = Coord::new(x as isize, y as isize);
                width += 1;
                let value = match c {
                    '#' => 1,
                    '.' => 0,
                    _ => panic!("Invalid character {} at position {}", c, coord),
                };
                if value == 1 {
                    positions.insert(coord, value);
                }
            }
        }
        AsteroidMap{positions: positions, width: width, height: height}
    }

    fn can_see(&self, looker: &Coord, target: &Coord) -> bool {
        let tdx: isize = target.x - looker.x;
        let tdy: isize = target.y - looker.y;
        let dx;
        let dy;
        let g;

        if tdx == 0 {
            g = tdy.abs();
            dx = 0;
            dy = tdy/tdy.abs();
        } else if tdy == 0 {
            g = tdx.abs();
            dx = tdx/tdx.abs();
            dy = 0;
        } else {
            g = gcd(tdx, tdy).abs();
            dx = tdx / g;
            dy = tdy / g;
        }

        for m in 1..g {
            let x = (looker.x + m*dx) as isize;
            let y = (looker.y + m*dy) as isize;
            let c = Coord::new(x, y);
            if self.positions.contains_key(&c) {
                return false;
            }
        }
        true
    }

    fn count_detectable(&self, looker: &Coord) -> u64 {
        let mut count = 0;
        for (&pos, _) in self.positions.iter() {
            if pos != *looker {
                if self.can_see(looker, &pos) {
                    //println!("{} can see {}", looker, pos);
                    count += 1;
                }
            }
        }
        count
    }

    fn print_ray(&self, center: &Coord, target: &Coord) {
        for y in 0..self.height {
            for x in 0..self.width {
                let coord = Coord::new(x as isize, y as isize);
                if self.positions.contains_key(&coord) {
                    if coord == *center {
                        print!("O");
                    } else if coord == *target {
                        print!("X");
                    } else if *self.positions.get(&coord).unwrap() == 1 {
                        print!("#");
                    } else {
                        print!(".");
                    }
                } else {
                    print!(".");
                }
            }
            println!("");
        }
        println!("");
    }
}

impl fmt::Display for AsteroidMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                let coord = Coord::new(x as isize, y as isize);
                if self.positions.contains_key(&coord) {
                    if *self.positions.get(&coord).unwrap() == 1 {
                        write!(f, "#");
                    } else {
                        write!(f, ".");
                    }
                } else {
                    write!(f, ".");
                }
            }
            writeln!(f, "");
        }
        write!(f, "")
    }
}


fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let s = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");
    let mut map = AsteroidMap::new(&s);

    let mut best_pos = Coord::new(0, 0);
    let mut best_count = 0;

    for (pos, _) in map.positions.iter() {
        let c = map.count_detectable(pos);
        //println!("{}: {}", pos, c);
        if best_count < c {
            best_count = c;
            best_pos = *pos;
        }
    }

    if part == 1 {
        println!("{}: {}", best_pos, best_count);
    } else {
        let center = best_pos;
        let mut clockwise = Vec::new();
        let mut remaining: i64 = map.positions.iter().count() as i64 - 1;
        let mut count = 0;
        let mut answer = 0;

        while remaining > 0 {
            for (pos, _) in &map.positions {
                if *pos != center && map.can_see(&center, &pos) {
                    clockwise.push(pos.clone());
                }
            }
            println!("{}", clockwise.len());

            clockwise.sort_by(|a,b| {
                let aa = a.diff(&center).angle();
                let ba = b.diff(&center).angle();
                aa.partial_cmp(&ba).unwrap()
            });

            for pos in &clockwise {
                count += 1;
                remaining -= 1;
                if count == 200 {
                    answer = 100*pos.x + pos.y;
                }

                let angle = pos.diff(&center).angle();
                println!("{}({}). {}, {} -> {}", count, remaining, center, pos, angle);
                map.print_ray(&center, &pos);
                map.positions.remove(&pos);
            }
            clockwise.clear();
        }
        println!("Answer: {}", answer);
    }
}
