extern crate intcode;
extern crate itertools;
extern crate grid;

use std::env;
use std::fmt;
use std::fs;
use std::io;

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;

use grid::*;
use intcode::*;

#[derive(Copy, Clone)]
enum Direction {
    North, South, West, East
}

use Direction::*;

impl Direction {
    fn from_str(s: &str) -> Option<Direction> {
        match s.to_lowercase().as_str() {
            "north" => Some(North),
            "south" => Some(South),
            "west" => Some(West),
            "east" => Some(East),
            _ => None,
        }
    }

    fn reverse(&self) -> Direction {
        match self {
            North => South,
            South => North,
            West => East,
            East => West,
        }
    }
}

impl fmt::Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            North => "north",
            South => "south",
            West => "west",
            East => "east",
        };
        write!(f, "{}", s)
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct Node {
    position: Coord,
    distance: usize,
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        other.distance.cmp(&self.distance).
            then_with(|| self.position.x.cmp(&other.position.x)).
            then_with(|| self.position.y.cmp(&other.position.y))
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn print_path(path: &Vec<Direction>) {
    for direction in path {
        print!("{} ", direction);
    }
    println!("");
}

enum Command {
    Move(Direction),
    Take(String),
    Drop(String),
    Inv,
}

use Command::*;

impl Command {
    fn from_str(s: &str) -> Command {
        let lower_s = s.to_lowercase();
        if &lower_s[0..]  == "inv" {
            Inv
        } else if &lower_s[0..4] == "take" {
            if &lower_s[4..5] != " " || lower_s.len() < 6 {
                panic!("Item name missing");
            }
            let item = String::from(&lower_s[5..]);
            Take(item)
        } else if &lower_s[0..4] == "drop" {
            if &lower_s[4..5] != " " || lower_s.len() < 6 {
                panic!("Item name missing");
            }
            let item = String::from(&lower_s[5..]);
            Drop(item)
        } else if let Some(direction) = Direction::from_str(&lower_s) {
            Move(direction)
        } else {
            panic!("Invalid command!");
        }
    }
}

struct Room {
    name: String,
    description: String,
    position: Coord,
    doors: Vec<Direction>,
    items: Vec<String>,
}

impl fmt::Display for Room {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "== {} ==", self.name);
        writeln!(f, "{}\n", self.description);
        writeln!(f, "Location: {}\n", self.position);
        writeln!(f, "Doors here lead:");
        for door in self.doors.iter() {
            writeln!(f, "- {}", door);
        }
        writeln!(f, "");
        writeln!(f, "Items here:");
        for item in self.items.iter() {
            writeln!(f, "- {}", item);
        }
        writeln!(f, "")
    }
}

impl Room {
    fn new(text: &str, position: Coord) -> Room {
        let mut name = String::new();
        let mut description = String::new();
        let mut doors = Vec::new();
        let mut items = Vec::new();

        if &text[0..3] != "\n\n\n" {
            panic!("Missing initial carriage returns in room descriptions");
        }

        if &text[3..6] != "== " {
            panic!("Missing initial prefix for room name");
        }

        let mut index: usize = (&text[6..]).find(" ==\n").
            expect("Missing room name postfix") + 6;
        name.push_str(&text[6..index]);
        index += 4;

        let door_prefix_index = index + (&text[index..]).find("Doors here lead:\n").
            expect("Missing prefix for doors section");

        description.push_str(&text[index..door_prefix_index]);
        description = description.trim().to_string();

        index += 17;

        while let Some(door_index) = &text[index..].find("- ") {
            index += door_index + 2;
            let end_index = index + &text[index..].find("\n").expect("Missing door name termination");
            let direction = Direction::from_str(&text[index..end_index]).expect("Invalid direction");
            doors.push(direction);
            index = end_index + 1;
            if &text[index..(index+1)] == "\n" {
                break;
            }
        }

        if doors.len() == 0 {
            panic!("Missing doors");
        }

        if let Some(item_prefix_index) = (&text[index..]).find("Items here:\n") {
            index += item_prefix_index + 12;

            while let Some(item_index) = &text[index..].find("- ") {
                index += item_index + 2;
                let end_index = index + &text[index..].find("\n").expect("Missing item name termination");
                let item = &text[index..end_index];
                items.push(item.to_string());
                index = end_index + 1;
                if &text[index..(index+1)] == "\n" {
                    break;
                }
            }

            if items.len() == 0 {
                panic!("Missing items");
            }
        }

        Room{ name, position, description, doors, items }
    }
}


struct Robot {
    machine: Machine,
    position: Coord,
    map: Grid<usize>,
    items: Vec<String>,
    rooms: Vec<Room>,
    quiet: bool,
}

impl fmt::Display for Robot {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.rooms.len() > 0 {
            let room = self.map.get_cell_value(self.position);
            writeln!(f, "Current Location: Room {} at {}", room, self.position);
        } else {
            writeln!(f, "Current Location: {}", self.position);
        }
        for (index, room) in self.rooms.iter().enumerate() {
            writeln!(f, "**** Room {} ****\n", index+1);
            writeln!(f, "{}", room);
        }
        writeln!(f, "\n---\n");
        for (index, item) in self.items.iter().enumerate() {
            writeln!(f, "****Item {} ****\n", index+1);
            writeln!(f, "{}", item);
        }
        writeln!(f, "\n**** Map ****\n");
        writeln!(f, "{}", self.map)
    }
}

impl Robot {
    fn new(machine: Machine) -> Robot {
        let position = Coord::new(0, 0);
        let map = Grid::new('.');
        let items = Vec::new();
        let rooms = Vec::new();
        let quiet = false;

        Robot{ machine, position, map, items, rooms, quiet }
    }

    fn in_new_room(&self) -> bool {
        !self.map.has_cell_value(self.position)
    }

    fn sense_environment(&mut self) {
        self.machine.execute_memory();
        let mut output_text = String::new();
        while self.machine.has_outputs() {
            let output = (self.machine.fetch_output().unwrap() as u8) as char;
            output_text.push(output);
        }
        if output_text.len() > 0 && self.in_new_room() {
            self.add_room(Room::new(&output_text, self.position));
        }
        if !self.quiet {
            println!("{}", output_text);
        }
    }

    fn add_room(&mut self, room: Room) {
        match self.rooms.iter().position(|x| *x.name == room.name) {
            None => {
                self.rooms.push(room);
                self.map.set_cell_value(self.position, self.rooms.len() - 1);
            },
            _ => (),
        }
    }

    fn get_moves(&self) -> Vec<Direction> {
        let room_index = self.map.get_cell_value(self.position);
        self.rooms[room_index].doors.clone()
    }

    fn get_neighbor(&self, direction: Direction) -> Coord {
        let mut position = self.position.clone();
        match direction {
            North => position.y -= 1,
            South => position.y += 1,
            East => position.x += 1,
            West => position.x -= 1,
        }
        position
    }

    fn make_move(&mut self, direction: Direction) {
        match direction {
            North => self.position.y -= 1,
            South => self.position.y += 1,
            East => self.position.x += 1,
            West => self.position.x -= 1,
        }
    }

    fn execute_move(&mut self, direction: Direction) {
        self.make_move(direction);
        let mut command_str = direction.to_string();
        command_str.push('\n');

        for c in command_str.chars() {
            self.machine.send_input((c as u8) as i64);
        }
        self.sense_environment();
    }

    fn take_path(&mut self, path: &Vec<Direction>) {
        for &direction in path {
            self.execute_move(direction);
        }
    }

    fn execute_command(&mut self, command_str: &str) {
        let command = Command::from_str(&command_str);
        let mut command_string = String::from(command_str);
        command_string.push('\n');

        match command {
            Move(direction) => self.make_move(direction),
            Take(item) => self.items.push(item),
            Drop(item) => {
                let index = self.items.iter().position(|x| *x == item).
                    expect("Unable to drop item");
                self.items.remove(index);
            },
            Inv => {
                for item in self.items.iter() {
                    println!("- {}", item);
                }
            }
        }

        for c in command_string.chars() {
            self.machine.send_input((c as u8) as i64);
        }

        self.sense_environment();
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    //let _part: u32 = args[2].parse().expect("Invalid part!");
    let manual: bool = args[2].parse().expect("Invalid part!");

    let mut machine = Machine::new();
    machine.load_program(&program);

    let mut robot = Robot::new(machine);

    if manual {
        robot.sense_environment();
        loop {
            let mut input_text;
            input_text = String::new();
            io::stdin().read_line(&mut input_text).expect("failed to read from stdin");
            input_text = input_text.trim().to_string();
            robot.execute_command(&input_text);
        }
    } else {
        let mut distances = HashMap::new();
        let mut paths = HashMap::new();
        distances.insert(robot.position, 0);
        paths.insert(robot.position, vec![]);

        let mut heap = BinaryHeap::new();
        heap.push(Node{position: robot.position, distance: 0});

        let mut visited = HashSet::new();

        robot.quiet = true;
        robot.sense_environment();
        println!("Exploring...");

        while let Some(Node {position, distance: current_distance}) = heap.pop() {
            let path = {
                paths.get(&position).unwrap().clone()
            };

            let reverse_path: Vec<Direction> = path.iter().
                rev().map(|d: &Direction| d.reverse()).collect();

            visited.insert(position);

            robot.take_path(&path);

            for current_move in robot.get_moves() {
                let new_position = robot.get_neighbor(current_move);
                if !visited.contains(&new_position) {
                    robot.execute_move(current_move);

                    let neighbor_distance = current_distance + 1;
                    let mut new_path = path.clone();
                    new_path.push(current_move);
                    paths.insert(new_position, new_path);

                    let distance = distances.entry(new_position).or_insert(neighbor_distance);
                    if *distance > neighbor_distance {
                        *distance = neighbor_distance;
                    }
                    heap.push(Node{position: new_position, distance: *distance});
                    robot.execute_move(current_move.reverse());
                }
            }
            robot.take_path(&reverse_path);
        }
        println!("Finished exploring!\n");

        let mut possible_items = Vec::new();
        for (&position, &room_index) in robot.map.cells.iter() {
            let room = &robot.rooms[room_index];
            for item in room.items.iter() {
                possible_items.push((item.to_string(), position));
            }
        }

        let analysis_room_position = robot.rooms[10].positions;
        let path_to_analysis_room = {
            paths.get(&analysis_room_position).unwrap().clone()
        };

        let reverse_path_to_analysis_room: Vec<Direction> =
            path_to_analysis_room.iter().
            rev().map(|d: &Direction| d.reverse()).collect();

        let mut found_correct_weight = false;
        let n = possible_items.len();
        for m in 1..=n {
            for indices in (0..n).combinations(m) {
                for &index in indices.iter() {
                    let (item, position) = possible_items[index];
                    let path = {
                        paths.get(&position).unwrap().clone()
                    };

                    let reverse_path: Vec<Direction> = path.iter().
                        rev().map(|d: &Direction| d.reverse()).collect();
                    robot.take_path(&path);

                    let mut command_string = String::from("take ");
                    command_string.push_str(item.to_string());
                    robot.execute_command(&command_string);

                    robot.take_path(&reverse_path);
                }
            }
        }

    }
}
