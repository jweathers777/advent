extern crate intcode;
extern crate grid;

use std::env;
use std::fs;
use grid::*;

fn find_intersections(grid: &Grid<char>) -> Vec<Coord> {
    let mut intersections = Vec::new();
    for (&coord, &value) in grid.cells.iter() {
        if value == '#' {
            let neighbors = grid.get_neighbors(coord);
            if neighbors.len() == 4 {
                let mut intersection = true;
                for neighbor in neighbors {
                    if grid.get_cell_value(neighbor) != '#' {
                        intersection = false;
                        break;
                    }
                }
                if intersection {
                    intersections.push(coord);
                }
            }
        }
    }
    intersections
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let _part: u32 = args[2].parse().expect("Invalid part!");

    let mut machine = intcode::Machine::new();
    machine.load_program(&program);
    machine.execute_memory();

    let mut image: Grid<char> = Grid::new();
    let mut x: u32 = 0;
    let mut y: u32 = 0;
    while machine.has_outputs() {
        let c = (machine.fetch_output().unwrap() as u8) as char;
        let coord = Coord::new(x, y);

        if c == '\n' {
            y += 1;
            x = 0;
        } else {
            image.set_cell_value(coord, c);
            x += 1;
        }
    }

    let result: u32 = find_intersections(&image).iter().
        map(|c| c.x*c.y).sum();
    println!("{}", result);
}
