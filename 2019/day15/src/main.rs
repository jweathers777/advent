extern crate intcode;

use std::env;
use std::fs;
use std::fmt;

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;

const EMPTY: u32 = 0;
const WALL: u32 = 1;
const MARKER: u32 = 2;
const OXYGEN: u32 = 3;

#[derive(Copy, Clone)]
enum Direction {
    North, West, East, South
}

use Direction::*;

impl Direction {
    const DIRECTIONS: [Direction;4] = [North, West, East, South];

    fn to_u32(&self) -> u32 {
        match self {
            North => 1,
            South => 2,
            West => 3,
            East => 4,
        }
    }

    fn reverse(&self) -> Direction {
        match self {
            North => South,
            South => North,
            West => East,
            East => West,
        }
    }

    fn to_string(&self) -> &'static str {
        match self {
            North => "North",
            South => "South",
            West => "West",
            East => "East",
        }
    }
}

#[derive(Copy, Clone)]
enum Status {
    HitWall, TookStep, FoundTankWithStep
}

use Status::*;

impl Status {
    fn from_u32(value: u32) -> Status {
        match value {
            0 => HitWall,
            1 => TookStep,
            2 => FoundTankWithStep,
            _ => panic!("Invalid status code"),
        }
    }
}


#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord {
    x: i32,
    y: i32,
}

impl Coord {
    fn new(x: i32, y: i32) -> Coord {
        Coord{x: x, y: y}
    }
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

struct Robot {
    machine: intcode::Machine,
    position: Coord,
    tank_position: Option<Coord>,
    grid: HashMap<Coord, u32>,
    min_x: i32,
    max_x: i32,
    min_y: i32,
    max_y: i32,
}

impl Robot {
    fn new(machine: intcode::Machine) -> Robot {
        let position = Coord::new(0, 0);
        let mut grid = HashMap::new();
        grid.insert(position, EMPTY);

        Robot{
            machine: machine,
            position: position,
            tank_position: None,
            grid: grid,
            min_x: 0,
            max_x: 0,
            min_y: 0,
            max_y: 0,
        }
    }

    fn found_tank(&self) -> bool {
        match self.tank_position {
            None => false,
            Some(_) => true,
        }
    }

    fn get_current_neighbor(&mut self, direction: Direction) -> Coord {
        self.get_neighbor(self.position, direction)
    }

    fn get_neighbor(&mut self, position: Coord, direction: Direction) -> Coord {
        let mut neighbor = position.clone();

        match direction {
            North => {
                neighbor.y += 1;
                if neighbor.y > self.max_y {
                    self.max_y = neighbor.y;
                }
            },
            West => {
                neighbor.x -= 1;
                if neighbor.x < self.min_x {
                    self.min_x = neighbor.x;
                }
            },
            East => {
                neighbor.x += 1;
                if neighbor.x > self.max_x {
                    self.max_x = neighbor.x;
                }
            },
            South => {
                neighbor.y -= 1;
                if neighbor.y < self.min_y {
                    self.min_y = neighbor.y;
                }
            },
        }
        neighbor
    }

    fn take_step(&mut self, direction: Direction) -> u32 {
        let new_position = self.get_current_neighbor(direction);
        self.machine.send_input(direction.to_u32() as i64);
        self.machine.execute_memory();

        let status_code = self.machine.fetch_output().expect("Failed to process move!");
        let status = Status::from_u32(status_code as u32);
        let cell = match status {
            HitWall => WALL,
            TookStep => {
                self.position = new_position;
                EMPTY
            }
            FoundTankWithStep => {
                self.position = new_position;
                self.tank_position = Some(self.position);
                EMPTY
            }
        };
        self.grid.insert(new_position, cell);
        cell
    }

    fn take_path(&mut self, path: &Vec<Direction>) {
        for &direction in path {
            self.take_step(direction);
        }
    }

    fn mark_path(&mut self, path: &Vec<Direction>) {
        let mut current_position = self.position;
        for &direction in path {
            current_position = self.get_neighbor(current_position, direction);
            self.grid.insert(current_position, MARKER);
        }
    }

    fn get_moves(&mut self) -> Vec<Direction> {
        let mut moves = Vec::new();
        for &direction in &Direction::DIRECTIONS {
            let new_position = self.get_current_neighbor(direction);
            if !self.grid.contains_key(&new_position) {
                moves.push(direction);
            } else if *self.grid.get(&new_position).unwrap() != WALL {
                moves.push(direction);
            }
        }
        moves
    }

    fn print_grid_with_path(&mut self, path: &Vec<Direction>) {
        self.print_grid();
        self.mark_path(path);
        self.print_grid();
    }

    fn print_grid(&self) {
        let origin = Coord::new(0, 0);
        for _ in self.min_x..=self.max_x {
            print!("-");
        }
        println!("");
        for y in (self.min_y..=self.max_y).rev() {
            for x in self.min_x..=self.max_x {
                let pos = Coord::new(x, y);
                if self.position == pos {
                    print!("D");
                } else if origin == pos {
                    print!("o");
                } else if self.tank_position == Some(pos) {
                    print!("T");
                } else if self.grid.contains_key(&pos) {
                    let cell = self.grid.get(&pos).unwrap();
                    if *cell == EMPTY {
                        print!(".");
                    } else if *cell == WALL {
                        print!("#");
                    } else if *cell == MARKER {
                        print!("'");
                    } else if *cell == OXYGEN {
                        print!("O");
                    } else {
                        panic!("Invalid cell!")
                    }
                } else {
                    print!(" ");
                }
            }
            println!("");
        }
        for _ in self.min_x..=self.max_x {
            print!("-");
        }
        println!("");
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct Node {
    position: Coord,
    distance: usize,
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        other.distance.cmp(&self.distance).
            then_with(|| self.position.x.cmp(&other.position.x)).
            then_with(|| self.position.y.cmp(&other.position.y))
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn print_path(path: &Vec<Direction>) {
    for direction in path {
        print!("{} ", direction.to_string());
    }
    println!("");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut machine = intcode::Machine::new();
    machine.load_program(&program);

    let mut robot = Robot::new(machine);

    let mut distances = HashMap::new();
    let mut paths = HashMap::new();
    distances.insert(robot.position, 0);
    paths.insert(robot.position, vec![]);

    let mut heap = BinaryHeap::new();
    heap.push(Node{position: robot.position, distance: 0});

    let mut visited = HashSet::new();

    while let Some(Node {position, distance: current_distance}) = heap.pop() {
        let path = {
            paths.get(&position).unwrap().clone()
        };

        let reverse_path: Vec<Direction> = path.iter().
            rev().map(|d: &Direction| d.reverse()).collect();

        visited.insert(position);

        robot.take_path(&path);
        if part == 1 {
            if Some(robot.position) == robot.tank_position {
                robot.take_path(&reverse_path);
                robot.print_grid_with_path(&path);
                println!("Best Path Length: {}", path.len());
                break;
            }
        }

        for current_move in robot.get_moves() {
            let new_position = robot.get_current_neighbor(current_move);
            if !visited.contains(&new_position) {
                let cell = robot.take_step(current_move);

                let neighbor_distance = current_distance + 1;
                let mut new_path = path.clone();
                new_path.push(current_move);
                paths.insert(new_position, new_path);

                let distance = distances.entry(new_position).or_insert(neighbor_distance);
                if *distance > neighbor_distance {
                    *distance = neighbor_distance;
                }
                if cell != WALL {
                    heap.push(Node{position: new_position, distance: *distance});
                    robot.take_step(current_move.reverse());
                }
            }
        }
        robot.take_path(&reverse_path);
    }
    if part == 2 {
        robot.print_grid();
        let mut spread_minutes = 0;
        let mut queue = VecDeque::new();
        queue.push_back((robot.tank_position.unwrap(), 0));
        while let Some((position, minute)) = queue.pop_front() {
            robot.grid.insert(position, OXYGEN);
            spread_minutes = minute;

            let neighbors: Vec<Coord> = Direction::DIRECTIONS.iter().
                map(|&d| robot.get_neighbor(position, d)).
                collect();
            let next_minute = minute + 1;

            for neighbor in neighbors {
                if robot.grid.contains_key(&neighbor) {
                    let cell = robot.grid.get(&neighbor).unwrap();
                    if *cell != WALL && *cell != OXYGEN {
                        queue.push_back((neighbor, next_minute));
                    }
                }
            }
        }
        println!("Minutes to Fill with Oxygen: {}", spread_minutes);
    }
}
