use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::VecDeque;

const POSITION_MODE: usize = 0;
const IMMEDIATE_MODE: usize = 1;
const RELATIVE_MODE: usize = 2;

pub enum ProgramStatus {
    READY, PAUSED, HALTED
}

pub struct MachineState {
    memory: BTreeMap<usize,i64>,
    inputs: VecDeque<i64>,
    outputs: VecDeque<i64>,
    instruction_pointer: usize,
    relative_base_address: usize,
    status: ProgramStatus,
    debug_level: u32,
}

impl MachineState {
    pub fn new() -> MachineState {
        MachineState{
            memory: BTreeMap::new(),
            inputs: VecDeque::new(),
            outputs: VecDeque::new(),
            instruction_pointer: 0,
            relative_base_address: 0,
            status: ProgramStatus::READY,
            debug_level: 0,
        }
    }

    pub fn get_address_contents(&self, address: usize) -> i64 {
        if self.memory.contains_key(&address) {
            *self.memory.get(&address).unwrap()
        } else {
            0
        }
    }

    pub fn set_address_contents(&mut self, parameter: (i64, usize), value: i64) {
        let (param_value, mode) = parameter;
        let address = match mode {
            POSITION_MODE => param_value as usize,
            RELATIVE_MODE => {
                if param_value >= 0 {
                    self.relative_base_address + (param_value as usize)
                } else {
                    self.relative_base_address - (-param_value as usize)
                }
            },
            _ => panic!("Unsupported mode {}", mode),
        };
        self.memory.insert(address, value);
    }

    pub fn load_program(&mut self, program: &String) {
        self.memory = BTreeMap::new();
        let values = program.
            split(',').
            map(|s| s.trim().parse::<i64>().expect("Invalid code value!")).
            enumerate();

        for (index, value) in values {
            self.memory.insert(index, value);
        }
        self.instruction_pointer = 0;
        self.status = ProgramStatus::READY;
    }

    pub fn get_parameter_value(&self, parameter: (i64,usize)) -> i64 {
        let (value, mode) = parameter;
        match mode {
            POSITION_MODE => self.get_address_contents(value as usize),
            IMMEDIATE_MODE => value,
            RELATIVE_MODE => {
                let address = if value >= 0 {
                    self.relative_base_address + (value as usize)
                } else {
                    self.relative_base_address - (-value as usize)
                };
                self.get_address_contents(address)
            },
            _ => panic!("Unsupported mode {}", mode),
        }
    }
}

pub struct Instruction {
    name: &'static str,
    opcode: usize,
    parameter_count: usize,
    procedure: fn(&mut MachineState, &Vec<(i64, usize)>),
}

impl Instruction {
    const INSTRUCTION_SET: [Instruction;10] = [
        Instruction{
            name: "ADD",
            opcode: 1,
            parameter_count: 3,
            procedure: (|state, params| {
                let a = state.get_parameter_value(params[0]);
                let b = state.get_parameter_value(params[1]);

                state.set_address_contents(params[2], a + b);
                state.instruction_pointer += params.len() + 1;
            })
        },
        Instruction{
            name: "MUL",
            opcode: 2,
            parameter_count: 3,
            procedure: (|state, params| {
                let a = state.get_parameter_value(params[0]);
                let b = state.get_parameter_value(params[1]);

                state.set_address_contents(params[2], a * b);
                state.instruction_pointer += params.len() + 1;
            })
        },
        Instruction{
            name: "IN",
            opcode: 3,
            parameter_count: 1,
            procedure: (|state, params| {
                match state.inputs.pop_front() {
                    Some(value) => {
                        if state.debug_level > 0 {
                            println!("Received Input {}", value);
                        }
                        state.set_address_contents(params[0], value);
                        state.instruction_pointer += params.len() + 1;
                    },
                    None => state.status = ProgramStatus::PAUSED,
                }
            })
        },
        Instruction{
            name: "OUT",
            opcode: 4,
            parameter_count: 1,
            procedure: (|state, params| {
                let val = state.get_parameter_value(params[0]);

                state.outputs.push_back(val);
                state.instruction_pointer += params.len() + 1;
            })
        },
        Instruction{
            name: "JNZ",
            opcode: 5,
            parameter_count: 2,
            procedure: (|state, params| {
                let a = state.get_parameter_value(params[0]);
                let b = state.get_parameter_value(params[1]);

                if a != 0 {
                    state.instruction_pointer = b as usize;
                } else {
                    state.instruction_pointer += params.len() + 1;
                }
            })
        },
        Instruction{
            name: "JZ",
            opcode: 6,
            parameter_count: 2,
            procedure: (|state, params| {
                let a = state.get_parameter_value(params[0]);
                let b = state.get_parameter_value(params[1]);

                if a == 0 {
                    state.instruction_pointer = b as usize;
                } else {
                    state.instruction_pointer += params.len() + 1;
                }
            })
        },
        Instruction{
            name: "SLT",
            opcode: 7,
            parameter_count: 3,
            procedure: (|state, params| {
                let a = state.get_parameter_value(params[0]);
                let b = state.get_parameter_value(params[1]);

                state.set_address_contents(
                    params[2],
                    if a < b { 1 } else { 0 }
                );
                state.instruction_pointer += params.len() + 1;
            })
        },
        Instruction{
            name: "SEQ",
            opcode: 8,
            parameter_count: 3,
            procedure: (|state, params| {
                let a = state.get_parameter_value(params[0]);
                let b = state.get_parameter_value(params[1]);

                state.set_address_contents(
                    params[2],
                    if a == b { 1 } else { 0 }
                );
                state.instruction_pointer += params.len() + 1;
            })
        },
        Instruction{
            name: "SBAS",
            opcode: 9,
            parameter_count: 1,
            procedure: (|state, params| {
                let adjustment = state.get_parameter_value(params[0]);
                if adjustment >= 0 {
                    state.relative_base_address += adjustment as usize;
                }
                else {
                    state.relative_base_address -= -adjustment as usize;
                }
                state.instruction_pointer += params.len() + 1;
            })
        },
        Instruction{
            name: "HLT",
            opcode: 99,
            parameter_count: 0,
            procedure: (|state, _| state.status = ProgramStatus::HALTED)
        },
    ];
}

// Instruction procedures

pub struct Machine {
    state: MachineState,
    instruction_set: HashMap<usize, &'static Instruction>,
}

impl Machine {
    pub fn new() -> Machine {
        let state = MachineState::new();
        let mut instruction_set = HashMap::new();

        for instruction in &Instruction::INSTRUCTION_SET {
            instruction_set.insert(instruction.opcode, instruction);
        }

        Machine{ state: state, instruction_set: instruction_set }
    }

    pub fn set_debug_level(&mut self, level: u32) {
        self.state.debug_level = level;
    }

    pub fn load_program(&mut self, program: &String) {
        self.state.load_program(program);
    }

    pub fn clear_streams(&mut self) {
        self.state.inputs.clear();
        self.state.outputs.clear();
    }

    pub fn send_input(&mut self, input: i64) {
        self.state.inputs.push_back(input);
    }

    pub fn fetch_output(&mut self) -> Option<i64> {
        self.state.outputs.pop_front()
    }

    pub fn get_instruction(&self) -> &Instruction {
        let address = self.state.instruction_pointer;
        let opcode = self.state.get_address_contents(address) as usize % 100;
        self.instruction_set.get(&opcode).expect("Invalid opcode!")
    }

    pub fn get_parameters(&self,  instruction: &Instruction) -> Vec<(i64, usize)> {
        let parameter_count = instruction.parameter_count;

        let mut address = self.state.instruction_pointer;
        let mut parameters = Vec::new();
        let mut encoded_modes = self.state.get_address_contents(address) as usize / 100;
        let mut mode;

        for _ in 0..parameter_count {
            address += 1;

            if encoded_modes > 0 {
                mode = encoded_modes % 10;
                encoded_modes /= 10;
            } else {
                mode = POSITION_MODE;
            }

            parameters.push((self.state.get_address_contents(address), mode));
        }

        parameters
    }

    pub fn print_operation(&self, instruction: &Instruction, parameters: &Vec<(i64,usize)>) {
        print!("---> {}", instruction.name);
        for &(value, mode) in parameters {
            match mode {
                POSITION_MODE => {
                    let contents = self.state.get_address_contents(value as usize);
                    print!(" [{}]({})", value, contents);
                },
                RELATIVE_MODE => {
                    let address = if value >= 0 {
                        self.state.relative_base_address + (value as usize)
                    } else {
                        self.state.relative_base_address - (-value as usize)
                    };
                    print!(" [{}:{}]({})", value, address, self.state.get_address_contents(address));
                },
                _ => print!(" {}", value),
            }
        }
        println!("");
    }

    pub fn print_memory(&self) {
        for (address, val) in self.state.memory.iter() {
            if *address > 0 {
                print!(",");
            }
            if self.state.instruction_pointer == *address {
                print!("|{}|", val);
            } else {
                print!("{}", val);
            }
        }
        println!("");
        println!("RELBASE: {}", self.state.relative_base_address);
    }

    pub fn print_outputs(&self) {
        if self.state.outputs.len() > 0 {
            print!("{}", self.state.outputs[0]);
            for val in self.state.outputs.iter().skip(1) {
                print!(",{}", val);
            }
        }
        println!("");
    }

    pub fn has_outputs(&self) -> bool {
        self.state.outputs.len() > 0
    }

    pub fn is_halted(&self) -> bool {
        match self.state.status {
            ProgramStatus::HALTED => true,
            _ => false,
        }
    }

    pub fn set_memory_value(&mut self, address: usize, value: i64) {
        self.state.memory.insert(address, value);
    }

    pub fn set_status_to_ready(&mut self) {
        self.state.instruction_pointer += 1;
        self.state.status = ProgramStatus::READY;
    }

    pub fn execute_memory(&mut self) -> bool {
        if self.state.debug_level > 1 {
            self.print_memory();
        }
        match self.state.status {
            ProgramStatus::HALTED => true,
            _ => {
                let mut status = false;
                self.state.status = ProgramStatus::READY;
                loop {
                    let instruction = self.get_instruction();
                    let parameters = self.get_parameters(instruction);

                    if self.state.debug_level > 0 {
                        self.print_operation(&instruction, &parameters);
                    }
                    (instruction.procedure)(&mut self.state, &parameters);
                    if self.state.debug_level > 1 {
                        self.print_memory();
                    }

                    match self.state.status {
                        ProgramStatus::READY => continue,
                        ProgramStatus::HALTED => {
                            status = true;
                            break;
                        },
                        _ => break,
                    }
                }
                status
            },
        }
    }
}
