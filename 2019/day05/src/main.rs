use std::env;
use std::fs;

extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut machine = intcode::Machine::new();

    let input = if part == 1 { 1 } else { 5 };

    machine.receive_input(input);
    machine.execute_program(&program);
    machine.display_outputs();
}
