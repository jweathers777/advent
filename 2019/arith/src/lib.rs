use std::collections::HashMap;

pub fn factorize(n: u64) -> Vec<u64> {
    let mut factors: Vec<u64> = vec![];
    let mut cofactors: Vec<u64> = vec![];
    let mut d: u64 = 1u64;
    let bound: u64 = (n as f64).sqrt().floor() as u64;

    while d <= bound {
        if n % d == 0 {
            factors.push(d);
            cofactors.push(n/d);
        }
        d += 1;
    }

    if n > 1 {
        cofactors.reverse();
        factors.append(&mut cofactors);
    }

    factors
}

pub fn is_prime(n: u64) -> bool {
    factorize(n).len() == 2
}

pub fn divisions(n: u64, d: u64) -> u64 {
    let mut divs = 0u64;
    let mut m = n;

    while m % d == 0 {
        divs += 1;
        m /= d;
    }
    divs
}

pub fn lcm(nums: &Vec<u64>) -> u64 {
    let mut primes: HashMap<u64,u32> = HashMap::new();

    for &num in nums {
        let prime_factors = factorize(num)
            .into_iter()
            .filter(|&n| is_prime(n));

        for prime in prime_factors {
            primes.entry(prime).or_insert(0);

            let current_exp = divisions(num, prime) as u32;
            if let Some(exp) = primes.get_mut(&prime) {
                if *exp < current_exp {
                    *exp = current_exp;
                }
            }
        }
    }

    let mut prod: u64 = 1;
    for (prime, exp) in &primes {
        prod *= prime.pow(*exp);
    }
    prod
}
