use std::env;
use std::fmt;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;

use std::collections::HashMap;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord {
    row: i32,
    col: i32,
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(row: {}, col: {})", self.row, self.col)
    }
}

impl Coord {
    fn manhattan_length(&self) -> u32 {
        (self.row.abs() as u32) + (self.col.abs() as u32)
    }
}

fn parse_wire(wire_str: &str) -> HashMap<Coord,u32> {
    let mut current_position = vec![0,0];
    let mut positions = HashMap::new();
    let mut total_steps: u32 = 0;

    let mut current_dim: usize;
    let mut current_step: i32;

    for s in wire_str.split(',') {
        let trimmed = s.trim();
        let direction = &trimmed[0..1];
        let moves = trimmed[1..].parse::<u32>().expect("Invalid step!");

        if direction == "U" {
            current_dim = 0;
            current_step = 1;
        } else if direction == "D" {
            current_dim = 0;
            current_step = -1;
        } else if direction == "L" {
            current_dim = 1;
            current_step = -1;
        } else if direction == "R" {
            current_dim = 1;
            current_step = 1;
        } else {
            panic!("Invalid direction: {}!", direction);
        }

        for _ in 0..moves {
            current_position[current_dim] += current_step;
            total_steps += 1;

            let row = current_position[0];
            let col = current_position[1];
            let pos = Coord{row: row, col: col};

            positions.entry(pos).or_insert(total_steps);
        }
    }
    positions
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let f = File::open(&args[1]).expect("File not found!");
    let reader = BufReader::new(&f);

    let part: u32 = args[2].parse().expect("Invalid part!");
    let wires: Vec<HashMap<Coord, u32>> = reader.
        lines().
        map(|l| parse_wire(l.unwrap().trim())).
        collect();

    if part == 1 {
        let mut min_distance = 0;
        for pos in wires[0].keys() {
            if wires[1].contains_key(&pos) {
                let distance = pos.manhattan_length();
                if min_distance == 0 || distance < min_distance {
                    min_distance = distance;
                }
            }
        }
        println!("{}", min_distance);
    } else {
        let mut min_distance = 0;
        for pos in wires[0].keys() {
            if wires[1].contains_key(&pos) {
                let dist1 = wires[0].get(&pos).unwrap();
                let dist2 = wires[1].get(&pos).unwrap();
                let distance = dist1 + dist2;
                if min_distance == 0 || distance < min_distance {
                    min_distance = distance;
                }
            }
        }
        println!("{}", min_distance);
    }
}
