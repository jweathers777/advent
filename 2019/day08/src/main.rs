use std::env;
use std::fs;

fn print_layers(layers: &Vec<Vec<Vec<u32>>>) {
    for (l, layer) in layers.iter().enumerate() {
        println!("Layer {}", l+1);
        for row in layer.iter() {
            for col in row.iter() {
                print!("{}", col);
            }
            println!("");
        }
    }
}

fn collapse_layers(layers: &Vec<Vec<Vec<u32>>>) -> Vec<Vec<u32>> {
    let mut collapsed: Vec<Vec<u32>> = Vec::new();
    let mut finished: Vec<Vec<bool>> = Vec::new();

    for (r, row) in layers[0].iter().enumerate() {
        let width = row.len();
        collapsed.push(vec![0;width]);
        finished.push(vec![false;width]);
    }

    for layer in layers.iter() {
        for (r, row) in layer.iter().enumerate() {
            for (c, &col) in row.iter().enumerate() {
                if !finished[r][c] {
                    if col != 2 {
                        collapsed[r][c] = col;
                        finished[r][c] = true;
                    }
                }
            }
        }
    }

    collapsed
}

fn print_image(image: &Vec<Vec<u32>>) {
    for row in image.iter() {
        for &col in row.iter() {
            print!("{}", if col == 1 { '*' } else { ' ' });
        }
        println!("");
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 5 { panic!("Too few arguments!")  }

    let image = fs::read_to_string(&args[1]).expect("File not found!");
    let width: u32 = args[2].parse().expect("Invalid width!");
    let height: u32 = args[3].parse().expect("Invalid height!");
    let part: u32 = args[4].parse().expect("Invalid part!");

    let layer_size = width * height;
    let pixels = image.len() as u32;
    let depth = pixels / layer_size;

    let mut digits = image.chars().
        map(|c| c.to_digit(10).expect("Invalid digit!"));

    let mut layers: Vec<Vec<Vec<u32>>> = Vec::new();
    for _ in 0..depth {
        let mut layer = Vec::new();
        for _ in 0..height {
            let mut row = Vec::new();
            for _ in 0..width {
                let d = digits.next().unwrap();
                row.push(d);
            }
            layer.push(row);
        }
        layers.push(layer);
    }

    if part == 1 {
        let mut fewest_zeros = pixels;
        let mut best_layer_index = 0;
        for (l, layer) in layers.iter().enumerate() {
            let mut zeros = 0;
            for row in layer {
                for &col in row {
                    if col == 0 {
                        zeros += 1;
                    }
                }
            }
            if zeros < fewest_zeros {
                fewest_zeros = zeros;
                best_layer_index = l;
            }
        }

        let mut ones = 0;
        let mut twos = 0;
        for row in &layers[best_layer_index] {
            for &col in row {
                if col == 1 {
                    ones += 1;
                } else if col == 2 {
                    twos += 1;
                }
            }
        }
        println!("{}", ones * twos);
    } else {
        let collapsed = collapse_layers(&layers);
        print_image(&collapsed);
    }
}
