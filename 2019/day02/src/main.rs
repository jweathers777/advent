use std::env;
use std::fs;

const OP_ADD: usize = 1;
const OP_MUL: usize = 2;
const OP_END: usize = 99;

fn instruction_count(opcode: usize) -> usize {
    if opcode == OP_END {
        1
    } else {
        4
    }
}

fn execute_program_in_memory(memory: &mut Vec<usize>) {
    let max_address = memory.len() - 1;
    let mut address = 0;

    while address <= max_address {
        let instruction_pointer = address;
        let opcode = memory[instruction_pointer];
        let parameter_count = instruction_count(opcode) - 1;

        if opcode == OP_END {
            break;
        }

        if instruction_pointer + parameter_count > max_address {
            panic!("Invalid number of parameters for opcode at address {}", instruction_pointer);
        }

        let mut parameters = Vec::new();
        for _ in 0..parameter_count {
            address += 1;
            parameters.push(memory[address]);
        }

        memory[parameters[2]] = if opcode == OP_ADD {
            memory[parameters[0]] + memory[parameters[1]]
        } else if opcode == OP_MUL {
            memory[parameters[0]] * memory[parameters[1]]
        } else {
            panic!("Invalid opcode {} at address {}", opcode, address);
        };

        address += 1;
    }
}

#[allow(dead_code)]
fn print_memory(memory: &Vec<usize>) {
    for address in 0..(memory.len()) {
        if address == 0 {
            print!("{}", memory[address]);
        } else {
            print!(",{}", memory[address]);
        }
    }
    println!("");
}

fn load_program(program: &String) -> Vec<usize> {
    program.
        split(',').
        map(|s| s.trim().parse::<usize>().unwrap()).
        collect()
}

fn set_noun_and_verb(memory: &mut Vec<usize>, noun: usize, verb: usize) {
    memory[1] = noun;
    memory[2] = verb;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut memory: Vec<usize> = load_program(&program);

    if part == 1 {
        set_noun_and_verb(&mut memory, 12, 2);
        execute_program_in_memory(&mut memory);
        println!("address 0 = {}", memory[0]);
    } else {
        let mut finished = false;

        for noun in 0..100 {
            for verb in 0..100 {
                set_noun_and_verb(&mut memory, noun, verb);
                execute_program_in_memory(&mut memory);
                if memory[0] == 19690720 {
                    let answer = 100 * noun + verb;
                    finished = true;
                    println!("Noun: {}\nVerb: {}\nAnswer: {}", noun, verb, answer);
                    break;
                } else {
                    memory = load_program(&program);
                }
            }
            if finished {
                break;
            }
        }

    }
}
