extern crate intcode;

use std::env;
use std::fmt;
use std::fs;
use std::io;

use std::collections::HashMap;

#[derive(Copy,Clone)]
enum Tile {
    EMPTY, WALL, BLOCK, HORIZONTAL_PADDLE, BALL
}

impl Tile {
    fn from_u32(value: u32) -> Tile {
        match value {
            0 => Tile::EMPTY,
            1 => Tile::WALL,
            2 => Tile::BLOCK,
            3 => Tile::HORIZONTAL_PADDLE,
            4 => Tile::BALL,
            _ => panic!("Unknown value: {}", value),
        }
    }
}

enum JoystickPosition {
    NEUTRAL, LEFT, RIGHT
}

impl JoystickPosition {
    fn to_i64(position: JoystickPosition) -> i64 {
        match position {
            JoystickPosition::NEUTRAL => 0,
            JoystickPosition::LEFT => -1,
            JoystickPosition::RIGHT => 1,
        }
    }
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Coord {
    x: u32,
    y: u32,
}

impl Coord {
    fn new(x: u32, y: u32) -> Coord {
        Coord{x: x, y: y}
    }
}

struct Game {
    grid: HashMap<Coord, Tile>,
    score: i64,
    max_x: u32,
    max_y: u32,
    paddle: Coord,
    ball: Coord,
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..=(self.max_y) {
            for x in 0..=(self.max_x) {
                let pos = Coord::new(x as u32, y as u32);
                let cell = match self.grid.get(&pos) {
                    None => " ",
                    Some(tile) => match *tile {
                        Tile::EMPTY => " ",
                        Tile::WALL => "#",
                        Tile::BLOCK => ":",
                        Tile::HORIZONTAL_PADDLE => "=",
                        Tile::BALL => "O",
                    }
                };
                write!(f, "{}", cell);
            }
            writeln!(f, "");
        }
        writeln!(f, "Score: {}", self.score)
    }
}

impl Game {
    fn new() -> Game {
        Game{
            grid: HashMap::new(),
            score: 0,
            max_x: 0,
            max_y: 0,
            paddle: Coord::new(0,0),
            ball: Coord::new(0,0),
        }
    }

    fn set_pixel(&mut self, x: u32, y: u32, tile: Tile) {
        self.grid.insert(Coord::new(x, y), tile);
        self.max_x = std::cmp::max(self.max_x, x);
        self.max_y = std::cmp::max(self.max_y, y);
        match tile {
            Tile::HORIZONTAL_PADDLE => {
                self.paddle.x = x;
                self.paddle.y = y;
            },
            Tile::BALL => {
                self.ball.x = x;
                self.ball.y = y;
            },
            _ => (),
        }
    }

    fn set_score(&mut self, score: i64) {
        self.score = score;
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut machine = intcode::Machine::new();
    let mut game = Game::new();

    machine.load_program(&program);
    machine.execute_memory();

    loop {
        match machine.fetch_output() {
            None => break,
            Some(x) => {
                let y = machine.fetch_output().expect("Missing y-coordinate") as u32;
                let tile_value = machine.fetch_output().expect("Missing tile value") as u32;
                let tile = Tile::from_u32(tile_value as u32);
                game.set_pixel(x as u32, y, tile);
            }
        }
    }

    println!("{}", game);

    if part == 1 {
        let block_count = game.grid.iter().
            map(|(_,v)| v).
            filter(|v| match v {
                Tile::BLOCK => true,
                _ => false
            }).count();

        println!("Blocks: {}", block_count);
    } else {
        let manual = false;
        let mut game_updated = false;
        let mut input_text;
        let mut halted = false;
        machine.set_memory_value(0, 2);
        machine.set_status_to_ready();
        println!("Starting game");

        loop {
            halted = machine.execute_memory();
            while machine.has_outputs() {
                game_updated = true;
                let v1 = machine.fetch_output().expect("Missing first value");
                let v2 = machine.fetch_output().expect("Missing second value");
                let v3 = machine.fetch_output().expect("Missing third value");

                if v1 == -1 && v2 == 0 {
                    game.set_score(v3)
                } else {
                    let tile = Tile::from_u32(v3 as u32);
                    game.set_pixel(v1 as u32, v2 as u32, tile);
                }
            }

            if game_updated {
                println!("{}", game);
            }
            game_updated = false;

            if halted {
                break;
            }

            if manual {
                input_text = String::new();
                io::stdin().read_line(&mut input_text).expect("failed to read from stdin");
                input_text = input_text.trim().to_string();

                let input = if input_text == "j" {
                    println!("'{}' -> NEUTRAL", input_text);
                    JoystickPosition::NEUTRAL
                } else if input_text == "h" {
                    println!("'{}' -> LEFT", input_text);
                    JoystickPosition::LEFT
                } else {
                    println!("'{}' -> RIGHT", input_text);
                    JoystickPosition::RIGHT
                };
                println!("");
                machine.send_input(JoystickPosition::to_i64(input));
            } else {
                let direction: i64 = (game.ball.x as i64 - game.paddle.x as i64).signum();
                machine.send_input(direction);
            }

        }
    }
}
