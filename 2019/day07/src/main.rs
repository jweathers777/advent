extern crate intcode;
extern crate itertools;

use std::env;
use std::fs;

use itertools::Itertools;

fn print_results(output: i32, phases: &Vec<i32>) {
    println!("Thruster signal is {}", output);
    print!("From phase sequence: {}", phases[0]);
    for phase in phases.iter().skip(1) {
        print!(",{}", phase);
    }
    println!("");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 { panic!("Too few arguments!")  }

    let program = fs::read_to_string(&args[1]).expect("File not found!");
    let part: u32 = args[2].parse().expect("Invalid part!");

    let mut best_output = 0;
    let mut best_amp_phases = vec![0;5];

    if part == 1 {
        let mut machine = intcode::Machine::new();

        for amp_phases in (0..5).permutations(5) {
            let mut input = 0;
            machine.clear_streams();

            for phase in &amp_phases {
                machine.send_input(*phase);
                machine.send_input(input);

                machine.load_program(&program);
                machine.execute_memory();

                input = machine.fetch_output().expect("Missing output!");
            }

            let output = input;

            if output > best_output {
                best_output = output;
                best_amp_phases = amp_phases.clone();
            }
        }
    } else {
        let mut machines = Vec::new();
        for _ in 0..5 {
            machines.push(intcode::Machine::new());
        }

        for amp_phases in (5..10).permutations(5) {
            for machine in &mut machines {
                machine.clear_streams();
                machine.load_program(&program);
            }

            for (phase, machine) in amp_phases.iter().zip(&mut machines) {
                machine.send_input(*phase);
            }

            let mut input = 0;
            let mut halted = false;
            loop {
                for (_, machine) in amp_phases.iter().zip(&mut machines) {
                    machine.send_input(input);
                    halted = machine.execute_memory();
                    input = machine.fetch_output().expect("Missing output!");
                }
                if halted {
                    break;
                }
            }

            let output = input;

            if output > best_output {
                best_output = output;
                best_amp_phases = amp_phases.clone();
            }
        }
    }

    print_results(best_output, &best_amp_phases);
}
