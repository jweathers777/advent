#!/usr/bin/env ruby

require 'set'
require 'priority_queue'  # gem install PriorityQueue

class Graph
  attr_reader :width, :height, :size

  def initialize(cells)
    @width = cells.first.length
    @height = cells.length
    @size = @width * @height
    @nodes = cells.flatten
  end

  def index_to_coords(index)
    [index / @width, index % @width]
  end

  def coords_to_index(r, c)
    (r * @width) + c
  end

  def adj_nodes(node)
    r,c = index_to_coords(node)
    [].tap do |neighbors|
      neighbors.push(coords_to_index(r-1, c)) if r > 0
      neighbors.push(coords_to_index(r, c-1)) if c > 0
      neighbors.push(coords_to_index(r+1, c)) if r < @height - 1
      neighbors.push(coords_to_index(r, c+1)) if c < @width - 1
    end
  end

  def shortest_distance(start, finish)
    infinity = @nodes.sum
    distances = Array.new(@size) { infinity }
    distances[0] = 0
    unvisited = Set.new((0...@size).to_a)
    priority = PriorityQueue.new
    unvisited.each {|n| priority.push(n, distances[n]) }

    current = start
    while current != finish
      neighbors = adj_nodes(current).select {|n| unvisited.member?(n)}
      current_distance = distances[current]
      unvisited.delete(current)

      neighbors.each do |neighbor|
        new_distance = current_distance + @nodes[neighbor]
        if distances[neighbor] > new_distance
          distances[neighbor] = new_distance
          priority.change_priority(neighbor, new_distance)
        end
      end

      current = priority.delete_min_return_key
    end
    distances[finish]
  end

  def Graph.parse_cells(s)
    cells = s.split("\n").map {|row| row.split('').map(&:to_i)}
  end

  def Graph.tiled_cells(cells, n)
    width = cells.first.length
    height = cells.length
    Array.new(n*height) { Array.new(n*width) { nil } }.tap do |tiled_cells|
      n.times do |tr|
        n.times do |tc|
          height.times do |r|
            width.times do |c|
              tiled_cells[r + height*tr][c + width*tc] = ((cells[r][c] + tr + tc) - 1) % 9 + 1
            end
          end
        end
      end
    end
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  cells = Graph.parse_cells(IO.read(fname))
  if part == 2
    cells = Graph.tiled_cells(cells, 5)
  end
  graph = Graph.new(cells)
  start = 0
  finish = graph.size - 1

  starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  d = graph.shortest_distance(start, finish)
  ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  puts d
  puts ending - starting
end

if __FILE__ == $0
  main(ARGV)
end
