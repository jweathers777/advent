#!/usr/bin/env ruby

class Grid
  def initialize(dots=nil)
    @cells = {}
    @width = 0
    @height = 0

    dots.each do |dot|
      self.mark(*dot)
    end
  end

  def [](r,c)
    (@cells[r] || {})[c] || '.'
  end

  def mark(r,c)
    @cells[r] ||= {}
    @cells[r][c] = '#'
    @height = r+1 if r >= @width
    @width = c+1 if c >= @height
  end

  def fold(axis, pos)
    if axis == 'y'
      @cells.keys.each do |r|
        if r > pos
          nr = pos - (r - pos)
          @cells[r].keys.each do |c|
            self.mark(nr, c)
          end
          @cells.delete(r)
        end
      end
      @cells.delete(pos)
      @height = pos
    else
      @cells.keys.each do |r|
        @cells[r].keys.each do |c|
          if c > pos
            nc = pos - (c - pos)
            self.mark(r,nc)
            @cells[r].delete(c)
          end
          @cells.delete(r) if @cells[r].empty?
        end
        @cells[r].delete(pos)
      end
      @width = pos
    end
  end

  def dot_count
    @cells.values.reduce(0) {|a,row| a + row.length}
  end

  def to_s
    @height.times.map {|r| @width.times.map {|c| self[r,c]}.join(' ')}.join("\n")
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  lines = IO.readlines(fname, chomp: true)
  divider = lines.find_index {|l| l == ''}
  dots = lines[0...divider].map {|l| l.split(',').map(&:to_i).reverse}
  folds = lines[divider+1..].map {|l| l.sub('fold along ', '').split('=')}.map {|a,b| [a, b.to_i]}

  grid = Grid.new(dots)
  if part == 1
    grid.fold(*folds[0])
    puts grid.dot_count
  else
    folds.each {|f| grid.fold(*f)}
    puts grid
  end
end

if __FILE__ == $0
  main(ARGV)
end
