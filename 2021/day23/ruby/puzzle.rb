#!/usr/bin/env ruby

require 'pry'
require 'pry-byebug'
require 'set'
require 'priority_queue'  # gem install PriorityQueue

class Move
  attr_accessor :mover_type, :mover_index, :start, :dest, :cost

  def initialize(mover_type, mover_index, start, dest, cost)
    @mover_type = mover_type
    @mover_index = mover_index
    @start = start
    @dest = dest
    @cost = cost
  end

  def to_s
    "#{@mover_type}: [#{@start[0]}, #{@start[1]}] -> [#{@dest[0]}, #{@dest[1]}] (#{@cost})"
  end

  def inverse_of?(m)
    m && @mover_type == m.mover_type && @start == m.dest && @dest == m.start
  end
end

class Burrow
  AMPHIPOD_MATCHER = /^[A-D]$/
  HAS_AMPHIPOD_MATCHER = /[A-D]/
  AMPHIPOD_NAMES = {'A' => :amber, 'B' => :bronze, 'C' => :copper, 'D' => :desert}
  AMPHIPOD_ENERGY = {'A' => 1, 'B' => 10, 'C' => 100, 'D' => 1000}
  AMPHIPOD_DESTINATION_ROOMS = %w{A B C D}.each_with_index.to_h

  attr_accessor :cells, :amphipods, :room_locations, :hallway_locations

  def initialize(cells, amphipods, room_locations, hallway_locations)
    @cells = cells
    @amphipods = amphipods
    @room_locations = room_locations
    @hallway_locations = hallway_locations
  end

  def to_s
    @cells.map(&:join).join("\n")
  end

  def hash
    @cells.hash
  end

  def eql?(b)
    @cells == b.cells
  end

  alias :== eql?

  def height
    @height ||= @cells.length
  end

  def width
    @width ||= @cells.map(&:length).max
  end

  def room_entrances
    @room_entrances ||= @room_locations.map {|ls| l = ls.min_by(&:first); [l[0]-1, l[1]]}
  end

  def in_non_blocking_correct_position?(amphipod_type, room, location)
    room.include?(location) && (room.count {|r,c| r > location[0] && @cells[r][c] != amphipod_type} == 0)
  end

  def room_includes_no_other_amphipod?(room, amphipod_type)
    room.map {|r,c| @cells[r][c]}.count {|cell| cell != amphipod_type && cell != '.'} == 0
  end

  def room_correctly_filled?(room, amphipod_type)
    room.all? {|r,c| @cells[r][c] == amphipod_type}
  end

  def copy
    new_cells = @cells.map(&:dup)
    new_amphipods = @amphipods.map {|type, locations| [type, locations.map(&:dup)]}.to_h
    new_room_locations = @room_locations.map {|room| room.map(&:dup)}
    new_hallway_locations = @hallway_locations.map(&:dup)

    Burrow.new(new_cells, new_amphipods, new_room_locations, new_hallway_locations)
  end

  def position_is_lost?(move)
    return false unless move && @hallway_locations.include?(move.dest)

    amphipod_type = move.mover_type
    room = @room_locations[AMPHIPOD_DESTINATION_ROOMS[amphipod_type]]

    if room.first[1] < move.dest[1]
      delta = -1
    else
      delta = 1
    end

    reachable_hallway_spaces = 0
    r,c = move.dest
    c += delta
    while (cell = @cells[r][c]) != '#'
      if cell == '.'
        reachable_hallway_spaces += 1
      else
        if available_moves_for(cell, [[r,c]], move).length > 0
          reachable_hallway_spaces += 1
        end
        break
      end
      c += delta
    end

    spaces_above_non_mover = room.reverse.
      drop_while {|r,c| @cells[r][c] == amphipod_type}.
      count {|r,c| @cells[r][c] != '.'}

    spaces_above_non_mover > reachable_hallway_spaces
  end

  def goal_burrow
    self.copy.tap do |new_burrow|
      new_burrow.amphipods.values.each do |locations|
        locations.each do |r,c|
          new_burrow.cells[r][c] = '.'
        end
      end

      new_amphipods = {}
      new_burrow.room_locations.zip(AMPHIPOD_NAMES.keys).each do |locations, amphipod_type|
        locations.each do |r,c|
          new_burrow.cells[r][c] = amphipod_type
          new_amphipods[amphipod_type] ||= []
          new_amphipods[amphipod_type] << [r,c]
        end
      end
      new_burrow.amphipods = new_amphipods
    end
  end

  def empty_neighbor_cells(r,c)
    [].tap do |neighbors|
      neighbors << [r-1, c] if r > 0 && @cells[r-1][c] == '.'
      neighbors << [r, c-1] if c > 0 && @cells[r][c-1] == '.'
      neighbors << [r+1, c] if r < height-1 && @cells[r+1][c] == '.'
      neighbors << [r, c+1] if c < width-1 && @cells[r][c+1] == '.'
    end
  end

  def available_moves_for(amphipod_type, locations, last_move)
    [].tap do |moves|
      room = @room_locations[AMPHIPOD_DESTINATION_ROOMS[amphipod_type]]
      next if room_correctly_filled?(room, amphipod_type)
      locations.each_with_index do |location, mover_index|
        next if in_non_blocking_correct_position?(amphipod_type, room, location)
        next if last_move && last_move.mover_type == amphipod_type && last_move.dest == location

        starts = [[location, 0]]
        dests = Set.new()

        starting_in_hallway = @hallway_locations.include?(location)
        moving_through_hallway = starting_in_hallway

        while not starts.empty?
          start_loc, start_cost = starts.pop
          cost = start_cost + AMPHIPOD_ENERGY[amphipod_type]
          neighbors = empty_neighbor_cells(*start_loc)
          neighbors.each do |dest_loc|
            next if dests.member?(dest_loc)
            dests.add(dest_loc)
            starts.push([dest_loc, cost])

            moving_through_hallway ||= @hallway_locations.include?(start_loc)
            destination_is_a_room = !@hallway_locations.include?(dest_loc)
            destination_is_correct_room = room.include?(dest_loc)
            in_same_room = !starting_in_hallway && destination_is_a_room && location[1] == dest_loc[1]

            # Never move within a room if starting there and not leaving
            next if in_same_room

            # Amphipods will never stop on the space immediately outside any room
            next if room_entrances.include?(dest_loc)

            if destination_is_a_room
              # Amphipods will never move from the hallway into a room unless that room
              # is their destination room and that room contains no amphipods which do
              # not also have that room as their own destination.
              if destination_is_correct_room && room_includes_no_other_amphipod?(room, amphipod_type)
                # Never move into correct room and stop with space left empty
                dest_r, dest_c = dest_loc
                next if @cells[dest_r+1][dest_c] == '.'
              else
                next
              end
            else
              # Once an amphipod stops moving in the hallway, it will stay in that spot
              # until it can move into a room.
              next if starting_in_hallway
            end

            moves << Move.new(amphipod_type, mover_index, location, dest_loc, cost)
          end
        end
      end
    end
  end

  def available_moves(last_move=nil)
    [].tap do |moves|
      @amphipods.each do |amphipod_type, locations|
        moves.push(*available_moves_for(amphipod_type, locations, last_move))
      end
    end
  end

  def make_move(move)
    self.copy.tap do |new_burrow|
      new_burrow.cells[move.start[0]][move.start[1]] = '.'
      new_burrow.cells[move.dest[0]][move.dest[1]] = move.mover_type
      new_burrow.amphipods[move.mover_type][move.mover_index] = [move.dest[0], move.dest[1]]
    end
  end

  def Burrow.distance(s, g)
    distance = 0
    AMPHIPOD_ENERGY.keys.each do |type|
      s.amphipods[type].sort.zip(g.amphipods[type]).sort.each do |s_loc, g_loc|
        distance += ((s_loc[0] - g_loc[0]).abs + (s_loc[1] - g_loc[1]).abs)
      end
    end
    distance
  end

  def Burrow.h(s, g)
    min_cost = 0
    AMPHIPOD_ENERGY.each do |type, cost|
      s.amphipods[type].sort.zip(g.amphipods[type]).sort.each do |s_loc, g_loc|
        min_cost += ((s_loc[0] - g_loc[0]).abs + (s_loc[1] - g_loc[1]).abs) * cost
      end
    end
    min_cost
  end

  def Burrow.reconstruct_state_path(came_from, current)
    path = [current]
    while (current, _ = came_from[current])
      path.push(current)
    end
    path.reverse
  end

  def Burrow.is_loop_present?(came_from, current)
    Burrow.reconstruct_move_path(came_from, current).map(&:hash).tally.values.max > 1
  end

  def Burrow.reconstruct_move_path(came_from, current)
    path = []
    while (current, last_move = came_from[current])
      path.push(last_move)
    end
    path.reverse
  end

  def find_cheapest_path(verbose=false)
    start = self
    goal = self.goal_burrow

    infinity = AMPHIPOD_ENERGY.values.max * (width * height)**2

    came_from = {}
    g_score = Hash.new(infinity)
    f_score = Hash.new(infinity)

    g_score[start] = 0
    f_score[start] = Burrow.h(start, goal)

    seen_count = 0

    open_set = Set.new
    open_set.add(start)

    queue = PriorityQueue.new
    queue.push(start, f_score[start])

    while not queue.empty?
      current = queue.delete_min_return_key
      _, last_move = came_from[current]
      seen_count += 1
      if verbose
        puts "Seen: #{seen_count}"
        puts "Current Cost: #{g_score[current]}, Heuristic Cost: #{f_score[current] - g_score[current]}, Estimated Cost: #{f_score[current]}"
        puts "Current Distance: #{Burrow.distance(current, goal)}"
        puts Burrow.reconstruct_move_path(came_from, current).map(&:to_s).join('; ')
        puts current.to_s + "\n\n"
      else
        if seen_count % 10000 == 0
          puts "Seen: #{seen_count}"
          puts "Current Cost: #{g_score[current]}, Heuristic Cost: #{f_score[current] - g_score[current]}, Estimated Cost: #{f_score[current]}"
        end
      end

      open_set.delete(current)

      return [Burrow.reconstruct_move_path(came_from, current), g_score[current]] if current == goal

      moves = current.available_moves(last_move)
      moves_added = 0

      moves.each do |move|
        next_burrow = current.make_move(move)
        next if next_burrow.position_is_lost?(move)
        tentative_g_score = g_score[current] + move.cost
        if tentative_g_score < g_score[next_burrow]
          old_came_from = came_from[next_burrow]
          came_from[next_burrow] = [current, move]
          g_score[next_burrow] = tentative_g_score
          f_score[next_burrow] = tentative_g_score + Burrow.h(next_burrow, goal)

          if not open_set.member?(next_burrow)
            open_set.add(next_burrow)
            moves_added += 1
            queue.push(next_burrow, f_score[next_burrow])
          end
        end
      end

      if verbose
        if moves.empty?
          puts "Dead end node reached."
        else
          puts "Generated #{moves.length} moves."
          puts "Added #{moves_added} moves to open set."
        end
      end

    end

    puts "Failed to find solution."
    nil
  end

  def Burrow.parse(s)
    cells = s.split("\n").map {|l| l.split('')}

    amphipods = {}
    hallway_locations = []
    hallway_row = cells.find_index {|row| row.any? {|cell| cell != '#' && cell != ' '}}
    room_entrance_row = cells.find_index {|row| row.each_cons(3).any? {|c1,c2,c3| c1 == '#' && c2 != '#' && c3 == '#'}}
    room_end_row = room_entrance_row - 1 + cells[room_entrance_row..].
      find_index {|row| row.each_cons(3).none? {|c1,c2,c3| c1 == '#' && c2 != '#' && c3 == '#'}}

    all_room_locations = []
    cells.each_with_index do |row, r|
      row.each_with_index do |cell, c|
        if cells[r][c] != '#' && cells[r][c] != ' '
          if r >= hallway_row && r < room_entrance_row
            hallway_locations << [r,c]
          elsif r >= room_entrance_row && r <= room_end_row
            all_room_locations << [r,c]
          end
        end

        if cell =~ AMPHIPOD_MATCHER
          amphipods[cell] ||= []
          amphipods[cell] << [r,c]
        end
      end
    end

    room_locations = all_room_locations.sort_by(&:last).group_by(&:last).values.map(&:sort)

    Burrow.new(cells, amphipods, room_locations, hallway_locations)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  verbose = !args[2].nil?

  lines = IO.readlines(fname, chomp: true)
  if part == 2
    additional_lines = IO.readlines('additional_lines.dat', chomp: true)
    first_amphipods_index = lines.find_index {|l| l =~ Burrow::HAS_AMPHIPOD_MATCHER}
    lines.insert(first_amphipods_index+1, *additional_lines)
  end

  burrow = Burrow.parse(lines.join("\n"))
  starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  move_path, total_cost = burrow.find_cheapest_path(verbose)
  ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  elapsed = ending - starting
  puts "Cheapest Total Cost: #{total_cost}"
  puts "Found after #{elapsed} seconds"
  puts move_path.map(&:to_s).join('; ')
end

if __FILE__ == $0
  main(ARGV)
end
