#!/usr/bin/env ruby

class SnailFishNumber
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def to_s
    @value.to_s.gsub(' ', '')
  end

  def magnitude
    SnailFishNumber.magnitude(@value)
  end

  def reduce_number
    s = @value.to_s.gsub(' ','')
    steps = 0
    state = :exploding
    no_splits = false

    while true
      case state
      when :exploding
        new_s = SnailFishNumber.explode(s)
        if new_s == s
          state = no_splits ? :finished : :splitting
        else
          steps += 1
          eval new_s
          no_splits = false
        end
      when :splitting
        new_s = SnailFishNumber.split(s)
        if new_s == s
          no_splits = true
        else
          steps += 1
          eval new_s
        end
        state = :exploding
      else
        break
      end

      s = new_s
    end
    SnailFishNumber.parse(s)
  end

  def self.magnitude(value)
    if value.is_a?(Numeric)
      value
    else
      3*SnailFishNumber.magnitude(value[0]) + 2*SnailFishNumber.magnitude(value[1])
    end
  end

  def self.split(s)
    s = s.dup
    too_large_index = s.index(/(\d\d+)/)
    if too_large_index
      size = $1.length
      half = s[too_large_index,size].to_i / 2.0
      s[too_large_index,size] = "[#{half.floor},#{half.ceil}]"
    end
    s
  end

  def self.explode(s)
    s = s.dup
    left_brackets = 0

    explode_left_start_index = nil
    explode_left_end_index = nil
    explode_right_start_index = nil
    explode_right_end_index = nil
    left_start_index = nil
    left_end_index = nil
    right_start_index = nil
    right_end_index = nil
    $stop = false

    s.chars.each_with_index do |ch, index|
      #binding.pry if $stop
      if right_start_index
        if ch =~ /\d/
          right_end_index = index
        else
          break
        end
      elsif explode_right_end_index
        if ch =~ /\d/
          right_start_index = index
          right_end_index = index
        end
      elsif explode_right_start_index
        if ch !~ /\d/
          explode_right_end_index = index - 1
        end
      elsif explode_left_start_index
        if ch !~ /\d/
          explode_left_end_index = index - 1
          explode_right_start_index = index + 1
        end
      else
        if ch =~ /\d/
          if left_brackets >= 5
            explode_left_start_index = index
          elsif left_end_index || !left_start_index
            left_start_index = index
            left_end_index = nil
          end
        else
          if left_start_index &&  !left_end_index
            left_end_index = index - 1
          end
          if ch == '['
            left_brackets += 1
          elsif ch == ']'
            left_brackets -= 1
          end
        end
      end
    end

    if explode_left_start_index
      left_value = s[explode_left_start_index..explode_left_end_index].to_i
      right_value = s[explode_right_start_index..explode_right_end_index].to_i

      s[explode_left_start_index-1..explode_right_end_index+1] = "0"
      index_delta = explode_right_end_index + 1 - (explode_left_start_index - 1)
      if right_start_index
        right_start_index -= index_delta
        right_end_index -= index_delta
        right_value += s[right_start_index..right_end_index].to_i
        s[right_start_index..right_end_index] = right_value.to_s
      end

      if left_start_index
        left_value += s[left_start_index..left_end_index].to_i
        s[left_start_index..left_end_index] = left_value.to_s
      end
    end
    s
  end

  def +(n)
    SnailFishNumber.new([@value, n.value])
  end

  def self.parse(s)
    SnailFishNumber.new(eval s)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  numbers = IO.readlines(fname, chomp: true).map {|s| SnailFishNumber.parse(s)}

  if part == 1
    sum = numbers.reduce(nil) do |acc, e|
      if acc.nil?
        result = e
      else
        result = (acc + e).reduce_number
      end
      result
    end
    puts sum.magnitude
  else
    max_mag = -1
    numbers.each do |a|
      numbers.each do |b|
        if a != b
          mag1 = (a + b).reduce_number.magnitude
          mag2 = (b + a).reduce_number.magnitude
          max_mag = [max_mag, mag1, mag2].max
        end
      end
    end
    puts max_mag
  end
end

if __FILE__ == $0
  main(ARGV)
end
