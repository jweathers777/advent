#!/usr/bin/env ruby

def advance_day(fishes)
  {}.tap do |new_fishes|
    new_fishes[8] = fishes[0] || 0
    fishes.keys.each do |s|
      new_s = s == 0 ? 6 : (s - 1)
      new_fishes[new_s] ||= 0
      new_fishes[new_s] += fishes[s]
    end
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  verbose = args[2] == 'true'

  days = part == 1 ? 80 : 256

  fishes = IO.read(fname).strip.split(',').map(&:to_i).tally

  puts "Day 0: #{fishes.sort.to_h}" if verbose
  days.times do |day|
    fishes = advance_day(fishes)
    puts "Day #{day+1}: #{fishes.sort.reject {|k,v| v == 0}.to_h}" if verbose
  end
  puts fishes.values.sum
end

if __FILE__ == $0
  main(ARGV)
end
