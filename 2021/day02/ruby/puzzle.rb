#!/usr/bin/env ruby

class SimpleSubmarine
  attr_reader :horizontal_pos, :depth

  def initialize
    @horizontal_pos = 0
    @depth = 0
  end

  def forward(dist)
    @horizontal_pos += dist
  end

  def down(dist)
    @depth += dist
  end

  def up(dist)
    @depth -= dist
  end
end

class SubmarineWithAim < SimpleSubmarine
  attr_reader :aim

  def initialize
    super
    @aim = 0
  end

  def forward(x)
    @horizontal_pos += x
    @depth += x * @aim
  end

  def down(x)
    @aim += x
  end

  def up(x)
    @aim -= x
  end
end


def main(args)
  part = args[0].to_i
  fname = args[1]

  submarine = part == 1 ? SimpleSubmarine.new : SubmarineWithAim.new

  messages = IO.readlines(fname).map {|l| l.strip.split}.
    map {|msg| [msg[0], msg[1].to_i]}

  messages.each do |msg|
    submarine.send(*msg)
  end

  puts submarine.horizontal_pos * submarine.depth
end

if __FILE__ == $0
  main(ARGV)
end
