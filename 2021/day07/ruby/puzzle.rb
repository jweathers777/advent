#!/usr/bin/env ruby

def fuel_cost(v, z, inc_rate)
  units = (v-z).abs

  if inc_rate == 0
    units
  elsif inc_rate == 1
    units * (units+1) / 2
  else
    abort "Cost increase rate #{inc_rate} not implemented!"
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  inc_rate = part == 1 ? 0 : 1
  positions = IO.read(fname).strip.split(',').map(&:to_i)

  z,c = (positions.min..positions.max).
    map {|z| [z, positions.map {|v| fuel_cost(v, z, inc_rate)}.sum]}.
    min_by {|z,c| c}
  puts c
end

if __FILE__ == $0
  main(ARGV)
end
