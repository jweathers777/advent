#!/usr/bin/env ruby

require 'pry'
require 'pry-byebug'

class DeterministicD100
  attr_reader :rolls

  def initialize
    @next_roll_index = 0
    @rolls = 0
  end

  def roll
    (@next_roll_index + 1).tap do
      @rolls += 1
      @next_roll_index = (@next_roll_index + 1) % 100
    end
  end
end

class DiracDice
  attr_accessor :positions, :die, :scores, :turn, :status, :winner, :max_win_score

  def initialize(positions, die, max_win_score=1000)
    @positions = positions
    @die = die
    @max_win_score = max_win_score
    @scores = [0,0]
    @turn = 0
    @status = :playing
    @winner = nil
  end

  def hash_state
    @positions + @scores
  end

  def deep_clone
    self.dup.tap do |other|
      other.positions = self.positions.dup
      other.scores = self.scores.dup
    end
  end

  def is_over?
    @status == :finished
  end

  def loser
    @winner.nil? ? nil : 1 - @winner
  end

  def take_turn
    rolls = 3.times.map { die.roll }
    steps = rolls.sum
    @positions[@turn] = ((@positions[@turn] - 1) + steps) % 10 + 1
    @scores[@turn] += @positions[@turn]

    if @scores[@turn] >= @max_win_score
      @status = :finished
      @winner = @turn
    end
    @turn = 1 - @turn
  end
end

class QuantumDiracDice < DiracDice
  FACES = [1,2,3]
  ROLLS = FACES.product(FACES).product(FACES).map(&:flatten)
  STEPS_HASH = ROLLS.map(&:sum).tally

  def take_turn
    STEPS_HASH.map do |steps, count|
      uni = self.deep_clone
      uni.positions[uni.turn] = ((uni.positions[uni.turn] - 1) + steps) % 10 + 1
      uni.scores[uni.turn] += uni.positions[uni.turn]

      if uni.scores[uni.turn] >= uni.max_win_score
        uni.status = :finished
        uni.winner = uni.turn
      end
      uni.turn = 1 - uni.turn
      [uni, count]
    end
  end
end

class UniverseSearch
  class SearchNode
    attr_accessor :game, :count, :predecessors

    def initialize(game, count, predecessors)
      @game = game
      @count = count
      @predecessors = predecessors
    end
  end

  def initialize(game)
    @game = game
  end

  def explore_node(node, nodes, wins)
    game_state = node.game.hash_state
    wins[game_state] ||= [0, 0]

    if node.game.is_over?
      w = node.game.winner
      wins[game_state][w] = 1

      node.predecessors.reverse.each_with_index do |g, index|
        wins[g][w] += node.count
      end
    else
      next_predecessors = node.predecessors + [node.game.hash_state]
      depth = next_predecessors.length
      print (' ' * depth) + "#{depth} - " if $verbose
      children = node.game.take_turn.reverse
      children.each do |next_g, count|
        print "#{next_g.hash_state} (#{count})   " if $verbose
        nodes.push(SearchNode.new(next_g, node.count * count, next_predecessors))
      end
      puts "" if $verbose
    end
  end

  def search
    wins = {}

    nodes = []
    nodes.push(SearchNode.new(@game, 1, []))
    puts "0 - #{@game.hash_state}" if $verbose

    while not nodes.empty?
      node = nodes.pop
      explore_node(node, nodes, wins)
    end

    wins[@game.hash_state]
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  target = (args[2] || 21).to_i
  verbose = !args[3].nil?

  positions = IO.readlines(fname, chomp: true).map {|l| l.split(": ")[1].to_i}

  if part == 1
    game = DiracDice.new(positions, DeterministicD100.new)
    while not game.is_over?
      game.take_turn
    end
    puts game.scores[game.loser] * game.die.rolls
  else
    search_agent = UniverseSearch.new(QuantumDiracDice.new(positions, nil, target))

    starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    wins = search_agent.search
    ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    elapsed = ending - starting
    puts wins
    puts "Found after #{elapsed} seconds"
  end
end

if __FILE__ == $0
  main(ARGV)
end
