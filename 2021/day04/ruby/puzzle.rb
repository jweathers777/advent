#!/usr/bin/env ruby

class BingoBoard
  def initialize(rows)
    @rows = rows.map {|row| row.split.map {|v| [v.to_i, false]}}
  end

  def has_complete_row?
    @rows.any? {|row| row.all? {|col| col[1] == true}}
  end

  def has_complete_col?
    @rows[0].length.times.any? {|c| @rows.all? {|row| row[c][1] == true}}
  end

  def is_winner?
    has_complete_row? || has_complete_col?
  end

  def unmarked_sum
    @rows.reduce(0) {|a, row| a + row.reduce(0) {|ra,col| col[1] ? ra : (ra + col[0])}}
  end

  def is_marked?(row, col)
    @rows[row][col][1]
  end

  def mark_pos(row, col)
    @rows[row][col][1] = true
  end

  def find_number(value)
    @rows.each_with_index do |row, r|
      row.each_with_index do |col, c|
        return [r,c] if col[0] == value
      end
    end
    nil
  end

  def mark_number(value)
    pos = find_number(value)
    if pos
      mark_pos(pos[0], pos[1])
    end
  end
end

class Bingo
  def initialize(numbers, boards)
    @numbers = numbers
    @boards = boards
  end

  def score(last_call, board)
    last_call * board.unmarked_sum
  end

  def first_winning_score
    @numbers.each do |number|
      @boards.each do |board|
        board.mark_number(number)
        return score(number, board) if board.is_winner?
      end
    end
    -1
  end

  def last_winning_score
    non_winner_count = @boards.count {|b| not b.is_winner?}
    if non_winner_count > 0
      @numbers.each do |number|
        @boards.each do |board|
          unless board.is_winner?
            board.mark_number(number)
            if board.is_winner?
              non_winner_count -= 1
              return score(number, board) if non_winner_count == 0
            end
          end
        end
      end
    end
    -1
  end
end


def main(args)
  part = args[0].to_i
  fname = args[1]

  lines = IO.readlines(fname, chomp: true)

  numbers = lines.shift.split(',').map(&:to_i)
  boards = lines.reject {|l| l.empty?}.each_slice(5).map {|rows| BingoBoard.new(rows)}

  game = Bingo.new(numbers, boards)

  if part == 1
    puts game.first_winning_score
  else
    puts game.last_winning_score
  end
end

if __FILE__ == $0
  main(ARGV)
end
