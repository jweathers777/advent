#!/usr/bin/env ruby

class Graph
  def initialize(s)
    @edges = s.split("\n").map {|l| l.split('-')}
  end

  def is_big_cave?(node)
    node[0] == node[0].upcase
  end

  def find_valid_paths(allow_second_visits=false)
    frontier = []
    frontier.push([true, 'start'])
    paths = []
    $stop = false

    while frontier.length > 0
      path = frontier.pop
      src = path.last
      if src == 'end'
        path.shift
        paths.push(path)
      else
        @edges.select {|e| e.include?(src)}.map {|e| e - [src]}.flatten.each do |dest|
          next if dest == 'start'
          visits = path.count(dest)
          next_path = path + [dest]
          if is_big_cave?(dest)
            visit = true
          else
            if visits < 1
              visit = true
            elsif allow_second_visits && path.first && visits < 2
              visit = true
              next_path[0] = false
            else
              visit = false
            end
          end

          frontier.push(next_path) if visit
        end
      end
    end
    paths
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  graph = Graph.new(IO.read(fname))
  puts graph.find_valid_paths(part == 2).length
end

if __FILE__ == $0
  main(ARGV)
end
