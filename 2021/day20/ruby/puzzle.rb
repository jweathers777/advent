#!/usr/bin/env ruby

def extend_image(img, default_pixel='.')
  width = img.first.length + 4
  [].tap do |extended|
    2.times { extended << [default_pixel]*width }
    img.each {|row| extended << [default_pixel]*2 + row + [default_pixel]*2}
    2.times { extended << [default_pixel]*width }
  end
end

def crop_image(img, default_pixel='.')
  cropped = img.drop_while {|row| row.uniq == [default_pixel]}
  cropped = cropped.reverse.drop_while {|row| row.uniq == [default_pixel]}.reverse
  cols = (0...cropped.first.length).drop_while {|i| cropped.map {|row| row[i]}.uniq == [default_pixel]}
  cols = cols.reverse.drop_while {|i| cropped.map {|row| row[i]}.uniq == [default_pixel]}.reverse
  cropped.map {|row| cols.map {|c| row[c]}}
end

def enhance_image(img_enh_alg, img, default_pixel='.')
  extended = extend_image(img, default_pixel)
  height = extended.length
  width = extended.first.length

  if default_pixel == '.'
    default_pixel = img_enh_alg[0]
  else
    default_pixel = img_enh_alg[-1]
  end

  enhanced = Array.new(height) { Array.new(width) { default_pixel } }
  extended.each_with_index do |row,r|
    next if r < 1 || r > height - 2
    row.each_with_index do |pixel,c|
      next if c < 1 || c > width - 2
      s = ""
      (r-1..r+1).each do |rp|
        (c-1..c+1).each do |cp|
          s << (extended[rp][cp] == '.' ? '0' : '1')
        end
      end
      index = s.to_i(2)
      enhanced[r][c] = img_enh_alg[index]
    end
  end
  [crop_image(enhanced, default_pixel), default_pixel]
end

def puts_img(img)
  puts img.map(&:join).join("\n") + "\n\n"
end

def corner_img(img, size)
  img[0,size].map {|row| row[0,size]}
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  img_enh_alg, s = IO.read(fname).split("\n\n")
  img = s.split("\n").map(&:chars)

  iterations = part == 1 ? 2 : 50

  default_pixel = '.'
  iterations.times do
    img, default_pixel = enhance_image(img_enh_alg, img, default_pixel)
  end

  puts img.map {|row| row.count('#')}.sum
end

if __FILE__ == $0
  main(ARGV)
end
