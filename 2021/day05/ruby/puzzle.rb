#!/usr/bin/env ruby

class LineSegment
  attr_reader :x1,:y1,:x2,:y2

  def initialize(s)
    p1,p2 = s.split(' -> ').map {|t| t.split(',').map(&:to_i)}
    @x1,@y1 = p1
    @x2,@y2 = p2
  end

  def points
    x_delta = @x2 - @x1
    y_delta = @y2 - @y1

    if x_delta == 0 and y_delta == 0
      return [@x1, @y1]
    elsif x_delta == 0
      dir = y_delta / y_delta.abs
      steps = y_delta.abs
      (0..steps).map {|d| [@x1, @y1 + dir*d]}
    elsif y_delta == 0
      dir = x_delta / x_delta.abs
      steps = x_delta.abs
      (0..steps).map {|d| [@x1 + dir*d, @y1]}
    elsif x_delta.abs == y_delta.abs
      x_dir = x_delta / x_delta.abs
      y_dir = y_delta / y_delta.abs
      steps = x_delta.abs
      (0..steps).map {|d| [@x1 + x_dir*d, @y1 + y_dir*d]}
    else
      abort "Not implemened for diagonals that are not 45 degrees!"
    end
  end

  def x_min
    [@x1,@x2].min
  end

  def x_max
    [@x1,@x2].max
  end

  def y_min
    [@y1,@y2].min
  end

  def y_max
    [@y1,@y2].max
  end

  def is_vertical?
    @x1 == @x2
  end

  def is_horizontal?
    @y1 == @y2
  end

  def is_diagonal_45?
    (x_max - x_min) == (y_max - y_min)
  end
end

class Grid
  def initialize
    @cells = {}
  end

  def [](x,y)
    @cells[x] ||= {}
    @cells[x][y] || 0
  end

  def []=(x,y,value)
    @cells[x] ||= {}
    @cells[x][y] ||= 0
    @cells[x][y] = value
  end

  def add_segment(segment)
    segment.points.each {|x,y| self[x,y] += 1}
  end

  def values
    @cells.values.map(&:values).flatten
  end

  def count
    if block_given?
      values.count {|value| yield value }
    else
      values.length
    end
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = Grid.new
  segments = IO.readlines(fname, chomp: true).map {|l| LineSegment.new(l)}

  if part == 1
    segments.select! {|s| s.is_horizontal? || s.is_vertical?}
  end

  segments.each {|segment| grid.add_segment(segment)}

  puts grid.count {|v| v >= 2}
end

if __FILE__ == $0
  main(ARGV)
end
