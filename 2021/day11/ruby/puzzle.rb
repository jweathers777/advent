#!/usr/bin/env ruby

class OctupusGrid
  attr_accessor :flashes

  def initialize(s)
    @grid = s.split("\n").map {|l| l.split('').map(&:to_i)}
    @rows_num = @grid.length
    @cols_num = @grid.first.length
    @cell_num = @rows_num * @cols_num
    @flashes = 0
  end

  def size
    @cell_num
  end

  def to_s
    @grid.map {|row| row.map(&:to_s).join}.join("\n")
  end

  def neighbors(r,c)
    [].tap do |ns|
      ns << [r-1, c] if r > 0 # Left
      ns << [r+1, c] if r < @rows_num - 1 # Right
      ns << [r, c-1] if c > 0 # Up
      ns << [r, c+1] if c < @cols_num - 1 # Down
      ns << [r-1, c-1] if r > 0 && c > 0 # LeftUp
      ns << [r+1, c-1] if r < @rows_num - 1 && c > 0 # RightUp
      ns << [r-1, c+1] if r > 0 && c < @cols_num - 1 # LeftDown
      ns << [r+1, c+1] if r < @rows_num - 1 && c < @cols_num - 1 # RightDown
    end
  end

  def advance_step
    wave = []
    flashes_added = 0
    @rows_num.times do |r|
      @cols_num.times do |c|
        @grid[r][c] += 1
        wave.push([r,c]) if @grid[r][c] > 9
      end
    end

    while wave.length > 0
      next_wave = []
      wave.each do |r,c|
        @flashes += 1
        flashes_added += 1
        @grid[r][c] = 0
        neighbors(r,c).each do |nr,nc|
          if @grid[nr][nc] > 0 && @grid[nr][nc] <= 9
            @grid[nr][nc] += 1
            next_wave.push([nr,nc]) if @grid[nr][nc] > 9
          end
        end
      end
      wave = next_wave
    end

    flashes_added
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  grid = OctupusGrid.new(IO.read(fname))

  if part == 1
    100.times { grid.advance_step }
    puts grid.flashes
  else
    step = 0
    flashes_added = 0
    while flashes_added != grid.size
      flashes_added = grid.advance_step
      step += 1
    end
    puts step
  end
end

if __FILE__ == $0
  main(ARGV)
end
