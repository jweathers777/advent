#!/usr/bin/env ruby

require 'matrix'
require 'set'

class SymFourGroup
  ELEMENTS = {
    :I=>Matrix[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
    :X90=>Matrix[[1, 0, 0], [0, 0, -1], [0, 1, 0]],
    :X90_2=>Matrix[[1, 0, 0], [0, -1, 0], [0, 0, -1]],
    :X90_3=>Matrix[[1, 0, 0], [0, 0, 1], [0, -1, 0]],
    :Y90=>Matrix[[0, 0, 1], [0, 1, 0], [-1, 0, 0]],
    :Y90_2=>Matrix[[-1, 0, 0], [0, 1, 0], [0, 0, -1]],
    :Y90_3=>Matrix[[0, 0, -1], [0, 1, 0], [1, 0, 0]],
    :Z90=>Matrix[[0, -1, 0], [1, 0, 0], [0, 0, 1]],
    :Z90_2=>Matrix[[-1, 0, 0], [0, -1, 0], [0, 0, 1]],
    :Z90_3=>Matrix[[0, 1, 0], [-1, 0, 0], [0, 0, 1]],
    :X90_Y90=>Matrix[[0, 0, 1], [1, 0, 0], [0, 1, 0]],
    :X90_Y90_2=>Matrix[[-1, 0, 0], [0, 0, 1], [0, 1, 0]],
    :X90_Y90_3=>Matrix[[0, 0, -1], [-1, 0, 0], [0, 1, 0]],
    :X90_Z90=>Matrix[[0, -1, 0], [0, 0, -1], [1, 0, 0]],
    :X90_Z90_2=>Matrix[[-1, 0, 0], [0, 0, -1], [0, -1, 0]],
    :X90_Z90_3=>Matrix[[0, 1, 0], [0, 0, -1], [-1, 0, 0]],
    :X90_2_Y90=>Matrix[[0, 0, 1], [0, -1, 0], [1, 0, 0]],
    :X90_2_Y90_3=>Matrix[[0, 0, -1], [0, -1, 0], [-1, 0, 0]],
    :X90_2_Z90=>Matrix[[0, -1, 0], [-1, 0, 0], [0, 0, -1]],
    :X90_2_Z90_3=>Matrix[[0, 1, 0], [1, 0, 0], [0, 0, -1]],
    :X90_3_Y90=>Matrix[[0, 0, 1], [-1, 0, 0], [0, -1, 0]],
    :X90_3_Y90_3=>Matrix[[0, 0, -1], [1, 0, 0], [0, -1, 0]],
    :X90_3_Z90=>Matrix[[0, -1, 0], [0, 0, 1], [-1, 0, 0]],
    :X90_3_Z90_3=>Matrix[[0, 1, 0], [0, 0, 1], [1, 0, 0]]
  }

  INVERSES = {
    :I=>:I,
    :X90=>:X90_3,
    :X90_2=>:X90_2,
    :X90_3=>:X90,
    :Y90=>:Y90_3,
    :Y90_2=>:Y90_2,
    :Y90_3=>:Y90,
    :Z90=>:Z90_3,
    :Z90_2=>:Z90_2,
    :Z90_3=>:Z90,
    :X90_Y90=>:X90_3_Z90_3,
    :X90_Y90_2=>:X90_Y90_2,
    :X90_Y90_3=>:X90_3_Z90,
    :X90_Z90=>:X90_3_Y90,
    :X90_Z90_2=>:X90_Z90_2,
    :X90_Z90_3=>:X90_3_Y90_3,
    :X90_2_Y90=>:X90_2_Y90,
    :X90_2_Y90_3=>:X90_2_Y90_3,
    :X90_2_Z90=>:X90_2_Z90,
    :X90_2_Z90_3=>:X90_2_Z90_3,
    :X90_3_Y90=>:X90_Z90,
    :X90_3_Y90_3=>:X90_Z90_3,
    :X90_3_Z90=>:X90_Y90_3,
    :X90_3_Z90_3=>:X90_Y90
  }

  def self.apply_element(element, v)
    (SymFourGroup::ELEMENTS[element] * Matrix.column_vector(v)).to_a.flatten
  end
end

def add_points(a, b)
  x0,y0,z0 = a
  x1,y1,z1 = b
  [x0+x1, y0+y1, z0+z1]
end

def subtract_points(a, b)
  x0,y0,z0 = a
  x1,y1,z1 = b
  [x0-x1, y0-y1, z0-z1]
end

class Scanner
  attr_reader :label, :beacons

  def initialize(label, beacons)
    @label = label
    @beacons = beacons
  end

  def apply_rotation(element)
    rotated_beacons = @beacons.map do |b|
      SymFourGroup.apply_element(element, b)
    end
    Scanner.new(@label, rotated_beacons)
  end

  def subtract_vector(v)
    translated_beacons = @beacons.map {|b| subtract_points(b, v)}
    Scanner.new(@label, translated_beacons)
  end

  def add_vector(v)
    translated_beacons = @beacons.map {|b| add_points(b, v)}
    Scanner.new(@label, translated_beacons)
  end

  def find_overlap(scanner)
    @beacons.each do |pivot_beacon|
      pivoted_beacons = @beacons.map {|b| subtract_points(b, pivot_beacon)}
      scanner.beacons.each_with_index do |other_pivot_beacon, index|
        other_pivoted_beacons = scanner.beacons.map {|b| subtract_points(b, other_pivot_beacon)}
        SymFourGroup::ELEMENTS.keys.each do |element|
          transformed_other_beacons = other_pivoted_beacons.map {|b| SymFourGroup.apply_element(element, b)}
          overlap = pivoted_beacons.intersection(transformed_other_beacons)
          if overlap.length >= 12
            rotation = SymFourGroup::INVERSES[element]
            abs_overlap = overlap.map {|b| add_points(b, pivot_beacon)}
            rel_overlap = overlap.map {|b| add_points(SymFourGroup.apply_element(rotation, b), other_pivot_beacon)}
            abs_pt = abs_overlap[0]
            rel_pt = rel_overlap[0]
            abs_scanner_pos = subtract_points(abs_pt, SymFourGroup.apply_element(element, rel_pt))
            return [abs_overlap, rel_overlap, abs_scanner_pos, rotation]
          end
        end
      end
    end
    nil
  end

  def transform(element, pos)
    self.apply_rotation(SymFourGroup::INVERSES[element]).add_vector(pos)
  end

  def self.parse(s)
    lines = s.split("\n")
    label_line = lines.shift
    label_line =~ /scanner (\d+)/

    label = $1.to_i
    beacons = lines.map {|l| l.split(",").map(&:to_i)}
    Scanner.new(label, beacons)
  end
end

def manhatten_distance(p,q)
  x0,y0,z0 = p
  x1,y1,z1 = q
  (x0-x1).abs + (y0-y1).abs + (z0-z1).abs
end

def parse_points(s)
  s.split("\n").map {|l| l.split(",").map(&:to_i)}
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  scanners = IO.read(fname).split("\n\n").map {|s| Scanner.parse(s)}

  i = 0
  if part == 1
    beacons = Set.new()
    scanners[i].beacons.each {|b| beacons.add(b)}
  else
    scanner_positions = [[0,0,0]]
  end

  indices = [i]
  processed = Set.new()
  not_processed = Set.new(1...scanners.length)
  while !indices.empty?
    i = indices.pop
    puts "i=#{i}"
    processed.add(i)
    scanner = scanners[i]
    not_processed.each do |j|
      next if processed.member?(j)
      other_scanner = scanners[j]
      result = scanner.find_overlap(other_scanner)
      if result
        puts "j=#{j}+"
        puts "#{not_processed.length} left to process"
        _, _, abs_scanner_pos, rotation = result
        scanners[j] = scanners[j].transform(rotation, abs_scanner_pos)
        if part == 1
          scanners[j].beacons.each {|b| beacons.add(b)}
        else
          scanner_positions.push(abs_scanner_pos)
        end
        not_processed.delete(j)
        indices.push(j)
      else
        puts "j=#{j}"
      end
    end
  end

  if part == 1
    puts beacons.length
  else
    max_distance = -1
    scanner_positions.each do |p1|
      scanner_positions.each do |p2|
        max_distance = [manhatten_distance(p1,p2), max_distance].max
      end
    end
    puts max_distance
  end
end

if __FILE__ == $0
  main(ARGV)
end
