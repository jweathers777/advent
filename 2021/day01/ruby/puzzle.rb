#!/usr/bin/env ruby

def count_increases(seq)
  seq.each_cons(2).count {|a,b| b > a}
end

def triple_sums(seq)
  seq.each_cons(3).map(&:sum)
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  depths = IO.readlines(fname).map {|l| l.strip.to_i}
  result = part == 1 ? count_increases(depths) : count_increases(triple_sums(depths))
  puts result
end

if __FILE__ == $0
  main(ARGV)
end
