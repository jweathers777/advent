#!/usr/bin/env ruby

OPEN_CHARS = "([{<".chars
CLOSE_CHARS = ")]}>".chars
OPEN_TO_CLOSE = OPEN_CHARS.zip(CLOSE_CHARS).to_h

CORRUPTED_SCORE_TABLE = {
  ')' => 3,
  ']' => 57,
  '}' => 1197,
  '>' => 25137
}

COMPLETION_SCORE_TABLE = {
  ')' => 1,
  ']' => 2,
  '}' => 3,
  '>' => 4
}

def completion_score(chrs)
  chrs.reduce(0) {|score,ch| 5*score + COMPLETION_SCORE_TABLE[ch]}
end

def validate_chunks(s)
  open_chunks = []
  s.chars.each do |ch|
    if OPEN_CHARS.include?(ch)
      open_chunks.push(ch)
    elsif CLOSE_CHARS.include?(ch)
      if OPEN_TO_CLOSE[open_chunks.last] == ch
        open_chunks.pop
      else
        return [:CORRUPTED, ch, open_chunks]
      end
    else
      return [:CORRUPTED, ch, open_chunks]
    end
  end

  if open_chunks.empty?
    [:VALID, nil, open_chunks]
  else
    [:INCOMPLETE, nil, open_chunks]
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  lines = IO.readlines(fname, chomp: true)
  if part == 1
    corrupted = lines.map {|l| validate_chunks(l)}.select {|st,_,_| st == :CORRUPTED}
    score = corrupted.map {|_,ch,_| CORRUPTED_SCORE_TABLE[ch]}.sum
  else
    incomplete = lines.map {|l| validate_chunks(l)}.select {|st,_,_| st == :INCOMPLETE}
    middle_index = (incomplete.length + 1)/2 - 1
    closing = incomplete.map {|_,_,ochrs| ochrs.reverse.map {|ch| OPEN_TO_CLOSE[ch]}}
    score = closing.map {|chrs| completion_score(chrs)}.sort[middle_index]
  end

  puts score
end

if __FILE__ == $0
  main(ARGV)
end
