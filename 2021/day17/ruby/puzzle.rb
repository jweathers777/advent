#!/usr/bin/env ruby

def next_state(state)
  pos, vel = state
  x,y = pos
  vx,vy = vel

  next_pos = [x+vx, y+vy]
  if vx > 0
    vx_delta = -1
  elsif vx < 0
    vx_delta = 1
  else
    vx_delta = 0
  end
  next_vel = [vx+vx_delta, vy-1]

  [next_pos, next_vel]
end

def in_target_area(state, target_area)
  pos, _ = state
  x, y = pos
  x_range, y_range = target_area
  x_range.include?(x) && y_range.include?(y)
end

def beyond_target_area(state, target_area)
  pos, _ = state
  x, y = pos
  x_range, y_range = target_area
  x_range.last < x || y_range.first > y
end

def hits_target_area(start, target_area)
  state = start
  hits = false
  max_y = 0
  while not beyond_target_area(state, target_area)
    hits = in_target_area(state, target_area)
    max_y = [state[0][1], max_y].max
    state = next_state(state)
  end
  [hits, max_y]
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  target_area = IO.read(fname).split(': ')[-1].split(', ').
    map {|e| Range.new(*e.split('=')[-1].split('..').map(&:to_i))}

  if part == 1
    max_y = 0
    (0..target_area[0].last).each do |vx|
      (0..target_area[0].last).each do |vy|
        hits, current_max_y = hits_target_area([[0,0],[vx,vy]], target_area)
        if hits && current_max_y > max_y
          max_y = current_max_y
        end
      end
    end
    puts max_y
  else
    count = 0
    (0..target_area[0].last).each do |vx|
      ((target_area[1].first)..([target_area[0].last, target_area[1].last].max)).each do |vy|
        hits, _ = hits_target_area([[0,0],[vx,vy]], target_area)
        count += 1 if hits
      end
    end
    puts count
  end
end

if __FILE__ == $0
  main(ARGV)
end
