#!/usr/bin/env ruby

class Packet
  include Enumerable

  attr_reader :binary, :version, :type_id, :base_value, :subpackets, :value

  def initialize(binary, version, type_id, base_value, subpackets)
    @binary = binary
    @version = version
    @type_id = type_id
    @base_value = base_value
    @subpackets = subpackets

    @value = case @type_id
             when 0
               self.subpackets.map {|sp| sp.value}.sum
             when 1
               self.subpackets.map {|sp| sp.value}.reduce(&:*)
             when 2
               self.subpackets.map {|sp| sp.value}.min
             when 3
               self.subpackets.map {|sp| sp.value}.max
             when 4
               @base_value
             when 5
               self.subpackets[0].value > self.subpackets[1].value ? 1: 0
             when 6
               self.subpackets[0].value < self.subpackets[1].value ? 1: 0
             when 7
               self.subpackets[0].value == self.subpackets[1].value ? 1: 0
             else
               abort "Unknown type id #{@type_id}!"
             end
  end

  def each
    packets = [self]
    while packets.length > 0
      packet = packets.pop
      yield packet
      packet.subpackets.each {|sp| packets.push(sp)}
    end
  end

  def Packet.parse_hex(hex)
    binary = hex.to_i(16).to_s(2).rjust(hex.length*4, '0')
    Packet.parse_binary(binary)
  end

  def Packet.parse_binary(binary)
    version = binary[0..2].to_i(2)
    type_id = binary[3..5].to_i(2)
    base_value = nil
    subpackets = []

    case type_id
    when 4
      groups = binary[6..].chars.each_slice(5).to_a
      last_index = groups.find_index {|g| g[0] == '0'}
      base_value = groups[0..last_index].map {|g| g[1..]}.join.to_i(2)
      extra = groups[last_index+1..].join
    else
      length_type_id = binary[6].to_i
      if length_type_id == 0
        total_sub_packet_bit_length = binary[7..21].to_i(2)
        subpacket_binary = binary[22..]
        remainder = total_sub_packet_bit_length
        extra = ""
        while remainder > 0
          subpacket, extra = Packet.parse_binary(subpacket_binary)
          remainder -= subpacket.binary.length
          subpackets.push(subpacket)
          subpacket_binary = extra
        end
      else # == 1
        num_contained_sub_packets = binary[7..17].to_i(2)
        subpacket_binary = binary[18..]
        remaining = num_contained_sub_packets
        extra = ""
        while remaining > 0
          subpacket, extra = Packet.parse_binary(subpacket_binary)
          remaining -= 1
          subpackets.push(subpacket)
          subpacket_binary = extra
        end
      end
    end
    binary.sub!(/#{extra}/,'')
    [Packet.new(binary, version, type_id, base_value, subpackets), extra]
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  s = IO.read(fname)
  root_packet, extra = Packet.parse_hex(s)

  if part == 1
    puts root_packet.reduce(0) {|acc,p| acc + p.version}
  else
    puts root_packet.value
  end
end

if __FILE__ == $0
  main(ARGV)
end
