#!/usr/bin/env ruby

def most_common_bit(index, values)
  values.group_by {|b| b[index]}.max_by {|_,b| b.length}[0]
end

def least_common_bit(index, values)
  values.group_by {|b| b[index]}.min_by {|_,b| b.length}[0]
end

def gamma_rate(values)
  values.length.times.map {|index| most_common_bit(index, values)}.join('').to_i(2)
end

def epsilon_rate(values)
  values.length.times.map {|index| least_common_bit(index, values)}.join('').to_i(2)
end

def power_consumption(values)
  gamma_rate(values) * epsilon_rate(values)
end

def matching_most_common_bit(index, values)
  matches = values.group_by {|b| b[index]}.max_by {|_,b| b.length}[1]
  if 2*matches.length == values.length
    values.select {|b| b[index] == "1"}
  else
    matches
  end
end

def matching_least_common_bit(index, values)
  matches = values.group_by {|b| b[index]}.min_by {|_,b| b.length}[1]
  if 2*matches.length == values.length
    values.select {|b| b[index] == "0"}
  else
    matches
  end
end

def oxygen_generator_rating(values)
  n = values.length
  index = 0
  while values.length > 1 and index < n
    values = matching_most_common_bit(index, values)
    index += 1
  end
  values[0].to_i(2)
end

def co2_scrubber_rating(values)
  n = values.length
  index = 0
  while values.length > 1 and index < n
    values = matching_least_common_bit(index, values)
    index += 1
  end
  values[0].to_i(2)
end

def life_support_rating(values)
  oxygen_generator_rating(values) * co2_scrubber_rating(values)
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  values = IO.readlines(fname, chomp: true)
  if part == 1
    puts power_consumption(values)
  else
    puts life_support_rating(values)
  end
end

if __FILE__ == $0
  main(ARGV)
end
