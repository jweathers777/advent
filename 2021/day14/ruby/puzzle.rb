#!/usr/bin/env ruby

class PolymerProcess
  attr_reader :state

  def initialize(s)
    lines = s.split("\n")
    @template = lines.shift
    lines.shift
    @state = @template.chars.each_cons(2).map(&:join).tally.sort.to_h
    @rules = lines.map {|l| l.split(' -> ')}.to_h
  end

  def transform(state)
    {}.tap do |new_state|
      state.keys.each do |pair|
        insert = @rules[pair]
        [pair[0]+insert, insert+pair[1]].each do |new_pair|
          new_state[new_pair] ||= 0
          new_state[new_pair] += state[pair]
        end
      end
    end
  end

  def advance_step
    @state = transform(@state)
  end

  def counts
    {}.tap do |h|
      h[@template[-1]] = 1
      state.each do |pair, count|
        h[pair[0]] ||= 0
        h[pair[0]] += count
      end
    end
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  polymer_process = PolymerProcess.new(IO.read(fname))

  steps = part == 1 ? 10 : 40

  steps.times { |i| polymer_process.advance_step }
  counts = polymer_process.counts
  most_common_count = counts.max_by {|k,v| v}.last
  least_common_count = counts.min_by {|k,v| v}.last

  puts most_common_count - least_common_count
end

if __FILE__ == $0
  main(ARGV)
end
