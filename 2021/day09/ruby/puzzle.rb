#!/usr/bin/env ruby

require 'set'

class HeightMap
  DIRECTIONS = [:up, :down, :left, :right]

  def initialize(g)
    @grid = g
    @rows = g.length
    @cols = g.first.length
  end

  def up(r,c)
    r > 0 ?  [r-1, c] : nil
  end

  def down(r,c)
    r < @rows - 1 ?  [r+1, c] : nil
  end

  def left(r,c)
    c > 0 ? [r, c-1] : nil
  end

  def right(r,c)
    c < @cols - 1 ?  [r, c+1] : nil
  end

  def neighbors(r,c)
    DIRECTIONS.map {|dir| self.send(dir, r, c)}.compact
  end

  def higher_neighbors(r,c)
    h = @grid[r][c]
    neighbors(r,c).select {|nr,nc| @grid[nr][nc] > h}
  end

  def low_points
    [].tap do |points|
      @rows.times do |r|
        @cols.times do |c|
          h = @grid[r][c]
          if neighbors(r,c).all? {|nr,nc| @grid[nr][nc] > h}
            points << [r,c]
          end
        end
      end
    end
  end

  def basins
    [].tap do |basins|
      low_points.each do |r,c|
        nodes = [[r,c]]
        basin = Set.new
        while nodes.length > 0
          r,c = nodes.pop
          basin.add([r,c])
          higher_neighbors(r,c).reject {|nr,nc| @grid[nr][nc] == 9 || basin.member?([nr, nc])}.
            each {|nr,nc| nodes.push([nr,nc])}
        end
        basins << basin
      end
    end
  end

  def risk_level(r,c)
    @grid[r][c] + 1
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  height_map = HeightMap.new(IO.readlines(fname, chomp: true).map {|l| l.split('').map(&:to_i)})

  if part == 1
    puts height_map.low_points.map {|r,c| height_map.risk_level(r,c)}.sum
  else
    puts height_map.basins.map(&:size).sort.reverse[0...3].reduce(&:*)
  end
end

if __FILE__ == $0
  main(ARGV)
end
