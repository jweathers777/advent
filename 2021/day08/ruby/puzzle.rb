#!/usr/bin/env ruby

require 'set'

class Entry
  attr_reader :signals, :digits

  SEGMENTS = ('a'..'g').to_a

  DIGITS_TO_SEGMENT_COUNT = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6]
  SEGMENT_COUNT_TO_DIGITS = DIGITS_TO_SEGMENT_COUNT.each_with_index.
    group_by {|e,i| e}.
    map {|k,v| [k, v.map {|e,i| i}]}.
    to_h

  UNIQ_COUNTS = [1,4,7,8].map {|i| DIGITS_TO_SEGMENT_COUNT[i]}

  DIGIT_MAPPING = [
    "abcefg",
    "cf",
    "acdeg",
    "acdfg",
    "bcdf",
    "abdfg",
    "abdefg",
    "acf",
    "abcdefg",
    "abcdfg"
  ].map {|s| Set.new(s.chars)}

  STRING_TO_DIGIT = DIGIT_MAPPING.each_with_index.map {|s,i| [s.sort.join,i]}.to_h

  def initialize(signals, digits)
    @signals = signals
    @digits = digits
  end

  def self.parse(s)
    Entry.new(*s.split(' | ').map(&:split))
  end

  def mapping
    @mapping ||= deduce_mapping
  end

  def translate_digit(digit)
    digit.tr(mapping.keys.join, mapping.values.join).chars.sort.join
  end

  def reduce_with_balanced_maps(balanced_maps, m)
    reduced = m
    balanced_maps.keys.each do |key|
      val = balanced_maps[key].first
      reduced = reduced.map do |k,vs|
        if key != k && key.subset?(k)
          [k - key, vs.select {|v| val.subset?(v) }.map {|v| v - val}]
        else
          [k, vs]
        end
      end
    end
    reduced.to_h
  end

  def find_balanced_maps(m)
    m.select {|k,vs| vs.length == 1 && vs.first.length == k.length}.to_h
  end

  def is_one_to_one_mapping?(m)
    m.all? {|k,v| k.size == 1 && v.size == 1 && v.first.size == 1}
  end

  def deduce_mapping
    sorted_signals = signals.sort_by {|s| s.length}.map {|s| Set.new(s.chars)}
    current_mapping = sorted_signals.map {|x| [x, DIGIT_MAPPING.select {|y| x.length == y.length}]}.to_h
    while not is_one_to_one_mapping?(current_mapping)
      current_mapping = reduce_with_balanced_maps(find_balanced_maps(current_mapping), current_mapping)
    end
    @mapping = current_mapping.map {|k,v| [k.first, v.first.first]}.to_h
  end

  def output_value
    digits.map {|digit| STRING_TO_DIGIT[translate_digit(digit)]}.map(&:to_s).join.to_i
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]


  entries = IO.readlines(fname, chomp: true).map {|l| Entry.parse(l)}

  if part == 1
    puts entries.map {|e| e.digits.count {|d| Entry::UNIQ_COUNTS.include?(d.length)}}.sum
  else
    puts entries.map {|e| e.output_value}.sum
  end
end

if __FILE__ == $0
  main(ARGV)
end
