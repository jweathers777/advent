#!/usr/bin/env ruby

class Region
  attr_reader :x_range, :y_range, :z_range

  def initialize(x_range, y_range, z_range)
    @x_range = x_range
    @y_range = y_range
    @z_range = z_range
  end

  def ranges
    [@x_range, @y_range, @z_range]
  end

  def size
    @x_range.size * @y_range.size * @z_range.size
  end

  def empty?
    size == 0
  end

  def intersection(other_region)
    shared_ranges = ranges.zip(other_region.ranges).map do |range, other_range|
      range_intersection(range, other_range)
    end
    Region.new(*shared_ranges)
  end

  def subtract(other_region)
    [].tap do |pieces|
      x_difference = range_subtract(@x_range, other_region.x_range)
      unless x_difference.empty?
        x_difference.each do |diff|
          pieces << Region.new(diff, @y_range, @z_range) unless diff.first > diff.last
        end
      end

      y_difference = range_subtract(@y_range, other_region.y_range)
      unless y_difference.empty?
        y_difference.each do |diff|
          pieces << Region.new(other_region.x_range, diff, @z_range) unless diff.first > diff.last
        end
      end

      z_difference = range_subtract(@z_range, other_region.z_range)
      unless z_difference.empty?
        z_difference.each do |diff|
          pieces << Region.new(other_region.x_range, other_region.y_range, diff) unless diff.first > diff.last
        end
      end
    end
  end

  private
  def range_intersection(r1, r2)
    u1,u2 = r1.first, r1.last
    v1,v2 = r2.first, r2.last

    if u2 < v1 || v2 < u1
      (1..0)
    elsif u1 <= v1 && v2 <= u2
      Range.new(v1, v2)
    elsif v1 <= u1 && u2 <= v2
      Range.new(u1, u2)
    elsif u1 <= v1
      Range.new(v1, u2)
    else # v1 <= u1
      Range.new(u1, v2)
    end
  end

  private
  def range_subtract(r1, r2)
    u1,u2 = r1.first, r1.last
    v1,v2 = r2.first, r2.last

    if u2 < v1 || v2 < u1
      [r1]
    elsif u1 == v1 && v2 == u2
      []
    elsif u1 == v1 && v2 < u2
      [Range.new(v2+1,u2)]
    elsif u1 < v1 && v2 == u2
      [Range.new(u1, v1-1)]
    elsif u1 < v1 && v2 < u2
      [Range.new(u1, v1-1), Range.new(v2+1,u2)]
    elsif v1 <= u1 && u2 <= v2
      []
    elsif u1 <= v1
      if u1 == v1
        []
      else
        [Range.new(u1, v1-1)]
      end
    else # v1 <= u1
      if v2 == u2
        []
      else
        [Range.new(v2+1, u2)]
      end
    end
  end
end

class Reactor
  def initialize
    @regions = []
  end

  def cubes
    @regions.map(&:size).sum
  end

  def execute_step(step)
    msg, ranges = step
    send(msg, *ranges)
  end

  def on(x_range, y_range, z_range)
    on_regions = [Region.new(x_range, y_range, z_range)]
    cubes_turned_on = 0

    @regions.each do |region|
      new_on_regions = []
      on_regions.each do |on_region|
        shared_region = region.intersection(on_region)
        if shared_region.empty?
          new_on_regions << on_region
        else
          on_region.subtract(shared_region).reject {|r| r.empty?}.each do |piece|
            new_on_regions << piece
          end
        end
      end
      on_regions = new_on_regions
      if on_regions.empty?
        return 0
      end
    end

    on_regions.each do |r|
      @regions << r
      cubes_turned_on += r.size
    end
    cubes_turned_on
  end

  def off(x_range, y_range, z_range)
    off_region = Region.new(x_range, y_range, z_range)
    cubes_turned_off = 0

    new_regions = []
    @regions.each do |region|
      shared_region = region.intersection(off_region)
      if shared_region.empty?
        new_regions << region
      else
        region.subtract(shared_region).each do |r|
          new_regions << r unless r.empty?
        end
        cubes_turned_off += shared_region.size
      end
    end

    @regions = new_regions
    cubes_turned_off
  end
end

def parse_range(s)
  l,u = s.split("..").map(&:to_i).sort
  Range.new(l, u)
end

def parse_step(s)
  msg, s = s.split(" ")
  ranges = s.split(",").map {|r| parse_range(r.split('=')[1])}
  [msg, ranges]
end

def constrain_to_boundary(step, bounds)
  lb, ub = bounds
  msg, ranges = step
  new_ranges = ranges.map do |range|
    l = range.min
    u = range.max

    return nil if l > ub || u < lb
    Range.new(l < lb ? lb : l, u > ub ? ub : u)
  end
  [msg, new_ranges]
end

def bounds(steps)
  l = steps.map(&:last).flatten.map(&:min).min
  u = steps.map(&:last).flatten.map(&:max).max
  [l,u]
end

def normalize_steps(steps)
  l,_ = bounds(steps)
  steps.map do |step|
    msg, ranges = step
    ranges = ranges.map {|range| Range.new(range.min - l, range.max - l)}
    [msg, ranges]
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  steps = IO.readlines(fname, chomp: true).map {|s| parse_step(s)}
  if part == 1
    steps = steps.map {|step| constrain_to_boundary(step, [-50,50])}.compact
  end

  reactor = Reactor.new
  steps.each {|step| reactor.execute_step(step) }
  puts reactor.cubes
end

if __FILE__ == $0
  main(ARGV)
end
