#!/usr/bin/env ruby

class SeaFloor
  def initialize(s)
    @cells = s.split("\n").map(&:chars)
    @row_count = @cells.length
    @column_count = @cells.first.length
  end

  def to_s
    @cells.map {|row| row.join}.join("\n")
  end

  def cells_copy
    Array.new(@row_count) { Array.new(@column_count) { '.' }}.tap do |copy|
      @row_count.times do |r|
        @column_count.times do |c|
          copy[r][c] = @cells[r][c]
        end
      end
    end
  end

  def advance_step
    changes = 0
    updated_cells = cells_copy

    @row_count.times do |r|
      @column_count.times do |c|
        next_c = (c+1) % @column_count
        if @cells[r][c] == '>' && @cells[r][next_c] == '.'
          updated_cells[r][c] = '.'
          updated_cells[r][next_c] = '>'
          changes += 1
        end
      end
    end

    @cells = updated_cells
    updated_cells = cells_copy

    @row_count.times do |r|
      @column_count.times do |c|
        next_r = (r+1) % @row_count
        if @cells[r][c] == 'v' && @cells[next_r][c] == '.'
          updated_cells[r][c] = '.'
          updated_cells[next_r][c] = 'v'
          changes += 1
        end
      end
    end

    @cells = updated_cells
    changes
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]
  verbose = !args[2].nil?

  sea_floor = SeaFloor.new(IO.read(fname))

  if verbose
    puts "Initial state:"
    puts sea_floor
    puts ""
  end
  step = 0
  while (changes = sea_floor.advance_step) > 0
    step += 1
    if verbose
      puts "After #{step} steps:"
      puts sea_floor
      puts ""
    end
  end
  puts step+1
end

if __FILE__ == $0
  main(ARGV)
end
