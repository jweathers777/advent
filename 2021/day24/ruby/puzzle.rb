#!/usr/bin/env ruby

# ALU programs consist of subprograms that are always of two forms
# characterized by constants u and in the second form also v:
#
# Form 1: z, d -> 26*z + (d + u)
#
# Form 2: z, d -> (m - v == d) ? k : 26*k + (d + u)
#         where z = 26*k + m
#
# Here z is the contents of register z at the start of this subprogram
# while d is the incoming digit that gets input into the ALU
#

class Node
  # A node for representing a subprogram computation step in a computation
  # tree for an ALU program where the attributes are the following:
  #
  # u_values: The u values that remain from previous computation nodes
  #           on the path to this node. Form 2 computations can result in "popping"
  #           off u values through the process of integer division
  #
  # digit_indexes: The corresponding input digit index to each u value that remains at
  #                this computation node
  #
  # v_value: The v value that resulted in this node from its parent node. This value
  #          will be nil if the node resulted from a Form 1 computation from its parent.
  #
  # parent: The computation node that produced this node via computation step of Form 1 or Form 2
  #
  # left: A child computation node that resulted from a computation step of Form 1 or
  #       of Form 2 where (m - v == d) is false.
  #
  # right: A child computation node that resulted from a computation step of Form 2
  #        where (m - v == d) is true. This value will be nil if the computation step performed
  #        on this node is of Form 1.
  include Enumerable

  attr_accessor :u_values, :digit_indexes, :v_value, :parent, :left, :right

  def initialize
    @u_values = []
    @digit_indexes = []
  end

  def to_s
    "u_values: [#{u_values.map(&:to_s).join(', ')}], [#{digit_indexes.map(&:to_s).join(', ')}], v_value: #{v_value}"
  end

  def is_leaf?
    left == nil && right == nil
  end

  def leaves
    self.select(&:is_leaf?)
  end

  def ancestors
    path = []
    current = self
    while current != nil
      path.push(current)
      current = current.parent
    end
    path.reverse
  end

  def add_step(u_value, v_value, digit_index)
    if v_value
      self.left = Node.new
      self.left.u_values = self.u_values.dup
      self.left.u_values.pop
      self.left.u_values.push(u_value)
      self.left.digit_indexes = self.digit_indexes.dup
      self.left.digit_indexes.pop
      self.left.digit_indexes.push(digit_index)
      self.left.v_value = v_value
      self.left.parent = self

      self.right = Node.new
      self.right.u_values = self.u_values.dup
      self.right.u_values.pop
      self.right.digit_indexes = self.digit_indexes.dup
      self.right.digit_indexes.pop
      self.right.v_value = v_value
      self.right.parent = self
    else
      self.left = Node.new
      self.left.u_values = self.u_values.dup
      self.left.u_values.push(u_value)
      self.left.digit_indexes = self.digit_indexes.dup
      self.left.digit_indexes.push(digit_index)
      self.left.parent = self
    end
    [self.left, self.right]
  end

  def each
    if self.left
      self.left.each {|child| yield child }
    end
    yield self
    if self.right
      self.right.each {|child| yield child }
    end
  end
end

class ArithmeticLogicUnit
  attr_reader :w, :x, :y, :z, :program, :full_program
  attr_accessor :inputs, :outputs, :verbose

  REGISTERS = %w{w x y z}

  def initialize
    reset
    @program = nil
    @full_program = nil
    @subprogram_indices = nil
    @verbose = false
  end

  def reset
    @w = 0
    @x = 0
    @y = 0
    @z = 0

    @inputs = nil
    @outputs = nil
  end

  def register_state
    [@w, @x, @y, @z]
  end

  def puts_state
    puts "w : #{@w}"
    puts "x : #{@x}"
    puts "y : #{@y}"
    puts "z : #{@z}"
  end

  def full_program=(p)
    @full_program = p
    self.program = p
  end

  def program=(p)
    @subprogram_indices= nil
    @program = p
  end

  def subprograms
    subprogram_indices.map {|s,f| @program[s..f]}
  end

  def subprogram(index)
    s,f = subprogram_indices[index]
    @program[s..f]
  end

  def subprogram_indices
    @subprogram_indices ||=
      begin
        if @program
          (@program.each_with_index.select {|l,index| l =~ /inp/}.map(&:last) + [@program.length]).
            each_cons(2).map {|s,f| [s, f-1]}
        else
          nil
        end
      end
  end

  def u_values_for_subprograms
    @u_for_subprograms ||=
      begin
        (0...subprograms.length).map {|index| subprogram_for_value_and_digit(index, 0, 0)}
      end
  end

  def u_for_subprogram(index)
    u_values_for_subprogram[index]
  end

  def v_values_for_subprograms
    @v_for_subprograms ||=
      begin
        (0...subprograms.length).map do |index|
          (0..25).to_a.product((1..9).to_a).
            select {|z_i,d_i| subprogram_for_value_and_digit(index, z_i, d_i) == 0}.
            map {|z_i, d_i| z_i - d_i}.uniq.first
        end
      end
  end

  def v_for_subprogram(index)
    v_values_for_subprogram[index]
  end

  def computation_tree
    @computation_tree ||=
      begin
        root = Node.new
        nodes = [root]
        u_values_for_subprograms.zip(v_values_for_subprograms).each_with_index do |u_v, index|
          u,v = u_v
          new_nodes = []
          nodes.each do |node|
            next if node.nil?
            l,r = node.add_step(u,v,index)
            new_nodes.push(l)
            new_nodes.push(r)
          end
          nodes = new_nodes
        end
        root
      end
  end

  def run_program_with_inputs(inputs)
    reset
    @inputs = inputs
    puts "Start: #{register_state.join(", ")}" if @verbose
    execute_instructions(@program)
  end

  def run_program_with_registers_and_inputs(registers, inputs)
    reset
    @w, @x, @y, @z = registers
    @inputs = inputs
    puts "Start: #{register_state.join(", ")}" if @verbose
    execute_instructions(@program)
  end

  def compute_model_number_value(number)
    s = number.to_s
    return nil if s =~ /0/
    inputs = s.split('').map(&:to_i)
    run_program_with_inputs(inputs)
    @z
  end

  def valid_model_number?(number)
    compute_model_number_value(number) == 0
  end

  def subprogram_for_value_and_digit(index, z_i, d_i)
    self.program = subprogram(index)
    run_program_with_registers_and_inputs([0,0,0,z_i], [d_i])
    next_z = @z
    self.program = full_program
    next_z
  end

  def execute_instructions(insts)
    insts.each {|instruction| execute_instruction(instruction)}
  end

  def execute_instruction(inst)
    send(*inst.split)
    puts "Next: #{register_state.join(", ")}" if @verbose
  end

  def inp(a)
    value = @inputs.shift
    set_register_value(a, value)
  end

  def add(a, b)
    operation('+', a, b)
  end

  def mul(a, b)
    operation('*', a, b)
  end

  def div(a, b)
    operation('/', a, b)
  end

  def mod(a, b)
    operation('%', a, b)
  end

  def eql(a, b)
    a_value = register_value(a)
    b_value = register_or_literal_value(b)
    set_register_value(a, a_value == b_value ? 1 : 0)
  end

  private
  def operation(operator, a, b)
    a_value = register_value(a)
    b_value = register_or_literal_value(b)
    set_register_value(a, a_value.send(operator,b_value))
  end

  def register_value(a)
    instance_variable_get("@#{a}")
  end

  def register_or_literal_value(a)
    if REGISTERS.include?(a)
      register_value(a)
    elsif a =~ /\d/
      a.to_i
    else
      a
    end
  end

  def set_register_value(a, value)
    instance_variable_set("@#{a}", value)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  alu = ArithmeticLogicUnit.new
  alu.full_program = IO.readlines(fname, chomp: true)
  alu.verbose = !args[2].nil?

  # Parse a computation tree from the input program
  root = alu.computation_tree

  # Find all leaf computation nodes that have no remaining u values
  # The number of u values left at a node corresponds to the number
  # of powers of 26 that the resulting z register will contain after
  # computation at that node. Thus, if we want z to be 0 we cannot
  # have any powers of 26 left.
  zero_leaf = root.leaves.select {|l| l.u_values.length == 0}.first
  digits = Array.new(14)

  if part == 1
    # We want the biggest possible 14 digit number along this path.
    #
    # Skip the root node of the tree and then work down the computation
    # path to the zero leaf setting digits to 9 when there are no constraints
    # on them # (i.e. for Form 1 computations) and otherwise adjusting the two
    # involved digits to fit the condition that led to the followed branch at
    # a Form 2 computation node.
    zero_leaf.ancestors.drop(1).each_with_index do |node, current_index|
      if node.v_value
        other_index = node.parent.digit_indexes.last
        u = node.parent.u_values.last
        v = node.v_value
        shift = u - v
        if shift == 0
          digits[current_index] = digits[other_index]
        elsif shift > 0
          digits[current_index] = 9
          digits[other_index] = digits[current_index] - shift
        else # shift < 0
          digits[current_index] = digits[other_index] + shift
        end
      else
        digits[current_index] = 9
      end
    end
  else
    # We want the smallest possible 14 digit number along this path.
    #
    # Skip the root node of the tree and then work down the computation
    # path to the zero leaf setting digits to 1 when there are no constraints
    # on them # (i.e. for Form 1 computations) and otherwise adjusting the two
    # involved digits to fit the condition that led to the followed branch at
    # a Form 2 computation node.
    zero_leaf.ancestors.drop(1).each_with_index do |node, current_index|
      if node.v_value
        other_index = node.parent.digit_indexes.last
        u = node.parent.u_values.last
        v = node.v_value
        shift = u - v
        if shift == 0
          digits[current_index] = digits[other_index]
        elsif shift > 0
          digits[current_index] = digits[other_index] + shift
        else # shift < 0
          digits[current_index] = 1
          digits[other_index] = digits[current_index] - shift
        end
      else
        digits[current_index] = 1
      end
    end
  end

  number = digits.map(&:to_s).join('').to_i

  if alu.valid_model_number?(number)
    puts number
  else
    puts "An error has occurred. No valid model number found!"
  end
end

if __FILE__ == $0
  main(ARGV)
end
