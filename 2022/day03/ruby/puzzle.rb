#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]

  priority_map = Hash[*(('a'..'z').to_a + ('A'..'Z').to_a).each_with_index.map {|e,i| [e,i+1]}.flatten]

  rucksacks = IO.readlines(fname, chomp: true).map {|r| r.split('')}

  if part == 1
    rucksacks = rucksacks.map do |r|
      n = r.length / 2
      [r[0,n], r[n, 2*n]]
    end

    shared_types = rucksacks.map {|a,b| a.intersection(b)}.flatten
  else
    shared_types = rucksacks.each_slice(3).map {|g| g.reduce {|a,e| a.intersection(e)}}.flatten
  end

  result = shared_types.map {|e| priority_map[e]}.sum
  puts result
end

if __FILE__ == $0
  main(ARGV)
end
