#!/usr/bin/env ruby

class Stack
  attr_reader :id, :elements

  def initialize(id, elements)
    @id = id
    @elements = elements
  end

  def to_s
    "#{@id}: #{@elements.to_s}"
  end
end

class Move
  attr_reader :count, :src, :dest

  def initialize(count, src, dest)
    @count = count
    @src = src
    @dest = dest
  end

  def to_s
    "move #{@count} from #{@src} #{@dest}"
  end

  def make(stacks)
    source = stacks.find { |s| s.id == @src }
    dest = stacks.find { |s| s.id == @dest }
    @count.times do
      dest.elements.push(source.elements.pop)
    end
  end
end

def parse_stacks(s)
  lines = s.split("\n")
  index = lines.length - 1
  stack_ids = lines[index].split.map(&:to_i)
  stack_elements = stack_ids.map { |id| [] }

  column_matcher = /(?:\[([A-Z])\] ?)|(   ) ?/
  while index > 0
    index -= 1
    lines[index].scan(column_matcher).each_with_index do |match, i|
      if match[0]
        stack_elements[i].push(match[0])
      end
    end
  end

  stacks = stack_ids.zip(stack_elements).map { |id, elements| Stack.new(id, elements) }
end

def parse_moves(s)
  s.split("\n").map do |line|
    line =~ /move (\d+) from (\d+) to (\d+)/
    Move.new($1.to_i, $2.to_i, $3.to_i)
  end
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  stacks_str, moves_str = IO.read(fname).split(/^$/)
  moves_str.strip!

  stacks = parse_stacks(stacks_str)
  moves = parse_moves(moves_str)

  moves.each do |move|
    move.make(stacks)
  end

  puts stacks.map { |s| s.elements.last }.join
end

if __FILE__ == $0
  main(ARGV)
end
