#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]

  pairs = IO.readlines(fname, chomp: true).
    map {|p| p.split(',').map {|r| Range.new(*r.split('-').map(&:to_i))}}

  if part == 1
    result = pairs.count {|a,b| a.cover?(b) || b.cover?(a)}
  else
    result = pairs.count {|a,b| a.include?(b.first) || b.include?(a.first)}
  end

  puts result
end

if __FILE__ == $0
  main(ARGV)
end
