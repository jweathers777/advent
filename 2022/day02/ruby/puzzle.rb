#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]

  move_values = {ROCK: 1, PAPER: 2, SCISSORS: 3}
  outcome_values = {LOSS: 0, DRAW: 3, WIN: 6}

  if part == 1
    code = {A: :ROCK, B: :PAPER, C: :SCISSORS, X: :ROCK, Y: :PAPER, Z: :SCISSORS}
    outcomes = {
      ROCK: {SCISSORS: :WIN, PAPER: :LOSS, ROCK: :DRAW},
      SCISSORS: {PAPER: :WIN, ROCK: :LOSS, SCISSORS: :DRAW},
      PAPER: {ROCK: :WIN, SCISSORS: :LOSS, PAPER: :DRAW}
    }
    rounds = IO.readlines(fname, chomp: true).map {|l| l.split.map {|e| code[e.to_sym]}}
    total = rounds.reduce(0) do |acc, round|
      your_move, my_move = round
      outcome = outcomes[my_move][your_move]

      round_score = move_values[my_move] + outcome_values[outcome]
      acc + round_score
    end
  else
    code = {A: :ROCK, B: :PAPER, C: :SCISSORS, X: :LOSS, Y: :DRAW, Z: :WIN}
    outcomes = {
      ROCK: {LOSS: :SCISSORS, DRAW: :ROCK, WIN: :PAPER},
      SCISSORS: {LOSS: :PAPER, DRAW: :SCISSORS, WIN: :ROCK},
      PAPER: {LOSS: :ROCK, DRAW: :PAPER, WIN: :SCISSORS}
    }
    rounds = IO.readlines(fname, chomp: true).map {|l| l.split.map {|e| code[e.to_sym]}}
    total = rounds.reduce(0) do |acc, round|
      your_move, outcome = round
      my_move = outcomes[your_move][outcome]

      round_score = move_values[my_move] + outcome_values[outcome]
      acc + round_score
    end
  end

  puts total
end

if __FILE__ == $0
  main(ARGV)
end
