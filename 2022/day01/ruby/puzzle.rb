#!/usr/bin/env ruby

def main(args)
  part = args[0].to_i
  fname = args[1]

  food_by_elf = IO.read(fname).split(/^$/).map {|e| e.split.map(&:to_i)}

  if part == 1
    elf_with_most = food_by_elf.each_with_index.max_by {|e,i| e.sum}
    total = elf_with_most.first.sum
  else
    top_three_elves = food_by_elf.each_with_index.reduce([nil, nil, nil]) do |acc,pair|
      first, second, third = acc
      elf, index = pair
      elf_sum = elf.sum
      new_value = [elf, elf_sum]

      next [new_value, nil, nil] if first.nil?
      next [new_value, first, second] if first[1] < elf_sum
      next [first, new_value, nil] if second.nil?
      next [first, new_value, second] if second[1] < elf_sum
      next [first, second, new_value] if third.nil? || third[1] < elf_sum

      [first, second, third]
    end
    total = top_three_elves.map {|e| e[1]}.sum
  end

  puts total
end

if __FILE__ == $0
  main(ARGV)
end
