module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Map (Map)
import qualified Data.Map.Strict as Map

type LightsGrid = Map (Int,Int) Int
type Pos = (Int, Int)

type Command = LightsGrid -> Pos -> Pos -> LightsGrid

turnOnLight :: LightsGrid -> Pos -> LightsGrid
turnOnLight grid pos = Map.insert pos 1 grid

turnOffLight :: LightsGrid -> Pos -> LightsGrid
turnOffLight grid pos = Map.delete pos grid

toggleLight :: LightsGrid -> Pos -> LightsGrid
toggleLight grid pos =
  case Map.lookup pos grid of
    Nothing -> Map.insert pos 1 grid
    Just b -> Map.delete pos grid

turnOnLights :: LightsGrid -> Pos -> Pos -> LightsGrid
turnOnLights grid (x0,y0) (x1,y1) =
  foldl' turnOnLight grid [(x,y)| x <- [x0..x1], y <- [y0..y1]]

turnOffLights :: LightsGrid -> Pos -> Pos -> LightsGrid
turnOffLights grid (x0,y0) (x1,y1) =
  foldl' turnOffLight grid [(x,y)| x <- [x0..x1], y <- [y0..y1]]

toggleLights :: LightsGrid -> Pos -> Pos -> LightsGrid
toggleLights grid (x0,y0) (x1,y1) =
  foldl' toggleLight grid [(x,y)| x <- [x0..x1], y <- [y0..y1]]

turnUpLight :: LightsGrid -> Pos -> LightsGrid
turnUpLight grid pos =
  case Map.lookup pos grid of
    Nothing -> Map.insert pos 1 grid
    Just b -> Map.insert pos (b+1) grid

turnDownLight :: LightsGrid -> Pos -> LightsGrid
turnDownLight grid pos =
  case Map.lookup pos grid of
    Nothing -> grid
    Just b -> case b of
                1 -> Map.delete pos grid
                otherwise -> Map.insert pos (b-1) grid

turnUpLightTwice :: LightsGrid -> Pos -> LightsGrid
turnUpLightTwice grid pos =
  case Map.lookup pos grid of
    Nothing -> Map.insert pos 2 grid
    Just b -> Map.insert pos (b+2) grid

turnUpLights :: LightsGrid -> Pos -> Pos -> LightsGrid
turnUpLights grid (x0,y0) (x1,y1) =
  foldl' turnUpLight grid [(x,y)| x <- [x0..x1], y <- [y0..y1]]

turnDownLights :: LightsGrid -> Pos -> Pos -> LightsGrid
turnDownLights grid (x0,y0) (x1,y1) =
  foldl' turnDownLight grid [(x,y)| x <- [x0..x1], y <- [y0..y1]]

turnUpLightsTwice :: LightsGrid -> Pos -> Pos -> LightsGrid
turnUpLightsTwice grid (x0,y0) (x1,y1) =
  foldl' turnUpLightTwice grid [(x,y)| x <- [x0..x1], y <- [y0..y1]]

parsePos :: String -> Pos
parsePos s = (x,y)
  where
    commaToSpace ch = if ch == ',' then ' ' else ch
    [x,y] = map (\w -> read w :: Int) $ words $ map commaToSpace s

parseInstruction :: String -> Bool -> (Command, Pos, Pos)
parseInstruction s improved  = (command, start, finish)
  where
    allTokens = words s
    tokens = case head allTokens of
      "turn" -> tail allTokens
      otherwise -> allTokens
    command = case improved of
      False -> case head tokens of
                "on" -> turnOnLights
                "off" -> turnOffLights
                "toggle" -> toggleLights
                otherwise -> error ("Unknown command: " ++ (head tokens))
      True -> case head tokens of
                "on" -> turnUpLights
                "off" -> turnDownLights
                "toggle" -> turnUpLightsTwice
                otherwise -> error ("Unknown command: " ++ (head tokens))
    start = parsePos (tokens !! 1)
    finish = parsePos (tokens !! 3)

executeInstruction :: Bool -> LightsGrid -> String -> LightsGrid
executeInstruction improved grid s = f grid start finish
  where (f, start, finish) = parseInstruction s improved

solve :: Int -> String -> Int
solve 1 s = length grid
  where
    grid = foldl' (executeInstruction False) Map.empty $ lines s
solve 2 s = foldl' (+) 0 grid
  where
    grid = foldl' (executeInstruction True) Map.empty $ lines s

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
