module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Function
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Tuple

data ItemType = Weapon | Armor | Ring
  deriving (Eq, Show)

data Item = Item { itemName :: String
                 , itemType :: ItemType
                 , itemCost :: Int
                 , itemDamage :: Int
                 , itemArmor :: Int
                 } deriving (Eq, Show)

data Inventory = Inventory { weaponList :: [Item]
                           , armorList :: [Item]
                           , ringList :: [Item]
                           } deriving (Show)

data Outfit = Outfit { armedWeapon :: Item
                     , wornArmor :: Maybe Item
                     , leftRing :: Maybe Item
                     , rightRing :: Maybe Item
                     } deriving (Show)

data Combatant = Combatant { name :: String
                           , hitPoints :: Int
                           , damage :: Int
                           , armor :: Int
                           } deriving (Show)

readInt s = read s :: Int

mkCombatant :: String -> String -> Combatant
mkCombatant n s = Combatant n hp dmg arm
  where
    [hp, dmg, arm] = map (readInt . last . words) $ lines s

setHitPoints :: Int -> Combatant -> Combatant
setHitPoints hp c = Combatant (name c) hp (damage c) (armor c)

adjHitPoints :: Int -> Combatant -> Combatant
adjHitPoints delta c = Combatant (name c) (hp + delta) (damage c) (armor c)
  where hp = hitPoints c

setDamage :: Int -> Combatant -> Combatant
setDamage dmg c = Combatant (name c) (hitPoints c) dmg (armor c)

setArmor :: Int -> Combatant -> Combatant
setArmor arm c = Combatant (name c) (hitPoints c) (damage c) arm

mkItem :: ItemType -> String -> Item
mkItem tp s = Item nm tp cst dmg arm
  where
    armS:dmgS:cstS:tokens = reverse $ words s
    nm = unwords $ reverse tokens
    [arm, dmg, cst] = map readInt [armS, dmgS, cstS]

mkInventory :: String -> Inventory
mkInventory s = Inventory weapons armors rings
  where
    itemTypes = [Weapon, Armor, Ring]
    itemSections = splitOn "\n\n" s
    mkSection tp sect = map (mkItem tp) $ tail $ lines sect
    [weapons, armors, rings] = map (uncurry mkSection) $ zip itemTypes itemSections

getOutfits :: Inventory -> [Outfit]
getOutfits inv = map (\(w, a, r1, r2) -> Outfit w a r1 r2) options
  where
    weapons = weaponList inv
    armors = armorList inv
    maybeArmors = Nothing : map Just armors
    rings = ringList inv
    maybeRings = Nothing : map Just rings
    maybeOtherRings maybeRing = case maybeRing of
      Nothing -> [Nothing]
      Just r -> Nothing : map Just (delete r rings)
    options = [(w, a, r1, r2) | w <- weapons
                              , a <- maybeArmors
                              , r1 <- maybeRings
                              , r2 <- maybeOtherRings r1
              ]

outfitCost :: Outfit -> Int
outfitCost outfit = sum $ map itemCost items
  where
    items = mapMaybe ($ outfit) [Just . armedWeapon, wornArmor, leftRing, rightRing]

applyOutfit :: Combatant -> Outfit -> Combatant
applyOutfit c o = setArmor arm $ setDamage dmg c
  where
    dmgAdjusters = mapMaybe ($ o) [Just . armedWeapon, leftRing, rightRing]
    armAdjusters = mapMaybe ($ o) [wornArmor, leftRing, rightRing]
    dmg = sum $ map itemDamage dmgAdjusters
    arm = sum $ map itemArmor armAdjusters

outfittedPlayerWins :: Combatant -> Combatant -> Outfit -> Bool
outfittedPlayerWins player boss outfit = name player == name winner
  where
    (winner, _) = performCombat (applyOutfit player outfit) boss

performCombatRound :: Combatant -> Combatant -> Combatant
performCombatRound attacker defender = adjHitPoints dmg defender
  where dmg = negate $ max 1 $ (damage attacker) - (armor defender)

performCombat :: Combatant -> Combatant -> (Combatant, Combatant)
performCombat attacker defender
  | hitPoints attacker  <= 0 = (defender, attacker)
  | otherwise = performCombat (performCombatRound attacker defender) attacker

leastGoldAmountToWin :: Combatant -> Combatant -> Inventory -> Int
leastGoldAmountToWin player boss inv = minimum $ map outfitCost winningOutfits
  where
    winningOutfits = filter (outfittedPlayerWins player boss) $ getOutfits inv

mostGoldAmountToLose :: Combatant -> Combatant -> Inventory -> Int
mostGoldAmountToLose player boss inv = maximum $ map outfitCost losingOutfits
  where
    losingOutfits = filter (not . outfittedPlayerWins player boss) $ getOutfits inv

solve :: Int -> String -> String -> Int
solve p bossStr invStr = goalFunction player boss inv
  where
    player = Combatant "player" 100 0 0
    boss = mkCombatant "boss" bossStr
    inv = mkInventory invStr
    goalFunction = if p == 1 then leastGoldAmountToWin else mostGoldAmountToLose

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       bossStr <- readFile fileName
       invStr <- readFile "inventory.dat"
       putStrLn $ show $ solve part bossStr invStr
