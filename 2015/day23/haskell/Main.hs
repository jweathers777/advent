module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map

data Operand = Hlf | Tpl | Inc | Jmp | Jie | Jio
  deriving (Eq)
data Register = RegA | RegB | NoReg
  deriving (Eq)

data Instruction = Instruction { operand :: Operand
                               , register :: Register
                               , offset :: Int
                               }

type Memory = Map Int Instruction

data Computer = Computer { registerA :: Int
                         , registerB :: Int
                         , instPtr :: Int
                         , memory :: Memory
                         }

instance Show Operand where
  show Hlf = "hlf"
  show Tpl = "tpl"
  show Inc = "inc"
  show Jmp = "jmp"
  show Jie = "jie"
  show Jio = "jio"

instance Show Register where
  show RegA = "a"
  show RegB = "b"
  show NoReg = "NONE"

instance Show Instruction where
  show inst = unwords [op, reg, off]
    where
      op = show $ operand inst
      reg = show $ register inst
      off = show $ offset inst

instance Show Computer where
  show comp = unwords [a, b, ptrStr, ni]
    where
      a = "a: " ++ (show $ registerA comp)
      b = "b: " ++ (show $ registerB comp)
      ptr = instPtr comp
      ptrStr = "ptr: " ++ (show ptr)
      currInst = currInstruction comp
      ni = if ptr > lastAddr comp then "ni: None" else "ni: "++(show currInst)

readReg :: String -> Register
readReg s = case s of
  "a" -> RegA
  "b" -> RegB
  _ -> error ("Invalid register name: "++s)

readOp :: String -> Operand
readOp s = case s of
  "hlf" -> Hlf
  "tpl" -> Tpl
  "inc" -> Inc
  "jmp" -> Jmp
  "jie" -> Jie
  "jio" -> Jio
  _ -> error ("Invalid operand name: "++s)

mkMemory :: Memory
mkMemory = Map.empty

mkComputer :: Computer
mkComputer = Computer 0 0 0 mkMemory

setRegister :: Computer -> Register -> Int -> Computer
setRegister comp RegA val = comp { registerA = val }
setRegister comp RegB val = comp { registerB = val }

readRegister :: Computer -> Register -> Int
readRegister comp RegA = registerA comp
readRegister comp RegB = registerB comp

moveInstPtr :: Computer -> Int -> Computer
moveInstPtr comp addr = comp { instPtr = addr }

nextInst :: Computer -> Computer
nextInst comp = comp { instPtr = addr }
  where addr = 1 + instPtr comp

currInstruction :: Computer -> Instruction
currInstruction comp = memory comp Map.! instPtr comp

runInstruction :: Computer -> Instruction -> Computer
runInstruction comp inst = case operand inst of
  Hlf -> nextInst $ setRegister comp reg $ regVal `div` 2
  Tpl -> nextInst $ setRegister comp reg $ regVal * 3
  Inc -> nextInst $ setRegister comp reg $ regVal + 1
  Jmp -> moveInstPtr comp $ (instPtr comp) + (offset inst)
  Jie -> if even regVal
            then moveInstPtr comp $ (instPtr comp) + (offset inst)
            else nextInst comp
  Jio -> if regVal == 1
            then moveInstPtr comp $ (instPtr comp) + (offset inst)
            else nextInst comp
  where
    reg = register inst
    regVal = readRegister comp reg

lastAddr :: Computer -> Int
lastAddr comp = maximum $ Map.keys $ memory comp

leftTrim :: Char -> String -> String
leftTrim ch [] = []
leftTrim ch xs = if head xs == ch then tail xs else xs

rightTrim :: Char -> String -> String
rightTrim ch [] = []
rightTrim ch xs = reverse rtrimmed
  where
    rxs = reverse xs
    rtrimmed = if head rxs == ch then tail rxs else rxs

readInstruction :: String -> Instruction
readInstruction s = Instruction op reg off
  where
    tokens = words s
    tokenCount = length tokens
    validOps = case tokenCount of
      2 -> [Hlf, Tpl, Inc, Jmp]
      3 -> [Jie, Jio]
      _ -> error ("Invalid instruction: '"++s++"'")
    op = readOp $ tokens !! 0
    reg = if op == Jmp
             then NoReg
             else readReg $ rightTrim ',' $ tokens !! 1
    off = if op `elem` validOps
             then
               if tokenCount == 3 || op == Jmp
               then read (leftTrim '+' $ last tokens) :: Int
               else 0
             else error ("Invalid instruction: '"++s++"'")

loadProgram :: Computer -> String -> Computer
loadProgram comp s = comp { memory = program }
  where
    instructions = map readInstruction $ lines s
    program = Map.fromList $ zip [0..] instructions

executeProgram :: Computer -> Computer
executeProgram comp
  | (instPtr comp) > (lastAddr comp) = comp
  | otherwise = executeProgram comp'
  where
    comp' = runInstruction comp $ currInstruction comp

solve :: Int -> String -> Int
solve p s = readRegister compFinal RegB
  where
    comp = loadProgram mkComputer s
    comp' = if p == 1 then comp else setRegister comp RegA 1
    compFinal = executeProgram comp'

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
