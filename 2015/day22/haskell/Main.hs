{-# LANGUAGE MultiParamTypeClasses #-}
module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

-- Data Definitions
data CombatantType = Warrior | Wizard deriving (Eq, Show)
data Combatant = Combatant { name :: String
                           , combatantType :: CombatantType
                           , hitPoints :: Int
                           , manaPoints :: Int
                           , damage :: Int
                           , armor :: Int
                           , ongoingEffects :: [Effect]
                           , manaSpent :: Int
                           }

data Spell = MagicMissile | Drain | Shield | Poison | Recharge deriving (Eq, Show, Read, Ord)

data Effect = Effect { effectSpell :: Spell, effectDuration :: Int } deriving (Eq, Ord)

data Weapon = Melee | Ranged deriving (Show)

data CombatState = CombatState { actingPlayer :: Combatant
                               , otherPlayer :: Combatant
                               , prevStates :: [CombatState]
                               , spellsCast :: [Spell]
                               , hardCombat :: Bool
                               , depth :: Int
                               }

data CombatSearchState = CombatSearchState { minManaSpent :: Maybe Int, bestPath :: [Spell] }

-- Type Class and Instance definitions
class Attack a where
  performAttack :: a -> CombatState -> CombatState

instance Attack Spell where
  performAttack = castSpell

instance Attack Weapon where
  performAttack = useWeapon

class (Show a) => SearchNode a where
  expandNode :: a -> [a]
  keyForNode :: a -> String

class (SearchNode a, Show b) => SearchState a b where
  initSearchState :: a -> b
  updateSearchState :: a -> b -> (b, Bool)

instance SearchNode CombatState where
  expandNode = getNextCombatStates
  keyForNode = getKeyForCombatState

instance SearchState CombatState CombatSearchState where
  initSearchState _ = initCombatSearchState
  updateSearchState = updateCombatSearchState

instance Show Combatant where
  show c
    | Wizard == (combatantType c) = unlines $ [unwords [ct, n, hp, mp, arm, spent], effects]
    | otherwise = unwords [ct, n, hp, dmg, arm, "\n"]
    where
      ct = show $ combatantType c
      n = show $ name c
      hp = "hp: " ++ (show $ hitPoints c)
      mp = "mp: " ++ (show $ manaPoints c)
      arm = "armor: " ++ (show $ armor c)
      spent = "spent: " ++ (show $ manaSpent c)
      dmg = "dmg: " ++ (show $ damage c)
      effects = "Effects " ++ (show $ sort $ ongoingEffects c)

instance Show CombatState where
  show cs = unlines [dep, state, ap, op, spells]
    where
      dep = "\nDEPTH = "++(show $ depth cs)++"\n"
      state = "\nStatus: "++(if combatFinished cs then "Game Over" else "In Progress")
      ap = "\n***Acting Player***\n"++(show $ actingPlayer cs)
      op = "***Other Player***\n"++(show $ otherPlayer cs)
      spells = "Spells Cast: "++(show $ spellsCast cs)

instance Show CombatSearchState where
  show css = case minManaSpent css of
    Nothing -> "\nNo optimal path found."
    Just mp -> "\nLeast Mana Spent: "++show mp++"\n"++(show $ bestPath css)++"\n"

instance Show Effect where
  show e = "("++(show $ effectSpell e)++", "++(show $ effectDuration e)++")"

-- Search functions

depthFirstSearch :: (SearchNode a, SearchState a b) => a -> b
depthFirstSearch startNode = go [startNode] (initSearchState startNode)
  where
    go [] searchState = searchState
    go (node:nodes) searchState = go nodes' searchState'
      where
        (searchState', prune) = updateSearchState node searchState
        nodes' = if prune then nodes else (expandNode node) ++ nodes

-- Construction functions
mkWarrior :: String -> String -> Combatant
mkWarrior n s = Combatant n Warrior hp 0 dmg 0 [] 0
  where
    [hp, dmg] = map (readInt . last . words) $ lines s

mkWizard :: String -> Int -> Int -> Combatant
mkWizard n hp mp = Combatant n Wizard hp mp 0 0 [] 0

initCombatSearchState :: CombatSearchState
initCombatSearchState = CombatSearchState Nothing []

-- Mana State functions
updateCombatSearchState :: CombatState -> CombatSearchState -> (CombatSearchState, Bool)
updateCombatSearchState cs searchState = case prevMinManaSpent of
  Nothing -> (searchState', False)
  Just value -> (searchStateOf value, prune value)
  where
    prevMinManaSpent = minManaSpent searchState
    player1 = actingPlayer cs
    player2 = otherPlayer cs
    wizardPlayer = if isWizard player1 then player1 else player2
    wizardManaSpent = manaSpent wizardPlayer
    wizardHasWon = combatFinished cs && (not $ isDefeated wizardPlayer)
    searchState' = if wizardHasWon
                      then CombatSearchState (Just wizardManaSpent) (spellsCast cs)
                      else searchState

    searchStateOf v = if wizardHasWon && wizardManaSpent < v
                         then CombatSearchState (Just wizardManaSpent) (spellsCast cs)
                         else searchState
    prune v = wizardManaSpent >= v

-- Combatant functions
isWizard :: Combatant -> Bool
isWizard = (Wizard==) . combatantType

adjustHitPoints :: Combatant -> Int -> Combatant
adjustHitPoints c adj = c { hitPoints = (adj + hitPoints c) }

adjustManaPoints :: Combatant -> Int -> Combatant
adjustManaPoints c adj = c'
  where
    manaPoints' = adj + manaPoints c
    manaSpent' = (negate adj) + manaSpent c
    c' = if adj < 0
            then c { manaPoints = manaPoints', manaSpent = manaSpent' }
            else c { manaPoints = manaPoints' }

adjArmor :: Combatant -> Int -> Combatant
adjArmor c adj = c { armor = (adj + armor c) }

isDefeated :: Combatant -> Bool
isDefeated c = (hitPoints c) <= 0 || ((isWizard c) && (noAffordableSpells c))

availableSpells :: Combatant -> [Spell]
availableSpells c = allSpells \\ (map effectSpell $ ongoingEffects c)

affordableSpells :: Combatant -> [Spell]
affordableSpells cs = filter ((<= (manaPoints cs)) . spellCost) $ availableSpells cs

noAffordableSpells :: Combatant -> Bool
noAffordableSpells = null . affordableSpells

-- Spell functions
spellCost :: Spell -> Int
spellCost MagicMissile = 53
spellCost Drain = 73
spellCost Shield = 113
spellCost Poison = 173
spellCost Recharge = 229

shieldValue :: Int
shieldValue = 7

allSpells :: [Spell]
allSpells = [MagicMissile, Drain, Shield, Poison, Recharge]

expireSpell :: Combatant -> Spell -> Combatant
expireSpell c Shield = c { armor = ((armor c) - shieldValue) }
expireSpell c _ = c

applySpell :: (Combatant, Combatant) -> Spell -> (Combatant, Combatant)
applySpell (c, e) Poison = (c, adjustHitPoints e (-3))
applySpell (c, e) Recharge = (adjustManaPoints c 101, e)
applySpell pair _ = pair

-- Effect functions
decDuration :: Effect -> Effect
decDuration e = e { effectDuration = ((effectDuration e) - 1) }

isExpired :: Effect -> Bool
isExpired = (<=0) . effectDuration

resolveHardEffects :: Combatant -> Bool -> Combatant
resolveHardEffects firstPlayer False = firstPlayer
resolveHardEffects firstPlayer True = firstPlayer'
  where
    firstPlayer' = if isWizard firstPlayer
                      then adjustHitPoints firstPlayer (-1)
                      else firstPlayer

resolveEffects :: Combatant -> Combatant -> (Combatant, Combatant)
resolveEffects c enemy
  | null effects = (c, enemy)
  | otherwise = (c'' { ongoingEffects = effects' }, enemy')
  where
    effects = ongoingEffects c
    (c', enemy') = foldl' applySpell (c, enemy) $ map effectSpell effects
    (expiredEffects, effects') = partition isExpired $ map decDuration $ ongoingEffects c
    c'' = foldl' expireSpell c' $ map effectSpell expiredEffects

-- Combat State functions
nextPlayer :: CombatState -> CombatState
nextPlayer cs = cs { actingPlayer = (otherPlayer cs), otherPlayer = (actingPlayer cs) }

dealDamage :: CombatState -> Int -> CombatState
dealDamage cs dmg = cs { otherPlayer = damagedPlayer }
  where damagedPlayer = adjustHitPoints (otherPlayer cs) dmg

stealHealth :: CombatState -> Int -> CombatState
stealHealth cs hp = cs { actingPlayer = actingPlayer', otherPlayer = otherPlayer' }
  where
    actingPlayer' = adjustHitPoints (actingPlayer cs) hp
    otherPlayer' = adjustHitPoints (otherPlayer cs) (negate hp)

addShield :: CombatState -> Int -> CombatState
addShield cs duration = cs { actingPlayer = player' }
  where
    player = actingPlayer cs
    armor' = shieldValue + armor player
    effects' = (Effect Shield duration) : ongoingEffects player
    player' = player { armor = armor', ongoingEffects = effects' }

addEffect :: CombatState -> Effect -> CombatState
addEffect cs effect = cs { actingPlayer = player' }
  where
    player = actingPlayer cs
    effects' = effect : (ongoingEffects player)
    player' = player { ongoingEffects = effects' }

castSpell :: Spell -> CombatState -> CombatState
castSpell spell cs
  | spell == MagicMissile = dealDamage cs' (-4)
  | spell == Drain = stealHealth cs' 2
  | spell == Shield = addShield cs' 6
  | spell == Poison = addEffect cs' (Effect Poison 6)
  | spell == Recharge = addEffect cs' (Effect Recharge 5)
  where
    adj = negate $ spellCost spell
    player = actingPlayer cs
    ps' = cs : (prevStates cs)
    sc' = (spellsCast cs) ++ [spell]
    d' = (depth cs) + 1
    cs' = cs { actingPlayer = (adjustManaPoints player adj), prevStates = ps', spellsCast = sc', depth = d' }

useWeapon :: Weapon -> CombatState -> CombatState
useWeapon _ cs = cs1 { prevStates = ps', depth = ((depth cs) + 1) }
  where
    dmg = negate $ max 1 $ (damage $ actingPlayer cs) - (armor $ otherPlayer cs)
    cs1 = dealDamage cs dmg
    ps' = cs : (prevStates cs)

readInt s = read s :: Int

resolveAllEffects :: CombatState -> CombatState
resolveAllEffects cs = cs { actingPlayer = firstPlayer3, otherPlayer = secondPlayer3 }
  where
    firstPlayer1 = resolveHardEffects (actingPlayer cs) (hardCombat cs)
    secondPlayer1 = otherPlayer cs
    (firstPlayer2, secondPlayer2) = resolveEffects firstPlayer1 secondPlayer1
    (secondPlayer3, firstPlayer3) = resolveEffects secondPlayer2 firstPlayer2

takeTurn :: Attack a => CombatState -> a -> CombatState
takeTurn cs attack = if combatFinished cs1 then cs1 else cs2
  where
    cs1 = resolveAllEffects cs
    cs2 = nextPlayer $ performAttack attack cs1

getWizard :: CombatState -> Combatant
getWizard cs = if isWizard player1 then player1 else player2
  where
    player1 = actingPlayer cs
    player2 = otherPlayer cs

combatFinished :: CombatState -> Bool
combatFinished cs = (isDefeated (actingPlayer cs)) || (isDefeated (otherPlayer cs))

getKeyForCombatState :: CombatState -> String
getKeyForCombatState cs = unlines [show $ actingPlayer cs, show $ otherPlayer cs]

getNextCombatStates :: CombatState -> [CombatState]
getNextCombatStates cs = nextCombatStates
  where
    cs1 = resolveAllEffects cs
    player = actingPlayer cs1
    rawNextCombatStates = case combatantType player of
      Warrior -> [nextPlayer $ performAttack Melee cs1]
      _ -> map (\sp -> nextPlayer $ performAttack sp cs1) $ affordableSpells player
    nextCombatStates = if (combatFinished cs)
                          then []
                          else
                            if (combatFinished cs1)
                            then [cs1]
                            else rawNextCombatStates

initCombatState :: String -> Bool -> IO CombatState
initCombatState filePath isHard = do
  s <- readFile filePath
  let boss = mkWarrior "boss" s
      player = mkWizard "player" 50 500
      cs = CombatState player boss [] [] isHard 0
  return cs

playerStatus :: Combatant -> String
playerStatus player = s
  where
    hp = (show $ hitPoints player) ++ " hit points"
    arm = (show $ armor player) ++ " armor"
    mp = (show $ manaPoints player) ++ " mana"
    s = "-- Player has "++hp++", "++arm++", "++mp++"."

bossStatus :: Combatant -> String
bossStatus boss = "-- Boss has "++hp
  where
    hp = (show $ hitPoints boss) ++ " hit points."

wearOffStatus :: Spell -> String
wearOffStatus spell
  | spell == Shield = "Shield wears off, decreasing armor by 7."
  | otherwise = show spell++" wears off."

effectStatus :: Effect -> String
effectStatus (Effect spell duration)
  | spell == Shield = "Shield's timer is now "++durationStatus
  | spell == Poison = "Poison deals 3 damage; its timer is now "++durationStatus
  | spell == Recharge = "Recharge provides 101 mana; its timer is now "++durationStatus
  | otherwise = error "Spell should not have an effect"
  where
    nextDuration = duration - 1
    durationStatus = if nextDuration == 0
                        then show nextDuration++".\n"++wearOffStatus spell
                        else show nextDuration++"."

spellStatus :: Spell -> String
spellStatus MagicMissile = "Player casts Magic Missile, dealing 4 damage."
spellStatus Drain = "Player casts Drain, dealing 2 damage, and healing 2 hit points."
spellStatus Shield = "Player casts Shield, increasing armor by 7."
spellStatus Poison = "Player casts Poison."
spellStatus Recharge = "Player casts Recharge."

weaponStatus :: Combatant -> Combatant -> String
weaponStatus boss player = s
  where
    dmg = damage boss
    arm = armor player
    net = dmg - arm
    s = if arm > 0
           then
             if net > 0
               then "Boss attacks for "++show dmg++" - "++show arm++" = "++show net++" damage!"
               else "Boss attacks for 1 damage!"
           else "Boss attacks for "++show dmg++" damage!"

runCombat :: CombatState -> [Spell] -> IO CombatState
runCombat cs spells
  | combatFinished cs = do
    return cs
  | Wizard == (combatantType $ actingPlayer cs) = do
    let effects = ongoingEffects $ actingPlayer cs
        cs1 = resolveAllEffects cs
        player = actingPlayer cs
        boss = otherPlayer cs
    putStrLn "\n-- Player turn --"
    putStrLn $ playerStatus player
    putStrLn $ bossStatus boss
    putStrLn $ unlines $ map effectStatus $ filter ((>0) . effectDuration) effects
    if combatFinished cs1
       then return cs1
       else do
         if null spells
            then do
              s <- getLine
              let spell = read s :: Spell
              putStrLn $ spellStatus spell
              result <- flip runCombat [] $ nextPlayer $ performAttack spell cs1
              return result
            else do
              let spell = head spells
              putStrLn $ spellStatus spell
              result <- flip runCombat (tail spells) $ nextPlayer $ performAttack spell cs1
              return result
  | otherwise = do
    let effects = ongoingEffects $ otherPlayer cs
        cs1 = resolveAllEffects cs
        player = otherPlayer cs
        boss = actingPlayer cs
    putStrLn "\n-- Boss turn --"
    putStrLn $ playerStatus player
    putStrLn $ bossStatus boss
    putStrLn $ unlines $ map effectStatus $ filter ((>0) . effectDuration) effects
    if combatFinished cs1
       then return cs1
       else do
         putStrLn $ weaponStatus boss player
         result <- flip runCombat spells $ nextPlayer $ performAttack Melee cs1
         return result

solve :: Int -> String -> CombatSearchState
solve p s = bestSearchState
  where
    hard = if p == 1 then False else True
    boss = mkWarrior "boss" s
    player = mkWizard "player" 50 500
    cs = CombatState player boss [] [] hard 0
    bestSearchState = depthFirstSearch cs
    result = case minManaSpent bestSearchState of
               Nothing -> -1
               Just v -> v

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName

       let bestSearchState = solve part contents
           result = case minManaSpent bestSearchState of
                      Nothing -> -1
                      Just v -> v

       putStrLn $ "Spells Cast: "++(show $ bestPath bestSearchState)
       putStrLn $ "Least Mana Spent: "++(show result)
