module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Bits
import Data.Word
import Data.List
import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as Map

readWeights :: String -> [Int]
readWeights = (map (\x -> read x :: Int)) . words

indicesToWord32 :: [Int] -> Word32
indicesToWord32 = foldl' setBit 0

word32ToIndices :: Word32 -> [Int]
word32ToIndices n = map fst $ filter ((>0) . snd) $ zip [0..] bits
  where
    bits = map (.&.1) $ takeWhile (>0) $ iterate (flip shiftR 1) n

weightsToWord32 :: [Int] -> [Int] -> Word32
weightsToWord32 xs ws = indicesToWord32 indices
  where
    indices = catMaybes $ map ((flip findIndex ws) . (==)) xs

word32ToWeights :: Word32 -> [Int] -> [Int]
word32ToWeights n ws = map (ws!!) $ word32ToIndices n

computeWeightSums :: Int -> [Int] -> Map Word32 Int
computeWeightSums sz ws = weightSums
  where
    t = sum ws `div` sz
    nextWeightSum sums n = Map.insert n (predSum+w) sums
      where
        zs = countTrailingZeros n
        pred = n `clearBit` zs
        predSum = sums Map.! pred
        w = ws !! zs
        sm = predSum+w
        sums' = if sm <= t then Map.insert n sm sums else sums
    indices = [(1::Word32)..fromIntegral $ 2^(length ws) - 1]
    weightSums = foldl' nextWeightSum (Map.singleton (0 :: Word32) 0) indices

solve :: Int -> String -> Int
solve _ _ = 0

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
