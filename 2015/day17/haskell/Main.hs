module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set

readJars :: String -> [(Int, Int)]
readJars s = zip (enumFromTo 1 (length sizes)) sizes
  where
    sizes = map (read :: String -> Int) $ lines s

combinations :: [(Int, Int)] -> Int -> Set (Set (Int, Int))
combinations jars total = Set.filter sumToTotal subsets
  where
    jarSet = Set.fromList jars
    subsets = Set.powerSet jarSet
    sumToTotal s = (Set.foldl' (\a e -> a + snd e) 0 s) == total

solve :: Int -> String -> Int
solve p s = Set.size targetCombs
  where
    combs = combinations (readJars s) 150
    minSizeComb = minimum $ Set.map Set.size combs
    targetCombs = if p == 1
      then combs
      else Set.filter ((minSizeComb==) . Set.size) combs

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
