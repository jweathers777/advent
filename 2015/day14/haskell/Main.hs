module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Text.ParserCombinators.Parsec

data Reindeer = Reindeer { name :: String
                         , speed :: Int
                         , duration :: Int
                         , restDuration :: Int
                         } deriving (Show)

integer :: Parser Int
integer = do
  firstDigit <- oneOf "123456789"
  otherDigits <- many digit
  return $ read (firstDigit:otherDigits)

reindeer :: Parser Reindeer
reindeer = do
  n <- many $ noneOf " "
  string " can fly "
  s <- integer
  string " km/s for "
  d <- integer
  string " seconds, but then must rest for "
  rd <- integer
  string " seconds."
  return (Reindeer n s d rd)

readReindeer :: String -> Reindeer
readReindeer s = case parse reindeer "" s of
  Left e -> error ("Error parsing reindeer\n"++(show e))
  Right r -> r

readReindeerRecords :: String -> [Reindeer]
readReindeerRecords s = map readReindeer $ lines s

reindeerDistance :: Reindeer -> Int -> Int
reindeerDistance reindeer time = speed reindeer * runTime
  where
    sprintDuration = duration reindeer
    lapDuration = sprintDuration + restDuration reindeer
    fullLaps = time `div` lapDuration
    partialLapTime = time `rem` lapDuration
    runTime = fullLaps * sprintDuration + min sprintDuration partialLapTime

raceTimeLimit :: Int
raceTimeLimit = 2503

raceDistances :: Int -> [Reindeer] -> [Int]
raceDistances = map . flip reindeerDistance

reindeerScores :: Int -> [Reindeer] -> [Int]
reindeerScores time reindeerRecords = go initialScores 1
  where
    initialScores = take (length reindeerRecords) $ repeat 0
    go scores t
      | t > time = scores
      | otherwise = go updatedScores (t+1)
      where
        distances = raceDistances t reindeerRecords
        winningDistance = maximum distances
        scoreDeltas = map (\d -> if d == winningDistance then 1 else 0) distances
        updatedScores = zipWith (+) scores scoreDeltas

solve :: Int -> String -> Int
solve p s = maximum $ f raceTimeLimit $ readReindeerRecords s
  where f = if p == 1 then raceDistances else reindeerScores

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
