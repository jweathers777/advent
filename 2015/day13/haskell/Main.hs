{-# LANGUAGE DeriveGeneric, TupleSections, LambdaCase #-}
module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Control.Applicative (Alternative(..), optional)
import Control.Monad (replicateM)
import Data.Char
import Data.Functor (($>))
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set

type Person = String
type SeatingRule = ((Person, Person), Int)
type SeatingRuleMap = Map (Person, Person) Int

mkSeatingRule a b delta = ((a, b), delta)
lookupHappinessDelta rules a b = Map.lookup (a,b) rules

newtype Parser i o = Parser { runParser :: i -> Maybe (i, o) }

instance Functor (Parser i) where
  fmap f parser = Parser $ fmap (fmap f) . runParser parser

instance Applicative (Parser i) where
  pure x = Parser $ pure . (, x)
  pf <*> po = Parser $ \input -> case runParser pf input of
    Nothing -> Nothing
    Just (rest, f) -> fmap f <$> runParser po rest

instance Alternative (Parser i) where
  empty = Parser $ const empty
  p1 <|> p2 = Parser $ \input -> runParser p1 input <|> runParser p2 input

instance Monad (Parser i) where
  p >>= f = Parser $ \input -> case runParser p input of
    Nothing -> Nothing
    Just (rest, o) -> runParser (f o) rest

satisfy :: (a -> Bool) -> Parser [a] a
satisfy predicate = Parser $ \case
  (x:xs) | predicate x -> Just (xs, x)
  _ -> Nothing

char :: Char -> Parser String Char
char c = satisfy (== c)

digitsToNumber :: Int -> Int -> [Int] -> Int
digitsToNumber base =
  foldl (\num d -> num * fromIntegral base + fromIntegral d)

digit :: Parser String Int
digit = digitToInt <$> satisfy isDigit

string :: String -> Parser String String
string "" = pure ""
string (c:cs) = (:) <$> char c <*> string cs

digit19 :: Parser String Int
digit19 = digitToInt <$> satisfy (\x -> isDigit x && x /= '0')

digits :: Parser String [Int]
digits = some digit

name :: Parser String String
name = some $ satisfy isAlpha

uint :: Parser String Int
uint = (\d ds -> digitsToNumber 10 0 (d:ds)) <$> digit19 <*> digits
  <|> fromIntegral <$> digit

happinessDelta :: Parser String Int
happinessDelta = happinessDeltaSign
  <$> (string " gain " <|> string " lose ") <*> uint
  where
    happinessDeltaSign " lose " i = negate i
    happinessDeltaSign _ i = i

seatingRule :: Parser String SeatingRule
seatingRule = do
  nameA <- name
  string " would"
  delta <- happinessDelta
  string " happiness unit"
  optional $ char 's'
  string " by sitting next to "
  nameB <- name
  char '.'
  return $ mkSeatingRule nameA nameB delta

readRule :: String -> Maybe SeatingRule
readRule s = fmap snd $ runParser seatingRule s

readRules :: String -> SeatingRuleMap
readRules s = Map.fromList $ catMaybes $ map readRule $ lines s

addPerson :: SeatingRuleMap -> String -> SeatingRuleMap
addPerson rules p = foldl' addRulesForNeighbor rules (getPeople rules)
  where
    addRulesForNeighbor rules0 n = rules2
      where
        rules1 = Map.insert (p,n) 0 rules0
        rules2 = Map.insert (n,p) 0 rules1

getPeople :: SeatingRuleMap -> [Person]
getPeople = Set.toList . Set.fromList . map fst . Map.keys

getNeighbors :: [Person] -> Person -> [Person]
getNeighbors people person
  | n <= 1 = []
  | n == 2 = case maybeIndex of
      Just index -> [people !! ((index + 1) `rem` 2)]
      _ -> []
  | otherwise = case maybeIndex of
      Just index -> map ((people!!) . (`rem`n)) [index-1+n, index+1]
      _ -> []
  where
    maybeIndex = elemIndex person people
    n = length people

totalHappinessDelta :: SeatingRuleMap -> [Person] -> Int
totalHappinessDelta rules people =
  sum $ catMaybes $ concatMap neighborHappiness people
  where
    neighborHappiness p = map (lookupHappinessDelta rules p) $ getNeighbors people p

solve :: Int -> String -> Int
solve p s = maximum $ map (totalHappinessDelta rules) $ permutations people
  where
    baseRules = readRules s
    rules = if p == 1 then baseRules else addPerson baseRules "Me"
    people = getPeople rules

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
