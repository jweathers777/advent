module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Char
import Data.List

interweave :: [Int] -> [Int] -> [Int]
interweave (x:xs) ys = x : (interweave ys xs)
interweave _ _ = []

lookAndSay :: [Int] -> [Int]
lookAndSay digits = interweave counts values
  where
    grouped = group digits
    counts = map length grouped
    values = map head grouped


solve :: Int -> String -> Int
solve p s = length $ map intToDigit result
  where
    n = if p == 1 then 40 else 50
    start = map digitToInt s
    result = iterate lookAndSay start !! n

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
