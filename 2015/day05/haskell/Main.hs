module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

vowels :: Set Char
vowels = Set.fromList "aeiou"

naughtyPairs :: Set String
naughtyPairs = Set.fromList ["ab", "cd", "pq", "xy"]

hasAtLeastNumVowels :: Int -> String -> Bool
hasAtLeastNumVowels num = (>=num) . length . (filter (flip Set.member vowels))

hasAtLeastOneDouble :: Eq a => [a] -> Bool
hasAtLeastOneDouble xs = hasAtLeastOneDouble' Nothing xs
  where
    hasAtLeastOneDouble' _ [] = False
    hasAtLeastOneDouble' Nothing (x:xs) = hasAtLeastOneDouble' (Just x) xs
    hasAtLeastOneDouble' (Just x1) (x2:xs)
      | x1 == x2 = True
      | otherwise = hasAtLeastOneDouble' (Just x2) xs

hasNoNaughtyPairs :: String -> Bool
hasNoNaughtyPairs s = hasNoNaughtyPairs' Nothing s
  where
    hasNoNaughtyPairs' _ [] = True
    hasNoNaughtyPairs' Nothing (c:cs) = hasNoNaughtyPairs' (Just c) cs
    hasNoNaughtyPairs' (Just c1) (c2:cs)
      | Set.member [c1,c2] naughtyPairs = False
      | otherwise = hasNoNaughtyPairs' (Just c2) cs

eachConsPair :: [a] -> [(a,a)]
eachConsPair [] = []
eachConsPair [z] = []
eachConsPair (x:y:zs) = (x,y) : eachConsPair (y:zs)

reduceAdjDups :: Eq a => [a] -> [a]
reduceAdjDups xs = reverse $ helper [] xs
  where
    helper ys [] = ys
    helper [] (x:xs) = helper [x] xs
    helper (z:zs) (x:[])
      | x == z = (z:zs)
      | otherwise = (x:z:zs)
    helper (z:zs) (x:y:xs)
      | x /= z = helper (x:z:zs) (y:xs)
      | x == z && y /= z = helper (z:zs) (y:xs)
      | x == z && y == z = helper (y:z:zs) xs
      | otherwise = helper (y:x:z:zs) xs

counts :: Ord a => [a] -> Map a Int
counts = counts' Map.empty
  where
    counts' m [] = m
    counts' m (x:xs) = counts' m' xs
      where m' = case Map.lookup x m of
                   Nothing -> Map.insert x 1 m
                   Just _ -> Map.adjust (+1) x m

hasNonOverlappingPairTwice :: String -> Bool
hasNonOverlappingPairTwice s =  not $ Map.null matchingPairs
  where
    reducedPairs = reduceAdjDups $ eachConsPair s
    matchingPairs = Map.filterWithKey (\_ c -> c>=2) $ counts $ reducedPairs

hasRepeatWithSeparator :: String -> Bool
hasRepeatWithSeparator s = helper [] s
  where
    helper _ [] = False
    helper [] (x:xs) = helper [x] xs
    helper [x] (y:xs) = helper [x,y] xs
    helper [x,y] (z:xs)
      | x == z = True
      | otherwise = helper [y,z] xs

isNice :: String -> Bool
isNice s = and $ map (\f -> f s) conditions
  where conditions = [hasAtLeastNumVowels 3, hasAtLeastOneDouble, hasNoNaughtyPairs]

isNicer :: String -> Bool
isNicer s = hasNonOverlappingPairTwice s && hasRepeatWithSeparator s

solve :: Int -> String -> Int
solve 1 s = length $ filter isNice (lines s)
solve 2 s = length $ filter isNicer (lines s)

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents

