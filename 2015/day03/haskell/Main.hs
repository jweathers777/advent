module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map

type Pos = (Int, Int)

move :: Pos -> Char -> Pos
move (x,y) ch
  | ch == '^' = (x,y-1)
  | ch == 'v' = (x,y+1)
  | ch == '>' = (x+1,y)
  | ch == '<' = (x-1,y)

deliverPresent :: Pos -> Map Pos Int -> Map Pos Int
deliverPresent pos houses =
  case Map.lookup pos houses of
    Nothing -> Map.insert pos 1 houses
    Just value -> Map.adjust (+1) pos houses

moveAndDeliverPresent :: (Pos, Map Pos Int) -> Char -> (Pos, Map Pos Int)
moveAndDeliverPresent (pos, m) ch = (pos', m')
  where
    pos' = move pos ch
    m' = deliverPresent pos' m

deliverAllPresents :: String -> (Pos, Map Pos Int)
deliverAllPresents s = foldl moveAndDeliverPresent (pos0, m0) s
  where
    pos0 = (0,0)
    m0 = Map.fromList [(pos0, 1)]

splitEveryOtherEven :: [a] -> [(a,a)]
splitEveryOtherEven [] = []
splitEveryOtherEven (x:y:zs) = (x,y) : splitEveryOtherEven zs

splitEveryOther :: [a] -> ([a], [a])
splitEveryOther zs
  | length zs `mod` 2 == 0 = unzip $ splitEveryOtherEven zs
  | otherwise = (x:xs, ys)
    where
      x = head zs
      (ys,xs) = unzip $ splitEveryOtherEven $ tail zs

solve :: Int -> String -> Int
solve 1 s = length $ Map.keys $ snd $ deliverAllPresents s
solve 2 s = length $ Map.keys allDeliveries
  where
    (santaInstructions, roboSantaInstructions) = splitEveryOther s
    (_, santaDeliveries) = deliverAllPresents santaInstructions
    (_, roboSantaDeliveries) = deliverAllPresents roboSantaInstructions
    allDeliveries = Map.unionWith (+) santaDeliveries roboSantaDeliveries


main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
