module Main where

import Debug.Trace
import System.Environment (getArgs)
import System.IO (readFile)
import Control.Monad.ST
import Data.List
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as MV

fullHouseRange :: Int -> Int -> [Int]
fullHouseRange elf n = enumFromThenTo elf (2*elf) n

atMostHouseRange :: Int -> Int -> Int -> [Int]
atMostHouseRange maxDeliv elf n = enumFromThenTo elf (2*elf) (min n (maxDeliv*elf))

deliverUntilTarget :: (Int -> Int -> [Int]) -> Int -> Int
deliverUntilTarget houseRange target = runST $ do
  houses <- MV.replicate n 0
  firstH <- updateHouses houses indexes n
  return firstH
  where
    n = target
    indexes = [(elf, h) | elf <- [1..n], h <- (houseRange elf n)]
    updateHouses _ [] bestH = do
      return bestH
    updateHouses houses ((elf,h):indexPairs) bestH
      | h <= n && bestH > elf = do
        let hIndex = h-1
        v <- MV.read houses hIndex
        let v' = v + elf
        MV.write houses hIndex v'
        v'' <- MV.read houses hIndex
        let bestH' = if v' >= target && bestH > h then h else bestH
        result <- updateHouses houses indexPairs bestH'
        return result
      | otherwise = do
        return bestH

solve :: Int -> String -> Int
solve p s = deliverUntilTarget houseRange houseCount
  where
    (d, houseRange) = if p == 1 then (10, fullHouseRange) else (11, atMostHouseRange 50)
    inputValue = read s :: Int
    houseCount = inputValue `div` d

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
