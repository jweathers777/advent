#!/usr/bin/env ruby

def deliver_until_target(target, houses)
  n = houses.length
  first_h = n
  (1..n).each do |elf|
    (elf..n).step(elf) do |h|
      if (houses[h-1] += elf) >= target
        first_h = h if first_h > h
        return first_h if first_h <= elf
      end
    end
  end
  -1 # Should not happen
end

def deliver_at_most_until_target(max_deliv, target, houses)
  n = houses.length
  first_h = n
  (1..n).each do |elf|
    ub = [max_deliv*elf,n].min
    (elf..ub).step(elf) do |h|
      if (houses[h-1] += elf) >= target
        first_h = h if first_h > h
        return first_h if first_h <= elf
      end
    end
  end
  -1 # Should not happen
end

def main(args)
  part = args[0].to_i
  fname = args[1]

  input_value = IO.read("input.dat").to_i
  if part == 1
    house_count = input_value/10
    target = input_value.to_f / 10
    houses = Array.new(house_count) { 0 }
    puts deliver_until_target(target, houses)
  else
    house_count = input_value/11
    target = input_value.to_f / 11
    houses = Array.new(house_count) { 0 }
    puts deliver_at_most_until_target(50, target, houses)
  end
end

if __FILE__ == $0
  main(ARGV)
end
