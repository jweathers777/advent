module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Function
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Tuple

readRulesAndMolecule :: String -> ([(String, String)], String)
readRulesAndMolecule s = (rules, m)
  where
    (m:"":ls) = reverse $ lines s
    rules = map ((\[left, m, right] -> (left, right)) . words) ls

replaceNth :: String -> String -> String -> Int -> String
replaceNth s t r n = if length indices < n then s else updated
  where
    indices = findIndices (isPrefixOf t) $ tails s
    index = indices !! (n - 1)
    (left, right) = splitAt index s
    updated = left ++ r ++ (drop (length t) right)

countOccurrences :: String -> String -> Int
countOccurrences s t = length $ findIndices (isPrefixOf t) $ tails s

allRuleApplications :: String -> (String, String) -> [String]
allRuleApplications s (t,r) = map (replaceNth s t r) [1..countOccurrences s t]

generateMolecules :: String -> [(String, String)] -> [String]
generateMolecules s rules = concatMap (allRuleApplications s) rules

updateStack :: [(String,Int)] -> [(String, String)] -> [(String, String)] -> [(String,Int)]
updateStack [] terminalRules otherRules = []
updateStack ((e,d):xs) terminalRules otherRules = ys ++ xs
  where
    ys = if e `elem` map fst terminalRules
      then [("e",d+1)]
      else map (\el -> (el, d+1)) $ generateMolecules e otherRules

processStack :: [(String,Int)] -> [(String, String)] -> [(String, String)] -> String -> Int
processStack [] _ _ _ = -1
processStack stack@((e,d):xs) terminalRules otherRules t = result
  where
    result = if e == t
      then d
      else processStack (updateStack stack terminalRules otherRules) terminalRules otherRules t

findMinSteps :: String -> [(String, String)] -> Int
findMinSteps t rules = processStack [(t,0)] terminalRules otherRules "e"
  where
    revRules = sortBy (compare `on` negate . length . fst) $ map swap rules
    terminalRules = filter (\(t,r) -> r == "e") revRules
    otherRules = filter (\(t,r) -> r /= "e") revRules

solve :: Int -> String -> Int
solve p s = result
  where
    (rules, m) = readRulesAndMolecule s
    result = if p == 1
      then Set.size $ Set.fromList $ generateMolecules m rules
      else findMinSteps m rules

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
