module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Char
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set

hasConsecutiveRun :: Int -> [Int] -> Bool
hasConsecutiveRun _ [] = False
hasConsecutiveRun 0 _ = False
hasConsecutiveRun n (x:xs) = helper (n-1) x xs
  where
    helper 0 _ _ = True
    helper m base [] = False
    helper m base (y:ys)
      | base+1 == y = helper (m-1) y ys
      | length ys >= n = helper (n-1) y ys
      | otherwise = False

hasStraight :: Int -> String -> Bool
hasStraight n s = hasConsecutiveRun n $ map ord s

hasNoConfusingChars :: String -> Bool
hasNoConfusingChars s = not $ or $ map (flip elem s) "iol"

hasNNonOverlappingPairs :: Int -> String -> Bool
hasNNonOverlappingPairs n s = Set.size pairs >= n
  where
    pairs = Set.fromList $ filter ((2==) . length) $ group s

validPassword :: String -> Bool
validPassword s = and $ map (\f -> f s) conditions
  where
    conditions = [hasStraight 3, hasNoConfusingChars, hasNNonOverlappingPairs 2]

-- Assumes no confusing characters
validPassword' :: String -> Bool
validPassword' s = and $ map (\f -> f s) conditions
  where
    conditions = [hasStraight 3, hasNNonOverlappingPairs 2]

nextPasswordChar :: Char -> Char
nextPasswordChar 'z' = 'a'
nextPasswordChar s = (chr . (+1) . ord) s

nextPassword :: String -> String
nextPassword cs = go [] (reverse cs)
  where
    go2 xs [] = xs
    go2 xs (y:ys) = go2 (y:xs) ys
    go xs [] = xs
    go xs (y:ys) =
      case y of
        'z' -> go ('a':xs) ys
        otherwise -> go2 ((nextPasswordChar y):xs) ys

bumpConfusingChars' :: String -> String -> String
bumpConfusingChars' xs [] =  xs
bumpConfusingChars' xs (y:ys) = bumpConfusingChars' xs' ys
  where
    y' = nextPasswordChar y
    as = take (length xs) $ repeat 'a'
    xs' = if y `elem` "iol" then (y':as) else (y:xs)

bumpConfusingChars :: String -> String
bumpConfusingChars s = bumpConfusingChars' [] (reverse s)

nextPasswordWithValidChars :: String -> String
nextPasswordWithValidChars s = if s' /= s then s' else go [] (reverse s)
  where
    s' = bumpConfusingChars s
    go xs [] = xs
    go xs (y:ys) = result
      where
        y' = nextPasswordChar y
        result = if y' == 'a'
                    then go (y':xs) ys
                    else bumpConfusingChars' xs (y':ys)

nextValidPassword :: String -> String
nextValidPassword s = head $ dropWhile (not . validPassword') itr
  where
    itr = iterate nextPasswordWithValidChars s

solve :: Int -> String -> String
solve 1 s = nextValidPassword s
solve 2 s = nextValidPassword s'
  where
    s' = nextPasswordWithValidChars $ nextValidPassword s

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ solve part contents
