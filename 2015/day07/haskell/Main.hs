module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Bits
import Data.Char
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Word

data DepNode = DepNode { variable :: String
                       , depNodes :: [String]
                       , line :: String
                       } deriving (Show)

type DepMap = Map String DepNode

parseDep :: String -> Maybe String
parseDep "->" = Nothing
parseDep "NOT" = Nothing
parseDep "OR" = Nothing
parseDep "AND" = Nothing
parseDep "LSHIFT" = Nothing
parseDep "RSHIFT" = Nothing
parseDep token
  | all isDigit token = Nothing
  | otherwise = Just token

parseDeps :: [String] -> [String]
parseDeps tokens = parseDeps' tokens []
  where
    parseDeps' [] deps = deps
    parseDeps' (t:ts) deps =
      case parseDep t of
        Nothing -> parseDeps' ts deps
        Just dep -> parseDeps' ts (dep:deps)


mkDepList :: [String] -> [(String, String, [String])]
mkDepList ls = map extract ls
  where
    extract l = (t, l, parseDeps ts) where (t:ts) = reverse $ words l

sortLinesByDeps :: [String] -> [String]
sortLinesByDeps ls = helper [] [] $ mkDepList ls
  where
    helper ls [] [] = reverse ls
    helper ls (n:ns) deps = helper (n:ls) ns deps
    helper ls [] deps = helper ls linesToAdd depsLeft
      where
        hasNoDeps (_, _, ds) = length ds == 0
        selectedDeps = filter hasNoDeps deps
        varsToRemove = map (\(x, _, _) -> x) selectedDeps
        linesToAdd = map (\(_, l, _) -> l) selectedDeps
        isKept y = not $ y `elem` varsToRemove
        remVars (x, l, ys) = (x, l, filter isKept ys)
        depsLeft = map remVars $ filter (\(x, _, _) -> isKept x) deps


data Machine = Machine { stack :: [Word16]
                       , memory :: Map String Word16
                       , operator :: Maybe String
                       } deriving (Show)

mkMachine :: Machine
mkMachine = Machine [] Map.empty Nothing

setOperator :: Machine -> String -> Machine
setOperator m s = Machine (stack m) (memory m) (Just s)

pushStack :: Machine -> Word16 -> Machine
pushStack m w = Machine (w:(stack m)) (memory m) (operator m)

popStack :: Machine -> (Word16, Machine)
popStack m = (x, Machine xs (memory m) (operator m))
  where x:xs = stack m

readMemory :: Machine -> String -> Machine
readMemory m var = Machine (val:xs) mem op
  where
    xs = stack m
    mem = memory m
    op = operator m
    val = case Map.lookup var mem of
            Just v -> v
            Nothing -> error ("Attempt to read unknown variable '" ++ var ++ "'")

applyNot :: Machine -> Machine
applyNot m = Machine xs' (memory m) Nothing
  where
    x:xs = stack m
    xs' = (complement x):xs

applyAnd :: Machine -> Machine
applyAnd m = Machine xs' (memory m) Nothing
  where
    x:y:xs = stack m
    xs' = (x .&. y):xs

applyOr :: Machine -> Machine
applyOr m = Machine xs' (memory m) Nothing
  where
    x:y:xs = stack m
    xs' = (x .|. y):xs

applyLeftShift :: Machine -> Machine
applyLeftShift m = Machine xs' (memory m) Nothing
  where
    x:y:xs = stack m
    xs' = (y `shiftL` (fromIntegral x)):xs

applyRightShift :: Machine -> Machine
applyRightShift m = Machine xs' (memory m) Nothing
  where
    x:y:xs = stack m
    xs' = (y `shiftR` (fromIntegral x)):xs

applyAssignment :: Machine -> String -> Machine
applyAssignment m var = Machine xs mem' Nothing
  where
    x:xs = stack m
    mem' = Map.insert var x (memory m)

processToken :: Machine -> String -> Machine
processToken m "NOT" = setOperator m "NOT"
processToken m "AND" = setOperator m "AND"
processToken m "OR" = setOperator m "OR"
processToken m "LSHIFT" = setOperator m "LSHIFT"
processToken m "RSHIFT" = setOperator m "RSHIFT"
processToken m "->" = setOperator m' "->"
  where
    m' = case operator m of
           Just "NOT" -> applyNot m
           Just "AND" -> applyAnd m
           Just "OR" -> applyOr m
           Just "LSHIFT" -> applyLeftShift m
           Just "RSHIFT" -> applyRightShift m
           otherwise -> m

processToken m value =
  case all isDigit value of
    True -> pushStack m (read value :: Word16)
    False -> case operator m of
      Just "->" -> applyAssignment m value
      _ -> readMemory m value

processTokens :: Machine -> [String] -> Machine
processTokens m [] = m
processTokens m (token:tokens) = processTokens (processToken m token) tokens

processLine :: Machine -> String -> Machine
processLine m s = processTokens m (words s)

processLines :: Machine -> [String] -> Machine
processLines m [] = m
processLines m (x:xs) = processLines (processLine m x) xs

getResult :: Machine -> Int
getResult m = fromIntegral result
  where (result, _) = popStack $ readMemory m "a"

solve :: Int -> String -> Int
solve 1 s = getResult $ processLines mkMachine (sortLinesByDeps $ lines s)
solve 2 s = getResult $ processLines mkMachine (newB:otherLines)
  where
    newB = (show $ solve 1 s) ++ " -> b"
    notAssignmentToB l = case reverse $ words l of
                           (x:ys) -> x /= "b"
                           _ -> True
    otherLines = filter notAssignmentToB $ sortLinesByDeps $ lines s

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
