module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List

data Box = Box {bxLength :: Int, bxWidth :: Int, bxHeight :: Int} deriving (Show)

readBox :: String -> Box
readBox s = Box l w h
  where
    xToSpace ch = if ch == 'x' then ' ' else ch
    [l, w, h] = map (\w -> read w :: Int) $ words $ map xToSpace s

lwArea :: Box -> Int
lwArea b = bxLength b * bxWidth b

lhArea :: Box -> Int
lhArea b = bxLength b * bxHeight b

whArea :: Box -> Int
whArea b = bxWidth b * bxHeight b

minFaceArea :: Box -> Int
minFaceArea b = minimum $ map (\f -> f b) [lwArea, lhArea, whArea]

surfaceArea :: Box -> Int
surfaceArea b = 2 * (lwArea b + lhArea b + whArea b)

wrappingPaperArea :: Box -> Int
wrappingPaperArea b = surfaceArea b + minFaceArea b

lwPerimeter :: Box -> Int
lwPerimeter b = 2 * (bxLength b + bxWidth b)

lhPerimeter :: Box -> Int
lhPerimeter b = 2 * (bxLength b + bxHeight b)

whPerimeter :: Box -> Int
whPerimeter b = 2 * (bxWidth b + bxHeight b)

minFacePerimeter :: Box -> Int
minFacePerimeter b = minimum $ map (\f -> f b) [lwPerimeter, lhPerimeter, whPerimeter]

volume :: Box -> Int
volume b = bxLength b * bxWidth b * bxHeight b

ribbonLength :: Box -> Int
ribbonLength b = minFacePerimeter b + volume b

solve :: Int -> String -> Int
solve 1 s = sum $ map (wrappingPaperArea . readBox) $ lines s
solve 2 s = sum $ map (ribbonLength . readBox) $ lines s

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
