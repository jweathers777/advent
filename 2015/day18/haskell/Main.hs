module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Maybe
import Data.Function
import Data.Map (Map)
import qualified Data.Map.Strict as Map

type GridLoc = (Int, Int)
type LightGrid = Map GridLoc Char

lightOn, lightOff :: Char
lightOn = '#'
lightOff = '.'

getGridValue :: LightGrid -> GridLoc -> Char
getGridValue grid loc = case Map.lookup loc grid of
  Just ch -> ch
  Nothing -> lightOff

updateGridValue :: LightGrid -> GridLoc -> Char -> LightGrid
updateGridValue grid loc value = Map.insert loc value grid

readLightGrid :: String -> LightGrid
readLightGrid s = Map.fromList $ concatMap rowAndLoc indexedLines
  where
    indexedLines = zip (lines s) [0..]
    indexedRow r = zip r [0..]
    rowAndLoc (r, i) = map (\(c,j) -> ((i,j), c)) $ indexedRow r

showLightGrid :: LightGrid -> String
showLightGrid grid = intercalate "\n" rowLines
  where
    rows = groupBy ((==) `on` fst) $ sort $ Map.keys $ grid
    rowLines = map (map (getGridValue grid)) rows


turnOnLight :: LightGrid -> GridLoc -> LightGrid
turnOnLight grid loc = updateGridValue grid loc lightOn

turnOffLight :: LightGrid -> GridLoc -> LightGrid
turnOffLight grid loc = updateGridValue grid loc lightOff

getNeighbors :: LightGrid -> GridLoc -> [GridLoc]
getNeighbors grid (r,c) = [upLeft, up, upRight, left, right, downLeft, down, downRight]
  where
    up = (r-1, c)
    down = (r+1, c)
    left = (r, c-1)
    right = (r, c+1)
    upLeft = (r-1, c-1)
    upRight = (r-1, c+1)
    downLeft = (r+1, c-1)
    downRight = (r+1, c+1)

countOnNeighbors :: LightGrid -> GridLoc -> Int
countOnNeighbors grid loc = length $ filter neighborOn $ getNeighbors grid loc
  where
    neighborOn n = (getGridValue grid n) == lightOn

countOffNeighbors :: LightGrid -> GridLoc -> Int
countOffNeighbors grid loc = length $ filter neighborOff $ getNeighbors grid loc
  where
    neighborOff n = (getGridValue grid n) == lightOff

advanceFrame :: LightGrid -> LightGrid
advanceFrame grid = foldl' updateCell grid $ Map.keys grid
  where
    updatedOnCell loc = if enoughOnNeighbors then lightOn else lightOff
      where
        onNeighbors = countOnNeighbors grid loc
        enoughOnNeighbors = onNeighbors == 2 || onNeighbors == 3
    updatedOffCell loc = if onNeighbors == 3 then lightOn else lightOff
      where
        onNeighbors = countOnNeighbors grid loc
    updatedCell loc = if (getGridValue grid loc) == lightOn
      then updatedOnCell loc
      else updatedOffCell loc
    updateCell g loc = updateGridValue g loc $ updatedCell loc

turnOnCorners :: LightGrid -> LightGrid
turnOnCorners grid = turnOnLight g3 (maxR, maxC)
  where
    (maxR, maxC) = head $ reverse $ sort $ Map.keys grid
    g1 = turnOnLight grid (0,0)
    g2 = turnOnLight g1 (0, maxC)
    g3 = turnOnLight g2 (maxR, 0)

advanceFrameFixedCorners :: LightGrid -> LightGrid
advanceFrameFixedCorners grid = turnOnCorners $ advanceFrame $ grid

countOnLights :: LightGrid -> Int
countOnLights grid = length $ filter ((lightOn==) . (getGridValue grid)) $ Map.keys grid

solve :: Int -> String -> Int
solve p s = countOnLights $ (iterate advance $ adjGrid) !! 100
  where
    grid = readLightGrid s
    adjGrid = if p == 1 then grid else turnOnCorners grid
    advance = if p == 1 then advanceFrame else advanceFrameFixedCorners


main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
