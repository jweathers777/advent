{-# LANGUAGE DeriveGeneric, TupleSections, LambdaCase #-}
module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Control.Applicative (Alternative(..), optional)
import Control.Monad (replicateM)
import Data.Bits (shiftL)
import Data.Char hiding (isControl)
import Data.Functor (($>))
import Data.List
import Data.Map (Map)
import GHC.Generics (Generic)
import Numeric (showHex)
import qualified Data.Map as Map

-- JSON parsing code taken from:
-- https://abhinavsarkar.net/posts/json-parsing-from-scratch-in-haskell/
data JValue = JNull
            | JBool Bool
            | JString String
            | JNumber { int :: Integer, frac :: [Int], exponent :: Integer }
            | JArray [JValue]
            | JObject [(String, JValue)]
            deriving (Eq, Generic)

instance Show JValue where
  show value = case value of
    JNull -> "null"
    JBool True -> "true"
    JBool False -> "false"
    JString s -> showJSONString s
    JNumber n [] 0 -> show n
    JNumber n f 0 -> show n ++ "." ++ concatMap show f
    JNumber n [] e -> show n ++ "e" ++ show e
    JNumber n f e -> show n ++ "." ++ concatMap show f ++ "e" ++ show e
    JArray a -> "[" ++ intercalate ", " (map show a) ++ "]"
    JObject o -> "{" ++ intercalate ", " (map showKeyVal o) ++ "}"
    where
      showKeyVal (k, v) = showJSONString k ++ ": " ++ show v

showJSONString :: String -> String
showJSONString s = "\"" ++ concatMap showJSONChar s ++ "\""

isControl :: Char -> Bool
isControl c = c `elem` ['\0' .. '\31']

showJSONChar :: Char -> String
showJSONChar c = case c of
  '\'' -> "'"
  '\"' -> "\\\""
  '\\' -> "\\\\"
  '/' -> "\\/"
  '\b' -> "\\b"
  '\f' -> "\\f"
  '\n' -> "\\n"
  '\r' -> "\\r"
  '\t' -> "\\t"
  _ | isControl c -> "\\u" ++ showJSONNonASCIIChar c
  _ -> [c]
  where
    showJSONNonASCIIChar c =
      let a = "0000" ++ showHex (ord c) "" in drop (length a - 4) a

newtype Parser i o = Parser { runParser :: i -> Maybe (i, o) }

instance Functor (Parser i) where
  fmap f parser = Parser $ fmap (fmap f) . runParser parser

instance Applicative (Parser i) where
  pure x = Parser $ pure . (, x)
  pf <*> po = Parser $ \input -> case runParser pf input of
    Nothing -> Nothing
    Just (rest, f) -> fmap f <$> runParser po rest

instance Alternative (Parser i) where
  empty = Parser $ const empty
  p1 <|> p2 = Parser $ \input -> runParser p1 input <|> runParser p2 input

instance Monad (Parser i) where
  p >>= f = Parser $ \input -> case runParser p input of
    Nothing -> Nothing
    Just (rest, o) -> runParser (f o) rest

satisfy :: (a -> Bool) -> Parser [a] a
satisfy predicate = Parser $ \case
  (x:xs) | predicate x -> Just (xs, x)
  _ -> Nothing

char :: Char -> Parser String Char
char c = satisfy (== c)

jsonChar :: Parser String Char
jsonChar = string "\\\"" $> '"'
  <|> string "\\\\" $> '\\'
  <|> string "\\/" $> '/'
  <|> string "\\b" $> '\b'
  <|> string "\\f" $> '\f'
  <|> string "\\n" $> '\n'
  <|> string "\\r" $> '\r'
  <|> string "\\t" $> '\t'
  <|> unicodeChar
  <|> satisfy (\c -> not (c == '\"' || c == '\\' || isControl c))
  where
    unicodeChar =
      chr . fromIntegral . digitsToNumber 16 0
        <$> (string "\\u" *> replicateM 4 hexDigit)

    hexDigit = digitToInt <$> satisfy isHexDigit

digitsToNumber :: Int -> Integer -> [Int] -> Integer
digitsToNumber base =
  foldl (\num d -> num * fromIntegral base + fromIntegral d)

digit :: Parser String Int
digit = digitToInt <$> satisfy isDigit

string :: String -> Parser String String
string "" = pure ""
string (c:cs) = (:) <$> char c <*> string cs

jNull :: Parser String JValue
jNull = string "null" $> JNull

jBool :: Parser String JValue
jBool = string "true" $> JBool True
  <|> string "false" $> JBool False

jString :: Parser String JValue
jString = JString <$> (char '"' *> jString')
  where
    jString' = do
      optFirst <- optional jsonChar
      case optFirst of
        Nothing -> "" <$ char '"'
        Just first | not (isSurrogate first) -> (first:) <$> jString'
        Just first -> do
          second <- jsonChar
          if isHighSurrogate first && isLowSurrogate second
             then (combineSurrogates first second :) <$> jString'
             else empty

highSurrogateLowerBound, highSurrogateUpperBound :: Int
highSurrogateLowerBound = 0xD800
highSurrogateUpperBound = 0xDBFF

lowSurrogateLowerBound, lowSurrogateUpperBound :: Int
lowSurrogateLowerBound = 0xDC00
lowSurrogateUpperBound = 0xDFFF

isHighSurrogate, isLowSurrogate, isSurrogate :: Char -> Bool
isHighSurrogate a =
  ord a >= highSurrogateLowerBound && ord a <= highSurrogateUpperBound
isLowSurrogate a =
  ord a >= lowSurrogateLowerBound && ord a <= lowSurrogateUpperBound
isSurrogate a = isHighSurrogate a || isLowSurrogate a

combineSurrogates :: Char -> Char -> Char
combineSurrogates a b = chr $
  ((ord a - highSurrogateLowerBound) `shiftL` 10)
  + (ord b - lowSurrogateLowerBound) + 0x10000

jUInt :: Parser String Integer
jUInt = (\d ds -> digitsToNumber 10 0 (d:ds)) <$> digit19 <*> digits
  <|> fromIntegral <$> digit

digit19 :: Parser String Int
digit19 = digitToInt <$> satisfy (\x -> isDigit x && x /= '0')

digits :: Parser String [Int]
digits = some digit

jInt' :: Parser String Integer
jInt' = signInt <$> optional (char '-') <*> jUInt

signInt :: Maybe Char -> Integer -> Integer
signInt (Just '-') i = negate i
signInt _ i = i

jFrac :: Parser String [Int]
jFrac = char '.' *> digits

jExp :: Parser String Integer
jExp = (char 'e' <|> char 'E')
  *> (signInt <$> optional (char '+' <|> char '-') <*> jUInt)

jInt :: Parser String JValue
jInt = JNumber <$> jInt' <*> pure [] <*> pure 0

jIntExp :: Parser String JValue
jIntExp = JNumber <$> jInt' <*> pure [] <*> jExp

jIntFrac :: Parser String JValue
jIntFrac = JNumber <$> jInt' <*> jFrac <*> pure 0

jIntFracExp :: Parser String JValue
jIntFracExp = JNumber <$> jInt' <*> jFrac <*> jExp

jNumber :: Parser String JValue
jNumber = jIntFracExp <|> jIntExp <|> jIntFrac <|> jInt

surroundedBy :: Parser String a -> Parser String b -> Parser String a
surroundedBy p1 p2 = p2 *> p1 <* p2

separatedBy :: Parser i v -> Parser i s -> Parser i [v]
separatedBy v s = (:) <$> v <*> many (s *> v) <|> pure []

spaces :: Parser String String
spaces = many (char ' ' <|> char '\n' <|> char '\r' <|> char '\t')

jArray :: Parser String JValue
jArray = JArray <$>
  (char '['
    *> (jValue `separatedBy` char ',' `surroundedBy` spaces)
    <* char ']')

jObject :: Parser String JValue
jObject = JObject <$>
  (char '{' *> pair `separatedBy` char ',' `surroundedBy` spaces <* char '}')
  where
    pair = (\ ~(JString s) j -> (s, j))
      <$> (jString `surroundedBy` spaces)
      <* char ':'
      <*> jValue

jValue :: Parser String JValue
jValue = jValue' `surroundedBy` spaces
  where
    jValue' = jNull
      <|> jBool
      <|> jString
      <|> jNumber
      <|> jArray
      <|> jObject

parseJSON :: String -> Maybe JValue
parseJSON s = case runParser jValue s of
  Just ("", j) -> Just j
  _ -> Nothing

filterJSON :: (JValue -> Bool) -> JValue -> JValue
filterJSON pred value = if pred value then go pred value else JNull
  where
    f = filterJSON pred
    go pred value = case value of
      JObject pairs -> JObject $ map (fmap f) $ filter (snd . fmap pred) pairs
      JArray values -> JArray $ map f $ filter pred values
      _ -> value

hasRedAssocValues :: JValue -> Bool
hasRedAssocValues value = case value of
  JObject pairs -> any (snd . fmap (==JString "red")) pairs
  _ -> False

sumValuesInJson :: JValue -> Double
sumValuesInJson value = go 0 value
  where
    go acc value = case value of
      JObject pairs -> sum $ map (sumValuesInJson . snd) pairs
      JArray values -> sum $ map sumValuesInJson values
      JNumber _ _ _ -> read $ show value
      _ -> 0

solve :: Int -> String -> Int
solve p s = fromIntegral . truncate . sumValuesInJson $ json
  where
    pred = not . hasRedAssocValues
    json = case parseJSON s of
             Just j -> if p == 1 then j else filterJSON pred j
             Nothing -> error "Invalid JSON"

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
