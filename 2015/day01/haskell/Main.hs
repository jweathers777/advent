module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List

countChar :: Char -> String -> Int
countChar ch = length . (filter (==ch))

stepSize :: Char -> Int
stepSize '(' = 1
stepSize ')' = -1

partialSum :: [Int] -> Int -> Int
partialSum xs n = sum $ take n xs

computeFloors :: String -> [Int]
computeFloors s = map (partialSum (map stepSize s)) [1..length s]

solve :: Int -> String -> Int
solve 1 s = countChar '(' s - countChar ')' s
solve 2 s = 1 + (length $ takeWhile (/=(-1)) $ computeFloors s)

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
           fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
