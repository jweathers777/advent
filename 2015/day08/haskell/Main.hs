module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.Char
import Data.List

charsNotStoredCount :: String -> Int
charsNotStoredCount s = counter 0 s
  where
    counter n [] = n
    counter n ('\\':'\\':xs) = counter (n+1) xs
    counter n ('\\':'"':xs) = counter (n+1) xs
    counter n ('\\':'x':x:y:xs) =
      case isHexDigit x &&  isHexDigit y of
        True -> counter (n+3) xs
        False -> error ("'" ++ (x:y:"' is not a valid hex code"))
    counter n ('"':xs) = counter (n+1) xs
    counter n (x:xs) = counter n xs

charsAddedToEncodeCount :: String -> Int
charsAddedToEncodeCount s = counter 2 s
  where
    counter n [] = n
    counter n ('\\':xs) = counter (n+1) xs
    counter n ('"':xs) = counter (n+1) xs
    counter n (x:xs) = counter n xs

solve :: Int -> String -> Int
solve 1 s = sum $ map charsNotStoredCount $ lines s
solve 2 s = sum $ map charsAddedToEncodeCount $ lines s

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
