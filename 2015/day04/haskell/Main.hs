module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as BLC
import Data.Digest.Pure.MD5

computeHash :: String -> Int -> String
computeHash key num =
  show $ md5 $ BLC.pack $ key ++ (show num)

startsWithFiveZeroes :: String -> Bool
startsWithFiveZeroes = isPrefixOf "00000"

startsWithSixZeroes :: String -> Bool
startsWithSixZeroes = isPrefixOf "000000"

solve :: Int -> String -> Int
solve 1 s = fromJust $ find (startsWithFiveZeroes . (computeHash s)) [1..]
solve 2 s = fromJust $ find (startsWithSixZeroes . (computeHash s)) [1..]

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
