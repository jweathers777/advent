module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map
import Text.ParserCombinators.Parsec

data SueRecord = SueRecord { sid :: Int, compounds :: Map String Int }
  deriving (Show)

integer :: Parser Int
integer = rd <$> (zero <|> number <|> minus)
  where
    rd = read :: String -> Int
    zero = string "0"
    number = many1 digit
    minus = (:) <$> char '-' <*> number

sue :: Parser SueRecord
sue = SueRecord <$> sid <*> compounds
  where
    sid = string "Sue " *> integer <* string ": "
    compound = (,) <$> (many letter <* string ": ") <*> integer
    compounds = Map.fromList <$> compound `sepBy` string ", "

readSueRecord :: String -> SueRecord
readSueRecord s = case parse sue "" s of
  Left e -> error ("Error parsing Sue Record\n"++(show e))
  Right sr -> sr

readSueRecords :: String -> [SueRecord]
readSueRecords s = map readSueRecord $ lines s

detectedCompounds :: [(String, Int, Int -> Int -> Bool)]
detectedCompounds = [ ("children", 3, (==))
                    , ("cats", 7, (>))
                    , ("samoyeds", 2, (==))
                    , ("pomeranians", 3, (<))
                    , ("akitas", 0, (==))
                    , ("vizslas", 0, (==))
                    , ("goldfish", 5, (<))
                    , ("trees", 3, (>))
                    , ("cars", 2, (==))
                    , ("perfumes", 1, (==))
                    ]

filterByCompound :: [SueRecord] -> (String, Int, Int -> Int -> Bool) -> [SueRecord]
filterByCompound sueRecords (compound, value, cmpOp) = matchingRecords
  where
    matchesCompound sueRecord = case Map.lookup compound (compounds sueRecord) of
      Just v -> cmpOp v value
      Nothing -> True
    matchingRecords = filter matchesCompound sueRecords

solve :: Int -> String -> Int
solve p s = sid bestMatchingRecord
  where
    sueRecords = readSueRecords s
    compoundTuples = map (if p == 1 then (\(x,y,z) -> (x,y,(==))) else id) detectedCompounds
    matchingRecords = foldl' filterByCompound sueRecords compoundTuples
    compoundKeys = map (\(x,_,_) -> x) detectedCompounds
    compareByCompound bestRecord record = best
      where
        bestKeys = intersect compoundKeys $ Map.keys $ compounds bestRecord
        currKeys = intersect compoundKeys $ Map.keys $ compounds record
        best = if length bestKeys < length currKeys then record else bestRecord

    bestMatchingRecord = foldl1' compareByCompound matchingRecords


main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
