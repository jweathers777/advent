module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

type CostMap = Map (String, String) Int
type DistanceMap = Map (Set String, String) Int
type PredecessorMap = Map (Set String, String) String

data HKState = HKState { start :: String
                       , costs :: CostMap
                       , distances :: DistanceMap
                       , predecessors :: PredecessorMap
                       }

data Direction = Min | Max deriving (Eq)

parseCostMap :: String -> CostMap
parseCostMap s = Map.fromList allPairs
  where
    tokensToPair (start:"to":finish:"=":cost:[]) = ((start, finish), read cost :: Int)
    tokensToPair _ = error "Invalid tokens"
    tokenPairs = map (tokensToPair . words) $ lines s
    revTokenPars = map (\((s, f), d) -> ((f,s), d)) tokenPairs
    allPairs = tokenPairs ++ revTokenPars

getCost :: String -> String -> CostMap -> Int
getCost start finish costs =
  case Map.lookup (start, finish) costs of
    Nothing -> error ("Invalid start and finish: ("++start++","++finish++")")
    Just c -> c

getDistance :: Set String -> String -> DistanceMap -> Int
getDistance set finish distances =
  case Map.lookup (set, finish) distances of
    Nothing -> error ("Invalid set and finish: ("++(show set)++","++finish++")")
    Just d -> d

getPredecessor :: Set String -> String -> PredecessorMap -> String
getPredecessor set finish predecessors =
  case Map.lookup (set, finish) predecessors of
    Nothing -> error ("Invalid set and finish: ("++(show set)++","++finish++")")
    Just p -> p

setDistance :: Set String -> String -> Int -> DistanceMap -> DistanceMap
setDistance set finish d distances = Map.insert (set, finish) d distances

setPredecessor :: Set String -> String -> String -> PredecessorMap -> PredecessorMap
setPredecessor set finish p predecessors = Map.insert (set, finish) p predecessors

setDistanceForHKState :: Set String -> String -> Int -> HKState -> HKState
setDistanceForHKState set finish d h = HKState start' costs' distances' predecessors'
  where
    start' = start h
    costs' = costs h
    distances' = setDistance set finish d (distances h)
    predecessors' = predecessors h

setDistAndPredForHKState :: Set String -> String -> Int -> String -> HKState -> HKState
setDistAndPredForHKState set finish d p h = HKState start' costs' distances' predecessors'
  where
    start' = start h
    costs' = costs h
    distances' = setDistance set finish d (distances h)
    predecessors' = setPredecessor set finish p (predecessors h)

computeDistanceAndPredecessor :: Direction -> Set String -> String -> HKState -> HKState
computeDistanceAndPredecessor dir set f h
  | Set.size set == 0 = setDistanceForHKState set f c h
  | Set.size set == 1 = setDistAndPredForHKState set f d1 e1 h
  | otherwise = setDistAndPredForHKState set f d p h
  where
    hcosts = costs h
    hdists = distances h
    c = getCost (start h) f hcosts
    setList = Set.toList set
    e1 = head setList
    d1 = (getCost e1 f hcosts) + (getDistance Set.empty e1 hdists)
    comp e = Set.delete e set
    efSum e = (getCost e f hcosts) + (getDistance (comp e) e hdists)
    op = if dir == Min then (<) else (>)
    combine (bestS, bestE) (s,e) = if op s bestS then (s,e) else (bestS, bestE)
    (d, p) = foldl1' combine $ map (\e -> (efSum e, e)) setList

computeSetValues :: Direction -> Set String -> [Set String] -> HKState -> HKState
computeSetValues dir _ [] h = h
computeSetValues dir universe (set:sets) h = computeSetValues dir universe sets h'
  where
    complement = Set.toList $ Set.difference universe set
    compute hCurr f = computeDistanceAndPredecessor dir set f hCurr
    h' = foldl' compute h complement

extendPath :: HKState -> Set String -> [String] -> [String]
extendPath h elements path
  | Set.size elements == 0 = (start h) : path
  | otherwise = extendPath h elements' path'
  where
    last = head path
    pred = getPredecessor elements last (predecessors h)
    elements' = Set.delete pred elements
    path' = pred : path

findOptimalTour :: Direction -> CostMap -> ([String], Int)
findOptimalTour dir costs = (path, dist)
  where
    set = Set.fromList $ concat $ unzip $ Map.keys costs
    setList = Set.toList set
    s = head setList
    others = Set.fromList $ tail setList
    subsets = Set.toList $ Set.delete others $ Set.powerSet others
    sortedSubsets = sortBy (\s1 s2 -> compare (Set.size s1) (Set.size s2)) subsets
    h0 = HKState s costs Map.empty Map.empty
    h1 = computeSetValues dir others sortedSubsets h0
    h2 = computeDistanceAndPredecessor dir others s h1
    dist = getDistance others s (distances h2)
    path = extendPath h2 others [start h2]

solve :: Int -> String -> Int
solve p s = snd $ findOptimalTour dir costs
  where
    dir = if p == 1 then Min else Max
    baseCosts = parseCostMap s
    nodes = Set.toList $ Set.fromList $ concat $ unzip $ Map.keys baseCosts
    update oldCosts node = newCosts
      where
        midCosts = Map.insert ("NorthPole", node) 0 oldCosts
        newCosts = Map.insert (node, "NorthPole") 0 midCosts
    costs = foldl' update baseCosts nodes

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
