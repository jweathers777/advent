module Main where

import System.Environment (getArgs)
import System.IO (readFile)
import Data.List
import Text.ParserCombinators.Parsec

data Ingredient = Ingredient { name :: String
                             , capacity :: Int
                             , durability ::  Int
                             , flavor :: Int
                             , texture :: Int
                             , calories :: Int
                             } deriving (Show)

integer :: Parser Int
integer = rd <$> (zero <|> number <|> minus)
  where
    rd = read :: String -> Int
    zero = string "0"
    number = many1 digit
    minus = (:) <$> char '-' <*> number

ingredient :: Parser Ingredient
ingredient = do
  iName <- many $ noneOf " :"
  string ": capacity "
  iCapacity <- integer
  string ", durability "
  iDurability <- integer
  string ", flavor "
  iFlavor <- integer
  string ", texture "
  iTexture <- integer
  string ", calories "
  iCalories <- integer
  return $ Ingredient iName iCapacity iDurability iFlavor iTexture iCalories

readIngredient :: String -> Ingredient
readIngredient s = case parse ingredient "" s of
  Left e -> error ("Error parsing ingredient\n"++(show e))
  Right i -> i

readIngredients :: String -> [Ingredient]
readIngredients s = map readIngredient $ lines s

ingredientVector :: Ingredient -> [Int]
ingredientVector i = [capacity i, durability i, flavor i, texture i, calories i]

ingredientScores :: [(Int, Ingredient)] -> [Int]
ingredientScores scaledIngredients = foldl1' (\u v -> zipWith (+) u v) scaledVectors
  where
    scaledVectors = map (\(x,y) -> fmap (x*) $ ingredientVector y) scaledIngredients

cookieScore :: [(Int, Ingredient)] -> Int
cookieScore scaledIngredients =
  foldl1' (\p e -> p * (max 0 e)) $ take 4 $ ingredientScores scaledIngredients

cookieScoreForCalories :: Int -> [(Int, Ingredient)] -> Int
cookieScoreForCalories targetCalories  scaledIngredients = score
  where
    ingScores = ingredientScores scaledIngredients
    calorieScore = last ingScores
    score = if calorieScore == targetCalories
            then foldl1' (\p e -> p * (max 0 e)) $ take 4 ingScores
            else -1

scaleVectors :: Int -> Int -> [[Int]]
scaleVectors 1 total = [[total]]
scaleVectors n total = foldl' (\a e -> a ++ pivotVectors e) [] pivots
  where
    pivots = enumFromTo 1 (total-1)
    pivotVectors x = map (x:) $ scaleVectors (n-1) (total -x)

maxCookieScore :: [Ingredient] -> Int
maxCookieScore ingredients = maximum scores
  where
    n = length ingredients
    scores = map (cookieScore . flip zip ingredients) $ scaleVectors n 100

maxCookieScoreForCalories :: Int -> [Ingredient] -> Int
maxCookieScoreForCalories targetCalories ingredients = maximum scores
  where
    n = length ingredients
    scores = map (cookieScoreForCalories targetCalories . flip zip ingredients) $ scaleVectors n 100

solve :: Int -> String -> Int
solve p s = f $ readIngredients s
  where f = if p == 1 then maxCookieScore else maxCookieScoreForCalories 500

main :: IO ()
main = do
  args <- getArgs
  if length args < 2
     then do
       putStrLn "Invalid number of arguments!"
     else do
       let part = read (args !! 0) :: Int
       let fileName = args !! 1

       contents <- readFile fileName
       putStrLn $ show $ solve part contents
