class Screen
  def initialize(width, length)
    @width = width
    @length = length
    @pixels = Array.new(@length) { Array.new(@width, 0) }
  end

  def inspect
    to_s
  end

  def to_s
    puts
    @length.times do |row|
      puts @width.times.map {|col| @pixels[row][col] == 1 ? '#' : '.'}.join(' ')
    end
  end

  def rect(a, b)
    b.times do |row|
      a.times do |col|
        @pixels[row][col] = 1
      end
    end
    self
  end

  def rotate_row(a, b)
    new_row = Array.new(@width)
    @width.times do |col|
      new_col = (col + b) % @width
      new_row[new_col] = @pixels[a][col]
    end
    @width.times do |col|
      @pixels[a][col] = new_row[col]
    end
    self
  end

  def rotate_col(a, b)
    new_col = Array.new(@length)
    @length.times do |row|
      new_row = (row + b) % @length
      new_col[new_row] = @pixels[row][a]
    end
    @length.times do |row|
      @pixels[row][a] = new_col[row]
    end
    self
  end

  def run_file(fname)
    IO.readlines(fname).each do |instruction|
      instruction.strip!
      if match = instruction.match(/rect (\d+)x(\d+)/)
        a = match[1].to_i
        b = match[2].to_i
        rect(a,b)
      elsif match = instruction.match(/rotate row y=(\d+) by (\d+)/)
        a = match[1].to_i
        b = match[2].to_i
        rotate_row(a, b)
      elsif match = instruction.match(/rotate column x=(\d+) by (\d+)/)
        a = match[1].to_i
        b = match[2].to_i
        rotate_col(a, b)
      end
    end
  end

  def lights_on
    @pixels.reduce(0) {|sum, row| sum + row.reduce(0) {|s,e| s+e} }
  end
end

