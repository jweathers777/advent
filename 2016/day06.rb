def compute_message(source)
  data = IO.readlines(source).map {|row| row.strip.split('')}
  tdata = data[0].length.times.map {|i| data.map {|row| row[i]}}
  tdata.length.times.
    map {|i| tdata[i].group_by {|e| e}.
    values.sort_by(&:length).reverse.first.first}.join('')
end

def compute_message2(source)
  data = IO.readlines(source).map {|row| row.strip.split('')}
  tdata = data[0].length.times.map {|i| data.map {|row| row[i]}}
  tdata.length.times.
    map {|i| tdata[i].group_by {|e| e}.
    values.sort_by(&:length).first.first}.join('')
end

