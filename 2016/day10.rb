class Bot
  attr_accessor :instructions, :values

  def initialize(factory, bot_id)
    @factory = factory
    @bot_id = bot_id
    @instructions = []
    @values = []
    @given = []
  end

  def inspect
    "{@bot_id=#{@bot_id}, @instructions=#{@instructions.inspect}, @values=#{@values.inspect}}"
  end

  def add_value_to_recipient(recipient, recipient_id, value)
    if recipient == 'bot'
      @factory.bots[recipient_id].add_value(value)
    else
      @factory.outputs[recipient_id] = value
    end
  end

  def add_value(value)
    @values << value
    try_executing_instruction
  end

  def add_giving(low_recipient, low_recipient_id, high_recipient, high_recipient_id)
    @instructions.push([low_recipient, low_recipient_id, high_recipient, high_recipient_id])
    try_executing_instruction
  end

  def try_executing_instruction
    if @values.length == 2 and @instructions.length > 0
      puts "Executing #{@instructions.last} for #{@bot_id}"
      low_recipient, low_recipient_id, high_recipient, high_recipient_id = @instructions.pop
      add_value_to_recipient(low_recipient, low_recipient_id, @values.min)
      add_value_to_recipient(high_recipient, high_recipient_id, @values.max)
      #@given.push([@values.min, @values.max])
      #@values.clear
    end
  end
end

class BotFactory
  VALUE_INSTRUCTION = /value (\d+) goes to bot (\d+)/
  GIVE_INSTRUCTION = /bot (\d+) gives low to (output|bot) (\d+) and high to (output|bot) (\d+)/

  attr_accessor :bots, :outputs

  def initialize
    @bots = Hash.new {|h,k| h[k] = Bot.new(self, k)}
    @outputs = Hash.new
    @recipients = {'bot' => @bots, 'output' => @outputs}
  end

  def inspect
    "{@bots=#{@bots.inspect}, @outputs=#{@outputs.inspect}}"
  end

  def process_instruction(instruction)
    case instruction
    when VALUE_INSTRUCTION
      bot_id, value = $2,$1
      @bots[bot_id].add_value(value.to_i)
      puts "#{instruction} -> #{@bots[bot_id]}"
    when GIVE_INSTRUCTION
      giver_id, low_recipient, low_recipient_id, high_recipient, high_recipient_id = $1,$2,$3,$4,$5
      puts "#{instruction} stored."
      @bots[giver_id].add_giving(low_recipient, low_recipient_id, high_recipient, high_recipient_id)
    end
  end

  def run_program(fname)
    File.new(fname).each_line {|line| process_instruction(line.strip)}
  end

  def who_compared_values(a, b)
    m, x = [a,b].sort
    @bots.values.select {|bot| bot.values.sort == [m,x]}
  end
end

def doit
  bf = BotFactory.new
  bf.run_program('day10.dat')
  puts bf.who_compared_values(61,17)
  puts %w{0 1 2}.reduce(1) {|p,e| p * bf.outputs[e]}
end
