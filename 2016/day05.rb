require 'digest'

def make_password(door_id)
  md5 = Digest::MD5.new
  index = 0
  8.times.map do
    while true
      hash = md5.hexdigest("#{door_id}#{index}")
      break if hash[0..4] == '00000'
      index += 1
    end
    index += 1
    hash[5]
  end.join('')
end

def make_password2(door_id)
  md5 = Digest::MD5.new
  index = 0
  password = ' '*8
  letters_found = 0
  while letters_found < 8
    while true
      hash = md5.hexdigest("#{door_id}#{index}")
      break if hash[0..4] == '00000'
      index += 1
    end
    index += 1
    if ('0'..'7').include?(hash[5])
      pos = hash[5].to_i
      if password[pos] == ' '
        password[pos] = hash[6]
        letters_found += 1
        puts password
      end
    end
  end
  password
end
