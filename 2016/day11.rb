require 'fc' # gem install priority_queue_cxx

class RadioisotopeTestingFacility
  INFINITY = 1_000_000

  attr_reader :floors, :fuels, :top_floor
  attr_reader :arrangement

  def initialize(floors, fuels, elevator_floor, microchip_floors, generator_floors)
    @floors = floors
    @fuels = fuels
    @columns = 2*(@fuels.length) + 1
    @arrangement = Array.new(floors*@columns, 0)
    @top_floor = @floors - 1

    @last_index = @arrangement.length - 1
    @top_floor_start_index = @last_index - @columns + 1

    @elevator = elevator_floor
    elevator_index = index_from_row_column(elevator_floor, 0)
    @arrangement[elevator_index] = 1

    @fuels.each_with_index do |fuel, index|
      generator_row = generator_floors[fuel]
      microchip_row = microchip_floors[fuel]
      generator_col = 2*index + 1
      microchip_col = generator_col + 1
      generator_index = index_from_row_column(generator_row, generator_col)
      microchip_index = index_from_row_column(microchip_row, microchip_col)
      @arrangement[microchip_index] = 1
      @arrangement[generator_index] = 1
    end
  end

  def inspect
    s = "\n"

    start_index = @top_floor_start_index
    (@floors-1).downto(0) do |floor|
      s << "F#{floor+1} "
      index = start_index
      s << (@arrangement[index] == 1 ? 'E' : '.')
      s << '   '
      @fuels.each do |fuel|
        %w{g m}.each do |type|
          index += 1
          s << ' '
          s << (@arrangement[index] == 1 ? "#{fuel[0..2]}#{type}" : '.   ')
        end
      end
      s << "\n"
      start_index -= @columns
    end
    s
  end

  def to_s
    inspect
  end

  def arrangement=(arrangement)
    @elevator = @floors.times.find {|floor| arrangement[floor*@columns] == 1}
    @arrangement = arrangement
  end

  def index_from_row_column(row, col)
    row*@columns + col
  end

  def end_state?
    (@top_floor_start_index..@last_index).all? {|e| @arrangement[e] == 1}
  end

  def remaining_cost_estimate
    cost = 0
    @top_floor.times do |floor|
      @columns.times do |col|
        index = floor*@columns + col
        if @arrangement[index] == 1
          cost += @top_floor - floor
        end
      end
    end
    cost
  end

  def apply_move(move)
    floor_shift, piece_load = move
    new_elevator = @elevator + floor_shift
    floor_start_index = @elevator*@columns
    new_floor_start_index = new_elevator*@columns

    @arrangement[floor_start_index] = 0
    @arrangement[new_floor_start_index] = 1

    piece_load.each_with_index do |piece,index|
      @arrangement[floor_start_index + index + 1] -= piece
      @arrangement[new_floor_start_index + index + 1] += piece
    end
    @elevator = new_elevator
    self
  end

  def unapply_move(move)
    floor_shift, piece_load = move
    apply_move([-floor_shift, piece_load])
  end

  def valid_floor?(floor)
    any_generators = floor.each_index.any? {|index| floor[index] == 1 and index % 2 == 0}

    if any_generators
      floor.each_index do |index|
        return false if floor[index] == 1 and index % 2 == 1 and floor[index - 1] == 0
      end
    end

    return true
  end

  def valid_move?(move)
    floor_shift, piece_load = move
    new_elevator = @elevator + floor_shift
    start_index = @elevator*@columns + 1
    end_index = start_index + @columns - 2

    # Validate starting floor after move
    floor = (start_index..end_index).
      map {|i| @arrangement[i] - piece_load[i-start_index]}
    return false unless valid_floor?(floor)

    # Validate destination floor after move
    start_index = new_elevator*@columns + 1
    end_index = start_index + @columns - 2
    floor = (start_index..end_index).
      map {|i| @arrangement[i] + piece_load[i-start_index]}
    valid_floor?(floor)
  end

  def generate_moves
    start_index = @elevator*@columns + 1
    end_index = start_index + @columns - 2
    candidates = []
    candidates = (start_index..end_index).select {|i| @arrangement[i] == 1}.
      map {|i| (i % @columns) - 1}
    microchips = candidates.select {|c| c % 2 == 1}
    generators = candidates.select {|c| c % 2 == 0}

    b = [0]*(@columns - 1)
    moves = []

    candidates.each do |c|
      pl = b.clone
      pl[c] = 1
      moves << [1, pl] if @elevator < @floors - 1

      if @elevator > 0
        # Don't move down by itself if this is a microchip paired with a generator
        unless c % 2 == 1 and generators.include?(c-1)
          moves << [-1, pl]
        end
      end
    end

    [microchips, generators].each do |type|
      type.combination(2).each do |i,j|
        pl = b.clone
        pl[i] = 1
        pl[j] = 1
        moves << [1,pl] if @elevator < @floors - 1
        moves << [-1,pl] if @elevator > 0
      end
    end

    generators.each do |c|
      if microchips.include?(c+1)
        pl = b.clone
        pl[c] = 1
        pl[c+1] = 1
        moves << [1,pl] if @elevator < @floors - 1
        moves << [-1,pl] if @elevator > 0
      end
    end

    moves.select {|move| valid_move?(move)}
  end

  class << self
    def extract_components(tokens)
      tokens.each_with_index.select {|token,index| token == 'microchip' or token == 'generator'}.
        map {|token,index| [tokens[index - 1].upcase, tokens[index]]}
    end

    def from_string(s)
      floor_indices = %w{first second third fourth}.each_with_index.
        reduce({}) {|h,e| h[e[0]] = e[1]; h}

      floors = 4
      fuels = {}
      elevator_floor = 0
      type_floors = {'microchip' => {}, 'generator' => {}}

      s.split("\n").each do |line|
        line.gsub!(',','')
        line =~ /The (.+) floor contains (.+)\./
        floor_index = floor_indices[$1]
        tokens = $2.split(/-compatible | /)
        extract_components(tokens).each do |fuel, type|
          fuels[fuel] = true
          type_floors[type][fuel] = floor_index
        end
      end

      RadioisotopeTestingFacility.new(floors, fuels.keys, 0, type_floors['microchip'], type_floors['generator'])
    end
  end

  def reconstruct_path(came_from, state)
    path = [[state,nil]]
    while prev = came_from[state]
      state,move = prev
      path << prev
    end
    path.reverse
  end

  def search
    original_state = arrangement.dup

    state = arrangement.dup
    closed_set = {}
    open_set = {}
    queue = FastContainers::PriorityQueue.new(:min)
    depth_for = {}
    came_from = {}
    cost_from_start = {}
    cost_to_goal_estimate = {}

    depth_for[state] = 0
    cost_from_start[state] = 0
    cost_to_goal_estimate[state] = remaining_cost_estimate
    queue.push(state, remaining_cost_estimate)
    open_set[state] = true

    while not queue.empty?
      state = queue.pop
      depth = depth_for[state]
      self.arrangement = state
      if end_state?
        self.arrangement = original_state
        return reconstruct_path(came_from, state)
      end

      open_set[state] = false
      closed_set[state] = true
      current_cost = cost_from_start[state]
      current_cost_estimate = cost_to_goal_estimate[state]

      next_moves = generate_moves
      puts "*"*20
      puts self
      used_moves = []
      next_moves.each do |move|
        apply_move(move)
        neighbor = arrangement.dup
        unless closed_set[neighbor]
          cost = current_cost + 1
          cost_estimate = nil
          neighbor_cost_from_start = cost_from_start[neighbor]
          cost_estimate = cost + remaining_cost_estimate
          unless open_set[neighbor]
            open_set[neighbor] = true
            used_moves << [cost_estimate, move]
            queue.push(neighbor, cost_estimate)
          end

          unless neighbor_cost_from_start and cost >= neighbor_cost_from_start
            came_from[neighbor] = [state,move]
            depth_for[neighbor] = depth + 1
            cost_from_start[neighbor] = cost
            cost_to_goal_estimate[neighbor] = cost_estimate
          end
        end
        unapply_move(move)
      end
      puts used_moves.inspect
      #puts "#{depth}, #{current_cost+1}, #{max_cost+1} #{used_moves.length}, #{queue.size}"
    end

    self.arrangement = original_state
    return false
  end
end

def main(fname)
  rf = RadioisotopeTestingFacility.from_string(IO.read(fname))
  path = rf.search
  return [rf, path]
end

if __FILE__ == $0
  rf,path = main(ARGV.first)
  puts path.length - 1
end
