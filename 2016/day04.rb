def segments(room)
  room =~ /([a-z-]+)(\d+)\[([a-z]+)\]/
  [$1,$2.to_i,$3]
end

def checksum(name)
  name.gsub('-','').split('').group_by {|l| l}.values.
    sort {|a,b| f = (b.length <=> a.length); (f == 0) ? (a[0] <=> b[0]) : f}[0..4].
    map(&:first).join('')
end

def real(room)
  name, sector_id, checksum_value = segments(room)
  checksum_value == checksum(name)
end

def sector_id(room)
  name, sector_id, checksum_value = segments(room)
  sector_id
end

def decrypt(room)
  name, sector_id, checksum_value = segments(room)
  name.gsub!('-', ' ')
  name.each_char.map {|c| (c == ' ') ? c : ((c.ord - 'a'.ord + sector_id) % 26 + 'a'.ord).chr}.join('').strip
end

rooms = IO.readlines('day4.dat').map(&:strip)
real_rooms = rooms.select {|room| real(room)}
real_rooms.map {|room| sector_id(room)}.reduce(&:+)

north_pole_objects = real_rooms.map {|room| [decrypt(room), sector_id(room)]}.select {|pair| pair[0] =~ /north/}
