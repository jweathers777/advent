class Grid
  def initialize(buttons)
    @size = buttons.length
    @grid = Array.new(@size) { Array.new(@size) }
    @size.times do |row|
      @size.times do |col|
        @grid[row][col] = buttons[row][col]
      end
    end
  end

  def button_for(position)
    @grid[position[0]][position[1]]
  end

  def position_for(button)
    @size.times do |row|
      @size.times do |col|
        if @grid[row][col] == button
          return [row,col]
        end
      end
    end
    return nil
  end

  def apply_move(position, move)
    row = position[0]
    col = position[1]
    case move
    when 'U'
      row -= 1
    when 'D'
      row += 1
    when 'L'
      col -= 1
    when 'R'
      col += 1
    end

    if 0 <= row && row < @size && 0 <= col && col < @size && @grid[row][col]
      [row, col]
    else
      position
    end
  end

  def compute_code(lines)
    button = '5'
    position = position_for(button)
    code = []
    lines.split("\n").each do |line|
      line.split('').each do |move|
        position = apply_move(position, move)
      end
      code << button_for(position)
    end
    code.map(&:to_s).join('')
  end
end

def three_by_three_pad
  [%w{1 2 3}, %w{4 5 6}, %w{7 8 9}]
end

def diamond_five_by_five
  [[nil]*2 + %w{1} + [nil]*2,
   [nil] + %w{2 3 4} + [nil],
   %w{5 6 7 8 9},
   [nil] + %w{A B C} + [nil],
   [nil]*2 + %w{D} + [nil]*2]
end
