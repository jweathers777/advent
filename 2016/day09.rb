def decode(s)
  h,t = '',s
  while t
    m = t.match(/^([^(]*)\((\d+)x(\d+)\)(.*)$/)
    if m
      h << m[1]
      repeat = m[3].to_i
      repeated_size = m[2].to_i
      repeated = m[4][0...repeated_size]
      h << (repeated * repeat)
      t = m[4][repeated_size..-1]
    else
      h << t
      t = nil
    end
  end
  h
end

def find_marker(s)
  length = 0
  index = 0
  state = :consume
  while ch = s[index]
    case state
    when :consume
      if ch == '('
        state = :parse_repeated_size
        repeated_size = ''
        parsed_count = 1
        index += 1
      else
        length += 1
        index += 1
      end
    when :parse_repeated_size
      if ch >= '0' and ch <= '9'
        repeated_size << ch
        parsed_count += 1
        index += 1
      elsif ch == 'x'
        state = :parse_repeat_count
        repeated_size = repeated_size.to_i
        repeat_count = ''
        parsed_count += 1
        index += 1
      else
        length += parsed_count + 1
        index += 1
      end
    when :parse_repeat_count
      if ch >= '0' and ch <= '9'
        repeat_count << ch
        parsed_count += 1
        index += 1
      elsif ch == ')'
        state = :consume
        repeat_count = repeat_count.to_i
        index += 1
        repeated = s[index..(index+repeated_size-1)]
        return [length, repeated, repeat_count, index + repeated_size]
      else
        length += parsed_count + 1
        index += 1
      end
    end
  end
  return [length, '', 0, -1]
end

def decode_v2_length_helper(n, s)
  m, repeated, repeat_count, offset = find_marker(s)
  if offset == -1
    n+m
  else
    k = decode_v2_length_helper(0, repeated) * repeat_count
    decode_v2_length_helper(n+m+k, s[offset..-1])
  end
end

def decode_v2_length(s)
  decode_v2_length_helper(0, s)
end

def decode_file(fname)
  decode(IO.read(fname).gsub(/\s+/,''))
end

def decode_v2_length_file(fname)
  decode_v2_length(IO.read(fname).gsub(/\s+/,''))
end
