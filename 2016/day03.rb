def possible_triangle(t)
  t[0] + t[1] > t[2] and t[0] + t[2] > t[1] and t[1] + t[2] > t[0]
end

candidates = IO.readlines('day3.dat').map {|line| line.strip.split(/\s+/).map(&:to_i)}
possible_triangles = candidates.select {|t| possible_triangles(t)}

alt_candidates = [0,1,2].reduce([]) {|a,i| a + candidates.map {|c| c[i]}}.each_slice(3).to_a
possible_triangles = alt_candidates.select {|t| possible_triangles(t)}
