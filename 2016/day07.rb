require 'set'

def has_tls(address)
  inner = address.scan(/\[[^\[\]]+\]/)
  outer = inner.reduce(address) {|s,e| s.sub(e,'[]')}.split('[]')
  inner = inner.map {|a| a.gsub(/\[|\]/,'')}
  matches = [inner, outer].map do |layer|
    layer.map {|a| a.scan(/(.)(.)\2\1/)}.map {|a| a.map {|b| b[0] != b[1]}}.flatten.reduce(false) {|s,e| s || e}
  end
  !matches[0] && matches[1]
end

def has_ssl(address)
  inner = address.scan(/\[[^\[\]]+\]/)
  outer = inner.reduce(address) {|s,e| s.sub(e,'[]')}.split('[]')
  inner = inner.map {|a| a.gsub(/\[|\]/,'')}

  abas, babs = [inner, outer].map do |layer|
    layer.map {|w| w.each_char.each_cons(3).select {|a| a[0] == a[2] && a[0] != a[1]}.map {|a| [a[0],a[1]].join}}.flatten
  end

  Set.new(abas.map(&:reverse)).intersect?(Set.new(babs))
end

addresses = IO.readlines('day7.dat').map(&:strip)
tls_ips = addresses.select {|a| has_tls(a)}.length
ssl_ups = addresses.select {|a| has_ssl(a)}.length

